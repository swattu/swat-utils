/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.parquet.column.ColumnDescriptor;
import org.apache.parquet.hadoop.api.WriteSupport;
import org.apache.parquet.io.ParquetEncodingException;
import org.apache.parquet.io.api.Binary;
import org.apache.parquet.io.api.RecordConsumer;
import org.apache.parquet.schema.MessageType;

import java.util.HashMap;
import java.util.List;

/**
 * The type Parquet write support.
 */
public class ParquetWriteSupport extends WriteSupport<String[]> {
  /**
   * The Schema.
   */
  private final MessageType schema;
  /**
   * The Cols.
   */
  private final List<ColumnDescriptor> cols;
  /**
   * The Record consumer.
   */
  private RecordConsumer recordConsumer;

  /**
   * Instantiates a new Parquet write support.
   *
   * @param schema the schema
   */
  public ParquetWriteSupport(MessageType schema) {
    this.schema = schema;
    this.cols = schema.getColumns();
  }

  @Override
  public WriteContext init(Configuration config) {
    return new WriteContext(schema, new HashMap<>());
  }

  @Override
  public void prepareForWrite(RecordConsumer recordConsumer) {
    this.recordConsumer = recordConsumer;
  }

  @Override
  public void write(String... values) {
    if (values == null) {
      return;
    }
    if (values.length != cols.size()) {
      throw new ParquetEncodingException(
          "Invalid input data. Expecting " + cols.size() + " columns. Input had " + values.length + " columns (" +
              cols + ") : " + StringUtils.join(values, ", "));
    }

    recordConsumer.startMessage();
    for (int i = 0; i < cols.size(); ++i) {
      String val = values[i];
      if (val == null) {
        val = "";
      }
      // val.length() == 0 indicates a NULL value.
      if (val.length() > 0) {
        recordConsumer.startField(cols.get(i).getPath()[0], i);
        switch (cols.get(i).getPrimitiveType().getPrimitiveTypeName()) {
          case BOOLEAN:
            recordConsumer.addBoolean(Boolean.parseBoolean(val));
            break;
          case FLOAT:
            recordConsumer.addFloat(Float.parseFloat(val));
            break;
          case DOUBLE:
            recordConsumer.addDouble(Double.parseDouble(val));
            break;
          case INT32:
            recordConsumer.addInteger(Integer.parseInt(val));
            break;
          case INT64:
            recordConsumer.addLong(Long.parseLong(val));
            break;
          case BINARY:
            recordConsumer.addBinary(stringToBinary(val));
            break;
          default:
            throw new ParquetEncodingException(
                "Unsupported column type: " + cols.get(i).getPrimitiveType().getPrimitiveTypeName());
        }
        recordConsumer.endField(cols.get(i).getPath()[0], i);
      }
    }
    recordConsumer.endMessage();
  }

  private Binary stringToBinary(Object value) {
    return Binary.fromString(value.toString());
  }
}
