/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.hadoop.ParquetWriter;
import org.apache.parquet.hadoop.api.WriteSupport;
import org.apache.parquet.hadoop.metadata.CompressionCodecName;
import org.apache.parquet.schema.MessageType;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

import static org.swat.core.utils.ObjectUtil.cast;
import static org.swat.parquet.ParquetUtil.crcFile;
import static org.swat.parquet.SchemaCreator.asString;

/**
 * The type Swat parquet writer.
 */
public class GenericParquetWriter implements Closeable {
  /**
   * The constant SEQUENCE.
   */
  public static final String SEQUENCE = "SEQUENCE";

  private final MessageType messageType;
  private final String filePath;
  private final NumberFormat sequenceFormatter;
  private final CompressionCodecName codecName;
  private final int maxRecords;
  private final int maxAgeSec;
  private final String tmpPath;
  private final boolean retainCrc;
  private final Set<File> tmpFiles = new LinkedHashSet<>();
  private int sequence;
  private int count;
  private long createdAt;
  private ParquetWriter<String[]> writer;
  private File file;

  /**
   * Instantiates a new Swat parquet writer.
   *
   * @param builder the builder
   */
  GenericParquetWriter(Builder<?> builder) {
    this.messageType = builder.messageType;
    this.filePath = builder.filePath;
    this.sequenceFormatter = builder.sequenceFormatter;
    this.codecName = builder.codecName;
    this.maxRecords = builder.maxRecords;
    this.maxAgeSec = builder.maxAgeSec;
    this.tmpPath = builder.tmpPath;
    this.retainCrc = builder.retainCrc;
  }

  /**
   * Write data.
   *
   * @param values the values
   */
  public void writeData(Object... values) {
    String[] strs = new String[values.length];
    for (int x = 0; x < values.length; x++) {
      strs[x] = asString(values[x]);
    }
    writeData(strs);
  }

  /**
   * Write data.
   *
   * @param values the values
   */
  public void writeData(String... values) {
    try {
      getWriter().write(values);
      count++;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private ParquetWriter<String[]> getWriter() {
    closeIfNeeded();
    if (writer == null) {
      File file;
      if (StringUtils.isNotBlank(tmpPath)) {
        file = new File(tmpPath, UUID.randomUUID() + ".parquet");
        tmpFiles.add(file);
      } else {
        file = getNextFile();
      }
      ParquetBuilder builder = new ParquetBuilder(new Path("file:///" + file.getAbsolutePath()), messageType);
      this.file = file;
      builder.withCompressionCodec(codecName);
      writer = builder.build();
      createdAt = System.currentTimeMillis();
    }
    return writer;
  }

  private void closeIfNeeded() {
    if (maxRecords > 0 && count >= maxRecords) {
      closeInternal();
    } else if (maxAgeSec > 0 && System.currentTimeMillis() - createdAt >= maxAgeSec * 1000L) {
      closeInternal();
    }
  }

  private File getNextFile() {
    File file;
    if (maxRecords > 0 || maxAgeSec > 0) {
      do {
        String fileName = filePath.replace(SEQUENCE, sequenceFormatter.format(sequence));
        file = new File(fileName);
        sequence++;
      } while (file.exists());
    } else {
      file = new File(filePath);
      if (file.exists()) {
        throw new RuntimeException("File " + filePath + " already exists");
      }
    }
    return file;
  }

  private void closeInternal() {
    IOUtils.closeQuietly(writer);
    if (!retainCrc && file != null) {
      new File(file.getParentFile(), "." + file.getName() + ".crc").delete();
    }
    writer = null;
    count = 0;
    createdAt = 0;
  }

  /**
   * Close.
   */
  public void close() {
    closeInternal();
    for (File file : tmpFiles) {
      File finalFile = getNextFile();
      file.renameTo(finalFile);
      crcFile(file).renameTo(crcFile(finalFile));
    }
  }

  /**
   * Builder builder.
   *
   * @param filePath    the file path
   * @param messageType the message type
   * @return the builder
   */
  public static Builder<?> builder(String filePath, MessageType messageType) {
    return new Builder<>(filePath, messageType);
  }

  /**
   * The type Builder.
   */
  public static class Builder<T extends Builder<?>> {
    private final String filePath;
    private final MessageType messageType;
    private NumberFormat sequenceFormatter;
    private CompressionCodecName codecName;
    private int maxRecords;
    private int maxAgeSec;
    private String tmpPath;
    private boolean retainCrc;

    /**
     * Instantiates a new Builder.
     *
     * @param filePath    the file path
     * @param messageType the message type
     */
    Builder(String filePath, MessageType messageType) {
      this.filePath = filePath;
      this.messageType = messageType;
    }

    /**
     * Sequence formatter builder.
     *
     * @param value the value
     * @return the builder
     */
    public T sequenceFormatter(NumberFormat value) {
      this.sequenceFormatter = value;
      return cast(this);
    }

    /**
     * Codec name builder.
     *
     * @param value the value
     * @return the builder
     */
    public T codecName(CompressionCodecName value) {
      this.codecName = value;
      return cast(this);
    }

    /**
     * Max records builder.
     *
     * @param value the value
     * @return the builder
     */
    public T maxRecords(int value) {
      this.maxRecords = value;
      return cast(this);
    }

    /**
     * Max age sec builder.
     *
     * @param value the value
     * @return the builder
     */
    public T maxAgeSec(int value) {
      this.maxAgeSec = value;
      return cast(this);
    }

    /**
     * Tmp path builder.
     *
     * @param value the value
     * @return the builder
     */
    public T tmpPath(String value) {
      this.tmpPath = value;
      return cast(this);
    }

    /**
     * Remove crc builder.
     *
     * @param value the value
     * @return the builder
     */
    public T retainCrc(boolean value) {
      this.retainCrc = value;
      return cast(this);
    }

    /**
     * Build swat parquet writer.
     *
     * @return the swat parquet writer
     */
    public GenericParquetWriter build() {
      validate();
      return new GenericParquetWriter(this);
    }

    /**
     * Validate.
     */
    protected void validate() {
      if (!filePath.contains(SEQUENCE)) {
        maxRecords = 0;
        maxAgeSec = 0;
      }

      if (sequenceFormatter == null) {
        sequenceFormatter = NumberFormat.getNumberInstance();
        sequenceFormatter.setMinimumIntegerDigits(0);
      }

      if (codecName == null) {
        codecName = CompressionCodecName.GZIP;
      }
    }
  }

  /**
   * The type Builder.
   */
  private static class ParquetBuilder extends ParquetWriter.Builder<String[], ParquetBuilder> {
    private final MessageType schema;

    /**
     * Instantiates a new Builder.
     *
     * @param path   the path
     * @param schema the schema
     */
    protected ParquetBuilder(Path path, MessageType schema) {
      super(path);
      this.schema = schema;
    }

    @Override
    protected ParquetBuilder self() {
      return this;
    }

    @Override
    protected WriteSupport<String[]> getWriteSupport(Configuration conf) {
      return new ParquetWriteSupport(schema);
    }

    @Override
    public ParquetWriter<String[]> build() {
      try {
        return super.build();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }
}
