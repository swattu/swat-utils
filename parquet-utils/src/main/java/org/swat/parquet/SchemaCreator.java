/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import lombok.extern.slf4j.Slf4j;
import org.apache.parquet.schema.LogicalTypeAnnotation;
import org.apache.parquet.schema.MessageType;
import org.apache.parquet.schema.PrimitiveType;
import org.apache.parquet.schema.Types;
import org.swat.core.utils.ObjectUtil;
import org.swat.reflect.utils.ReflectUtils;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static org.apache.parquet.schema.LogicalTypeAnnotation.dateType;
import static org.apache.parquet.schema.LogicalTypeAnnotation.stringType;
import static org.apache.parquet.schema.LogicalTypeAnnotation.timeType;
import static org.apache.parquet.schema.LogicalTypeAnnotation.timestampType;

/**
 * The type Schema creator.
 */
@Slf4j
public class SchemaCreator {
  private boolean built = false;
  private MessageType messageType;
  private final Types.MessageTypeBuilder builder = Types.buildMessage();

  /**
   * Add schema creator.
   *
   * @param fieldName the field name
   * @param typeName  the type name
   * @return the schema creator
   */
  public SchemaCreator add(String fieldName, PrimitiveType.PrimitiveTypeName typeName) {
    return add(false, fieldName, typeName);
  }

  /**
   * Add schema creator.
   *
   * @param optional  the optional
   * @param fieldName the field name
   * @param typeName  the type name
   * @return the schema creator
   */
  public SchemaCreator add(boolean optional, String fieldName, PrimitiveType.PrimitiveTypeName typeName) {
    return add(optional, fieldName, typeName, null);
  }

  /**
   * Add schema creator.
   *
   * @param required    the required
   * @param fieldName   the field name
   * @param typeName    the type name
   * @param typeAnnotation the logical type
   * @return the schema creator
   */
  public SchemaCreator add(boolean required, String fieldName, PrimitiveType.PrimitiveTypeName typeName,
      LogicalTypeAnnotation typeAnnotation) {
    if (!built) {
      Types.PrimitiveBuilder<PrimitiveType> type = required ? Types.required(typeName) : Types.optional(typeName);
      if (typeAnnotation != null) {
        type.as(typeAnnotation);
      }
      builder.addField(type.named(fieldName));
    }
    return this;
  }

  /**
   * Build message type.
   *
   * @return the message type
   */
  public MessageType build() {
    if (!built) {
      messageType = builder.named("m");
    }
    built = true;
    return messageType;
  }

  /**
   * Add.
   *
   * @param fieldName the field name
   * @param type      the type
   */
  public void add(String fieldName, Class<?> type) {
    if (ObjectUtil.equalsAny(type, byte.class, short.class, int.class, Byte.class, Short.class, Integer.class, AtomicInteger.class)) {
      add(type.isPrimitive(), fieldName, PrimitiveType.PrimitiveTypeName.INT32);
    } else if (ObjectUtil.equalsAny(type, long.class, Long.class, AtomicLong.class)) {
      add(type.isPrimitive(), fieldName, PrimitiveType.PrimitiveTypeName.INT64);
    } else if (ObjectUtil.equalsAny(type, float.class, Float.class)) {
      add(type.isPrimitive(), fieldName, PrimitiveType.PrimitiveTypeName.FLOAT);
    } else if (ObjectUtil.equalsAny(type, double.class, Double.class, BigDecimal.class)) {
      add(type.isPrimitive(), fieldName, PrimitiveType.PrimitiveTypeName.DOUBLE);
    } else if (ObjectUtil.equalsAny(type, boolean.class, Boolean.class, AtomicBoolean.class)) {
      add(type.isPrimitive(), fieldName, PrimitiveType.PrimitiveTypeName.BOOLEAN);
    } else if (ObjectUtil.equalsAny(type, char.class, Character.class)) {
      add(type.isPrimitive(), fieldName, PrimitiveType.PrimitiveTypeName.BINARY, stringType());
    } else if (ObjectUtil.equalsAny(type, Date.class, LocalDate.class)) {
      add(type.isPrimitive(), fieldName, PrimitiveType.PrimitiveTypeName.INT32, dateType());
    } else if (ObjectUtil.equalsAny(type, Timestamp.class, LocalDateTime.class, ZonedDateTime.class, java.util.Date.class, Calendar.class)) {
      add(type.isPrimitive(), fieldName, PrimitiveType.PrimitiveTypeName.INT64, timestampType(true, LogicalTypeAnnotation.TimeUnit.MILLIS));
    } else if (ObjectUtil.equalsAny(type, Time.class, LocalTime.class)) {
      add(type.isPrimitive(), fieldName, PrimitiveType.PrimitiveTypeName.INT32, timeType(true, LogicalTypeAnnotation.TimeUnit.MILLIS));
    } else if (ReflectUtils.isAssignable(CharSequence.class, type)) {
      add(type.isPrimitive(), fieldName, PrimitiveType.PrimitiveTypeName.BINARY, stringType());
    } else {
      log.warn("Undefined type:" + type + " for field:" + fieldName);
      add(type.isPrimitive(), fieldName, PrimitiveType.PrimitiveTypeName.BINARY, stringType());
    }
  }

  /**
   * As string string.
   *
   * @param value the value
   * @return the string
   */
  public static String asString(Object value) {
    if (value != null) {
      if (value instanceof LocalDateTime) {
        value = ((LocalDateTime) value).atZone(ZoneOffset.systemDefault());
      } else if (value instanceof Date) {
        value = ((Date) value).toLocalDate();
      } else if (value instanceof Calendar) {
        value = ((Calendar) value).getTime();
      }
      if (value instanceof Timestamp) {
        value = ((Timestamp) value).getTime();
      } else if (value instanceof java.util.Date) {
        value = ((java.util.Date) value).getTime();
      } else if (value instanceof LocalDate) {
        value = ((LocalDate) value).toEpochDay();
      } else if (value instanceof ZonedDateTime) {
        value = ((ZonedDateTime) value).toEpochSecond() * 1000;
      } else if (value instanceof LocalTime) {
        value = ((LocalTime) value).toSecondOfDay() * 1000L;
      }
      return value.toString();
    }
    return null;
  }
}
