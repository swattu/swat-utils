/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import org.apache.parquet.schema.MessageType;
import org.swat.sql.ColumnInfo;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.swat.core.utils.ObjectUtil.cast;
import static org.swat.sql.SqlUtil.count;
import static org.swat.sql.SqlUtil.metaData;

/**
 * The type Sql parquet writer.
 */
public class SqlParquetWriter extends GenericParquetWriter {
  private final DataSource dataSource;
  private final String sql;
  private final List<ColumnInfo> columnInfos;
  private final int limit;
  private final int threads;

  /**
   * Instantiates a new Sql parquet writer.
   *
   * @param builder the builder
   */
  SqlParquetWriter(Builder builder) {
    super(builder);
    this.dataSource = builder.dataSource;
    this.columnInfos = builder.columnInfos;
    this.sql = builder.sql;
    this.limit = builder.limit;
    this.threads = builder.threads;
  }

  /**
   * Write data.
   */
  public void writeData() {
    try {
      ExecutorService executor = Executors.newFixedThreadPool(threads);
      int count = count(dataSource, sql);
      int offset = 0;
      List<Future<List<Object[]>>> futures = new ArrayList<>();
      do {
        String limitSql = "SELECT * FROM (" + sql + ") a";
        if (limit > 0) {
          limitSql = limitSql + " LIMIT " + limit;
        }
        if (offset > 0) {
          limitSql = limitSql + " OFFSET " + offset;
        }
        offset += limit;
        String paginatedSql = limitSql;
        Callable<List<Object[]>> callable = () -> execute(paginatedSql);
        Future<List<Object[]>> future = executor.submit(callable);
        futures.add(future);
      } while (offset > 0 && offset < count);
      for (Future<List<Object[]>> future : futures) {
        List<Object[]> data = future.get();
        for (Object[] datum : data) {
          writeData(datum);
        }
      }
      close();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private List<Object[]> execute(String sql) {
    try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
      List<Object[]> data = new ArrayList<>();
      while (resultSet.next()) {
        Object[] datum = new Object[columnInfos.size()];
        for (int x = 0; x < columnInfos.size(); x++) {
          ColumnInfo columnInfo = columnInfos.get(x);
          datum[x] = resultSet.getObject(columnInfo.getIndex());
        }
        data.add(datum);
      }
      return data;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Builder builder.
   *
   * @param dataSource the data source
   * @param filePath   the file path
   * @param sql        the sql
   * @return the builder
   */
  public static Builder builder(DataSource dataSource, String filePath, String sql) {
    try {
      List<ColumnInfo> infos = metaData(dataSource, sql);
      SchemaCreator creator = new SchemaCreator();
      for (int x = 0; x < infos.size(); x++) {
        ColumnInfo info = infos.get(x);
        creator.add(info.getColumnLabel(), Class.forName(info.getColumnClassName()));
      }
      return new Builder(filePath, dataSource, sql, infos, creator.build());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * The type Builder.
   */
  public static class Builder extends GenericParquetWriter.Builder<Builder> {
    private final List<ColumnInfo> columnInfos;
    private final String sql;
    private final DataSource dataSource;
    private int limit;
    private int threads;

    /**
     * Instantiates a new Builder.
     *
     * @param filePath    the file path
     * @param dataSource  the data source
     * @param sql         the sql
     * @param columnInfos the meta data
     * @param messageType the message type
     */
    protected Builder(String filePath, DataSource dataSource, String sql, List<ColumnInfo> columnInfos,
        MessageType messageType) {
      super(filePath, messageType);
      this.dataSource = dataSource;
      this.sql = sql;
      this.columnInfos = columnInfos;
    }

    /**
     * Limit builder.
     *
     * @param value the value
     * @return the builder
     */
    public Builder limit(int value) {
      this.limit = value;
      return this;
    }

    /**
     * Threads builder.
     *
     * @param value the value
     * @return the builder
     */
    public Builder threads(int value) {
      this.threads = value;
      return this;
    }

    @Override
    public SqlParquetWriter build() {
      validate();
      if (threads <= 0) {
        threads = 1;
      }
      return new SqlParquetWriter(this);
    }
  }
}
