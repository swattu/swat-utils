/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

/**
 * The interface Field value extractor.
 *
 * @param <T> the type parameter
 */
public abstract class ValueExtractor<T> {
  /**
   * The Clazz.
   */
  protected final Class<T> clazz;

  /**
   * Instantiates a new Value extractor.
   *
   * @param clazz the clazz
   */
  protected ValueExtractor(Class<T> clazz) {
    this.clazz = clazz;
  }

  /**
   * Fields string [ ].
   *
   * @return the string [ ]
   */
  abstract String[] fields();

  /**
   * Add header.
   *
   * @param creator the creator
   * @param field   the field
   */
  abstract void addHeader(SchemaCreator creator, String field);

  /**
   * Value object.
   *
   * @param element   the element
   * @param fieldName the field name
   * @return the object
   */
  abstract Object value(T element, String fieldName);
}
