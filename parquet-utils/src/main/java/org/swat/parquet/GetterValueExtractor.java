/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The type Getter value extractor.
 *
 * @param <T> the type parameter
 */
public class GetterValueExtractor<T> extends ValueExtractor<T> {
  private final Map<String, Method> methodMap = new LinkedHashMap<>();

  /**
   * Instantiates a new Value extractor.
   *
   * @param clazz the clazz
   */
  protected GetterValueExtractor(Class<T> clazz) {
    super(clazz);
  }

  @Override
  public String[] fields() {
    Method[] methods = clazz.getMethods();
    for (Method method : methods) {
      if (Modifier.isStatic(method.getModifiers())) {
        continue;
      }
      if (!Modifier.isPublic(method.getModifiers())) {
        continue;
      }
      final Class<?> type = method.getReturnType();
      if (type == Void.class) {
        continue;
      }
      if (method.getParameterTypes().length != 0) {
        continue;
      }
      String name = method.getName();
      if (StringUtils.equalsAny(name, "getClass")) {
        continue;
      }
      if (!name.startsWith("get") && !name.startsWith("is")) {
        continue;
      }
      if (name.contains("$")) {
        continue;
      }
      String fieldName = fieldName(method);
      if (fieldName != null) {
        methodMap.put(fieldName, method);
      }
    }
    return new ArrayList<>(methodMap.keySet()).toArray(new String[0]);
  }

  @Override
  void addHeader(SchemaCreator creator, String field) {
    Class<?> type = methodMap.get(field).getReturnType();
    creator.add(field, type);
  }

  @Override
  public Object value(T element, String fieldName) {
    try {
      return methodMap.get(fieldName).invoke(element);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Field name string.
   *
   * @param method the method
   * @return the string
   */
  protected String fieldName(Method method) {
    String fieldName = method.getName();
    if (fieldName.startsWith("get")) {
      fieldName = fieldName.substring(3);
    } else if (fieldName.startsWith("is")) {
      fieldName = fieldName.substring(2);
    }
    return fieldName;
  }
}
