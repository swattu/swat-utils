/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import org.apache.parquet.schema.MessageType;

import java.util.Arrays;
import java.util.List;

import static org.swat.parquet.SchemaCreator.asString;

/**
 * The type Object parquet writer.
 *
 * @param <T> the type parameter
 */
public class ObjectParquetWriter<T> extends GenericParquetWriter {
  private final ValueExtractor<T> extractor;
  private final List<String> fields;

  /**
   * Instantiates a new Swat parquet writer.
   *
   * @param builder the builder
   */
  ObjectParquetWriter(Builder<T> builder) {
    super(builder);
    this.extractor = builder.extractor;
    fields = Arrays.asList(extractor.fields());
  }

  /**
   * Write data.
   *
   * @param element the element
   */
  public void writeData(T element) {
    String[] values = new String[fields.size()];
    for (int x = 0; x < fields.size(); x++) {
      String field = fields.get(x);
      Object value = extractor.value(element, field);
      values[x] = asString(value);
    }
    super.writeData(values);
  }

  /**
   * Builder builder.
   *
   * @param <S>       the type parameter
   * @param clazz     the clazz
   * @param filePath  the file path
   * @param extractor the extractor
   * @return the builder
   */
  public static <S> Builder<S> builder(Class<S> clazz, String filePath, ValueExtractor<S> extractor) {
    if (extractor == null) {
      extractor = new GetterValueExtractor<S>(clazz);
    }
    SchemaCreator creator = new SchemaCreator();
    for (String field : extractor.fields()) {
      extractor.addHeader(creator, field);
    }
    MessageType messageType = creator.build();
    return new Builder<S>(filePath, messageType, extractor);
  }

  /**
   * The type Builder.
   *
   * @param <T> the type parameter
   */
  public static class Builder<T> extends GenericParquetWriter.Builder<Builder<T>> {
    private final ValueExtractor<T> extractor;

    /**
     * Instantiates a new Builder.
     *
     * @param filePath    the file path
     * @param messageType the message type
     * @param clazz       the clazz
     * @param extractor   the extractor
     */
    protected Builder(String filePath, MessageType messageType, ValueExtractor<T> extractor) {
      super(filePath, messageType);
      this.extractor = extractor;
    }

    @Override
    public ObjectParquetWriter<T> build() {
      validate();
      return new ObjectParquetWriter<>(this);
    }
  }
}
