/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The type Getter value extractor.
 *
 * @param <T> the type parameter
 */
public class FieldValueExtractor<T extends Class<T>> extends ValueExtractor<T> {
  private final Map<String, Field> fieldMap = new LinkedHashMap<>();

  /**
   * Instantiates a new Value extractor.
   *
   * @param clazz the clazz
   */
  protected FieldValueExtractor(Class<T> clazz) {
    super(clazz);
  }

  @Override
  public String[] fields() {
    Field[] fields = clazz.getFields();
    for (Field field : fields) {
      if (Modifier.isStatic(field.getModifiers())) {
        continue;
      }
      if (Modifier.isFinal(field.getModifiers())) {
        continue;
      }
      String fieldName = fieldName(field);
      if (fieldName != null) {
        fieldMap.put(fieldName, field);
      }
    }
    return new ArrayList<>(fieldMap.keySet()).toArray(new String[0]);
  }

  @Override
  public void addHeader(SchemaCreator creator, String field) {
    Class<?> type = fieldMap.get(field).getType();
    creator.add(field, type);

  }

  @Override
  public Object value(T element, String fieldName) {
    try {
      return fieldMap.get(fieldName).get(element);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Field name string.
   *
   * @param field the method
   * @return the string
   */
  protected String fieldName(Field field) {
    return field.getName();
  }
}
