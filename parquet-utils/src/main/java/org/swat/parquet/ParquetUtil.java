/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.ParquetReadOptions;
import org.apache.parquet.column.ColumnDescriptor;
import org.apache.parquet.column.page.PageReadStore;
import org.apache.parquet.example.data.Group;
import org.apache.parquet.example.data.simple.convert.GroupRecordConverter;
import org.apache.parquet.hadoop.ParquetFileReader;
import org.apache.parquet.hadoop.util.HadoopInputFile;
import org.apache.parquet.io.ColumnIOFactory;
import org.apache.parquet.io.InputFile;
import org.apache.parquet.io.MessageColumnIO;
import org.apache.parquet.io.RecordReader;
import org.apache.parquet.schema.LogicalTypeAnnotation;
import org.apache.parquet.schema.LogicalTypeAnnotation.DateLogicalTypeAnnotation;
import org.apache.parquet.schema.LogicalTypeAnnotation.DecimalLogicalTypeAnnotation;
import org.apache.parquet.schema.LogicalTypeAnnotation.IntLogicalTypeAnnotation;
import org.apache.parquet.schema.LogicalTypeAnnotation.StringLogicalTypeAnnotation;
import org.apache.parquet.schema.LogicalTypeAnnotation.TimeLogicalTypeAnnotation;
import org.apache.parquet.schema.LogicalTypeAnnotation.TimestampLogicalTypeAnnotation;
import org.apache.parquet.schema.MessageType;
import org.apache.parquet.schema.PrimitiveType;
import org.apache.parquet.schema.Type;
import org.swat.core.utils.CoreRtException;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * The type Parquet util.
 */
public class ParquetUtil {
  /**
   * Crc file.
   *
   * @param file the file
   * @return the file
   */
  public static File crcFile(File file) {
    file = new File(file.getParentFile(), "." + file.getName() + ".crc");
    return file;
  }

  /**
   * Schema message type.
   *
   * @param metadataFile the metadata file
   * @return the message type
   */
  public static MessageType schema(File metadataFile) {
    return schema(getPath(metadataFile.getAbsolutePath()));
  }

  /**
   * Schema message type.
   *
   * @param path the metadata file
   * @return the message type
   */
  public static MessageType schema(Path path) {
    if (path == null) {
      return null;
    }
    try (ParquetFileReader reader = reader(path)) {
      MessageType schema = reader.getFileMetaData().getSchema();
      return schema;
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }

  /**
   * Input file input file.
   *
   * @param path the path
   * @return the input file
   */
  public static InputFile inputFile(Path path) {
    return inputFile(path, new Configuration());
  }

  /**
   * Input file input file.
   *
   * @param path the path
   * @param conf the conf
   * @return the input file
   */
  public static InputFile inputFile(Path path, Configuration conf) {
    try {
      return HadoopInputFile.fromPath(path, conf);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }

  /**
   * Fields list.
   *
   * @param file the parquet file
   * @return the list
   */
  public static List<String> fields(String file) {
    return fields(schema(getPath(file)));
  }

  /**
   * Fields list.
   *
   * @param messageType the message type
   * @return the list
   */
  public static List<String> fields(MessageType messageType) {
    List<String> fields = new ArrayList<>();
    if (messageType == null) {
      return fields;
    }
    for (ColumnDescriptor descriptor : messageType.getColumns()) {
      fields.add(descriptor.getPrimitiveType().getName());
    }
    return fields;
  }

  /**
   * Count int.
   *
   * @param file the file
   * @return the int
   */
  public static int count(String file) {
    int count = 0;
    try (ParquetFileReader reader = reader(file)) {
      PageReadStore pages;
      while (null != (pages = reader.readNextRowGroup())) {
        count += pages.getRowCount();
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return count;
  }

  /**
   * Read file list.
   *
   * @param file  the file
   * @param count the count
   * @return the list
   */
  public static List<Map<String, Object>> readFile(String file, int count) {
    count = Math.max(count, 1);
    count = Math.min(count, 100);

    List<Map<String, Object>> data = new ArrayList<>();
    PageReadStore pages;
    try (ParquetFileReader reader = reader(file)) {
      MessageType schema = reader.getFileMetaData().getSchema();
      while (null != (pages = reader.readNextRowGroup())) {
        final long rows = pages.getRowCount();

        final MessageColumnIO columnIO = new ColumnIOFactory().getColumnIO(schema);
        final RecordReader<Group> recordReader = columnIO.getRecordReader(pages, new GroupRecordConverter(schema));
        int remaining = count - data.size();
        for (int i = 0; i < rows && i < remaining; i++) {
          final Group g = recordReader.read();
          data.add(getDataForGroup(g));
        }
      }
    } catch (IOException e) {
      throw new CoreRtException(e);
    }

    return data;
  }

  private static Map<String, Object> getDataForGroup(Group g) {
    int fieldCount = g.getType().getFieldCount();
    Map<String, Object> values = new LinkedHashMap<>();
    for (int field = 0; field < fieldCount; field++) {
      int valueCount = g.getFieldRepetitionCount(field);

      Type fieldType = g.getType().getType(field);
      String fieldName = fieldType.getName();
      for (int index = 0; index < valueCount; index++) {
        if (fieldType.isPrimitive()) {
          String valueStr = g.getValueToString(field, index);
          Object value = valueStr;
          PrimitiveType.PrimitiveTypeName primitiveTypename = fieldType.asPrimitiveType().getPrimitiveTypeName();
          LogicalTypeAnnotation typeAnnotation = fieldType.getLogicalTypeAnnotation();
          switch (primitiveTypename) {
            case INT64:
              value = Long.valueOf(valueStr);
              value = getValue(valueStr, value, typeAnnotation);
              break;
            case INT32:
              value = Integer.valueOf(valueStr);
              value = getValue(valueStr, value, typeAnnotation);
              break;
            case BOOLEAN:
              value = Boolean.valueOf(valueStr);
              break;
            case BINARY:
              value = getValue(valueStr, value, typeAnnotation);
              break;
            case FLOAT:
              value = Float.valueOf(valueStr);
              break;
            case DOUBLE:
              value = Double.valueOf(valueStr);
              break;
            case INT96:
              value = new BigInteger(valueStr);
              break;
            default:
              throw new CoreRtException(primitiveTypename + " is not supported");
          }
          values.put(fieldName, value);
          break;
        }
      }
    }
    return values;
  }

  private static Object getValue(String valueStr, Object value, LogicalTypeAnnotation typeAnnotation) {
    if (typeAnnotation == null) {
      return value;
    }
    if (typeAnnotation instanceof StringLogicalTypeAnnotation) {
      //Do nothing
    } else if (typeAnnotation instanceof DecimalLogicalTypeAnnotation) {
      value = Double.valueOf(valueStr);
    } else if (typeAnnotation instanceof DateLogicalTypeAnnotation) {
      value = LocalDate.ofEpochDay(Long.parseLong(valueStr));
    } else if (typeAnnotation instanceof TimeLogicalTypeAnnotation) {
      value = LocalTime.ofSecondOfDay(Long.parseLong(valueStr) / 1000);
    } else if (typeAnnotation instanceof TimestampLogicalTypeAnnotation) {
      value = LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(valueStr)), ZoneId.systemDefault());
    } else if (typeAnnotation instanceof IntLogicalTypeAnnotation) {
      if (((IntLogicalTypeAnnotation) typeAnnotation).getBitWidth() <= 32) {
        value = Integer.valueOf(valueStr);
      } else {
        value = Long.valueOf(valueStr);
      }
    } else {
      throw new CoreRtException(typeAnnotation + " is not supported");
    }
    return value;
  }

  /**
   * Gets path.
   *
   * @param fileName the file name
   * @return the path
   */
  public static Path getPath(String fileName) {
    return getPath(new File(fileName));
  }

  /**
   * Gets path.
   *
   * @param file the file
   * @return the path
   */
  public static Path getPath(File file) {
    return new Path("file:///" + file.getAbsolutePath());
  }

  private static ParquetFileReader reader(String fileName) {
    return reader(new File(fileName));
  }

  private static ParquetFileReader reader(File file) {
    return reader(getPath(file));
  }

  /**
   * Reader parquet file reader.
   *
   * @param path the path
   * @return the parquet file reader
   */
  public static ParquetFileReader reader(Path path) {
    ParquetReadOptions options = ParquetReadOptions.builder().build();
    InputFile inputFile = inputFile(path);
    try {
      return ParquetFileReader.open(inputFile, options);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }
}
