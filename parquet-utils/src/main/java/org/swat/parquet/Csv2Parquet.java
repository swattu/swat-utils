package org.swat.parquet;

import com.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.parquet.schema.PrimitiveType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import static org.apache.parquet.schema.LogicalTypeAnnotation.stringType;

/**
 * The type Csv 2 parquet.
 */
@Slf4j
public class Csv2Parquet {
    /**
     * Convert.
     *
     * @param folder the folder
     * @param depth  the depth
     */
    public void convert(File folder, int depth) {
        if (depth < 0) {
            return;
        }
        if (folder.isDirectory()) {
            File[] files = folder.listFiles();
            if (files == null) {
                return;
            }
            for (File file : files) {
                convert(file, depth - 1);
            }
            return;
        }
        String fileName = folder.getName();
        if (!fileName.endsWith(".csv")) {
            return;
        }
        fileName = fileName.replace(".csv", ".parquet");
        File parquetFile = new File(folder.getParentFile(), fileName);
        parquetFile.delete();
        convertInternal(folder, parquetFile);
    }

    private void convertInternal(File csvFile, File parquetFile) {
        long startTime = System.currentTimeMillis();

        SchemaCreator creator = new SchemaCreator();

        GenericParquetWriter writer = null;
        int counter = -1;
        try (BufferedReader reader = new BufferedReader(new FileReader(csvFile))) {
            CSVReader csvReader = new CSVReader(reader);
            String[] data;
            while ((data = csvReader.readNext()) != null) {
                counter++;
                if (counter == 0) {
                    for (String value : data) {
                        creator.add(false, value, PrimitiveType.PrimitiveTypeName.BINARY, stringType());
                    }

                    writer = GenericParquetWriter.builder(parquetFile.getAbsolutePath(), creator.build()).build();
                    continue;

                }
                writer.writeData((Object[]) data);
            }
        } catch (Exception e) {
            log.error("Error reading parquet file.", e);
        } finally {
            IOUtils.closeQuietly(writer);
            log.info("Time taken to process {} records {} millis", counter, System.currentTimeMillis() - startTime);
        }
    }
}
