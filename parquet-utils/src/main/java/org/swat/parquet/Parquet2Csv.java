package org.swat.parquet;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.column.ColumnDescriptor;
import org.apache.parquet.column.page.PageReadStore;
import org.apache.parquet.example.data.Group;
import org.apache.parquet.example.data.simple.convert.GroupRecordConverter;
import org.apache.parquet.hadoop.ParquetFileReader;
import org.apache.parquet.io.ColumnIOFactory;
import org.apache.parquet.io.MessageColumnIO;
import org.apache.parquet.io.RecordReader;
import org.apache.parquet.schema.MessageType;
import org.apache.parquet.schema.Type;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The type Parquet 2 csv.
 */
@Slf4j
public class Parquet2Csv {
    private void closeAll() {
        for (BufferedWriter writer : writerMap.values()) {
            IOUtils.closeQuietly(writer);
        }
    }

    private final Map<List<String>, BufferedWriter> writerMap = new HashMap<>();

    /**
     * Convert.
     *
     * @param folder  the folder
     * @param depth   the depth
     * @param combine the combine
     */
    public void convert(File folder, int depth, boolean combine) {
        convertInternal(folder, depth, combine);
        closeAll();
    }

    private void convertInternal(File folder, int depth, boolean combine) {
        if (depth < 0) {
            return;
        }
        if (folder.isDirectory()) {
            File[] files = folder.listFiles();
            if (files == null) {
                return;
            }
            for (File file : files) {
                convertInternal(file, depth - 1, combine);
            }
            return;
        }
        String fileName = folder.getName();
        if (!fileName.endsWith(".parquet")) {
            return;
        }
        fileName = fileName.replace(".parquet", ".csv");
        File csvFile = new File(folder.getParentFile(), fileName);
        convertInternal(folder, csvFile, combine);
    }

    private void convertInternal(File parquetFile, File csvFile, boolean combine) {
        BufferedWriter writer = null;
        try {
            Path path = ParquetUtil.getPath(parquetFile);
            try (ParquetFileReader r = ParquetUtil.reader(path)) {
                MessageType schema = r.getFileMetaData().getSchema();
                writer = getWriter(csvFile, schema, combine);
                PageReadStore pages;
                long totalRows = 0;
                while (null != (pages = r.readNextRowGroup())) {
                    final long rows = pages.getRowCount();
                    totalRows += rows;
                    log.debug("{} - Group rows: {}, Total rows: {}", parquetFile.getAbsolutePath(), rows, totalRows);
                    final MessageColumnIO columnIO = new ColumnIOFactory().getColumnIO(schema);
                    final RecordReader<Group> recordReader =
                        columnIO.getRecordReader(pages, new GroupRecordConverter(schema));
                    for (int i = 0; i < rows; i++) {
                        final Group g = recordReader.read();
                        writeGroup(g, writer);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error reading parquet file.", e);
        } finally {
            if (!combine) {
                IOUtils.closeQuietly(writer);
            }
        }
    }

    private BufferedWriter getWriter(File csvFile, MessageType schema, boolean combine) {
        BufferedWriter writer;
        List<String> fields = null;
        if (combine) {
            fields = ParquetUtil.fields(schema);
            writer = writerMap.get(fields);
            if (writer != null) {
                return writer;
            }
        }
        try {
            writer = new BufferedWriter(new FileWriter(csvFile));
            writeSchema(schema, writer);
            if (combine) {
                writerMap.put(fields, writer);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return writer;
    }

    private void writeSchema(MessageType messageType, BufferedWriter writer) throws IOException {
        int count = 0;
        for (ColumnDescriptor descriptor : messageType.getColumns()) {
            if (count > 0) {
                writer.write(',');
            }
            writer.write(descriptor.getPrimitiveType().getName());
            count++;
        }
        writer.newLine();
    }

    private static void writeGroup(Group g, BufferedWriter writer) throws Exception {
        int fieldCount = g.getType().getFieldCount();
        int count = 0;
        for (int field = 0; field < fieldCount; field++) {
            int valueCount = g.getFieldRepetitionCount(field);

            Type fieldType = g.getType().getType(field);
            if (valueCount == 0) {
                if (count > 0) {
                    writer.write(',');
                }
            }
            for (int index = 0; index < valueCount; index++) {
                if (fieldType.isPrimitive()) {
                    if (count > 0) {
                        writer.write(',');
                    }
                    writer.write(g.getValueToString(field, index));
                }
            }
            count++;
        }
        writer.newLine();
    }
}
