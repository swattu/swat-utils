/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.swat.sql.SqlBaseTest;

public class SqlParquetWriterTest extends SqlBaseTest {
  private BaseWriterTest util = new BaseWriterTest();

  @Before
  public void beforeTest() {
    util.before();
  }

  @Test
  public void check() {
    executeUpdate("CREATE TABLE IF NOT EXISTS vehicle (id varchar, name varchar, mfg date, created timestamp)");
    executeUpdate("DELETE from vehicle");
    executeUpdate("INSERT INTO vehicle (id, name, mfg, created) VALUES('MERC', 'Mercedes', '2022-01-01', '2022-01-01')");

    SqlParquetWriter.Builder builder = SqlParquetWriter.builder(dataSource, util.fileName, "SELECT * from vehicle");
    SqlParquetWriter writer = builder.build();
    writer.writeData();
    util.assertFile(-1, true);
  }

  @After
  public void after() {
    util.after();
  }
}
