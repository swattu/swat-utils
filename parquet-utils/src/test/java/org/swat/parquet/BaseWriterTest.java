/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import org.apache.commons.lang3.StringUtils;
import org.apache.parquet.schema.MessageType;
import org.junit.After;
import org.junit.Before;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.swat.parquet.GenericParquetWriter.SEQUENCE;
import static org.swat.parquet.ParquetUtil.crcFile;

public class BaseWriterTest {
  public final String fileName;

  public BaseWriterTest() {
    this(null);
  }

  public BaseWriterTest(String fileName) {
    if (StringUtils.isBlank(fileName)) {
      fileName = "target/" + getClass().getSimpleName() + "-SEQUENCE.parquet";
    }
    this.fileName = fileName;
  }

  @Before
  public void before() {
    new File(fileName).delete();
    crcFile(new File(fileName)).delete();
    for (int x = 0; x < 10; x++) {
      File file = new File(fileName.replace(SEQUENCE, String.valueOf(x)));
      file.delete();
      crcFile(file).delete();
    }
  }

  public void assertFile(int index, boolean exists) {
    File file = getFile(index);
    assertEquals(exists, file.exists());
  }

  public File getFile(int index) {
    File file;
    if (index < 0) {
      file = new File(fileName);
    } else {
      String fileStr = fileName.replace(SEQUENCE, String.valueOf(index));
      file = new File(fileStr);
    }
    return file;
  }

  public File getCsvFile(int index) {
    String filePath = getFile(index).getAbsolutePath().replace(".parquet", ".csv");
    return new File(filePath);
  }

  public void assertCrc(int index, boolean exists) {
    File file = crcFile(getFile(index));
    assertEquals(exists, file.exists());
  }

  public void assertCsv(int index, boolean exists) {
    assertEquals(exists, getCsvFile(index).exists());
  }

  @After
  public void after() {
    File file = getFile(0);
    if (!file.exists()) {
      file = getFile(-1);
    }
    MessageType messageType = ParquetUtil.schema(file);
    System.out.println(messageType);
  }
}
