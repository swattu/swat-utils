/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Parquet2CsvTest extends BaseWriterTest {
  @Test
  public void convertor() throws Exception {
    ObjectParquetWriter.Builder<Vehicle> builder = ObjectParquetWriter.builder(Vehicle.class, fileName, null);
    ObjectParquetWriter<Vehicle> writer = builder.build();
    for (int x = 0; x < 2; x++) {
      Vehicle vehicle = new Vehicle();
      vehicle.setAnInt(x);
      writer.writeData(vehicle);
    }
    writer.close();
    Parquet2Csv parquet2Csv = new Parquet2Csv();
    parquet2Csv.convert(getFile(-1), 1, false);
    assertCsv(-1, true);

    List<String> lines = FileUtils.readLines(getCsvFile(-1), StandardCharsets.UTF_8);
    assertEquals(3, lines.size());

    before();

    Csv2Parquet csv2Parquet = new Csv2Parquet();
    csv2Parquet.convert(getCsvFile(-1), 0);
    assertFile(-1, true);
    assertEquals(2, ParquetUtil.count(getFile(-1).getAbsolutePath()));
  }
}
