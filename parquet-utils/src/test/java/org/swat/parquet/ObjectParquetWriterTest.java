/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ObjectParquetWriterTest extends BaseWriterTest {
  ObjectParquetWriter.Builder<Vehicle> builder;

  @Before
  public void before() {
    super.before();
    builder = ObjectParquetWriter.builder(Vehicle.class, fileName, null);
  }

  @Test
  public void basic() {
    Vehicle vehicle = new Vehicle();
    ObjectParquetWriter<Vehicle> writer = builder.build();
    writer.writeData(vehicle);
    writer.close();
    assertFile(-1, true);
    assertCrc(-1, false);
    List<Map<String, Object>> data = ParquetUtil.readFile(getFile(-1).getAbsolutePath(), 5);

    assertEquals(1, data.size());
    Map<String, Object> vehicleMap = data.get(0);

    System.out.println(vehicleMap);
    assertEquals("abc", vehicleMap.get("String"));
    assertEquals(1, vehicleMap.get("AByte"));
    assertEquals(2, vehicleMap.get("AShort"));
    assertEquals(3, vehicleMap.get("AnInt"));
    assertEquals(4L, vehicleMap.get("ALong"));
    assertEquals(5.23F, vehicleMap.get("AFloat"));
    assertEquals(6.23D, vehicleMap.get("ADouble"));
    assertEquals("$", vehicleMap.get("AChar"));
    assertEquals(true, vehicleMap.get("ABoolean"));
    assertEquals(true, vehicleMap.get("AtomicBoolean"));
    assertEquals(10, vehicleMap.get("AtomicInteger"));
    assertEquals(20L, vehicleMap.get("AtomicLong"));
    assertEquals(30.23, vehicleMap.get("BigDecimal"));
    assertEquals(vehicle.getLocalDate(), vehicleMap.get("LocalDate"));
    assertEquals(vehicle.getLocalTime(), vehicleMap.get("LocalTime"));
    assertEquals(vehicle.getLocalDateTime(), vehicleMap.get("LocalDateTime"));
    assertEquals(vehicle.getLocalDateTime(), vehicleMap.get("ZonedDateTime"));

    assertEquals(vehicle.getLocalDateTime(), vehicleMap.get("JavaDate"));
    assertEquals(vehicle.getLocalDate(), vehicleMap.get("SqlDate"));
    assertEquals(vehicle.getLocalDateTime(), vehicleMap.get("Calendar"));
  }
}
