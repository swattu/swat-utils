/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@Data
public class Vehicle {
  private String string = "abc";

  private byte aByte = 1;
  private short aShort = 2;
  private int anInt = 3;
  private long aLong = 4;
  private float aFloat = 5.23F;
  private double aDouble = 6.23D;
  private char aChar = '$';
  private boolean aBoolean = true;

  private AtomicBoolean atomicBoolean = new AtomicBoolean(true);
  private AtomicInteger atomicInteger = new AtomicInteger(10);
  private AtomicLong atomicLong = new AtomicLong(20);

  private BigDecimal bigDecimal = BigDecimal.valueOf(30.23);

  private LocalDate localDate = LocalDate.of(2022, Month.DECEMBER, 25);
  private LocalTime localTime = LocalTime.of(10, 0);
  private LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);
  private ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
  private Date javaDate = new Date(zonedDateTime.toEpochSecond() * 1000);
  private java.sql.Date sqlDate = java.sql.Date.valueOf(localDate);
  private Calendar calendar = Calendar.getInstance();

  {
    calendar.setTime(javaDate);
  }
}
