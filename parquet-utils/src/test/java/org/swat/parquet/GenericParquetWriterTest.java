/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.parquet;

import org.apache.parquet.schema.MessageType;
import org.apache.parquet.schema.PrimitiveType;
import org.junit.Before;
import org.junit.Test;
import org.swat.core.utils.SystemUtil;

import java.io.File;

import static org.apache.parquet.schema.LogicalTypeAnnotation.stringType;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.swat.parquet.ParquetUtil.crcFile;

public class GenericParquetWriterTest extends BaseWriterTest {
  private GenericParquetWriter.Builder builder;

  @Before
  public void before() {
    super.before();
    SchemaCreator creator = new SchemaCreator();
    creator.add(false, "name", PrimitiveType.PrimitiveTypeName.BINARY, stringType());
    MessageType messageType = creator.build();
    builder = new GenericParquetWriter.Builder(fileName, messageType);
  }

  @Test
  public void basic() {
    GenericParquetWriter writer = builder.build();
    writer.writeData("A");
    writer.writeData("B");
    writer.close();

    File file = new File(fileName);
    assertTrue(file.exists());
    assertFalse(crcFile(file).exists());
  }

  @Test
  public void retainCrc() {
    builder.retainCrc(true);
    GenericParquetWriter writer = builder.build();
    writer.writeData("A");
    writer.writeData("B");
    writer.close();

    File file = new File(fileName);
    assertTrue(file.exists());
    assertTrue(crcFile(file).exists());
  }

  @Test
  public void tmpPath() {
    builder.tmpPath(".");
    GenericParquetWriter writer = builder.build();
    writer.writeData("A");
    writer.writeData("B");
    writer.close();

    assertFile(-1, true);
    assertCrc(-1, false);
  }

  @Test
  public void rollingByCount() {
    builder.maxRecords(2);
    GenericParquetWriter writer = builder.build();
    writer.writeData("A");
    writer.writeData("B");
    writer.writeData("C");
    writer.close();

    assertFile(0, true);
    assertFile(1, true);
    assertCrc(0, false);
    assertCrc(1, false);
  }

  @Test
  public void rollingByTime() {
    builder.maxAgeSec(1);
    GenericParquetWriter writer = builder.build();
    writer.writeData("A");
    SystemUtil.sleep(1001);
    writer.writeData("B");
    writer.writeData("C");
    writer.close();

    assertFile(0, true);
    assertFile(1, true);
    assertFile(2, false);
    assertCrc(0, false);
    assertCrc(1, false);
    assertCrc(2, false);
  }

  @Test
  public void everything() {
    builder.maxRecords(2);
    builder.maxAgeSec(1);
    builder.retainCrc(true);
    builder.tmpPath(".");
    GenericParquetWriter writer = builder.build();
    writer.writeData("A");
    writer.writeData("B");
    SystemUtil.sleep(1001);
    writer.writeData("c");
    SystemUtil.sleep(1001);
    writer.writeData("D");
    writer.close();

    assertFile(0, true);
    assertFile(1, true);
    assertFile(2, true);
    assertCrc(0, true);
    assertCrc(1, true);
    assertCrc(2, true);
  }
}
