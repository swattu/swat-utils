/*
 * Copyright © 2023 Swatantra Agrawal. All rights reserved.
 */

package org.swat.sql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.Before;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * The type Sql base test.
 */
public class SqlBaseTest {
  /**
   * The Data source.
   */
  protected HikariDataSource dataSource;

  /**
   * Gets data source.
   *
   * @return the data source
   */
  public HikariDataSource getDataSource() {
    return dataSource;
  }

  /**
   * Before class.
   */
  @Before
  public final void before() {
    new File(System.getProperty("user.home"), "h2.mv.db").delete();
    new File(System.getProperty("user.home"), "h2.trace.db").delete();
    HikariConfig config = new HikariConfig();
    config.setJdbcUrl("jdbc:h2:~/h2");
    config.setUsername("h1");
    config.setPassword("h2");
    dataSource = new HikariDataSource(config);
  }

  /**
   * Execute update int.
   *
   * @param sql the sql
   * @return the int
   */
  public int executeUpdate(String sql) {
    try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
      return statement.executeUpdate();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
