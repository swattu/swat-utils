/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.sql;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Sql util.
 */
public class SqlUtil {
  /**
   * Gets column infos.
   *
   * @param dataSource the data source
   * @param sql        the sql
   * @return the column infos
   */
  public static List<ColumnInfo> metaData(DataSource dataSource, String sql) {
    try (Connection connection = dataSource.getConnection(); PreparedStatement stmt = connection.prepareStatement(sql)) {
      ResultSetMetaData rsmd = stmt.getMetaData();
      List<ColumnInfo> infos = new ArrayList<>();
      for (int x = 1; x <= rsmd.getColumnCount(); x++) {
        ColumnInfo columnInfo = new ColumnInfo();

        columnInfo.setIndex(x);
        columnInfo.setAutoIncrement(rsmd.isAutoIncrement(x));
        columnInfo.setCatName(rsmd.getCatalogName(x));
        columnInfo.setColumnLabel(rsmd.getColumnLabel(x));
        columnInfo.setColumnName(rsmd.getColumnName(x));
        columnInfo.setColPrecision(rsmd.getPrecision(x));
        columnInfo.setColScale(rsmd.getScale(x));
        columnInfo.setColType(rsmd.getColumnType(x));
        columnInfo.setCurrency(rsmd.isCurrency(x));
        columnInfo.setColumnDisplaySize(rsmd.getColumnDisplaySize(x));
        columnInfo.setNullable(rsmd.isNullable(x));
        columnInfo.setSigned(rsmd.isSigned(x));
        columnInfo.setSearchable(rsmd.isSearchable(x));
        columnInfo.setCaseSensitive(rsmd.isCaseSensitive(x));
        columnInfo.setSchemaName(rsmd.getSchemaName(x));
        columnInfo.setColTypeName(rsmd.getColumnTypeName(x));
        columnInfo.setReadOnly(rsmd.isReadOnly(x));
        columnInfo.setWritable(rsmd.isWritable(x));
        columnInfo.setColumnClassName(rsmd.getColumnClassName(x));

        infos.add(columnInfo);
      }
      return infos;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Count int.
   *
   * @param dataSource the data source
   * @param sql        the sql
   * @return the int
   */
  public static int count(DataSource dataSource, String sql) {
    sql = "SELECT count(1) FROM (" + sql + ") a";
    try (Connection connection = dataSource.getConnection(); PreparedStatement stmt = connection.prepareStatement(sql); ResultSet resultSet = stmt.executeQuery()) {
      if (resultSet.next()) {
        return resultSet.getInt(1);
      }
      return 0;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

  }
}
