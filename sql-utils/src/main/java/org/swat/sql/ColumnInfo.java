/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.sql;

import lombok.Data;

/**
 * The type Column info.
 */
@Data
public class ColumnInfo {
  private int index;
  private boolean autoIncrement;
  private boolean caseSensitive;
  private boolean currency;
  private int nullable;
  private boolean signed;
  private boolean searchable;
  private int columnDisplaySize;
  private String columnLabel;
  private String columnName;
  private String schemaName;
  private int colPrecision;
  private int colScale;
  private String catName;
  private int colType;
  private String colTypeName;
  private boolean readOnly;
  private boolean writable;
  private String columnClassName;

  /**
   * Instantiates a new Column info.
   */
  public ColumnInfo() {
    this.readOnly = false;
    this.writable = true;
  }
}
