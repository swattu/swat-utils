/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.cache.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Swatantra Agrawal
 * on 27-AUG-2017
 */
public class ExpiryCacheTest {
  @Test
  public void getPut() throws Exception {
    ExpiryCache<String, String> expiryCache = new ExpiryCache<>(1000);
    expiryCache.put(null, null);
    assertEquals(null, expiryCache.get(null));

    expiryCache.put(null, "abc");
    assertEquals(null, expiryCache.get(null));

    expiryCache.put("name", null);
    assertEquals(null, expiryCache.get("name"));

    expiryCache.put("name", "abc");
    assertEquals("abc", expiryCache.get("name"));
    Thread.sleep(1001);
    assertEquals(null, expiryCache.get("name"));
  }

  @Test
  public void put() throws Exception {
    ExpiryCache<String, String> expiryCache = new ExpiryCache<>(1000);
    expiryCache.put("name", "abc");
    assertEquals("abc", expiryCache.put("name", "xyz"));
    Thread.sleep(1001);
    assertEquals(null, expiryCache.put("name", "rst"));
  }

  @Test
  public void removeExpired() throws Exception {
    ExpiryCache<String, String> expiryCache = new ExpiryCache<>(1000);
    expiryCache.put("name", "abc");
    assertEquals(1, expiryCache.size());
    assertTrue(expiryCache.contains("name"));
    Thread.sleep(1001);
    expiryCache.removeExpired();
    assertFalse(expiryCache.contains("name"));
    assertEquals(0, expiryCache.size());
  }

  @Test
  public void nullValue() throws Exception {
    ExpiryCache<String, String> expiryCache = new ExpiryCache<>(1000);
    expiryCache.put("name", null);
    assertEquals(1, expiryCache.size());
    assertTrue(expiryCache.contains("name"));
    Thread.sleep(1001);
    expiryCache.removeExpired();
    assertFalse(expiryCache.contains("name"));
    assertEquals(0, expiryCache.size());
  }

  @Test
  public void clearAll(){
    ExpiryCache<String, String> expiryCache = new ExpiryCache<>(1000);
    expiryCache.put("name", "Swat");
    assertEquals("Swat", expiryCache.get("name"));
    CacheAware.clearAll();
    assertNull(expiryCache.get("name"));
  }
}
