/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.cache.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Very lazy expiry cache. Null key is not supported. It does not thrown any exception for null key.
 * It is built on {@link ConcurrentHashMap} so is thread safe.
 * It does not use any thread for removal of expired entries.
 * Expired entries are removed when {@link #get(Object)} or {@link #removeExpired()} is called.
 *
 * @param <K> the type parameter
 * @param <V> the type parameter
 * @author Swatantra Agrawal on 27-AUG-2017
 */
public class ExpiryCache<K, V> extends CacheAware {
  private final int millis;
  private final Map<K, ExpiryBean<V>> cache = new ConcurrentHashMap<>();

  /**
   * Initializes the cache
   *
   * @param expiryMillis Expiry duration in millis
   */
  public ExpiryCache(int expiryMillis) {
    this.millis = expiryMillis;
  }

  /**
   * Put the value against the key.
   *
   * @param key   The key
   * @param value The value
   * @return Previous value. null if previous value is expired
   */
  public V put(K key, V value) {
    return put(key, value, millis);
  }

  /**
   * Put the value against the key with defined expiry millis
   *
   * @param key    The key
   * @param value  The value
   * @param millis The expiry duration in millis
   * @return Previous value. null if previous value is expired
   */
  public V put(K key, V value, int millis) {
    if (key == null) {
      return null;
    }
    ExpiryBean<V> bean = cache.put(key, new ExpiryBean<>(value, millis));
    if (bean == null || bean.expireAt < System.currentTimeMillis()) {
      return null;
    }
    return bean.value;
  }

  /**
   * This may return an approximate size
   *
   * @return The size of cache
   */
  public int size() {
    return cache.size();
  }

  /**
   * Removes the expired objects
   */
  public void removeExpired() {
    for (K key : cache.keySet()) {
      get(key);
    }
  }

  /**
   * Get value associated with the key
   *
   * @param key The key
   * @return null if expired, else the value
   */
  public V get(K key) {
    if (key == null) {
      return null;
    }
    ExpiryBean<V> bean = cache.get(key);
    if (bean == null || bean.expireAt < System.currentTimeMillis()) {
      cache.remove(key);
      return null;
    }
    return bean.value;
  }

  /**
   * Remove v.
   *
   * @param key the key
   * @return the v
   */
  public V remove(K key) {
    if (key == null) {
      return null;
    }
    ExpiryBean<V> bean = cache.remove(key);
    if (bean == null || bean.expireAt < System.currentTimeMillis()) {
      return null;
    }
    return bean.value;
  }

  /**
   * Contains
   *
   * @param key the key
   * @return true if the cache contains the key
   */
  public boolean contains(K key) {
    if (key == null) {
      return false;
    }
    ExpiryBean<V> bean = cache.get(key);
    if (bean != null && bean.expireAt < System.currentTimeMillis()) {
      cache.remove(key);
    }
    return cache.containsKey(key);
  }


  /**
   * Clear the cache
   */
  @Override
  public void clear() {
    cache.clear();
  }

  private class ExpiryBean<T> {
    private final long expireAt;
    private final T value;

    private ExpiryBean(T value, int millis) {
      this.value = value;
      this.expireAt = System.currentTimeMillis() + millis;
    }
  }
}
