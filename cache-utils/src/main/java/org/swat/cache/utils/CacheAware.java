/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.cache.utils;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * The type Cache aware.
 */
public abstract class CacheAware {
  private static final Set<CacheAware> caches = new LinkedHashSet<>();

  {
    caches.add(this);
  }

  /**
   * Reset all.
   */
  public static void clearAll() {
    for (CacheAware context : caches) {
      context.clear();
    }
  }

  /**
   * Remove.
   */
  protected abstract void clear();
}
