/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.csv.utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.swat.excel.utils.ExcelUtils;
import org.swat.reflect.utils.ReflectOption;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Csv writer.
 *
 * @param <T> the type parameter
 */
public class XlsWriter<T> extends BaseWriter<T, XlsWriter<T>> {
  private final Sheet sheet;
  private final Map<String, CellFormatter<T>> cellFormatterMap = new HashMap<>();
  private CellFormatter<T> defaultCellFormatter;
  private HeaderFormatter headerFormatter;

  /**
   * Instantiates a new Xls writer.
   *
   * @param clazz the clazz
   * @param sheet the sheet
   */
  public XlsWriter(Class<T> clazz, Sheet sheet) {
    this(clazz, sheet, 0);
  }

  /**
   * Instantiates a new Xsv writer.
   *
   * @param clazz the clazz
   * @param sheet the csv writer
   * @param depth the depth
   */
  public XlsWriter(Class<T> clazz, Sheet sheet, int depth) {
    super(clazz, depth);
    this.sheet = sheet;
  }

  /**
   * Instantiates a new Xls writer.
   *
   * @param clazz  the clazz
   * @param sheet  the sheet
   * @param depth  the depth
   * @param option the option
   */
  public XlsWriter(Class<T> clazz, Sheet sheet, int depth, ReflectOption option) {
    super(clazz, depth, option);
    this.sheet = sheet;
  }

  /**
   * Sets default cell formatter.
   *
   * @param defaultCellFormatter the default cell formatter
   * @return the default cell formatter
   */
  public XlsWriter<T> setDefaultCellFormatter(CellFormatter<T> defaultCellFormatter) {
    this.defaultCellFormatter = defaultCellFormatter;
    return this;
  }

  /**
   * Sets header formatter.
   *
   * @param headerFormatter the header formatter
   * @return the header formatter
   */
  public XlsWriter<T> setHeaderFormatter(HeaderFormatter headerFormatter) {
    this.headerFormatter = headerFormatter;
    return this;
  }

  @Override
  void startRow() {
    ExcelUtils.newRow(sheet);
  }

  @Override
  void finishRow() {
  }

  @Override
  protected void writeHeader(String field, String name) {
    Row row = ExcelUtils.currentRow(sheet);
    Cell cell = ExcelUtils.newCell(row);
    cell.setCellValue(name);
    if (headerFormatter != null) {
      headerFormatter.formatCell(cell, field, name);
    }
  }

  /**
   * Add cell formatter xls writer.
   *
   * @param cellFormatter the cell formatter
   * @param fields        the fields
   * @return the xls writer
   */
  public XlsWriter<T> addCellFormatter(CellFormatter<T> cellFormatter, String... fields) {
    throwIfInitialized();
    for (String field : fields) {
      cellFormatterMap.putIfAbsent(field, cellFormatter);
    }
    return this;
  }

  @Override
  protected void writeValue(T bean, String field, boolean isCustom, Object origValue, Object value) {
    Row row = ExcelUtils.currentRow(sheet);
    Cell cell = ExcelUtils.newCell(row);
    if (value instanceof String) {
      cell.setCellValue((String) value);
    } else if (value instanceof Boolean) {
      cell.setCellValue((Boolean) value);
    } else if (value instanceof Date) {
      cell.setCellValue((Date) value);
    } else if (value instanceof Number) {
      cell.setCellValue(((Number) value).doubleValue());
    } else if (value instanceof Calendar) {
      cell.setCellValue((Calendar) value);
    } else if (value instanceof RichTextString) {
      cell.setCellValue((RichTextString) value);
    } else if (value != null) {
      cell.setCellValue(value.toString());
    }
    CellFormatter<T> cellFormatter = cellFormatterMap.get(field);
    if (cellFormatter != null) {
      cellFormatter.formatCell(bean, cell, field, isCustom, origValue, value);
    } else if (defaultCellFormatter != null) {
      defaultCellFormatter.formatCell(bean, cell, field, isCustom, origValue, value);
    }
  }
}
