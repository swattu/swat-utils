/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.csv.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * The type Date formatter.
 */
public class DateFormatter<T> implements XsvFormatter<T> {
  private final SimpleDateFormat simpleDateFormat;


  /**
   * Instantiates a new Date formatter.
   *
   * @param zone   the zone
   * @param format the format
   */
  public DateFormatter(String zone, String format) {
    simpleDateFormat = new SimpleDateFormat(format);
    simpleDateFormat.setTimeZone(TimeZone.getTimeZone(zone));
  }

  @Override
  public Object format(T bean, String field, boolean isCustom, Object value) {
    if (value instanceof Date) {
      return simpleDateFormat.format(value);
    }
    return value;
  }
}
