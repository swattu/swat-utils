/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.csv.utils;

import org.apache.poi.ss.usermodel.Cell;

/**
 * The interface Cell formatter.
 */
public interface HeaderFormatter {
  /**
   * Format cell.
   *
   * @param cell  the cell
   * @param field the field
   * @param name  the name
   */
  void formatCell(Cell cell, String field, String name);
}
