/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.csv.utils;

import org.swat.core.utils.CoreRtException;
import org.swat.json.utils.CustomFieldAware;
import org.swat.reflect.utils.CachedReflectUtils;
import org.swat.reflect.utils.FieldInfo;
import org.swat.reflect.utils.ReflectOption;
import org.swat.reflect.utils.ReflectUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * The type Base writer.
 *
 * @param <T> the type parameter
 * @param <V> the type parameter
 */
public abstract class BaseWriter<T, V extends BaseWriter> {
  /**
   * The Clazz.
   */
  private final Class<T> clazz;
  private final int depth;
  private final CachedReflectUtils<T> reflectUtils;
  private final ReflectOption reflectOption;

  /**
   * The Fields.
   */
  private final Set<String> fields = new LinkedHashSet<>();
  /**
   * The Header map.
   */
  private final Map<String, String> headerMap = new HashMap<>();
  /**
   * The Formatter map.
   */
  private final Map<String, XsvFormatter<T>> formatterMap = new HashMap<>();
  /**
   * The Bean fields.
   */
  private final Set<String> beanFields = new LinkedHashSet<>();

  /**
   * The Header.
   */
  private boolean header = true;
  /**
   * The Include fields.
   */
  private boolean includeFields = false;
  /**
   * The Include custom fields.
   */
  private boolean includeCustomFields = false;
  /**
   * The First line.
   */
  private boolean firstLine = true;
  /**
   * The Initialized.
   */
  private boolean initialized = false;
  private XsvFormatter<T> defaultFormatter = null;

  /**
   * Sets header.
   *
   * @param header the header
   */
  public void setHeader(boolean header) {
    throwIfInitialized();
    this.header = header;
  }

  /**
   * Add fields.
   *
   * @param fields the fields
   */
  public void addFields(String... fields) {
    throwIfInitialized();
    this.fields.addAll(Arrays.asList(fields));
  }

  /**
   * Add header.
   *
   * @param key  the key
   * @param name the name
   */
  public void addHeader(String key, String name) {
    throwIfInitialized();
    headerMap.put(key, name);
  }

  /**
   * Add formatter t.
   *
   * @param formatter the formatter
   * @param fields    the fields
   * @return the t
   */
  public V addFormatter(XsvFormatter<T> formatter, String... fields) {
    throwIfInitialized();
    for (String field : fields) {
      formatterMap.put(field, formatter);
    }
    return (V) this;
  }

  /**
   * Set Default formatter t.
   *
   * @param formatter the nf
   * @return the t
   */
  public V setDefaultFormatter(XsvFormatter<T> formatter) {
    throwIfInitialized();
    this.defaultFormatter = formatter;
    return (V) this;
  }

  /**
   * Sets include fields.
   *
   * @param includeFields the include fields
   */
  public void setIncludeFields(boolean includeFields) {
    throwIfInitialized();
    this.includeFields = includeFields;
  }

  /**
   * Sets include custom fields.
   *
   * @param includeCustomFields the include custom fields
   */
  public void setIncludeCustomFields(boolean includeCustomFields) {
    throwIfInitialized();
    this.includeCustomFields = includeCustomFields;
  }

  /**
   * Instantiates a new Base writer.
   *
   * @param clazz the clazz
   */
  protected BaseWriter(Class<T> clazz) {
    this(clazz, 0);
  }

  /**
   * Instantiates a new Base writer.
   *
   * @param clazz the clazz
   * @param depth the depth
   */
  protected BaseWriter(Class<T> clazz, int depth) {
    this(clazz, depth, null);
  }

  /**
   * Instantiates a new Base writer.
   *
   * @param clazz  the clazz
   * @param depth  the depth
   * @param option the option
   */
  protected BaseWriter(Class<T> clazz, int depth, ReflectOption option) {
    this.clazz = clazz;
    if (depth < 0) {
      depth = 0;
    }
    if (depth > 2) {
      depth = 2;
    }
    this.depth = depth;
    this.reflectUtils = new CachedReflectUtils<>(option);
    this.reflectOption = option;
  }

  /**
   * Throw if initialized.
   */
  void throwIfInitialized() {
    if (initialized) {
      throw new CoreRtException("The method cannot be called, as the writer is initialized");
    }
  }

  /**
   * Initialize.
   *
   * @param bean the bean
   */
  void initialize(T bean) {
    if (initialized) {
      return;
    }
    Map<String, FieldInfo> fieldInfoMap = new LinkedHashMap<>();
    for (FieldInfo fieldInfo : ReflectUtils.getters(clazz, depth, reflectOption)) {
      fieldInfoMap.put(fieldInfo.getName(), fieldInfo);
    }
    beanFields.addAll(fieldInfoMap.keySet());
    if (fields.isEmpty()) {
      fields.addAll(beanFields);
      if (bean instanceof CustomFieldAware) {
        fields.addAll(((CustomFieldAware) bean).customFields().keySet());
      }
    }
    if (includeFields) {
      fields.addAll(beanFields);
    }
    if (includeCustomFields) {
      if (bean instanceof CustomFieldAware) {
        fields.addAll(((CustomFieldAware) bean).customFields().keySet());
      }
    }

    for (String field : fields) {
      String display;
      FieldInfo fieldInfo = fieldInfoMap.get(field);
      if (fieldInfo != null) {
        display = fieldInfo.getDisplay();
      } else {
        display = ReflectUtils.getDisplay(field);
      }
      headerMap.putIfAbsent(field, display);
    }

    initialized = true;
  }

  /**
   * Write.
   *
   * @param iterable the iterable
   */
  public void write(Iterable<T> iterable) {
    boolean found = false;
    for (T bean : iterable) {
      if (bean != null) {
        write(bean);
        found = true;
      }
    }
    if (!found) {
      write((T) null);
    }
  }

  /**
   * Write.
   *
   * @param bean the bean
   */
  public void write(T bean) {
    initialize(bean);
    if (firstLine) {
      writeHeader();
    }
    if (bean != null) {
      startRow();
      for (String field : fields) {
        Object[] values = getValue(bean, field);
        writeValue(bean, field, !beanFields.contains(field), values[0], values[1]);
      }
      finishRow();
    }
    firstLine = false;
  }

  private Object[] getValue(T bean, String field) {
    Object origValue = null;
    boolean isCustom = false;
    if (fields.contains(field) && beanFields.contains(field)) {
      origValue = reflectUtils.getValue(bean, field);
    } else if (bean instanceof CustomFieldAware) {
      origValue = ((CustomFieldAware) bean).customField(field);
      isCustom = true;
    }
    XsvFormatter<T> xsvFormatter = formatterMap.get(field);
    Object value = origValue;
    if (xsvFormatter != null) {
      value = xsvFormatter.format(bean, field, isCustom, origValue);
    } else if (defaultFormatter != null) {
      value = defaultFormatter.format(bean, field, isCustom, origValue);
    }
    return new Object[]{origValue, value};
  }

  private void writeHeader() {
    if (!header) {
      return;
    }
    startRow();
    for (String field : fields) {
      writeHeader(field, headerMap.get(field));
    }
    finishRow();
  }

  /**
   * Start line.
   */
  abstract void startRow();

  /**
   * Finish line.
   */
  abstract void finishRow();

  /**
   * Write line.
   *
   * @param field the field
   * @param value the value
   */
  protected abstract void writeHeader(String field, String value);

  /**
   * Write value.
   *
   * @param bean      the bean
   * @param field     the field
   * @param isCustom  the is custom
   * @param origValue the orig value
   * @param value     the value
   */
  protected abstract void writeValue(T bean, String field, boolean isCustom, Object origValue, Object value);
}
