/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.csv.utils;

import com.opencsv.ICSVWriter;
import lombok.Setter;
import org.swat.reflect.utils.ReflectOption;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Csv writer.
 *
 * @param <T> the type parameter
 */
@Setter
public class XsvWriter<T> extends BaseWriter<T, XsvWriter<T>> {
  private final ICSVWriter csvWriter;
  private final List<String> values = new ArrayList<>();

  /**
   * Instantiates a new Xsv writer.
   *
   * @param clazz     the clazz
   * @param csvWriter the csv writer
   */
  public XsvWriter(Class<T> clazz, ICSVWriter csvWriter) {
    this(clazz, csvWriter, 0);
  }

  /**
   * Instantiates a new Xsv writer.
   *
   * @param clazz     the clazz
   * @param csvWriter the csv writer
   * @param depth     the depth
   */
  public XsvWriter(Class<T> clazz, ICSVWriter csvWriter, int depth) {
    super(clazz, depth);
    this.csvWriter = csvWriter;
  }

  /**
   * Instantiates a new Xsv writer.
   *
   * @param clazz     the clazz
   * @param csvWriter the csv writer
   * @param depth     the depth
   * @param option    the option
   */
  public XsvWriter(Class<T> clazz, ICSVWriter csvWriter, int depth, ReflectOption option) {
    super(clazz, depth, option);
    this.csvWriter = csvWriter;
  }

  /**
   * Close.
   *
   * @throws IOException the io exception
   */
  public void close() throws IOException {
    csvWriter.close();
  }

  @Override
  void startRow() {
    values.clear();
  }

  @Override
  void finishRow() {
    csvWriter.writeNext(values.toArray(new String[0]));
  }

  @Override
  protected void writeHeader(String field, String value) {
    values.add(value);
  }

  @Override
  protected void writeValue(T bean, String field, boolean isCustom,Object origValue, Object value) {
    if (value == null) {
      values.add("");
    } else {
      values.add(String.valueOf(value));
    }
  }
}
