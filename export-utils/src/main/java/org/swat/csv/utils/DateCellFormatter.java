/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.csv.utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.Date;

/**
 * The type Date formatter.
 */
public class DateCellFormatter<T> implements CellFormatter<T> {
  private final CellStyle cellStyle;

  /**
   * Instantiates a new Date formatter.
   *
   * @param workbook the workbook
   * @param zone     the zone
   * @param format   the format
   */
  public DateCellFormatter(Workbook workbook, String format) {
    cellStyle = workbook.createCellStyle();
    CreationHelper createHelper = workbook.getCreationHelper();
    cellStyle.setDataFormat(createHelper.createDataFormat().getFormat(format));
  }

  @Override
  public void formatCell(T bean, Cell cell, String field, boolean isCustom, Object value) {
    if (value instanceof Date) {
      cell.setCellStyle(cellStyle);
    }
  }
}
