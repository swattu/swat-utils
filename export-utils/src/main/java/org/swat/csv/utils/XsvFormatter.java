/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.csv.utils;

/**
 * The interface Xsv formatter.
 *
 * @param <T> the type parameter
 */
public interface XsvFormatter<T> {
  /**
   * Format string.
   *
   * @param bean     the bean
   * @param field    the field
   * @param isCustom the is custom
   * @param value    the value
   * @return the string
   */
  Object format(T bean, String field, boolean isCustom, Object value);
}
