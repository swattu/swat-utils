/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.csv.utils;

import org.apache.poi.ss.usermodel.Cell;
import org.swat.core.utils.CoreRtException;

/**
 * The interface Cell formatter.
 *
 * @param <T> the type parameter
 */
public interface CellFormatter<T> {
  /**
   * Format cell.
   *
   * @param bean     the bean
   * @param cell     the cell
   * @param field    the field
   * @param isCustom the is custom
   * @param value    the value
   */
  default void formatCell(T bean, Cell cell, String field, boolean isCustom, Object value) {
    throw new CoreRtException("Either override this method or the overloaded method.");
  }

  /**
   * Format cell.
   *
   * @param bean      the bean
   * @param cell      the cell
   * @param field     the field
   * @param isCustom  the is custom
   * @param origValue the orig value
   * @param value     the value
   */
  default void formatCell(T bean, Cell cell, String field, boolean isCustom, Object origValue, Object value) {
    formatCell(bean, cell, field, isCustom, value);
  }
}
