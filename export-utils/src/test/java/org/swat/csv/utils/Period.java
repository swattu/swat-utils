/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.csv.utils;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Period {
  private LocalDate from;
  private LocalDate to;
}
