/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.csv.utils;

import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;
import lombok.Getter;
import org.junit.Test;
import org.swat.json.utils.CustomFieldAware;

import java.io.StringWriter;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@Getter
public class XsvWriterTest implements CustomFieldAware {
  private String name;
  private Period period;
  private final Map<String, Object> customFields = new LinkedHashMap<>();

  @Test
  public void test() throws Exception {
    StringWriter writer = new StringWriter();
    ICSVWriter csvWriter = new CSVWriterBuilder(writer).withQuoteChar('\'').build();
    XsvWriter<XsvWriterTest> xsvWriter = new XsvWriter<>(XsvWriterTest.class, csvWriter, 2);
    NumberFormat numberFormat = NumberFormat.getInstance();
    numberFormat.setMaximumFractionDigits(2);
    numberFormat.setMinimumFractionDigits(2);
    xsvWriter.addFields("name", "period.from", "period.to");
    xsvWriter.addHeader("period.from", "Period From");
    xsvWriter.setIncludeCustomFields(true);
    xsvWriter.addFormatter((bean, field, isCustom, value) -> numberFormat.format(value), "salary");

    XsvWriterTest test = new XsvWriterTest();
    test.period = new Period();
    test.period.setFrom(LocalDate.of(2019, Month.NOVEMBER, 1));
    test.period.setTo(LocalDate.of(2019, Month.NOVEMBER, 30));
    test.name = "Swat";
    test.putCustomField("myAge", 46);
    test.putCustomField("salary", 1000);
    xsvWriter.write(test);
    xsvWriter.close();
    assertEquals(
        "'Name','Period From','To','My Age','Salary'\n" + "'Swat','2019-11-01','2019-11-30','46','1,000.00'\n", writer
            .toString());
  }

  @Test
  public void emptyRows() throws Exception {
    StringWriter writer = new StringWriter();
    ICSVWriter csvWriter = new CSVWriterBuilder(writer).withQuoteChar('\'').build();
    XsvWriter<XsvWriterTest> xsvWriter = new XsvWriter<>(XsvWriterTest.class, csvWriter, 2);
    NumberFormat numberFormat = NumberFormat.getInstance();
    numberFormat.setMaximumFractionDigits(2);
    numberFormat.setMinimumFractionDigits(2);
    xsvWriter.addFields("name", "period.from", "period.to");
    //Custom Field
    xsvWriter.addFields("age");
    xsvWriter.addHeader("period.from", "Period From");
    xsvWriter.setIncludeCustomFields(true);
    xsvWriter.addFormatter((bean, field, isCustom, value) -> numberFormat.format(value), "salary");

    xsvWriter.write(new ArrayList<>());
    xsvWriter.close();
    assertEquals("'Name','Period From','To','Age'\n", writer.toString());
  }

  @Override
  public Map<String, Object> customFields() {
    return customFields;
  }
}
