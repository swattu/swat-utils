/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.csv.utils;

import lombok.Getter;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.swat.excel.utils.ExcelUtils;
import org.swat.json.utils.CustomFieldAware;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@Getter
public class XlsWriterTest implements CustomFieldAware {
  private String name;
  private Date date;
  private double salary;
  private CellType cellType;
  private final Map<String, Object> customFields = new LinkedHashMap<>();

  @Test
  public void test() throws Exception {
    Workbook workbook = new XSSFWorkbook();
    DateCellFormatter<XlsWriterTest> cellFormatter = new DateCellFormatter<>(workbook, "dd/MM/yyyy");
    Sheet sheet = workbook.createSheet("Person");
    XlsWriter<XlsWriterTest> xsvWriter = new XlsWriter<>(XlsWriterTest.class, sheet);
    xsvWriter.setDefaultCellFormatter(cellFormatter);
    XlsWriterTest test = new XlsWriterTest();
    test.name = "Swat";
    test.putCustomField("myAge", 46);
    test.date = new Date();
    test.salary = Long.MAX_VALUE;
    test.cellType = CellType.BOOLEAN;
    xsvWriter.write(test);
    ExcelUtils.autoSizeColumns(workbook);
    workbook.write(Files.newOutputStream(new File("target/abc.xlsx").toPath()));
  }

  @Override
  public Map<String, Object> customFields() {
    return customFields;
  }
}
