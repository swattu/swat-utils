/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.http.utils;

import org.apache.http.config.SocketConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.swat.core.utils.PropertyUtils;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.util.Properties;

import static org.swat.core.utils.PropertyUtils.getBoolean;
import static org.swat.core.utils.PropertyUtils.getInt;
import static org.swat.core.utils.PropertyUtils.isConfigured;
import static org.swat.http.utils.HttpClientConstants.*;

/**
 * @author Swatantra Agrawal on 03-NOV-2017
 */
public class HttpClientUtils {
  private HttpClientUtils() {
  }

  /**
   * Gets client.
   *
   * @param supportedProtocols the supported protocols
   * @param properties         the properties
   * @param prefixes           the prefixes
   * @return the client
   */
  public static CloseableHttpClient getClient(String[] supportedProtocols, Properties properties, String... prefixes) {
    SSLConnectionSocketFactory factory = getSslConnectionSocketFactory(supportedProtocols);

    SocketConfig socketConfig = getConfiguredSocketConfig(properties, prefixes);

    HttpClientBuilder builder = HttpClients.custom().setDefaultSocketConfig(socketConfig).setSSLSocketFactory(factory);


    return builder.build();
  }

  private synchronized static SSLConnectionSocketFactory getSslConnectionSocketFactory(String[] supportedProtocols) {
    SSLContext sslContext = SSLContexts.createSystemDefault();
    return new SSLConnectionSocketFactory(sslContext, supportedProtocols, null, (HostnameVerifier) null);
  }


  /**
   * Gets configured socket config.
   *
   * @param properties the properties
   * @param prefixes   the prefixes
   * @return the configured socket config
   */
  public static synchronized SocketConfig getConfiguredSocketConfig(Properties properties, String[] prefixes) {
    return getSocketConfigBuilder(properties, prefixes).build();
  }

  /**
   * Gets socket config builder.
   *
   * @param p        the p
   * @param prefixes the prefixes
   * @return the socket config builder
   */
  public static synchronized SocketConfig.Builder getSocketConfigBuilder(Properties p, String[] prefixes) {
    SocketConfig.Builder builder = SocketConfig.custom();
    if (p == null) {
      return builder;
    }
    p = PropertyUtils.getProperties(p, ALL_KEYS, prefixes);
    if (isConfigured(p, SO_TIMEOUT)) {
      builder.setSoTimeout(getInt(p, SO_TIMEOUT));
    }
    if (isConfigured(p, SO_REUSE_ADDRESS)) {
      builder.setSoReuseAddress(getBoolean(p, SO_REUSE_ADDRESS));
    }
    if (isConfigured(p, SO_LINGER)) {
      builder.setSoLinger(getInt(p, SO_LINGER));
    }
    if (isConfigured(p, SO_KEEP_ALIVE)) {
      builder.setSoKeepAlive(getBoolean(p, SO_KEEP_ALIVE));
    }
    if (isConfigured(p, TCP_NO_DELAY)) {
      builder.setTcpNoDelay(getBoolean(p, TCP_NO_DELAY));
    }
    if (isConfigured(p, SND_BUF_SIZE)) {
      builder.setSndBufSize(getInt(p, SND_BUF_SIZE));
    }
    if (isConfigured(p, RCV_BUF_SIZE)) {
      builder.setRcvBufSize(getInt(p, RCV_BUF_SIZE));
    }
    if (isConfigured(p, BACKLOG_SIZE)) {
      builder.setBacklogSize(getInt(p, BACKLOG_SIZE));
    }
    return builder;
  }
}
