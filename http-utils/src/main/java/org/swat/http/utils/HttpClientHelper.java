/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.http.utils;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.swat.core.utils.CoreRtException;
import org.swat.json.utils.JsonConstants;
import org.swat.json.utils.JsonUtil;

import java.io.IOException;

/**
 * The type Http client service.
 *
 * @author Swatantra Agrawal on 03-NOV-2017
 */
public class HttpClientHelper {
  private final HttpClient httpClient;
  private final JsonUtil jsonUtil;

  /**
   * Instantiates a new Http client service.
   *
   * @param httpClient the http client
   */
  public HttpClientHelper(HttpClient httpClient) {
    this(httpClient, JsonConstants.JSON_UTIL);
  }

  /**
   * Instantiates a new Http client service.
   *
   * @param httpClient the http client
   * @param jsonUtil   the json util
   */
  public HttpClientHelper(HttpClient httpClient, JsonUtil jsonUtil) {
    this.httpClient = httpClient;
    this.jsonUtil = jsonUtil;
  }

  /**
   * Post t.
   *
   * @param <T>     the type parameter
   * @param clazz   the clazz
   * @param url     the url
   * @param entity  the entity
   * @param headers the headers
   * @return the t
   */
  public <T> T post(Class<T> clazz, String url, Object entity, Header... headers) {
    HttpPost method = new HttpPost(url);
    method.setHeaders(headers);

    String json = executeJson(method, entity);
    return jsonUtil.readObject(json, clazz);
  }

  /**
   * Execute json string.
   *
   * @param httpBase the http base
   * @param entity   the entity
   * @return the string
   */
  public String executeJson(HttpEntityEnclosingRequestBase httpBase, Object entity) {
    String requestJson = jsonUtil.toJsonString(entity);
    StringEntity stringEntity = new StringEntity(requestJson, ContentType.APPLICATION_JSON);
    httpBase.setEntity(stringEntity);

    return executeJson(httpBase);
  }

  /**
   * Execute json string.
   *
   * @param httpBase the http base
   * @return the string
   */
  public String executeJson(HttpRequestBase httpBase) {
    HttpResponse response;
    try {
      response = httpClient.execute(httpBase);
      int status = response.getStatusLine().getStatusCode();
      if (status != 200) {
        throw new CoreRtException("Status is " + status);
      }
      return EntityUtils.toString(response.getEntity());
    } catch (IOException e) {
      throw new CoreRtException("Exception thrown", e);
    }
  }

  /**
   * Put t.
   *
   * @param <T>     the type parameter
   * @param clazz   the clazz
   * @param url     the url
   * @param entity  the entity
   * @param headers the headers
   * @return the t
   */
  public <T> T put(Class<T> clazz, String url, Object entity, Header... headers) {
    HttpPut method = new HttpPut(url);
    method.setHeaders(headers);

    String json = executeJson(method, entity);
    return jsonUtil.readObject(json, clazz);
  }

  /**
   * Get t.
   *
   * @param <T>     the type parameter
   * @param clazz   the clazz
   * @param url     the url
   * @param headers the headers
   * @return the t
   */
  public <T> T get(Class<T> clazz, String url, Header... headers) {
    HttpGet method = new HttpGet(url);
    method.setHeaders(headers);

    String json = executeJson(method);
    return jsonUtil.readObject(json, clazz);
  }

  /**
   * Delete t.
   *
   * @param <T>     the type parameter
   * @param clazz   the clazz
   * @param url     the url
   * @param headers the headers
   * @return the t
   */
  public <T> T delete(Class<T> clazz, String url, Header... headers) {
    HttpDelete method = new HttpDelete(url);
    method.setHeaders(headers);

    String json = executeJson(method);
    return jsonUtil.readObject(json, clazz);
  }
}
