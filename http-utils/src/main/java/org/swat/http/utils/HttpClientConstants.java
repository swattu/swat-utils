/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.http.utils;


/**
 * @author Swatantra Agrawal on 03-NOV-2017
 */
public class HttpClientConstants {
  /**
   * The constant SO_TIMEOUT.
   */
  public static final String SO_TIMEOUT = "soTimeout";
  /**
   * The constant SO_REUSE_ADDRESS.
   */
  public static final String SO_REUSE_ADDRESS = "soReuseAddress";
  /**
   * The constant SO_LINGER.
   */
  public static final String SO_LINGER = "soLinger";
  /**
   * The constant SO_KEEP_ALIVE.
   */
  public static final String SO_KEEP_ALIVE = "soKeepAlive";
  /**
   * The constant TCP_NO_DELAY.
   */
  public static final String TCP_NO_DELAY = "tcpNoDelay";
  /**
   * The constant SND_BUF_SIZE.
   */
  public static final String SND_BUF_SIZE = "sndBufSize";
  /**
   * The constant RCV_BUF_SIZE.
   */
  public static final String RCV_BUF_SIZE = "rcvBufSize";
  /**
   * The constant BACKLOG_SIZE.
   */
  public static final String BACKLOG_SIZE = "backlogSize";

  /**
   * The constant for all configuration keys.
   */
  public static final String[] ALL_KEYS =
      {SO_TIMEOUT, SO_REUSE_ADDRESS, SO_LINGER, SO_KEEP_ALIVE, TCP_NO_DELAY, SND_BUF_SIZE, RCV_BUF_SIZE, BACKLOG_SIZE};

}
