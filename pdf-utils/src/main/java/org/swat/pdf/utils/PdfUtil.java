/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.pdf.utils;

import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.swat.core.utils.CoreRtException;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * The type Pdf util.
 */
public class PdfUtil {
  /**
   * Parse lines list.
   *
   * @param file the file
   * @return the list
   */
  public static List<String> parseLines(File file) {
    PDDocument document = null;
    try {
      document = PDDocument.load(file);
      PDFTextStripper pdfReader = new PDFTextStripper();
      String text = pdfReader.getText(document);
      List<String> lines = Arrays.asList(text.split("\n"));
      return Collections.unmodifiableList(lines);
    } catch (Exception e) {
      throw new CoreRtException("Unable to parse " + file.getName(), e);
    } finally {
      IOUtils.closeQuietly(document);
    }
  }
}
