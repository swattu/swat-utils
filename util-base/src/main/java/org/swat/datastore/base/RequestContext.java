/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.datastore.base;

/**
 * The placeholder for providing Tenant Id.
 * This class is not deliverable, rather the consumer should create a new class with same package and name.
 *
 * @author Swatantra Agrawal on 18-Mar-2018
 */
public class RequestContext {
  /**
   * Gets tenant id.
   *
   * @return the tenant id
   */
  public static Object getTenantId() {
    return null;
  }

  /**
   * Gets user id.
   *
   * @return the user id
   */
  public static Object getUserId() {
    return null;
  }
}
