/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.spring.mvc.base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The interface Auths.
 *
 * @author Swatantra Agrawal on 26-Sep-2018 06:35 AM
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Auths {
  /**
   * Auths auth type [ ].
   *
   * @return the auth type [ ]
   */
  AuthType[] auths() default {};
}
