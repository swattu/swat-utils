/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.spring.mvc.base;

import org.springframework.web.servlet.HandlerInterceptor;

/**
 * The enum Auth type.
 *
 * @author Swatantra Agrawal on 26-Sep-2018 06:35 AM
 */
public enum AuthType {
  /**
   * Guest user auth type.
   */
  GUEST_USER();

  private final Class<? extends HandlerInterceptor>[] interceptors;

  AuthType(Class<? extends HandlerInterceptor>... interceptors) {
    this.interceptors = interceptors;
  }

  /**
   * Get interceptors class [ ].
   * <p>
   * CAUTION: This must return HandlerInterceptors of type org.swat.spring.mvc.interceptor.BaseInterceptor
   * (Not provided here due to circular dependency)
   *
   * @return the class [ ]
   */
  public Class<? extends HandlerInterceptor>[] getInterceptors() {
    return interceptors;
  }
}
