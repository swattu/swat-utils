/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.jpa.base;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * The listener for {@link javax.persistence.EntityManagerFactory}
 */
public class EntityManagerFactoryListener {
  /**
   * Called after {@link EntityManagerFactory#createEntityManager}'s are called.
   *
   * @param entityManager The created Entity Manager
   */
  public static void afterCreateEntityManager(EntityManager entityManager) {
  }
}
