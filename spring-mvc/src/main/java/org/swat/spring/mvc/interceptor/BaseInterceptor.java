/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.spring.mvc.interceptor;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.swat.spring.mvc.base.AuthType;
import org.swat.spring.mvc.base.Auths;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Base Interceptor to enable/disable interceptors based on @{@link Auths} annotation
 *
 * @author Swatantra Agrawal on 26-Sep-2018 06:35 AM
 */
public class BaseInterceptor implements HandlerInterceptor {
  private static final Map<String, MethodInfo> interceptorMap = new ConcurrentHashMap<>(10);

  @Override
  public final boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
      Object object) throws Exception {
    if (object instanceof HandlerMethod) {
      HandlerMethod handlerMethod = (HandlerMethod) object;
      MethodInfo methodInfo = getInterceptorClasses(handlerMethod);
      if (methodInfo != null && methodInfo.contains(getClass())) {
        return doPreHandle(httpServletRequest, httpServletResponse, object, methodInfo.getAuths());
      }
    }
    return true;
  }

  @Override
  public final void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
      Object object, ModelAndView modelAndView) {
    if (object instanceof HandlerMethod) {
      HandlerMethod handlerMethod = (HandlerMethod) object;
      MethodInfo methodInfo = interceptorMap.get(getKey(handlerMethod));
      if (methodInfo != null && methodInfo.contains(getClass())) {
        doPostHandle(httpServletRequest, httpServletResponse, object, modelAndView, methodInfo.getAuths());
      }
    }
  }

  @Override
  public final void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
      Object object, Exception e) throws Exception {
    if (object instanceof HandlerMethod) {
      HandlerMethod handlerMethod = (HandlerMethod) object;
      MethodInfo methodInfo = interceptorMap.get(getKey(handlerMethod));
      if (methodInfo != null && methodInfo.contains(getClass())) {
        doAfterCompletion(httpServletRequest, httpServletResponse, object, e, methodInfo.getAuths());
      }
    }
  }

  /**
   * Do after completion.
   *
   * @param httpServletRequest  the http servlet request
   * @param httpServletResponse the http servlet response
   * @param object              the object
   * @param e                   the e
   * @param auths               the auths
   * @throws Exception the exception
   */
  public void doAfterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
      Object object, Exception e, Auths auths) throws Exception {
  }

  private String getKey(HandlerMethod handlerMethod) {
    Class beanType = handlerMethod.getBeanType();
    Method method = handlerMethod.getMethod();
    return beanType + " - " + method;
  }

  /**
   * Do post handle.
   *
   * @param httpServletRequest  the http servlet request
   * @param httpServletResponse the http servlet response
   * @param object              the object
   * @param modelAndView        the model and view
   * @param auths               the auths
   */
  public void doPostHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
      Object object, ModelAndView modelAndView, Auths auths) {
  }

  /**
   * Do pre handle boolean.
   *
   * @param httpServletRequest  the http servlet request
   * @param httpServletResponse the http servlet response
   * @param object              the object
   * @param auths               the auths
   * @return the boolean
   * @throws Exception the exception
   */
  public boolean doPreHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
      Object object, Auths auths) throws Exception {
    return true;
  }

  /**
   * Gets interceptor classes.
   *
   * @param handlerMethod the handler method
   * @return the interceptor classes
   */
  protected MethodInfo getInterceptorClasses(HandlerMethod handlerMethod) {
    final String key = getKey(handlerMethod);
    MethodInfo methodInfo = interceptorMap.get(key);
    if (methodInfo == null) {
      Method method = handlerMethod.getMethod();
      Auths auths = method.getAnnotation(Auths.class);
      if (auths == null) {
        auths = getAnnotation(handlerMethod.getBeanType(), Auths.class);
      }
      methodInfo = new MethodInfo(auths);
      addDefaultInterceptors(methodInfo);
      if (auths != null) {
        for (AuthType authType : auths.auths()) {
          methodInfo.addClasses(authType.getInterceptors());
        }
        addConditionalInterceptors(methodInfo, auths);
      }
      interceptorMap.put(key, methodInfo);
    }
    return methodInfo;
  }

  /**
   * Interceptors those are added by default.
   *
   * @param methodInfo the method info
   */
  protected void addDefaultInterceptors(MethodInfo methodInfo) {
  }

  /**
   * Post precess.
   *
   * @param methodInfo the method info
   * @param auths      the auths
   */
  protected void addConditionalInterceptors(MethodInfo methodInfo, Auths auths) {
  }

  private <A extends Annotation> A getAnnotation(final Class clazz, final Class<A> annClazz) {
    if (clazz == null || clazz == Object.class || annClazz == null) {
      return null;
    }
    A a = (A) clazz.getAnnotation(annClazz);
    if (a == null) {
      return getAnnotation(clazz.getSuperclass(), annClazz);
    }
    return a;
  }
}
