/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.spring.mvc.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.swat.spring.mvc.base.Auths;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * The type Method info.
 *
 * @author Swatantra Agrawal on 26-Sep-2018 06:35 AM
 */
public class MethodInfo {
  private final Collection<Class<? extends HandlerInterceptor>> classes = new HashSet<>();
  private final Auths auths;

  /**
   * Instantiates a new Method info.
   *
   * @param auths the auths
   */
  public MethodInfo(Auths auths) {
    this.auths = auths;
  }

  /**
   * Gets auths.
   *
   * @return the auths
   */
  public Auths getAuths() {
    return auths;
  }

  /**
   * Add classes.
   *
   * @param interceptorClasses the interceptor classes
   */
  public void addClasses(Class<? extends HandlerInterceptor>... interceptorClasses) {
    Collections.addAll(classes, interceptorClasses);
  }

  /**
   * Contains boolean.
   *
   * @param clazz the clazz
   * @return the boolean
   */
  public boolean contains(Class<? extends BaseInterceptor> clazz) {
    return classes.contains(clazz);
  }
}
