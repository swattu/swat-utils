#!/usr/bin/env bash
TAG="swat-utils-"
# Delete Remote tag
git push --delete origin ${TAG}

#Delete Local tag
git tag --delete ${TAG}
