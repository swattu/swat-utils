/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.excel.utils;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.swat.core.utils.CoreRtException;

import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class NamedSectionSheetTest {
  @Test
  public void verifySection() throws Exception {
    InputStream stream = getClass().getClassLoader().getResourceAsStream("SectionTest.xlsx");
    Workbook workbook = new XSSFWorkbook(stream);
    NamedSectionSheet sectionSheet = new NamedSectionSheet(workbook.getSheet("Test"));
    NamedSectionSheet.Section section = sectionSheet.getSection(0);

    NamedSectionSheet.Section.NamedRow row = section.getRow(0);
    assertEquals("Swat", row.asString("Name"));
    assertEquals("Raipur", row.asString("Location"));

    row = section.getRow(1);
    assertEquals("Kshitij", row.asString("Name"));
    assertEquals("Hyderabad", row.asString("Location"));

    row = section.getRow(-1);
    assertNull(row);

    row = section.getRow(2);
    assertNull(row);

    section = sectionSheet.getSection(1);

    row = section.getRow(0);
    assertEquals("Sandeep", row.asString("Name"));
    assertEquals("Mumbai", row.asString("Location"));
  }

  @Test
  public void verifyNamedSection() throws Exception {
    InputStream stream = getClass().getClassLoader().getResourceAsStream("NamedSectionTest.xlsx");
    Workbook workbook = new XSSFWorkbook(stream);
    NamedSectionSheet sectionSheet = new NamedSectionSheet(workbook.getSheet("Test"), true);
    NamedSectionSheet.Section section = sectionSheet.getSection("Hyderabad");

    NamedSectionSheet.Section.NamedRow row = section.getRow(0);
    assertEquals("Swat", row.asString("Name"));
    assertEquals("Raipur", row.asString("Location"));

    row = section.getRow(1);
    assertEquals("Kshitij", row.asString("Name"));
    assertEquals("Hyderabad", row.asString("Location"));

    row = section.getRow(-1);
    assertNull(row);

    row = section.getRow(2);
    assertNull(row);

    section = sectionSheet.getSection("Raipur");

    row = section.getRow(0);
    assertEquals("Sandeep", row.asString("Name"));
    assertEquals("Mumbai", row.asString("Location"));
  }


  @Test(expected = CoreRtException.class)
  public void duplicateNamedSection() throws Exception {
    InputStream stream = getClass().getClassLoader().getResourceAsStream("DuplicayeNamedSectionTest.xlsx");
    Workbook workbook = new XSSFWorkbook(stream);
    new NamedSectionSheet(workbook.getSheet("Test"), true);
  }
}
