/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.excel.utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * The type Excel utils.
 */
public class ExcelUtils {
  /**
   * Current row row.
   *
   * @param sheet the sheet
   * @return the row
   */
  public static Row currentRow(Sheet sheet) {
    int lastRowNum = sheet.getLastRowNum();
    Row row = sheet.getRow(lastRowNum);
    if (row == null) {
      row = sheet.createRow(lastRowNum);
    }
    return row;
  }

  /**
   * New row row.
   *
   * @param sheet the sheet
   * @return the row
   */
  public static Row newRow(Sheet sheet) {
    int lastRowNum = sheet.getLastRowNum();
    if (lastRowNum == 0) {
      Row row = sheet.getRow(lastRowNum);
      if (row == null) {
        return sheet.createRow(0);
      }
    }
    return sheet.createRow(lastRowNum + 1);
  }

  /**
   * Current cell cell.
   *
   * @param row the row
   * @return the cell
   */
  public static Cell currentCell(Row row) {
    int lastCellNum = row.getLastCellNum();
    if (lastCellNum == -1) {
      return row.createCell(0);
    }
    Cell cell = row.getCell(lastCellNum - 1);
    if (cell == null) {
      cell = row.createCell(lastCellNum - 1);
    }
    return cell;
  }

  /**
   * New cell cell.
   *
   * @param row the row
   * @return the cell
   */
  public static Cell newCell(Row row) {
    int lastCellNum = row.getLastCellNum();
    if (lastCellNum == -1) {
      lastCellNum = 0;
    }
    return row.createCell(lastCellNum);
  }

  /**
   * Auto size columns.
   *
   * @param workbook the workbook
   */
  public static void autoSizeColumns(Workbook workbook) {
    int numberOfSheets = workbook.getNumberOfSheets();
    for (int i = 0; i < numberOfSheets; i++) {
      Sheet sheet = workbook.getSheetAt(i);
      autoSizeColumns(sheet);
    }
  }

  /**
   * Auto size columns.
   *
   * @param sheet the sheet
   */
  public static void autoSizeColumns(Sheet sheet) {
    for (int index = 0; index <= sheet.getPhysicalNumberOfRows(); index++) {
      sheet.autoSizeColumn(index);
    }
  }
}
