/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.excel.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.swat.core.utils.CoreRtException;
import org.swat.json.utils.JsonConstants;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

/**
 * A wrapper for a Sheet with multiple Sections. Each section containing a Header Row
 */
public class NamedSectionSheet {
  private final Sheet sheet;
  private final boolean namedSection;
  private final List<Section> sections = new ArrayList<>();

  /**
   * Instantiates a new Named section sheet.
   *
   * @param sheet the sheet
   */
  public NamedSectionSheet(Sheet sheet) {
    this(sheet, false);
  }

  /**
   * Instantiates a new Named section sheet.
   *
   * @param sheet        the sheet
   * @param namedSection the named section
   */
  public NamedSectionSheet(Sheet sheet, boolean namedSection) {
    this.sheet = sheet;
    this.namedSection = namedSection;
    populateSections();
  }

  private void populateSections() {
    int start = -1;
    String name = null;
    for (int index = sheet.getFirstRowNum(); index <= sheet.getLastRowNum() + 1; index++) {
      if (start < 0 && !isEmptyRow(index)) {
        start = index;
        if (namedSection) {
          Row row = sheet.getRow(start);
          Cell cell = row.getCell(row.getFirstCellNum());
          if (cell != null && cell.getCellType() == CellType.STRING) {
            name = cell.getStringCellValue();
            for (Section section : sections) {
              if (StringUtils.equals(name, section.getName())) {
                throw new CoreRtException("Duplicate section name:" + name);
              }
            }
            start++;
          }
        }
      }
      if (start >= 0 && isEmptyRow(index)) {
        sections.add(new Section(name, start, index - 1));
        start = -1;
      }
    }
  }

  /**
   * The Section iterator
   *
   * @return the iterator
   */
  public Iterator<Section> iterator() {
    return new Iterator<Section>() {
      private int index = -1;

      @Override
      public boolean hasNext() {
        return index < sectionCount() - 1;
      }

      @Override
      public Section next() {
        if (hasNext()) {
          index++;
          return sections.get(index);
        }
        throw new NoSuchElementException((index + 1) + " does not exist.");
      }
    };
  }

  /**
   * Count of Sections
   *
   * @return the count
   */
  public int sectionCount() {
    return sections.size();
  }

  /**
   * Gets section.
   *
   * @param index the index
   * @return the section
   */
  public Section getSection(int index) {
    if (index >= 0 && index < sectionCount()) {
      return sections.get(index);
    }
    return null;
  }

  /**
   * Gets section.
   *
   * @param name the name
   * @return the section
   */
  public Section getSection(String name) {
    if (!namedSection) {
      throw new CoreRtException("The sections are not named");
    }
    for (Section section : sections) {
      if (StringUtils.equals(name, section.getName())) {
        return section;
      }
    }
    return null;
  }

  private boolean isEmptyRow(int index) {
    Row row = sheet.getRow(index);
    if (row == null) {
      return true;
    }
    return row.getFirstCellNum() == row.getLastCellNum();
  }

  /**
   * The type Section.
   */
  public class Section {
    private final String name;
    private final int start;
    private final int end;
    private final Map<String, Integer> nameIndexMap = new LinkedHashMap<>();

    /**
     * Instantiates a new Section.
     *
     * @param start the start
     * @param end   the end
     */
    private Section(String name, int start, int end) {
      this.name = name;
      this.start = start;
      this.end = end;
      readHeader();
    }

    /**
     * Add column if absent.
     *
     * @param name the name
     */
    public void addColumnIfAbsent(String name) {
      Integer index = nameIndexMap.get(name);
      if (index == null) {
        Row row = sheet.getRow(start);
        short idx = row.getLastCellNum();
        Cell cell = row.createCell(idx);
        cell.setCellValue(name);
        nameIndexMap.put(cell.getStringCellValue(), (int) idx);
      }
    }

    /**
     * Get the row at specified index
     *
     * @param index The index
     * @return The Row
     */
    public NamedRow getRow(int index) {
      if (index < 0 || index > end - start) {
        return null;
      }
      Iterator<NamedRow> iterator = iterator();
      while (iterator.hasNext()) {
        index--;
        NamedRow row = iterator.next();
        if (index < 0) {
          return row;
        }
      }
      return null;
    }

    /**
     * Gets headers.
     *
     * @return the headers
     */
    public Set<String> getHeaders() {
      return new LinkedHashSet<>(nameIndexMap.keySet());
    }

    /**
     * Gets start.
     *
     * @return the start
     */
    public int getStart() {
      return start;
    }

    /**
     * Gets end.
     *
     * @return the end
     */
    public int getEnd() {
      return end;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
      return name;
    }

    /**
     * Iterator iterator.
     *
     * @return the iterator
     */
    public Iterator<NamedRow> iterator() {
      return new Iterator<NamedRow>() {
        private int index = start;

        @Override
        public boolean hasNext() {
          return index < end;
        }

        @Override
        public NamedRow next() {
          if (hasNext()) {
            index++;
            return new NamedRow(sheet.getRow(index));
          }
          throw new NoSuchElementException((index + 1) + " does not exist.");
        }
      };
    }

    @Override
    public String toString() {
      return "Section{" + "start=" + start + ", end=" + end + '}';
    }

    private void readHeader() {
      Row row = sheet.getRow(start);
      nameIndexMap.clear();
      for (int index = row.getFirstCellNum(); index < row.getLastCellNum(); index++) {
        Cell cell = row.getCell(index);
        if (cell != null && cell.getCellType() == CellType.STRING) {
          nameIndexMap.put(cell.getStringCellValue(), index);
        }
      }
    }

    private int getIndex(String name) {
      Integer index = nameIndexMap.get(name);
      if (index == null) {
        return -1;
      }
      return index;
    }

    /**
     * Names row, where cell value can be fetched using Header Names.
     */
    public class NamedRow {
      private final Row row;

      /**
       * Instantiates a new Named row.
       *
       * @param row the row
       */
      public NamedRow(Row row) {
        this.row = row;
      }

      /**
       * As string string.
       *
       * @param name the name
       * @return the string
       */
      public String asString(String name) {
        int index = getIndex(name);
        return (String) getValue(name, index, "STRING");
      }

      /**
       * As long long.
       *
       * @param name the name
       * @return the long
       */
      public long asLong(String name) {
        return asNumber(name).longValue();
      }

      /**
       * As int int.
       *
       * @param name the name
       * @return the int
       */
      public int asInt(String name) {
        return asNumber(name).intValue();
      }

      /**
       * As double double.
       *
       * @param name the name
       * @return the double
       */
      public double asDouble(String name) {
        return asNumber(name).doubleValue();
      }

      private Number asNumber(String name) {
        return asNumber(name, 0);
      }

      private Number asNumber(String name, Number onError) {
        int index = getIndex(name);
        Number value = (Number) getValue(name, index, "NUMBER");
        if (value == null) {
          return onError;
        }
        return value;
      }

      /**
       * As boolean boolean.
       *
       * @param name the name
       * @return the boolean
       */
      public boolean asBoolean(String name) {
        String value = asString(name);
        if (value == null) {
          return false;
        }
        return StringUtils.equalsIgnoreCase(value, "true") || StringUtils.equalsIgnoreCase(value, "yes");
      }

      /**
       * As object object.
       *
       * @param name the name
       * @return the object
       */
      public Object asObject(String name) {
        int index = getIndex(name);
        if (index >= 0) {
          Cell cell = row.getCell(index);
          if (cell == null) {
            return null;
          }
          CellType type = cell.getCellType();
          try {
            switch (type) {
              case NUMERIC:
                return cell.getNumericCellValue();
              case STRING:
                return cell.getStringCellValue();
              case FORMULA:
                return cell.getCellFormula();
              case BOOLEAN:
                return cell.getBooleanCellValue();
            }
          } catch (Exception e) {
            String error = String
                .format("Row:%d, Cell:%d, Name:%s, Type:%s - %s", row.getRowNum(), index, name, type, e.getMessage());
            throw new CoreRtException(error, e);
          }
        }
        return null;
      }

      /**
       * As date date.
       *
       * @param name the name
       * @return the date in UTC
       */
      public Date asDate(String name) {
        Number number = asNumber(name, null);
        if (number == null) {
          return null;
        }
        return DateUtil.getJavaDate(number.doubleValue(), false, TimeZone.getTimeZone("UTC"));
      }

      /**
       * As local date local date.
       *
       * @param name the name
       * @return the local date in UTC
       */
      public LocalDate asLocalDate(String name) {
        Date date = asDate(name);
        if (date == null) {
          return null;
        }

        return date.toInstant().atZone(ZoneId.of("UTC")).toLocalDate();
      }

      /**
       * Gets cell.
       *
       * @param name the name
       * @return the cell
       */
      public Cell getCell(String name) {
        return getCell(name, false);
      }

      /**
       * Gets cell.
       *
       * @param name         the name
       * @param createIfNull the create if null
       * @return the cell
       */
      public Cell getCell(String name, boolean createIfNull) {
        Integer index = nameIndexMap.get(name);
        if (index == null) {
          return null;
        }
        Cell cell = row.getCell(index);
        if (createIfNull) {
          cell = row.createCell(index);
        }
        return cell;
      }

      private Object getValue(String name, int index, String type) {
        if (index < 0 || StringUtils.isBlank(type)) {
          return null;
        }
        Cell cell = row.getCell(index);
        if (cell == null) {
          return null;
        }
        try {
          switch (type) {
            case "STRING":
              return cell.getStringCellValue();
            case "NUMBER":
              return cell.getNumericCellValue();
          }
          return null;
        } catch (Exception e) {
          String error = String.format("Row:%d, Cell:%d, Name:%s Expected Type:%s, Actual Type:%s - %s", row
              .getRowNum(), index, name, type, cell.getCellType(), e.getMessage());
          throw new CoreRtException(error, e);
        }
      }

      /**
       * Gets row.
       *
       * @return the row
       */
      public Row getRow() {
        return row;
      }

      /**
       * Returns a Map of values
       *
       * @return the map
       */
      public Map<String, Object> toMap() {
        Map<String, Object> map = new LinkedHashMap<>();
        for (String header : getHeaders()) {
          map.put(header, asObject(header));
        }
        return map;
      }

      @Override
      public String toString() {
        return toString(true);
      }

      /**
       * To string string.
       *
       * @param pretty the pretty
       * @return the string
       */
      public String toString(boolean pretty) {
        Map<String, Object> map = toMap();
        if (pretty) {
          return JsonConstants.JSON_UTIL.pretty(map);
        }
        return JsonConstants.JSON_UTIL.toJsonString(map);
      }
    }
  }
}
