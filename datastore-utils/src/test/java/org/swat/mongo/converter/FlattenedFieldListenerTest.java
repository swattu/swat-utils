/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.converter;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.swat.mongo.dao.MongoDbTest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@ComponentScan(basePackages = {"org.swat.lock", "org.swat.mongo.converter"})
@ContextConfiguration(classes = FlattenedFieldListenerTest.class)
@DataMongoTest
@TestPropertySource(properties = "spring.mongodb.embedded.version=3.5.5")
public class FlattenedFieldListenerTest {
  @Autowired
  private MongoOperations mongoOperations;
  private final String collection = "dummy";

  @Test
  public void checkSearch() {
    final FlattenedPojo main = new FlattenedPojo();
    main.setName("Main");
    main.setAge(1);
    main.putCustomField("type", "main");

    FlattenedPojo pojo = new FlattenedPojo();
    pojo.setAge(2);
    pojo.putCustomField("name", "Array");
    pojo.putCustomField("type", "array");
    main.setPojoArray(ArrayUtils.toArray(pojo));

    pojo = new FlattenedPojo();
    pojo.setAge(3);
    pojo.putCustomField("name", "Set");
    pojo.putCustomField("type", "set");
    Set<FlattenedPojo> pojoSet = new HashSet<>();
    pojoSet.add(pojo);
    main.setPojoSet(pojoSet);

    pojo = new FlattenedPojo();
    pojo.setAge(4);
    pojo.putCustomField("name", "List");
    pojo.putCustomField("type", "list");
    main.setPojoList(Arrays.asList(pojo));

    pojo = new FlattenedPojo();
    pojo.setAge(5);
    pojo.putCustomField("name", "Map");
    pojo.putCustomField("type", "map");
    Map<String, FlattenedPojo> pojoMap = new LinkedHashMap<>();
    pojoMap.put("pojo", pojo);
    main.setPojoMap(pojoMap);

    mongoOperations.save(main, collection);

    Map map = mongoOperations.findOne(new Query(), Map.class, collection);
    verifyMainMap(map);

    pojo = mongoOperations.findOne(new Query(), FlattenedPojo.class, collection);
    verifyMainPojo(pojo);
  }

  private void verifyMainPojo(FlattenedPojo pojo) {
    assertNotNull(pojo);
    verify(pojo, "Main", 1, "main");

    assertEquals(1, pojo.getPojoArray().length);
    verify(pojo.getPojoArray()[0], "Array", 2, "array");

    assertEquals(1, pojo.getPojoSet().size());
    //Set is not supported
    verify(pojo.getPojoSet().iterator().next(), null, 3, "set");

    assertEquals(1, pojo.getPojoList().size());
    verify(pojo.getPojoList().get(0), "List", 4, "list");

    assertEquals(1, pojo.getPojoMap().size());
    verify(pojo.getPojoMap().get("pojo"), "Map", 5, "map");
  }

  private void verifyMainMap(Map map) {
    assertNotNull(map);
    verify(map, "Main", 1, "main");

    assertTrue(map.get("pojoArray") instanceof List);
    assertEquals(1, ((List) map.get("pojoArray")).size());
    verify((Map) ((List) map.get("pojoArray")).get(0), "Array", 2, "array");

    assertTrue(map.get("pojoSet") instanceof List);
    assertEquals(1, ((List) map.get("pojoSet")).size());
    //Set is not supported
    verify((Map) ((List) map.get("pojoSet")).get(0), null, 3, null);

    assertTrue(map.get("pojoList") instanceof List);
    assertEquals(1, ((List) map.get("pojoList")).size());
    verify((Map) ((List) map.get("pojoList")).get(0), "List", 4, "list");

    assertTrue(map.get("pojoMap") instanceof Map);
    assertEquals(1, ((Map) map.get("pojoMap")).size());
    assertTrue(((Map) map.get("pojoMap")).get("pojo") instanceof Map);
    verify((Map) ((Map) map.get("pojoMap")).get("pojo"), "Map", 5, "map");
  }

  private void verify(FlattenedPojo pojo, String name, int age, String type) {
    assertEquals("Name", name, pojo.getName());
    assertEquals("Age", age, pojo.getAge());
    assertEquals("Type", type, pojo.customField("type"));
  }

  private void verify(Map map, String name, int age, String type) {
    assertEquals("Name", name, map.get("name"));
    assertEquals("Age", age, map.get("age"));
    assertEquals("Type", type, map.get("type"));
  }
}
