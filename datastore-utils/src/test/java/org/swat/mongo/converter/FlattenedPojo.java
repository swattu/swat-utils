/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.converter;

import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
public class FlattenedPojo extends FlattenedFields {
  private String name;
  private int age;
  private FlattenedPojo[] pojoArray;
  private Set<FlattenedPojo> pojoSet;
  private List<FlattenedPojo> pojoList;
  private Map<String, FlattenedPojo> pojoMap;
}
