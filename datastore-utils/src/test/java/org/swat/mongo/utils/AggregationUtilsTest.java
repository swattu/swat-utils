/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.utils;

import org.bson.types.ObjectId;
import org.junit.Test;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.query.Criteria;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.swat.mongo.utils.AggregationUtils.mongoScript;

public class AggregationUtilsTest {
  @Test
  public void isoDate() throws ParseException {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    Date date = sdf.parse("2019-01-01");
    List<AggregationOperation> operations = new ArrayList<>();
    AggregationOperation dateOperation = match(Criteria.where("date").is(date));
    operations.add(dateOperation);
    assertEquals("[{\"$match\":{\"date\":ISODate(\"2019-01-01T00:00:00Z\")}}]", mongoScript(operations));
  }

  @Test
  public void localDate() throws ParseException {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    LocalDate date = LocalDate.of(2019, 1, 1);
    List<AggregationOperation> operations = new ArrayList<>();
    AggregationOperation dateOperation = match(Criteria.where("date").is(date));
    operations.add(dateOperation);
    assertEquals("[{\"$match\":{\"date\":ISODate(\"2019-01-01T00:00:00.000Z\")}}]", mongoScript(operations));
  }

  @Test
  public void objectId() {
    List<AggregationOperation> operations = new ArrayList<>();
    AggregationOperation oidOperation = match(Criteria.where("_id").is(new ObjectId("5de868347d3b452de91ba439")));
    operations.add(oidOperation);
    assertEquals("[{\"$match\":{\"_id\":ObjectId(\"5de868347d3b452de91ba439\")}}]", mongoScript(operations));
  }

  @Test
  public void enumUse() {
    List<AggregationOperation> operations = new ArrayList<>();
    AggregationOperation oidOperation = match(Criteria.where("type").is(Status.N_A));
    operations.add(oidOperation);
    assertEquals("[{\"$match\":{\"type\":\"N_A\"}}]", mongoScript(operations));
  }
}
