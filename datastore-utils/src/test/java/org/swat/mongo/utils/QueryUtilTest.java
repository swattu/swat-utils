/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.utils;

import org.junit.Test;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.swat.core.utils.CoreRtException;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class QueryUtilTest {

  @Test
  public void checkQuery() {
    Query requestedQuery = Query.query(Criteria.where("tenantId").is("A")).skip(10).limit(5);
    requestedQuery.fields().include("abc").exclude("xyz");
    String json = QueryUtil.json(requestedQuery);
    json = json.replaceAll("\"", "'");
    assertEquals("{'tenantId': 'A'}", json);

    Query allowedQuery = Query.query(Criteria.where("tenantId").in("A", "B"));
    json = QueryUtil.json(allowedQuery);
    json = json.replaceAll("\"", "'");
    assertEquals("{'tenantId': {'$in': ['A', 'B']}}", json);

    Query finalQuery = QueryUtil.and(requestedQuery, allowedQuery);
    json = QueryUtil.json(finalQuery);
    json = json.replaceAll("\"", "'");
    assertEquals("{'$and': [{'tenantId': 'A'}, {'tenantId': {'$in': ['A', 'B']}}]}", json);
    assertEquals(10, finalQuery.getSkip());
    assertEquals(5, finalQuery.getLimit());
    assertEquals(requestedQuery.getFieldsObject(), finalQuery.getFieldsObject());

    finalQuery = QueryUtil.or(requestedQuery, allowedQuery);
    json = QueryUtil.json(finalQuery);
    json = json.replaceAll("\"", "'");
    assertEquals("{'$or': [{'tenantId': 'A'}, {'tenantId': {'$in': ['A', 'B']}}]}", json);
    assertEquals(10, finalQuery.getSkip());
    assertEquals(5, finalQuery.getLimit());
    assertEquals(requestedQuery.getFieldsObject(), finalQuery.getFieldsObject());
  }

  @Test
  public void checkSort() {
    Sort sort = QueryUtil.getSort("name", null);
    assertEquals(Sort.by(Sort.Direction.ASC, "name"), sort);

    sort = QueryUtil.getSort("name,age", null);
    assertEquals(Sort.by(Sort.Direction.ASC, "name", "age"), sort);

    sort = QueryUtil.getSort("name,-age", null);
    assertEquals(Sort.by(Sort.Direction.ASC, "name").and(Sort.by(Sort.Direction.DESC, "age")), sort);

    sort = QueryUtil.getSort("name", "DESC");
    assertEquals(Sort.by(Sort.Direction.DESC, "name"), sort);

    sort = QueryUtil.getSort("name,age", "DESC");
    assertEquals(Sort.by(Sort.Direction.DESC, "name", "age"), sort);

    sort = QueryUtil.getSort("name, - age", "DESC");
    assertEquals(Sort.by(Sort.Direction.DESC, "name", "age"), sort);
  }

  @Test
  public void checkInClauseNonId() {
    Query query = Query.query(Criteria.where("tenantId").is("a"));

    String[] values = null;
    String key = "tenantId";

    Query actual = QueryUtil.inClause(query, key, values);
    String expected = "{'tenantId': 'a'}";
    String json = QueryUtil.json(actual);
    json = json.replaceAll("\"", "'");
    assertEquals(expected, json);

    values = new String[]{"a", "b"};
    actual = QueryUtil.inClause(query, key, values);
    expected = "{'tenantId': 'a'}";
    json = QueryUtil.json(actual);
    json = json.replaceAll("\"", "'");
    assertEquals(expected, json);
  }

  @Test
  public void checkInClauseId() {
    Query query = Query.query(Criteria.where("id").is("a"));

    String[] values = null;
    String key = "_id";

    Query actual = QueryUtil.inClause(query, key, values);
    String expected = "{'id': 'a'}";
    String json = QueryUtil.json(actual);
    json = json.replaceAll("\"", "'");
    assertEquals(expected, json);

    values = new String[]{"a", "b"};
    actual = QueryUtil.inClause(query, key, values);
    expected = "{'id': 'a'}";
    json = QueryUtil.json(actual);
    json = json.replaceAll("\"", "'");
    assertEquals(expected, json);
  }

  @Test
  public void checkInClauseArray() {
    Query query = Query.query(Criteria.where("id").in("a", "b"));

    String[] values = new String[]{"a", "c"};
    String key = "id";
    Query actual = QueryUtil.inClause(query, key, values);
    String expected =
        "{'$and': [{'id': {'$in': ['a', 'b']}}, {'id': {'$in': ['a', 'c']}}]}";
    String json = QueryUtil.json(actual);
    json = json.replaceAll("\"", "'");
    assertEquals(expected, json);
  }

  @Test(expected = CoreRtException.class)
  public void checkInClauseException() {
    final Query query = Query.query(Criteria.where("tenantId").is("a"));

    String key = "tenantId";

    String[] values = new String[]{"abc"};
    QueryUtil.inClause(query, key, values);
  }

  @Test
  public void addCriteria() {
    verifyQuery("Swat");
    verifyQuery(1);
    verifyQuery(true);
    verifyQuery(new Date());
  }

  private void verifyQuery(Object value) {
    Query query = Query.query(Criteria.where("key").is(value));
    QueryUtil.addCriteria(query, "key", value);
  }

  @Test
  public void search() {
    Criteria criteria = QueryUtil.searchCriteria("Swat", "name", "address");
    String json = QueryUtil.json(Query.query(criteria));
    json = json.replaceAll("\"", "'");
    assertEquals("{'$or': [{'name': {'$regularExpression': {'pattern': 'Swat', 'options': 'i'}}}, {'address': {'$regularExpression': {'pattern': 'Swat', 'options': 'i'}}}]}", json);

    criteria = QueryUtil.searchCriteria("123", "name", "address");
    json = QueryUtil.json(Query.query(criteria));
    json = json.replaceAll("\"", "'");
    assertEquals("{'$or': [{'$where': '/.*123.*/.test(this.name)'}, {'$where': '/.*123.*/.test(this.address)'}]}", json);
  }
}
