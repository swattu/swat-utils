/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.swat.mongo.utils.QueryUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@ComponentScan(basePackages = {"org.swat.lock"})
@ContextConfiguration(classes = MongoDbTest.class)
@DataMongoTest
@TestPropertySource(properties = "spring.mongodb.embedded.version=3.5.5")
public class MongoDbTest {
  @Autowired
  private MongoOperations mongoOperations;
  private final String collection = "dummy";

  @Test
  public void checkSearch() {
    add("Swat", "Raipur", 20000, 1001000.1501D);
    add("Sattu", "Bhilai", 15023, 200000D);
    add("Sonu", "Bhilai2000", 3456, 300000D);
    List<Map> records = getRecords("at");
    assertEquals(2, records.size());
    assertEquals("Sattu", records.get(0).get("name"));
    assertEquals("Swat", records.get(1).get("name"));

    records = getRecords("SWAT");
    assertEquals(1, records.size());
    assertEquals("Swat", records.get(0).get("name"));

    records = getRecords("150");
    assertEquals(2, records.size());
    assertEquals("Sattu", records.get(0).get("name"));
    assertEquals("Swat", records.get(1).get("name"));

    records = getRecords("2000");
    assertEquals(3, records.size());
    assertEquals("Sattu", records.get(0).get("name"));
    assertEquals("Sonu", records.get(1).get("name"));
    assertEquals("Swat", records.get(2).get("name"));
  }

  private List<Map> getRecords(String text) {
    Criteria criteria = QueryUtil.searchCriteria(text, "name", "address", "salary", "worth");
    return mongoOperations.find(Query.query(criteria).with(Sort.by(Sort.Direction.ASC, "name")), Map.class, collection);
  }

  private void add(String name, String address, int salary, double worth) {
    Map<String, Object> map = new HashMap<>();
    map.put("name", name);
    map.put("address", address);
    map.put("salary", salary);
    map.put("worth", worth);
    mongoOperations.save(map, collection);
  }
}
