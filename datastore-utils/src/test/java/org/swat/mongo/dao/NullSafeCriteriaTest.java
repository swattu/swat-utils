/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import org.bson.Document;
import org.junit.Test;
import org.springframework.data.mongodb.core.query.Criteria;

import static org.junit.Assert.assertEquals;

public class NullSafeCriteriaTest {

  @Test
  public void checkAnd() {
    NullSafeCriteria utils = new NullSafeCriteria(null);
    utils.and("key").is(null);
    Criteria criteria = utils.getCriteria();
    Document dbObject = criteria.getCriteriaObject();
    Document expected = new Criteria().getCriteriaObject();
    assertEquals(expected, dbObject);

    utils.and("key").is("value");
    criteria = utils.getCriteria();
    dbObject = criteria.getCriteriaObject();
    expected = Criteria.where("key").is("value").getCriteriaObject();
    assertEquals(expected, dbObject);

    utils.and("key1").is("value");
    criteria = utils.getCriteria();
    dbObject = criteria.getCriteriaObject();
    expected = Criteria.where("key").is("value").and("key1").is("value").getCriteriaObject();
    assertEquals(expected, dbObject);
    assertEquals(2, utils.getCriteriaCount());
  }

  @Test
  public void checkMulti() {
    NullSafeCriteria utils = new NullSafeCriteria(null);
    utils.and("key").lt(12).gt(15);
    Criteria criteria = utils.getCriteria();
    Document dbObject = criteria.getCriteriaObject();
    Document expected = Criteria.where("key").gt(15).lt(12).getCriteriaObject();
    assertEquals(expected, dbObject);
    assertEquals(1, utils.getCriteriaCount());

    utils = new NullSafeCriteria(null);
    utils.and("key").lt(null).gt(15);
    criteria = utils.getCriteria();
    dbObject = criteria.getCriteriaObject();
    expected = Criteria.where("key").gt(15).getCriteriaObject();
    assertEquals(expected, dbObject);
    assertEquals(1, utils.getCriteriaCount());

    utils = new NullSafeCriteria(null);
    utils.and("key").lt(null).gt(null);
    criteria = utils.getCriteria();
    dbObject = criteria.getCriteriaObject();
    expected = new Criteria().getCriteriaObject();
    assertEquals(expected, dbObject);
    assertEquals(0, utils.getCriteriaCount());
  }
}
