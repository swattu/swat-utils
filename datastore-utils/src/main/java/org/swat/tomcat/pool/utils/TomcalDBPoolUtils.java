/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.tomcat.pool.utils;

import org.apache.tomcat.jdbc.pool.DataSourceConstants;
import org.apache.tomcat.jdbc.pool.DataSourceFactory;
import org.apache.tomcat.jdbc.pool.PoolConfiguration;
import org.swat.core.utils.PropertyUtils;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Utilities for Tomcat Connection Pool.
 * This class needs following dependencies
 * <BR>
 * org.apache.tomcat:tomcat-jdbc:8.5.20
 * <BR>
 *
 * @author Swatantra Agrawal on 20-SEP-2017
 */
public class TomcalDBPoolUtils {
  /**
   * The Configurable properties of Datasource
   */
  private final static String[] KEYS = DataSourceConstants.getAllProperties();

  /**
   * Parses the given properties and creates Properties for Data Source
   *
   * @param properties the properties
   * @param prefixes   the prefixes
   * @return The Properties
   */
  public static PoolConfiguration getPoolProperties(Properties properties, String... prefixes) {
    return DataSourceFactory.parsePoolProperties(PropertyUtils.getProperties(properties, KEYS, prefixes));
  }

  /**
   * Creates and configures a {@link DataSource} instance based on the
   * given properties.
   *
   * @param properties the datasource configuration properties
   * @param prefixes   The prefixes
   * @return the datasource
   * @throws Exception if an error occurs creating the data source
   */
  public DataSource createDataSource(Properties properties, String... prefixes) throws Exception {
    properties = PropertyUtils.getProperties(properties, KEYS, prefixes);
    return new DataSourceFactory().createDataSource(properties);
  }
}
