/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.converter;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;

import java.lang.reflect.Array;
import java.util.List;
import java.util.Map;

import static org.swat.core.utils.ObjectUtil.cast;

/**
 * The listener to flatten the custom fields.
 * CAUTION:
 * This does not honour {@link org.springframework.data.mongodb.core.mapping.Field}.
 * This does not work with {@link FlattenedFieldAware} inside customFields.
 */
public class FlattenedFieldListener extends AbstractMongoEventListener<FlattenedFieldAware> {
  /**
   * Flattens custom fields
   *
   * @param event will never be {@literal null}.
   */
  @Override
  public void onBeforeSave(BeforeSaveEvent<FlattenedFieldAware> event) {
    Document document = event.getDocument();
    FlattenedFieldAware source = event.getSource();
    onBeforeObject("", source, document);
  }

  private void onBeforeObject(String fieldName, Object bean, Object document) {
    if (bean == null || bean.getClass().isPrimitive() || bean.getClass().isEnum()) {
      return;
    }
    if (document instanceof Map) {
      onBeforeMap(fieldName, bean, (Map) document);
    }
    if (document instanceof List) {
      onBeforeList(fieldName, bean, (List) document);
    }
  }

  private void onBeforeList(String fieldName, Object bean, List dbList) {
    if (bean.getClass().isArray()) {
      int length = Array.getLength(bean);
      for (int index = 0; index < length; index++) {
        onBeforeObject(fieldName + "[]", Array.get(bean, index), dbList.get(index));
      }
    }
    if (bean instanceof List) {
      int length = ((List) bean).size();
      for (int index = 0; index < length; index++) {
        onBeforeObject(fieldName + "[]", ((List) bean).get(index), dbList.get(index));
      }
    }
  }

  private void onBeforeMap(String fieldName, Object bean, Map document) {
    if (bean instanceof FlattenedFieldAware && document instanceof Map) {
      for (Map.Entry<String, Object> entry : ((FlattenedFieldAware) bean).customFields().entrySet()) {
        document.putIfAbsent(entry.getKey(), entry.getValue());
      }
      document.remove("customFields");
    }
    if (bean instanceof Map) {
      Map<String, Object> map = cast(bean);
      for (Map.Entry<String, Object> entry : map.entrySet()) {
        onBeforeObject(fieldName + "." + entry.getKey(), entry.getValue(), document.get(entry.getKey()));
      }
    } else {
      try {
        Map<String, Object> properties = PropertyUtils.describe(bean);
        for (Map.Entry<String, Object> entry : properties.entrySet()) {
          onBeforeObject(fieldName + "." + entry.getKey(), entry.getValue(), document.get(entry.getKey()));
        }
      } catch (Exception e) {
      }
    }
  }

  /**
   * Added the flattened data to custom fields.
   *
   * @param event will never be {@literal null}.
   */
  @Override
  public void onAfterConvert(AfterConvertEvent<FlattenedFieldAware> event) {
    onAfterObject("", event.getSource(), event.getDocument());
  }

  private void onAfterObject(String fieldName, Object bean, Object document) {
    if (bean == null || bean.getClass().isPrimitive() || bean.getClass().isEnum()) {
      return;
    }
    if (document instanceof Map) {
      onAfterMap(fieldName, bean, (Map) document);
    }
    if (document instanceof List) {
      onAfterList(fieldName, bean, (List) document);
    }
  }

  private void onAfterList(String fieldName, Object bean, List dbList) {
    if (bean.getClass().isArray()) {
      int length = Array.getLength(bean);
      for (int index = 0; index < length; index++) {
        onAfterObject(fieldName + "[]", Array.get(bean, index), dbList.get(index));
      }
    }
    if (bean instanceof List) {
      int length = ((List) bean).size();
      for (int index = 0; index < length; index++) {
        onAfterObject(fieldName + "[]", ((List) bean).get(index), dbList.get(index));
      }
    }
  }

  private void onAfterMap(String fieldName, Object bean, Map<String, Object> document) {
    try {
      Map<String, Object> properties;
      if (bean instanceof Map) {
        properties = cast(bean);
      } else {
        properties = PropertyUtils.describe(bean);
      }
      for (Map.Entry<String, Object> entry : document.entrySet()) {
        if (bean instanceof FlattenedFieldAware) {
          if (!properties.containsKey(entry.getKey()) && !StringUtils.containsAny(entry.getKey(), "_id", "class")) {
            ((FlattenedFieldAware) bean).putCustomField(entry.getKey(), entry.getValue());
          }
        }
        onAfterObject(fieldName + "." + entry.getKey(), properties.get(entry.getKey()), entry.getValue());
      }
    } catch (Exception e) {
    }
  }
}
