/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.converter;

import org.swat.json.utils.CustomFieldAware;

import java.util.Map;

/**
 * The interface Flattened custom field.
 */
public interface FlattenedFieldAware extends CustomFieldAware {
  /**
   * Gets custom fields.
   *
   * @return the custom fields
   */
  Map<String, Object> getCustomFields();
}
