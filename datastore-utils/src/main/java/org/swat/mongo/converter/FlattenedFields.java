/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.converter;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The type Flattened custom fields.
 */
public class FlattenedFields implements FlattenedFieldAware {
  private Map<String, Object> customFields;

  @Override
  public Map<String, Object> getCustomFields() {
    return customFields;
  }

  @Override
  public Map<String, Object> customFields() {
    if (customFields == null) {
      customFields = new LinkedHashMap<>();
    }
    return customFields;
  }
}
