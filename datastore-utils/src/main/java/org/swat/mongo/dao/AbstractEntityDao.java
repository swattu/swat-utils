/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import com.mongodb.annotations.Beta;
import com.mongodb.client.model.IndexOptions;
import org.bson.conversions.Bson;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Collection;
import java.util.List;

/**
 * The interface Abstract entity dao.
 *
 * @param <T> the type parameter
 * @author Swatantra Agrawal on 18-Mar-2018
 */
public interface AbstractEntityDao<T extends AbstractEntity> {
  /**
   * Find by id t.
   *
   * @param id           the id
   * @param updateViewed the update viewed
   * @return the t
   */
  T findById(String id, boolean updateViewed);

  /**
   * Find one t.
   *
   * @param query the query
   * @return the t
   */
  T findOne(Query query);

  /**
   * Find and remove t.
   *
   * @param query the query
   * @return the t
   */
  T findAndRemove(Query query);

  /**
   * Remove all int.
   *
   * @param query the query
   * @return the long
   */
  long removeAll(Query query);

  /**
   * Gets collection name.
   *
   * @return the collection name
   */
  String getCollectionName();

  /**
   * Directly queries and updates, without modifying {@link Query} and {@link Update} objects.
   *
   * @return the abstract entity dao
   */
  @Beta
  AbstractEntityDao<T> withDirect();

  /**
   * Directly queries and updates, without modifying {@link Query} object.
   *
   * @return the abstract entity dao
   */
  @Beta
  AbstractEntityDao<T> withDirectQuery();

  /**
   * Directly queries and updates, without modifying {@link Update} object.
   *
   * @return the abstract entity dao
   */
  @Beta
  AbstractEntityDao<T> withDirectUpdate();


  /**
   * Get the bulk operations.
   *
   * @param bulkMode the bulk mode
   * @return the bulk operations
   */
  BulkOperations bulkOperations(BulkOperations.BulkMode bulkMode);

  /**
   * Find by id t.
   *
   * @param id the id
   * @return the t
   */
  T findById(String id);

  /**
   * Find one t.
   *
   * @param query  the query
   * @param viewed the viewed
   * @return the t
   */
  T findOne(Query query, boolean viewed);

  /**
   * Insert t.
   *
   * @param entity the entity
   * @return the t
   */
  T insert(T entity);

  /**
   * Insert collection.
   *
   * @param entities the entities
   * @return the collection
   */
  Collection<T> insert(Collection<T> entities);

  /**
   * Find all list.
   *
   * @param viewedUpdate the viewed update
   * @return the list
   */
  List<T> findAll(boolean viewedUpdate);

  /**
   * Find all list.
   *
   * @param query the query
   * @return the list
   */
  List<T> findAll(Query query);

  /**
   * Find all list.
   *
   * @param query        the query
   * @param viewedUpdate the viewed update
   * @return the list
   */
  List<T> findAll(Query query, boolean viewedUpdate);

  /**
   * Find and modify t.
   *
   * @param query  the query
   * @param update the update
   * @return the t
   */
  T findAndModify(Query query, Update update);

  /**
   * Save t.
   *
   * @param entity the entity
   * @return the t
   */
  T save(T entity);

  /**
   * Save list.
   *
   * @param iterable the entities
   * @return the list
   */
  List<T> save(Iterable<T> iterable);

  /**
   * Find all list.
   *
   * @return the list
   */
  List<T> findAll();

  /**
   * Modify and find t.
   *
   * @param query  the query
   * @param update the update
   * @return the t
   */
  T modifyAndFind(Query query, Update update);

  /**
   * Upsert t.
   *
   * @param query  the query
   * @param update the update
   * @return the t
   */
  T upsert(Query query, Update update);

  /**
   * Update by id t.
   *
   * @param id     the id
   * @param update the update
   * @return the t
   */
  T updateById(String id, Update update);

  /**
   * Upsert return old t.
   *
   * @param query  the query
   * @param update the update
   * @return the t
   */
  T upsertReturnOld(Query query, Update update);

  /**
   * Remove and insert t.
   *
   * @param query  the query
   * @param entity the entity
   * @return the t
   */
  T removeAndInsert(Query query, T entity);

  /**
   * Delete by id t.
   *
   * @param id the id
   * @return the t
   */
  T deleteById(String id);

  /**
   * Update multi.
   *
   * @param query  the query
   * @param update the update
   * @return Number of records updated
   */
  long updateMulti(Query query, Update update);

  /**
   * Distinct list.
   *
   * @param <R>       the type parameter
   * @param fieldName the field name
   * @param query     the query
   * @param rClass    The result class
   * @return the list
   */
  <R> List<R> distinct(String fieldName, Query query, Class<R> rClass);

  /**
   * Count long.
   *
   * @param query the query
   * @return the long
   */
  long count(Query query);

  /**
   * Gets the {@link Criteria} containing tenantId and deleted filter.
   *
   * @return the criteria
   */
  Criteria getCriteria();

  /**
   * Ensure index.
   *
   * @param keys    the keys
   * @param options the options
   */
  void ensureIndex(Bson keys, IndexOptions options);

  /**
   * Find and replace t.
   *
   * @param query     the query
   * @param entity    the entity
   * @param returnNew the return new
   * @param upsert    the upsert
   * @return the t
   */
  T findAndReplace(Query query, T entity, boolean returnNew, boolean upsert);
}
