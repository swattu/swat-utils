/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

/**
 * The interface Validation aware.
 */
public interface ValidationAware {

  /**
   * Is valid boolean. Returns true if not null.
   * Override to add more checks.
   *
   * @param key   the key
   * @param value the value
   * @return the boolean
   */
  default boolean isValid(String key, Object value) {
    return isValid(value);
  }

  /**
   * Is valid boolean. Returns true if not null.
   * Override to add more checks.
   *
   * @param value the value
   * @return the boolean
   */
  boolean isValid(Object value);
}
