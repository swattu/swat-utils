/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import java.util.Date;

/**
 * The type Abstract entity dao wrapper.
 *
 * @param <T> the type parameter
 */
public abstract class AbstractEntityDaoInternal<T extends AbstractEntityInternal> extends AbstractEntityDaoImpl<T> {
  /**
   * Instantiates a new Abstract entity dao wrapper.
   *
   * @param clazz the clazz
   */
  protected AbstractEntityDaoInternal(Class<T> clazz) {
    super(clazz);
  }

  /**
   * Instantiates a new Abstract entity dao wrapper.
   *
   * @param clazz     the clazz
   * @param tenantKey the tenant key
   */
  protected AbstractEntityDaoInternal(Class<T> clazz, String tenantKey) {
    super(clazz, tenantKey);
  }

  @Override
  protected String getCreatedKey() {
    return "createdAt";
  }

  @Override
  protected String getCreatedByKey() {
    return "createdBy";
  }

  @Override
  protected String getUpdatedKey() {
    return "updatedAt";
  }

  @Override
  protected String getUpdatedByKey() {
    return "updatedBy";
  }

  @Override
  protected Object getNow() {
    return new Date();
  }
}
