/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import org.bson.Document;
import org.springframework.data.mongodb.core.query.Update;
import org.swat.core.utils.ObjectUtil;

/**
 * The type Null safe update.
 * It has most of the methods from {@link Update}
 */
public class NullSafeUpdate {
  private Update update;

  /**
   * Instantiates a new Null safe update.
   */
  public NullSafeUpdate() {
    this(null);
  }

  /**
   * Instantiates a new Null safe update.
   *
   * @param update the update
   */
  public NullSafeUpdate(Update update) {
    if (update == null) {
      update = new Update();
    }
    this.update = update;
  }

  /**
   * Gets update.
   *
   * @return the update
   */
  public Update getUpdate() {
    return update;
  }

  /**
   * Set null safe update.
   *
   * @param key   the key
   * @param value the value
   * @return the null safe update
   */
  public NullSafeUpdate set(String key, Object value) {
    if (isValid(value)) {
      update.set(key, value);
    }
    return this;
  }

  /**
   * Sets on insert.
   *
   * @param key   the key
   * @param value the value
   * @return the on insert
   */
  public NullSafeUpdate setOnInsert(String key, Object value) {
    if (isValid(value)) {
      update.setOnInsert(key, value);
    }
    return this;
  }

  /**
   * Unset null safe update.
   *
   * @param key the key
   * @return the null safe update
   */
  public NullSafeUpdate unset(String key) {
    update.unset(key);
    return this;
  }

  /**
   * Inc null safe update.
   *
   * @param key the key
   * @param inc the inc
   * @return the null safe update
   */
  public NullSafeUpdate inc(String key, Number inc) {
    if (isValid(inc)) {
      update.inc(key, inc);
    }
    return this;
  }

  /**
   * Push null safe update.
   *
   * @param key   the key
   * @param value the value
   * @return the null safe update
   */
  public NullSafeUpdate push(String key, Object value) {
    if (isValid(value)) {
      update.push(key, value);
    }
    return this;
  }

  /**
   * Push all null safe update.
   *
   * @param key    the key
   * @param values the values
   * @return the null safe update
   */
  public NullSafeUpdate pushAll(String key, Object[] values) {
    if (isValid(values)) {
      update.push(key).each(values);
    }
    return this;
  }

  /**
   * Add to set add to set builder.
   *
   * @param key the key
   * @return the add to set builder
   */
  public Update.AddToSetBuilder addToSet(String key) {
    return update.addToSet(key);
  }

  /**
   * Add to set null safe update.
   *
   * @param key   the key
   * @param value the value
   * @return the null safe update
   */
  public NullSafeUpdate addToSet(String key, Object value) {
    if (isValid(value)) {
      update.addToSet(key, value);
    }
    return this;
  }

  /**
   * Pull null safe update.
   *
   * @param key   the key
   * @param value the value
   * @return the null safe update
   */
  public NullSafeUpdate pull(String key, Object value) {
    if (isValid(value)) {
      update.pull(key, value);
    }
    return this;
  }

  /**
   * Pull all null safe update.
   *
   * @param key    the key
   * @param values the values
   * @return the null safe update
   */
  public NullSafeUpdate pullAll(String key, Object[] values) {
    if (isValid(values)) {
      update.pullAll(key, values);
    }
    return this;
  }

  /**
   * Rename null safe update.
   *
   * @param oldName the old name
   * @param newName the new name
   * @return the null safe update
   */
  public NullSafeUpdate rename(String oldName, String newName) {
    update.rename(oldName, newName);
    return this;
  }

  /**
   * Current date null safe update.
   *
   * @param key the key
   * @return the null safe update
   */
  public NullSafeUpdate currentDate(String key) {
    update.currentDate(key);
    return this;
  }

  /**
   * Current timestamp null safe update.
   *
   * @param key the key
   * @return the null safe update
   */
  public NullSafeUpdate currentTimestamp(String key) {
    update.currentTimestamp(key);
    return this;
  }

  /**
   * Multiply null safe update.
   *
   * @param key        the key
   * @param multiplier the multiplier
   * @return the null safe update
   */
  public NullSafeUpdate multiply(String key, Number multiplier) {
    if (isValid(multiplier)) {
      update.multiply(key, multiplier);
    }
    return this;
  }

  /**
   * Max null safe update.
   *
   * @param key   the key
   * @param value the value
   * @return the null safe update
   */
  public NullSafeUpdate max(String key, Object value) {
    if (isValid(value)) {
      update.max(key, value);
    }
    return this;
  }

  /**
   * Min null safe update.
   *
   * @param key   the key
   * @param value the value
   * @return the null safe update
   */
  public NullSafeUpdate min(String key, Object value) {
    if (isValid(value)) {
      update.min(key, value);
    }
    return this;
  }

  /**
   * Bitwise bitwise operator builder.
   *
   * @param key the key
   * @return the bitwise operator builder
   */
  public Update.BitwiseOperatorBuilder bitwise(String key) {
    return update.bitwise(key);
  }

  /**
   * Gets update object.
   *
   * @return the update object
   */
  public Document getUpdateObject() {
    return update.getUpdateObject();
  }

  /**
   * Modifies boolean.
   *
   * @param key the key
   * @return the boolean
   */
  public boolean modifies(String key) {
    return update.modifies(key);
  }


  public int hashCode() {
    return update.hashCode();
  }


  public boolean equals(Object obj) {
    return update.equals(obj);
  }


  public String toString() {
    return update.toString();
  }

  /**
   * Push push operator builder.
   *
   * @param key the key
   * @return the push operator builder
   */
  public Update.PushOperatorBuilder push(String key) {
    return update.push(key);
  }

  /**
   * Pop update.
   *
   * @param key the key
   * @param pos the pos
   * @return the update
   */
  public Update pop(String key, Update.Position pos) {
    return update.pop(key, pos);
  }

  /**
   * Is valid boolean.
   *
   * @param value the value
   * @return the boolean
   */
  protected boolean isValid(Object value) {
    return ObjectUtil.isNotNullEmpty(value);
  }
}
