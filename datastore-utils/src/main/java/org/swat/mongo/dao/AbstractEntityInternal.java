/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import lombok.Data;

import java.util.Date;

/**
 * The type Abstract entity wrapper.
 */
@Data
public class AbstractEntityInternal implements AbstractEntity {
  private Date createdAt;
  private Object createdBy;
  private Date updatedAt;
  private Object updatedBy;

  @Override
  public void updatedAt(Object now) {
    this.updatedAt = (Date) now;
  }

  @Override
  public void updatedBy(Object updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Override
  public Object createdAt() {
    return createdAt;
  }

  @Override
  public Object createdBy() {
    return createdBy;
  }

  @Override
  public void createdAt(Object now) {
    this.createdAt = (Date) now;
  }

  @Override
  public void createdBy(Object createdBy) {
    this.createdBy = createdBy;
  }
}
