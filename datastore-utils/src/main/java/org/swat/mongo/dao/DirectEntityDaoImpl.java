/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import com.mongodb.annotations.Beta;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

/**
 * The type Direct entity dao.
 *
 * @param <T> the type parameter
 */
@Beta
public class DirectEntityDaoImpl<T extends AbstractEntity> extends AbstractEntityDaoImpl<T> {
  private final AbstractEntityDaoImpl<T> entityDao;
  private final boolean directQuery;
  private final boolean directUpdate;

  /**
   * Instantiates a new Direct entity dao.
   *
   * @param entityDao    the entity dao
   * @param clazz        the clazz
   * @param tenantKey    the tenant key
   * @param directQuery  the query
   * @param directUpdate the update
   */
  protected DirectEntityDaoImpl(AbstractEntityDaoImpl<T> entityDao, Class<T> clazz, String tenantKey,
      boolean directQuery, boolean directUpdate) {
    super(clazz, tenantKey);
    this.entityDao = entityDao;
    this.directQuery = directQuery;
    this.directUpdate = directUpdate;
  }

  @Override
  protected String getEntityId(T entity) {
    return entityDao.getEntityId(entity);
  }

  @Override
  public Criteria getCriteria() {
    if (directQuery) {
      return new Criteria();
    }
    return entityDao.getCriteria();
  }

  @Override
  protected String getDeletedKey() {
    return entityDao.getDeletedKey();
  }

  @Override
  protected MongoTemplate getMongoTemplate() {
    return entityDao.getMongoTemplate();
  }

  @Override
  protected Query getTenantQuery(Query query) {
    if (this.directQuery) {
      if (query == null) {
        query = new Query();
      }
      return query;
    }
    return entityDao.getTenantQuery(query);
  }

  @Override
  public String getCollectionName() {
    return entityDao.getCollectionName();
  }

  @Override
  protected Update setValues(Update update) {
    if (this.directUpdate) {
      return update;
    }
    return entityDao.setValues(update);
  }

  @Override
  protected String getCreatedKey() {
    return entityDao.getCreatedKey();
  }

  @Override
  protected Object getNow() {
    return entityDao.getNow();
  }

  @Override
  protected String getCreatedByKey() {
    return entityDao.getCreatedByKey();
  }

  @Override
  protected String getUpdatedKey() {
    return entityDao.getUpdatedKey();
  }

  @Override
  protected String getUpdatedByKey() {
    return entityDao.getUpdatedByKey();
  }

  @Override
  protected String getViewedByKey() {
    return entityDao.getViewedByKey();
  }

  @Override
  protected String getViewedKey() {
    return entityDao.getViewedKey();
  }

  @Override
  protected void setEntityId(T entity, String id) {
    entityDao.setEntityId(entity, id);
  }

  @Override
  public T save(T entity) {
    if (directUpdate) {
      getMongoTemplate().save(entity, getCollectionName());
      return entity;
    }
    return super.save(entity);
  }

  @Override
  protected void onFetch(T entity) {
    entityDao.onFetch(entity);
  }
}
