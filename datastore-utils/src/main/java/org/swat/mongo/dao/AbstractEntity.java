/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

/**
 * The base class for any entity
 *
 * @author Swatantra Agrawal on 18-Mar-2018
 */
public interface AbstractEntity {
  /**
   * Gets tenant id.
   *
   * @return the tenant id
   */

  default Object tenantId() {
    return null;
  }

  /**
   * Sets tenant id.
   *
   * @param tenantId the tenant id
   */
  default void tenantId(Object tenantId) {
  }

  /**
   * Gets created at.
   *
   * @return the created at
   */
  default Object createdAt() {
    return null;
  }

  /**
   * Sets created at.
   *
   * @param now the now
   */
  default void createdAt(Object now) {
  }

  /**
   * Created by object.
   *
   * @return the object
   */
  default Object createdBy() {
    return null;
  }

  /**
   * Created by.
   *
   * @param createdBy the created by
   */
  default void createdBy(Object createdBy) {
  }

  /**
   * Sets updated at.
   *
   * @param now the now
   */
  default void updatedAt(Object now) {
  }

  /**
   * Updated by.
   *
   * @param updatedBy the updated by
   */
  default void updatedBy(Object updatedBy) {
  }
}
