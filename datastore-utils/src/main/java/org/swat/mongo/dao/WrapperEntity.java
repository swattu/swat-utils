/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

/**
 * Wrapper for Abstract Entity. Consumer can implement selected methods only.
 */
public class WrapperEntity implements AbstractEntity {
  @Override
  public Object tenantId() {
    throw new UnsupportedOperationException("This method is not implemented");
  }

  @Override
  public void tenantId(Object tenantId) {
    throw new UnsupportedOperationException("This method is not implemented");
  }

  @Override
  public Object createdAt() {
    throw new UnsupportedOperationException("This method is not implemented");
  }

  @Override
  public void createdAt(Object now) {
    throw new UnsupportedOperationException("This method is not implemented");
  }

  @Override
  public Object createdBy() {
    throw new UnsupportedOperationException("This method is not implemented");
  }

  @Override
  public void createdBy(Object createdBy) {
    throw new UnsupportedOperationException("This method is not implemented");
  }

  @Override
  public void updatedAt(Object now) {
    throw new UnsupportedOperationException("This method is not implemented");
  }

  @Override
  public void updatedBy(Object updatedBy) {
    throw new UnsupportedOperationException("This method is not implemented");
  }
}
