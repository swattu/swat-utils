/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import org.springframework.data.mongodb.core.query.Criteria;

import java.util.Collection;

import static org.swat.core.utils.ObjectUtil.cast;


/**
 * The type Null safe criteria.
 */
public abstract class SafeCriteria<T extends SafeCriteria<T>> implements ValidationAware {
  private Criteria criteria;
  private String key;
  private boolean used;
  private int count;

  /**
   * Gets the number of Criteria added
   *
   * @return the criteria count
   */
  public int getCriteriaCount() {
    return count;
  }

  /**
   * Instantiates a new Criteria utils.
   *
   * @param criteria the criteria
   */
  public SafeCriteria(Criteria criteria) {
    if (criteria == null) {
      criteria = new Criteria();
    }
    this.criteria = criteria;
  }

  /**
   * And criteria utils.
   *
   * @param key the key
   * @return the criteria utils
   */
  public T and(String key) {
    this.key = key;
    used = false;
    return cast(this);
  }

  /**
   * Is criteria utils.
   *
   * @param value the value
   * @return the criteria utils
   */
  public T is(Object value) {
    if (isValid(key, value)) {
      criteria().is(value);
    }
    return cast(this);
  }

  /**
   * Gets criteria.
   *
   * @return the criteria
   */
  public Criteria getCriteria() {
    return criteria;
  }

  private Criteria criteria() {
    if (!used) {
      criteria = criteria.and(key);
      used = true;
      count++;
    }
    return criteria;
  }

  /**
   * Lte criteria utils.
   *
   * @param value the value
   * @return the criteria utils
   */
  public T lte(Object value) {
    if (isValid(key, value)) {
      criteria().lte(value);
    }
    return cast(this);
  }

  /**
   * Lt criteria utils.
   *
   * @param value the value
   * @return the criteria utils
   */
  public T lt(Object value) {
    if (isValid(key, value)) {
      criteria().lt(value);
    }
    return cast(this);
  }

  /**
   * Gte criteria utils.
   *
   * @param value the value
   * @return the criteria utils
   */
  public T gte(Object value) {
    if (isValid(key, value)) {
      criteria().gte(value);
    }
    return cast(this);
  }

  /**
   * Gt criteria utils.
   *
   * @param value the value
   * @return the criteria utils
   */
  public T gt(Object value) {
    if (isValid(key, value)) {
      criteria().gt(value);
    }
    return cast(this);
  }

  /**
   * In criteria utils.
   *
   * @param values the values
   * @return the criteria utils
   */
  public T in(Object... values) {
    if (values != null && values.length > 0) {
      criteria().in(values);
    }
    return cast(this);
  }

  /**
   * In null safe criteria.
   *
   * @param values the values
   * @return the null safe criteria
   */
  public T in(Collection values) {
    if (values != null && values.size() > 0) {
      criteria().in(values);
    }
    return cast(this);
  }

  /**
   * Nin null safe criteria.
   *
   * @param values the values
   * @return the null safe criteria
   */
  public T nin(Object... values) {
    if (values != null && values.length > 0) {
      criteria().nin(values);
    }
    return cast(this);
  }

  /**
   * Nin null safe criteria.
   *
   * @param values the values
   * @return the null safe criteria
   */
  public T nin(Collection values) {
    if (values != null && values.size() > 0) {
      criteria().nin(values);
    }
    return cast(this);
  }

  /**
   * Ne null safe criteria.
   *
   * @param value the value
   * @return the null safe criteria
   */
  public T ne(Object value) {
    if (isValid(key, value)) {
      criteria().ne(value);
    }
    return cast(this);
  }
}
