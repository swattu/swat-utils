/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import lombok.Data;

import java.util.List;

/**
 * The type Paged response.
 *
 * @param <T> the type parameter
 */
@Data
public class PagedResponse<T> {
  private List<T> data;
  private long count;
  private long skip;
  private long limit;
  private List<Object> distinct;
}
