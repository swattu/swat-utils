/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

/**
 * The interface Null safe aware.
 */
public interface NullAware extends ValidationAware {
  /**
   * Is valid boolean.
   *
   * @param value the value
   * @return the boolean
   */
  default boolean isValid(Object value) {
    return value != null;
  }
}
