/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import java.util.HashMap;

/**
 * The type Map entity. This is used to disable IndexCreation on a ReadOnly Mongo.
 */
public class MapEntity extends HashMap implements AbstractEntity {
    @Override
    public Object tenantId() {
        return null;
    }

    @Override
    public void tenantId(Object tenantId) {

    }

    @Override
    public Object createdAt() {
        return null;
    }

    @Override
    public void createdAt(Object now) {

    }

    @Override
    public Object createdBy() {
        return null;
    }

    @Override
    public void createdBy(Object createdBy) {

    }

    @Override
    public void updatedAt(Object now) {

    }

    @Override
    public void updatedBy(Object updatedBy) {

    }
}
