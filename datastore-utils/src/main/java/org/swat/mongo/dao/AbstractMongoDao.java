/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.CountOperation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.LimitOperation;
import org.springframework.data.mongodb.core.aggregation.SkipOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.swat.mongo.utils.QueryUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.swat.core.utils.ObjectUtil.cast;
import static org.swat.mongo.utils.AggregationUtils.mongoScript;

/**
 * The type Mongo dao.
 */
@Slf4j
public abstract class AbstractMongoDao {
  private static final Set<String> EMPTY_EXCLUSIONS = new HashSet<>();

  /**
   * Gets mongo template.
   *
   * @return the mongo template
   */
  protected abstract MongoOperations getMongoOperations();

  /**
   * Aggregate list.
   *
   * @param <T>            the type parameter
   * @param operations     the operations
   * @param collectionName the collection name
   * @param clazz          the clazz
   * @return the list
   */
  public <T> List<T> aggregate(List<AggregationOperation> operations, String collectionName, Class<T> clazz) {
    AggregationResults<T> results = aggregateInternal(Aggregation.newAggregation(operations), collectionName, clazz);
    return results.getMappedResults();
  }

  /**
   * Should log boolean.
   *
   * @param clazz  the clazz
   * @param caller the caller
   * @param script the message
   * @param count  the count
   * @param millis the millis
   */
  protected void logSuccess(Class clazz, String caller, String script, int count, long millis) {
    String trace = caller + " returned " + count + " records in " + millis + " millis. " + script;
    log.debug(trace);
  }

  private <T> AggregationResults<T> aggregateInternal(Aggregation aggregation, String collectionName, Class<T> clazz) {
    long startTime = System.currentTimeMillis();
    String query = "db.getCollection('" + collectionName + "').aggregate(" + mongoScript(aggregation) + ")";
    try {
      AggregationOptions options = aggregation.getOptions();
      AggregationOptions.Builder builder = new AggregationOptions.Builder();
      options.getCursor().ifPresent(builder::cursor);
      builder.explain(options.isExplain());
      builder.allowDiskUse(true);
      aggregation = aggregation.withOptions(builder.build());
      AggregationResults<T> results = getMongoOperations().aggregate(aggregation, collectionName, clazz);
      int size = results.getMappedResults().size();
      long timeTaken = System.currentTimeMillis() - startTime;

      logSuccess(clazz, callerName(), query, size, timeTaken);
      return results;
    } catch (RuntimeException rte) {
      long timeTaken = System.currentTimeMillis() - startTime;
      logError(clazz, callerName(), query, timeTaken, rte);
      throw rte;
    }
  }

  /**
   * Log error.
   *
   * @param <T>    the type parameter
   * @param clazz  the clazz
   * @param caller the caller
   * @param query  the query
   * @param millis the millis
   * @param rte    the rte
   */
  protected <T> void logError(Class<T> clazz, String caller, String query, long millis, RuntimeException rte) {
    String trace = caller + " threw " + rte.getMessage() + " in " + millis + " millis. " + query;
    log.debug(trace);
  }

  private String callerName() {
    for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
      if (!StringUtils.equalsAny(element.getClassName(), AbstractMongoDao.class.getName(), Thread.class.getName())) {
        if (!callerExclusions().contains(element.getClassName())) {
          return element.getClassName() + "." + element.getMethodName() + "[" + element.getLineNumber() + "]";
        }
      }
    }
    return null;
  }

  /**
   * Caller exclusions set.
   *
   * @return the set
   */
  protected Set<String> callerExclusions() {
    return EMPTY_EXCLUSIONS;
  }

  /**
   * Default aggregation paged response.
   *
   * @param <T>            the type parameter
   * @param operations     the operations
   * @param collectionName the collection name
   * @param clazz          the clazz
   * @return the paged response
   */
  public <T> PagedResponse<T> defaultAggregation(final AggregationOperation[] operations, String collectionName,
      Class<T> clazz) {
    return defaultAggregation(Arrays.asList(operations), collectionName, clazz);
  }

  /**
   * Default aggregation paged response.
   *
   * @param <T>            the type parameter
   * @param operations     the operations
   * @param collectionName the collection name
   * @param clazz          the clazz
   * @return the paged response
   */
  public <T> PagedResponse<T> defaultAggregation(final List<AggregationOperation> operations, String collectionName,
      Class<T> clazz) {
    List<AggregationOperation> tmpOperations = new ArrayList<>(operations);
    Criteria criteria = getCriteria();
    if (criteria != null) {
      tmpOperations.add(match(criteria));
    }
    String searchText = getSearchText();
    String[] searchFields = getSearchFields();
    if (StringUtils.isNotBlank(searchText) && ArrayUtils.isNotEmpty(getSearchFields())) {
      criteria = QueryUtil.searchCriteria(true, searchText, searchFields);
      tmpOperations.add(match(criteria));
    }
    if (StringUtils.isNotBlank(getDistinctField())) {
      return distinctAggregation(tmpOperations, collectionName, getDistinctField());
    }
    long pageNo = getPageNo();
    long pageSize = getPageLimit();
    Sort sort = getSort();
    return defaultAggregation(tmpOperations, collectionName, clazz, pageNo, pageSize, sort);
  }

  /**
   * Gets sort.
   *
   * @return the sort
   */
  protected Sort getSort() {
    return null;
  }

  /**
   * Gets page limit.
   *
   * @return the page limit
   */
  protected long getPageLimit() {
    return -1;
  }

  /**
   * Gets page no.
   *
   * @return the page no.  This is 1 based.
   */
  protected long getPageNo() {
    return 0;
  }

  /**
   * Gets distinct field.
   *
   * @return the distinct field
   */
  protected String getDistinctField() {
    return null;
  }

  private <T> PagedResponse<T> distinctAggregation(final List<AggregationOperation> operations, String collectionName,
      String distinctField) {
    List<AggregationOperation> tmpOperations = new ArrayList<>(operations);
    GroupOperation groupOperation = group().addToSet(distinctField).as("distinct");
    tmpOperations.add(groupOperation);
    List<PagedResponse> results = aggregate(tmpOperations, collectionName, PagedResponse.class);
    return CollectionUtils.isEmpty(results) ? new PagedResponse<>() : cast(results.get(0));
  }

  private <T> PagedResponse<T> defaultAggregation(final List<AggregationOperation> operations, String collectionName,
      Class<T> clazz, long pageNo, long pageSize, Sort sort) {
    ArrayList<AggregationOperation> tmpOperations = new ArrayList<>(operations);
    PagedResponse<T> apiResponse = new PagedResponse<>();
    if (sort != null) {
      SortOperation sortOperation = sort(sort);
      tmpOperations.add(sortOperation);
    }
    if (pageSize <= 0 || pageNo < 1) {
      pageNo = -1;
      pageSize = -1;
    }
    long skip = 0;
    if (pageNo > 0) {
      skip = (pageNo - 1) * pageSize;
      apiResponse.setSkip(skip);
      apiResponse.setLimit(pageSize);
      SkipOperation skipOperation = skip(skip);
      LimitOperation limitOperation = limit(pageSize);
      tmpOperations.add(skipOperation);
      tmpOperations.add(limitOperation);
    }
    int minPageSize = getMinPageSize();
    if (pageSize > 0 && pageSize < minPageSize) {
      pageSize = minPageSize;
    }

    AggregationResults<T> results = aggregateInternal(newAggregation(tmpOperations), collectionName, clazz);
    apiResponse.setData(results.getMappedResults());
    if (pageNo > 0 && appendCount(pageNo)) {
      if (results.getMappedResults().size() == pageSize) {
        CountOperation countOperation = count().as("count");
        tmpOperations = new ArrayList<>(operations);
        tmpOperations.add(countOperation);
        AggregationResults<Map> countResults =
            aggregateInternal(newAggregation(tmpOperations), collectionName, Map.class);
        apiResponse.setCount(((Number) countResults.getUniqueMappedResult().get("count")).longValue());
      } else {
        apiResponse.setCount(skip + results.getMappedResults().size());
      }
    } else if (pageNo == -1) {
      apiResponse.setCount(results.getMappedResults().size());
    }
    return apiResponse;
  }

  /**
   * Append count boolean.
   *
   * @param pageNo the page no. This is 1 based.
   * @return the boolean
   */
  protected boolean appendCount(long pageNo) {
    return pageNo == 1;
  }

  /**
   * Gets min page size.
   *
   * @return the min page size
   */
  protected int getMinPageSize() {
    return 10;
  }

  /**
   * Gets criteria.
   *
   * @return the criteria
   */
  protected Criteria getCriteria() {
    return null;
  }

  /**
   * Gets search fields.
   *
   * @return the search fields
   */
  protected String[] getSearchFields() {
    return null;
  }

  /**
   * Gets search text.
   *
   * @return the search text
   */
  protected String getSearchText() {
    return null;
  }
}
