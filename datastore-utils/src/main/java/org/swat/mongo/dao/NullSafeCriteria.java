/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import org.springframework.data.mongodb.core.query.Criteria;

/**
 * The type Null safe criteria.
 */
public class NullSafeCriteria extends SafeCriteria<NullSafeCriteria> implements NullAware {
  /**
   * Instantiates a new Criteria utils.
   *
   * @param criteria the criteria
   */
  public NullSafeCriteria(Criteria criteria) {
    super(criteria);
  }
}
