/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import com.mongodb.MongoCommandException;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.conversions.Bson;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.swat.core.utils.CoreInterceptor;
import org.swat.core.utils.CoreRtException;
import org.swat.datastore.base.RequestContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import static org.swat.mongo.utils.QueryUtil.addCriteria;

/**
 * This DAO abstracts Multi Tenancy. Developers need not care about setting the Tenant Id while making Queries.
 * <p>
 * It supports Multi-tenancy at various levels:
 * Field(Column) Level: Instantiate this class with an appropriate fieldName, containing tenantId, as tenantKey.
 * Collection(Table) Level: Override getCollectionName method to return appropriate Collection Name for the Tenant context.
 * Database Level: In getMongoTemplate method, return appropriate MongoTemplate for the Tenant context.
 *
 * @param <T> the type parameter
 * @author Swatantra Agrawal on 18-Mar-2018
 */
public abstract class AbstractEntityDaoImpl<T extends AbstractEntity> implements AbstractEntityDao<T> {
  private static final FindAndModifyOptions UPDATE_NEW = FindAndModifyOptions.options().returnNew(true);
  private static final FindAndModifyOptions UPSERT_NEW = FindAndModifyOptions.options().returnNew(true).upsert(true);
  private static final FindAndModifyOptions UPSERT_OLD = FindAndModifyOptions.options().returnNew(false).upsert(true);
  /**
   * The Clazz.
   */
  public final Class<T> clazz;
  /**
   * The Tenant key.
   */
  public final String tenantKey;
  private final String defaultCollectionName;

  /**
   * Instantiates a new Abstract entity dao.
   *
   * @param clazz the clazz
   */
  protected AbstractEntityDaoImpl(Class<T> clazz) {
    this(clazz, "tenantId");
  }

  /**
   * Instantiates a new Abstract entity dao.
   *
   * @param clazz     the clazz
   * @param tenantKey the tenant key
   */
  protected AbstractEntityDaoImpl(Class<T> clazz, String tenantKey) {
    this.clazz = clazz;
    this.tenantKey = tenantKey;
    if (clazz != null) {
      String name = clazz.getSimpleName();
      name = StringUtils.uncapitalize(name.substring(0, 1)) + name.substring(1);
      defaultCollectionName = name;
    } else {
      defaultCollectionName = null;
    }
  }

  /**
   * Gets entity id.
   *
   * @param entity the entity
   * @return the entity id
   */
  protected String getEntityId(T entity) {
    throw new UnsupportedOperationException("This method is not implemented");
  }

  /**
   * Gets match operation containing tenantId and deleted filter.
   * Ideally this should be first {@link org.springframework.data.mongodb.core.aggregation.AggregationOperation} while creating {@link Aggregation}
   *
   * @return the match operation
   */
  protected MatchOperation getMatchOperation() {
    return Aggregation.match(getCriteria());
  }

  public Criteria getCriteria() {
    Criteria criteria = new Criteria();
    if (StringUtils.isNotBlank(tenantKey)) {
      criteria.and(tenantKey).is(getTenantId());
    }
    if (StringUtils.isNotBlank(getDeletedKey())) {
      criteria.and(getDeletedKey()).is(false);
    }
    return criteria;
  }

  /**
   * Gets tenant id.
   *
   * @return the tenant id
   */
  protected Object getTenantId() {
    return RequestContext.getTenantId();
  }

  /**
   * Get deleted key string.
   *
   * @return the string
   */
  protected String getDeletedKey() {
    return null;
  }

  private class BulkOperationInterceptor implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
      Object[] params = methodInvocation.getArguments();
      if (params != null) {
        for (Object object : params) {
          if (object == null) {
            continue;
          }
          if (object instanceof Query) {
            getTenantQuery((Query) object);
          } else if (object instanceof Update) {
            setValues((Update) object);
          } else if (methodInvocation.getMethod().getName().contains("insert") &&
              clazz.isAssignableFrom(object.getClass())) {
            setValuesOnInsert((T) object);
          }
        }
      }
      return methodInvocation.proceed();
    }
  }

  /**
   * Find and remove t.
   *
   * @param query the query
   * @return the t
   */
  @Override
  public T findAndRemove(Query query) {
    T entity = getMongoTemplate().findAndRemove(getTenantQuery(query), clazz, getCollectionName());
    return processAfterFetch(entity);
  }


  @Override
  public final long removeAll(Query query) {
    DeleteResult result = getMongoTemplate().remove(getTenantQuery(query), getCollectionName());
    return result.getDeletedCount();
  }


  /**
   * Gets mongo template.
   *
   * @return the mongo template
   */
  protected abstract MongoTemplate getMongoTemplate();


  /**
   * Gets tenant query.
   *
   * @param query the query
   * @return the tenant query
   */
  protected Query getTenantQuery(Query query) {
    if (query == null) {
      query = new Query();
    }
    if (StringUtils.isNotBlank(tenantKey)) {
      addCriteria(query, tenantKey, getTenantId());
    }
    addCriteria(query, getDeletedKey(), false);
    return query;
  }

  /**
   * Gets collection name.
   *
   * @return the collection name
   */
  @Override
  public String getCollectionName() {
    return defaultCollectionName;
  }

  @Override
  public AbstractEntityDao<T> withDirect() {
    return new DirectEntityDaoImpl<>(this, clazz, tenantKey, true, true);
  }


  @Override
  public AbstractEntityDao<T> withDirectQuery() {
    return new DirectEntityDaoImpl<>(this, clazz, tenantKey, true, false);
  }


  @Override
  public AbstractEntityDao<T> withDirectUpdate() {
    return new DirectEntityDaoImpl<>(this, clazz, tenantKey, false, true);
  }


  /**
   * Sets values.
   *
   * @param update the update
   * @return the values
   */
  protected Update setValues(Update update) {
    onUpdate(update);
    if (StringUtils.isNotBlank(getCreatedKey())) {
      update.setOnInsert(getCreatedKey(), getNow());
    }
    if (StringUtils.isNotBlank(getCreatedByKey())) {
      update.setOnInsert(getCreatedByKey(), RequestContext.getUserId());
    }
    if (StringUtils.isNotBlank(getUpdatedKey())) {
      update.set(getUpdatedKey(), getNow());
    }
    if (StringUtils.isNotBlank(getUpdatedByKey())) {
      update.set(getUpdatedByKey(), RequestContext.getUserId());
    }
    if (StringUtils.isNotBlank(tenantKey)) {
      update.set(tenantKey, getTenantId());
    }
    return update;
  }

  /**
   * Hook to update on update
   *
   * @param update the update
   */
  protected void onUpdate(Update update) {
  }


  /**
   * Get created key string.
   *
   * @return the string
   */
  protected String getCreatedKey() {
    return null;
  }

  /**
   * Get now object.
   *
   * @return the object
   */
  protected Object getNow() {
    return System.currentTimeMillis();
  }

  /**
   * Get created by key string.
   *
   * @return the string
   */
  protected String getCreatedByKey() {
    return null;
  }

  /**
   * Get updated key string.
   *
   * @return the string
   */
  protected String getUpdatedKey() {
    return null;
  }

  /**
   * Get updated by key string.
   *
   * @return the string
   */
  protected String getUpdatedByKey() {
    return null;
  }

  /**
   * Get viewed key string.
   *
   * @return the string
   */
  protected String getViewedKey() {
    return null;
  }

  /**
   * Get viewed by key string.
   *
   * @return the string
   */
  protected String getViewedByKey() {
    return null;
  }

  @Override
  public BulkOperations bulkOperations(BulkOperations.BulkMode bulkMode) {
    BulkOperations bulkOperations = getMongoTemplate().bulkOps(bulkMode, clazz, getCollectionName());
    return CoreInterceptor.intercept(bulkOperations, new BulkOperationInterceptor());
  }


  @Override
  public T findById(String id) {
    return findById(id, false);
  }

  @Override
  public T findById(String id, boolean updateViewed) {
    Query query = getIdQuery(id);
    return findOne(query, updateViewed);
  }

  @Override
  public T findOne(Query query) {
    T entity = getMongoTemplate().findOne(getTenantQuery(query), clazz, getCollectionName());
    return processAfterFetch(entity);
  }

  @Override
  public T findOne(Query query, boolean updateViewed) {
    if (updateViewed) {
      Update update = getViewedUpdate();
      if (update != null) {
        T entity = getMongoTemplate().findAndModify(getTenantQuery(query), update, clazz, getCollectionName());
        return processAfterFetch(entity);
      }
    }
    return findOne(query);
  }

  private Update getViewedUpdate() {
    Update update = null;
    if (StringUtils.isNotBlank(getViewedKey())) {
      update = new Update();
      update.set(getViewedKey(), getNow());
    }
    if (StringUtils.isNotBlank(getViewedByKey())) {
      if (update == null) {
        update = new Update();
      }
      update.set(getViewedByKey(), RequestContext.getUserId());
    }
    return update;
  }

  @Override
  public T insert(T entity) {
    setValuesOnInsert(entity);
    entity = getMongoTemplate().insert(entity, getCollectionName());
    return processAfterFetch(entity);
  }

  @Override
  public Collection<T> insert(Collection<T> entities) {
    for (T entity : entities) {
      setValuesOnInsert(entity);
    }
    entities = getMongoTemplate().insert(entities, getCollectionName());
    return processAfterFetch(entities);
  }

  /**
   * Sets values on insert.
   *
   * @param entity the entity
   */
  protected final void setValuesOnInsert(T entity) {
    onInsert(entity);
    String id = UUID.randomUUID().toString();
    setEntityId(entity, id);
    if (StringUtils.isNotBlank(getCreatedKey())) {
      entity.createdAt(getNow());
    }
    if (StringUtils.isNotBlank(getCreatedByKey())) {
      entity.createdBy(RequestContext.getUserId());
    }
    setValuesOnUpdate(entity);
  }

  /**
   * Hook to call on Insert
   *
   * @param entity the entity
   */
  protected void onInsert(T entity) {
  }

  /**
   * Sets entity id.
   *
   * @param entity the entity
   * @param id     the id
   */
  protected abstract void setEntityId(T entity, String id);


  /**
   * Sets values on update.
   *
   * @param entity the entity
   */
  protected final void setValuesOnUpdate(T entity) {
    onUpdate(entity);
    if (StringUtils.isNotBlank(getUpdatedKey())) {
      entity.updatedAt(getNow());
    }
    if (StringUtils.isNotBlank(getUpdatedByKey())) {
      entity.updatedBy(RequestContext.getUserId());
    }
    setTenantId(entity, getTenantId());
  }

  /**
   * Hook to call on update.
   *
   * @param entity the entity
   */
  protected void onUpdate(T entity) {
  }

  /**
   * Sets tenant id.
   *
   * @param entity   the entity
   * @param tenantId the tenant id
   */
  protected final void setTenantId(T entity, Object tenantId) {
    if (StringUtils.isNotBlank(tenantKey)) {
      entity.tenantId(tenantId);
    }
  }

  @Override
  public T save(T entity) {
    return saveInternal(entity);
  }

  private T saveInternal(T entity) {
    if (StringUtils.isBlank(getEntityId(entity))) {
      setValuesOnInsert(entity);
    } else {
      setValuesOnUpdate(entity);
    }
    entity = getMongoTemplate().save(entity, getCollectionName());
    return processAfterFetch(entity);
  }

  @Override
  public List<T> save(Iterable<T> iterable) {
    List<T> entities = new ArrayList<>();
    for (T entity : iterable) {
      entities.add(saveInternal(entity));
    }
    return processAfterFetch(entities);
  }

  @Override
  public List<T> findAll() {
    return findAll(null);
  }

  @Override
  public List<T> findAll(boolean viewedUpdate) {
    return findAll(null, viewedUpdate);
  }

  @Override
  public List<T> findAll(Query query) {
    List<T> entities = getMongoTemplate().find(getTenantQuery(query), clazz, getCollectionName());
    return processAfterFetch(entities);
  }

  @Override
  public List<T> findAll(Query query, boolean viewedUpdate) {
    List<T> list = findAll(query);
    if (viewedUpdate) {
      Update update = getViewedUpdate();
      if (update != null) {
        getMongoTemplate().updateMulti(getTenantQuery(query), update, clazz, getCollectionName());
      }
    }
    return processAfterFetch(list);
  }

  @Override
  public T findAndModify(Query query, Update update) {
    setValues(update);
    T entity = getMongoTemplate().findAndModify(getTenantQuery(query), update, clazz, getCollectionName());
    return processAfterFetch(entity);
  }

  @Override
  public T updateById(String id, Update update) {
    Query query = getIdQuery(id);
    return modifyAndFind(query, update);
  }

  @Override
  public T modifyAndFind(Query query, Update update) {
    setValues(update);
    T entity = getMongoTemplate().findAndModify(getTenantQuery(query), update, UPDATE_NEW, clazz, getCollectionName());
    return processAfterFetch(entity);
  }

  @Override
  public T upsert(Query query, Update update) {
    setValues(update);
    T entity = getMongoTemplate().findAndModify(getTenantQuery(query), update, UPSERT_NEW, clazz, getCollectionName());
    return processAfterFetch(entity);
  }

  @Override
  public T upsertReturnOld(Query query, Update update) {
    setValues(update);
    T entity = getMongoTemplate().findAndModify(getTenantQuery(query), update, UPSERT_OLD, clazz, getCollectionName());
    return processAfterFetch(entity);
  }

  @Override
  public T removeAndInsert(Query query, T entity) {
    setValuesOnInsert(entity);
    T oldEntity = findAndRemove(getTenantQuery(query));
    if (oldEntity != null) {
      if (StringUtils.isNotBlank(getCreatedKey())) {
        entity.createdAt(oldEntity.createdAt());
      }
      if (StringUtils.isNotBlank(getCreatedByKey())) {
        entity.createdBy(oldEntity.createdBy());
      }
      setEntityId(entity, getEntityId(oldEntity));
    }
    entity = getMongoTemplate().insert(entity, getCollectionName());
    return processAfterFetch(entity);
  }


  @Override
  public T deleteById(String id) {
    Query query = getIdQuery(id);
    T entity = findAndRemove(getTenantQuery(query));
    return processAfterFetch(entity);
  }

  /**
   * Gets id query.
   *
   * @param id the id
   * @return the id query
   */
  protected Query getIdQuery(String id) {
    return getTenantQuery(null).addCriteria(Criteria.where("_id").is(id));
  }

  @Override
  public long updateMulti(Query query, Update update) {
    setValues(update);
    UpdateResult result = getMongoTemplate().updateMulti(getTenantQuery(query), update, clazz, getCollectionName());
    return result.getModifiedCount();
  }

  @Override
  public void ensureIndex(Bson keys, IndexOptions options) {
    if (options == null || StringUtils.isBlank(options.getName())) {
      throw new CoreRtException("Index name is mandatory");
    }
    if (keys == null) {
      throw new CoreRtException("Keys are mandatory");
    }
    try {
      getMongoTemplate().getCollection(getCollectionName()).createIndex(keys, options);
    } catch (MongoCommandException ex) {
      String indexName = options.getName();
      getMongoTemplate().getCollection(getCollectionName()).dropIndex(indexName);
      getMongoTemplate().getCollection(getCollectionName()).createIndex(keys, options);
    }
  }

  @Override
  public <R> List<R> distinct(String fieldName, Query query, Class<R> rClass) {
    query = getTenantQuery(query);
    query.fields().include(fieldName);
    return getMongoTemplate().findDistinct(query, fieldName, getCollectionName(), rClass);
  }

  @Override
  public long count(Query query) {
    query = getTenantQuery(query);
    return getMongoTemplate().count(query, getCollectionName());
  }

  /**
   * Gets @{@link Aggregation} with appropriate @{@link MatchOperation} at the beginning
   *
   * @param operations the operations
   * @return the aggregation
   */
  protected Aggregation aggregation(List<? extends AggregationOperation> operations) {
    return aggregation(operations.toArray(new AggregationOperation[0]));
  }

  /**
   * Gets @{@link Aggregation} with appropriate @{@link MatchOperation} at the beginning
   *
   * @param operations the operations
   * @return the aggregation
   */
  protected Aggregation aggregation(AggregationOperation... operations) {
    if (StringUtils.isNotBlank(tenantKey) || StringUtils.isNotBlank(getDeletedKey())) {
      operations = ArrayUtils.insert(0, operations, getMatchOperation());
    }
    return Aggregation.newAggregation(operations);
  }

  @Override
  public T findAndReplace(Query query, T entity, boolean returnNew, boolean upsert) {
    FindAndReplaceOptions options = FindAndReplaceOptions.options();
    setValuesOnUpdate(entity);
    if (returnNew) {
      options.returnNew();
    }
    if (upsert) {
      options.upsert();
      setValuesOnInsert(entity);
    }
    entity = getMongoTemplate().findAndReplace(getTenantQuery(query), entity, options, getCollectionName());
    return processAfterFetch(entity);
  }

  private <C extends Collection<T>> C processAfterFetch(C entities) {
    if (entities != null) {
      entities.forEach(this::processAfterFetch);
    }
    return entities;
  }

  private T processAfterFetch(T entity) {
    if (entity != null) {
      onFetch(entity);
    }
    return entity;
  }

  /**
   * Perform any operation after fetching the entity
   *
   * @param entity the entity
   */
  protected void onFetch(T entity) {
  }
}
