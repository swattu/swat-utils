/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The type Ro abstract entity dao.
 *
 * @param <T> the type parameter
 */
public abstract class AbstractReadOnlyEntityDaoImpl<T extends AbstractEntity> extends AbstractEntityDaoImpl<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractReadOnlyEntityDaoImpl.class);
    private final MapEntityDaoImpl mapEntityDao;
    private final String defaultCollectionName;
    /**
     * The Clazz.
     */
    public final Class<T> clazz;
    /**
     * Instantiates a new Ro abstract entity dao.
     *
     * @param clazz the clazz
     */
    protected AbstractReadOnlyEntityDaoImpl(Class<T> clazz) {
        this(clazz, "tenantId");
    }

    /**
     * Instantiates a new Ro abstract entity dao.
     *
     * @param clazz     the clazz
     * @param tenantKey the tenant key
     */
    protected AbstractReadOnlyEntityDaoImpl(Class<T> clazz, String tenantKey) {
        //Null is passed as class name, so even by mistake the class is not passed to MongoTemplate.
        super(null, tenantKey);
        this.clazz = clazz;
        mapEntityDao = new MapEntityDaoImpl(MapEntity.class, tenantKey);
        String name = clazz.getSimpleName();
        name = StringUtils.uncapitalize(name.substring(0, 1)) + name.substring(1);
        defaultCollectionName = name;
    }

    @Override
    protected void setEntityId(T entity, String id) {
        throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
    }

    @Override
    public String getCollectionName() {
        return defaultCollectionName;
    }

    @Override
    public T findById(String id) {
        MapEntity mapEntity = mapEntityDao.findById(id);
        return convert(mapEntity);
    }

    private T convert(MapEntity mapEntity) {
        if (mapEntity == null) {
            return null;
        }
        try {
            return convertMap(mapEntity);

        } catch (RuntimeException e) {
            LOGGER.warn(
                "Exception for _id[{" + mapEntity.get("_id") + "}], collection[{" + getCollectionName() + "}] - " +
                    e.getMessage());
            throw e;
        }
    }

    /**
     * Convert map to entity.
     *
     * @param map the map
     * @return the entity
     */
    protected abstract T convertMap(Map map);

    public T findOne(Query query, boolean updateViewed) {
        if (updateViewed) {
            throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
        }
        return super.findOne(query, false);
    }

    @Override
    public T findOne(Query query) {
        MapEntity mapEntity = mapEntityDao.findOne(query);
        return convert(mapEntity);
    }

    @Override
    public List<T> findAll() {
        return findAll(null);
    }

    @Override
    public List<T> findAll(Query query) {
        List<T> list = new ArrayList<>();
        List<MapEntity> mapEntities = mapEntityDao.findAll(query);
        for (MapEntity mapEntity : mapEntities) {
            T entity = convert(mapEntity);
            if (entity != null) {
                list.add(entity);
            }
        }
        return list;
    }

    @Override
    public final T findAndRemove(Query query) {
        throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
    }

    @Override
    public final T insert(T entity) {
        throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
    }

    @Override
    public final T save(T entity) {
        throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
    }

    @Override
    public final T findAndModify(Query query, Update update) {
        throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
    }

    @Override
    public final T updateById(String id, Update update) {
        throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
    }

    @Override
    public final T modifyAndFind(Query query, Update update) {
        throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
    }

    @Override
    public final T upsert(Query query, Update update) {
        throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
    }

    @Override
    public final T upsertReturnOld(Query query, Update update) {
        throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
    }

    @Override
    public final T removeAndInsert(Query query, T entity) {
        throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
    }

    @Override
    public final T deleteById(String id) {
        throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
    }

    @Override
    public final long updateMulti(Query query, Update update) {
        throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
    }

    @Override
    public T findAndReplace(Query query, T entity, boolean returnNew, boolean upsert) {
        throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
    }

    private class MapEntityDaoImpl extends AbstractEntityDaoImpl<MapEntity> {
        /**
         * Instantiates a new Map entity dao.
         *
         * @param clazz     the clazz
         * @param tenantKey the tenant key
         */
        protected MapEntityDaoImpl(Class<MapEntity> clazz, String tenantKey) {
            super(clazz, tenantKey);
        }

        @Override
        protected MongoTemplate getMongoTemplate() {
            return AbstractReadOnlyEntityDaoImpl.this.getMongoTemplate();
        }

        @Override
        protected void setEntityId(MapEntity entity, String id) {
            throw new UnsupportedOperationException("Read Only collection: " + getCollectionName());
        }

        @Override
        public String getCollectionName() {
            return AbstractReadOnlyEntityDaoImpl.this.getCollectionName();
        }

        @Override
        protected String getDeletedKey() {
            return AbstractReadOnlyEntityDaoImpl.this.getDeletedKey();
        }
    }
}
