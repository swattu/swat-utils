/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

/**
 * The interface Null safe aware.
 */
public interface DefaultAware extends ValidationAware {
  /**
   * Is valid boolean.
   * Returns false if
   * - The value is null
   * - The value is a Blank CharSequence
   * - The value is an Empty Array
   * - The value is an Empty Collection
   * - The value is an Empty Map
   *
   * @param value the value
   * @return the boolean
   */
  default boolean isValid(Object value) {
    if (value == null) {
      return false;
    }
    if (value instanceof CharSequence && StringUtils.isBlank((CharSequence) value)) {
      return false;
    }
    if (value.getClass().isArray() && Array.getLength(value) == 0) {
      return false;
    }
    if (value instanceof Collection && ((Collection<?>) value).size() == 0) {
      return false;
    }
    if (value instanceof Map && ((Map<?, ?>) value).size() == 0) {
      return false;
    }
    return true;
  }
}
