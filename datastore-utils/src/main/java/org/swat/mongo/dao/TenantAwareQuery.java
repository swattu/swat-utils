/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.dao;


import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.swat.datastore.base.RequestContext;

/**
 * The type Tenant aware query.
 *
 * @author Swatantra Agrawal on 18-Mar-2018
 */
public class TenantAwareQuery extends Query {
  private static final String DEFAULT_TENANT_KEY = "tenantId";

  /**
   * Instantiates a new Tenant aware query.
   */
  public TenantAwareQuery() {
    this(DEFAULT_TENANT_KEY);
  }

  /**
   * Instantiates a new Tenant aware query.
   *
   * @param tenantKey the tenant key
   */
  public TenantAwareQuery(String tenantKey) {
    super();
    if (StringUtils.isNotBlank(tenantKey)) {
      addCriteria(Criteria.where(tenantKey).is(RequestContext.getTenantId()));
    }
  }

  /**
   * Query query.
   *
   * @param criteriaDefinition the criteria definition
   * @return the query
   */
  public static Query query(CriteriaDefinition criteriaDefinition) {
    return query(DEFAULT_TENANT_KEY, criteriaDefinition);
  }

  /**
   * Query query.
   *
   * @param tenantKey          the tenant key
   * @param criteriaDefinition the criteria definition
   * @return the query
   */
  public static Query query(String tenantKey, CriteriaDefinition criteriaDefinition) {
    Query query = new TenantAwareQuery(tenantKey);
    query.addCriteria(criteriaDefinition);
    return query;
  }
}
