/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.DateOperators;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * The type Aggregation utils.
 */
@Slf4j
public class AggregationUtils {
  /**
   * Mongo script string.
   *
   * @param operations the operations
   * @return the string
   */
  public static String mongoScript(List<AggregationOperation> operations) {
    return mongoJson(null, operations);
  }

  /**
   * Mongo script string.
   *
   * @param aggregation the aggregation
   * @return the string
   */
  public static String mongoScript(Aggregation aggregation) {
    return mongoJson(aggregation, null);
  }

  private static String mongoJson(Aggregation aggregation, List<AggregationOperation> operations) {
    if (operations != null && !operations.isEmpty()) {
      aggregation = Aggregation.newAggregation(operations);
    }
    if (aggregation == null) {
      return null;
    }
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      String json = aggregation.toString();
      json = preReplace(json);
      Object object = objectMapper.readValue(json, Object.class);
      if (object instanceof Map) {
        object = ((Map) object).get("pipeline");
        if (object != null) {
          epochObject(object);
          json = objectMapper.writeValueAsString(object);
        }
      }
      return replace(json);
    } catch (Exception e) {
      log.warn(e.getMessage(), e);
      return null;
    }
  }

  private static void epochObject(Object object) {
    if (object == null) {
      return;
    }
    if (object instanceof Map) {
      epochMap((Map) object);
    } else if (object instanceof List) {
      epochList((List) object);
    }
  }

  private static void epochList(List list) {
    if (list == null) {
      return;
    }
    for (Object value : list) {
      epochObject(value);
    }
  }

  private static void epochMap(Map<String, Object> map) {
    if (map == null) {
      return;
    }
    for (Map.Entry<String, Object> entry : map.entrySet()) {
      if (entry.getKey().equals("$date") && entry.getValue() instanceof Long) {
        Date date = new Date((Long) entry.getValue());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        map.put(entry.getKey(), sdf.format(date));
      } else {
        epochObject(entry.getValue());
      }
    }
  }

  private static String preReplace(String mongoScript) {
    mongoScript = mongoScript.replaceAll("\\{[ \"]*\\$java[ \"]*: *([0-9-]+) *}", "{ \"\\$date\" : \"$1T00:00:00.000Z\"}");
    mongoScript = mongoScript.replaceAll("\\{[ \"]*\\$java[ \"]*: *([a-zA-Z][a-zA-Z0-9_]*) *}", "\"$1\"");
    return mongoScript;
  }

  private static String replace(String mongoScript) {
    mongoScript = mongoScript.replaceAll("\\{ *\"\\$date\" *: *\"([0-9TZ:. -]+)\" *}", "ISODate(\"$1\")");
    mongoScript = mongoScript.replaceAll("\\{ *\"\\$oid\" *: *\"([0-9A-Za-z-]+)\" *}", "ObjectId(\"$1\")");
    return mongoScript;
  }
}
