/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.bson.Document;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.InvalidMongoDbApiUsageException;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.swat.core.utils.CoreRtException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * The type Query util.
 */
public class QueryUtil {
  /**
   * And query. Only filter is AND'ed while retaining first query for Fields, Sort, skip, limit.
   *
   * @param queries the queries
   * @return the query. CAUTION:{@link Query#equals(Object)}, {@link Query#hashCode()} and {@link Query#querySettingsEquals(Query)} of returned {@link Query} could not be implemented due to technical limitation.
   */
  public static Query and(Query... queries) {
    return append("$and", queries);
  }

  /**
   * Or query. Only filter is OR'ed while retaining first query for Fields, Sort, skip, limit.
   *
   * @param queries the queries
   * @return the query. CAUTION:{@link Query#equals(Object)}, {@link Query#hashCode()} and {@link Query#querySettingsEquals(Query)} of returned {@link Query} could not be implemented due to technical limitation.
   */
  public static Query or(Query... queries) {
    return append("$or", queries);
  }

  private static Query append(String key, Query... queries) {
    if (ArrayUtils.isEmpty(queries)) {
      return new Query();
    }
    ObjectMapper mapper = new ObjectMapper();
    List<Object> list = new ArrayList<>();
    try {
      Query first = null;
      for (Query query : queries) {
        if (query == null) {
          continue;
        }
        if (first == null) {
          first = query;
        }
        String json = json(query);
        list.add(mapper.readValue(json, Object.class));
      }
      Map<String, List<Object>> andMap = new HashMap<>();
      andMap.put(key, list);
      String json = mapper.writeValueAsString(andMap);
      BasicQuery query = new BasicQueryWrapper(first, json);

      return query;
    } catch (IOException e) {
      throw new CoreRtException(e.getMessage(), e);
    }
  }

  /**
   * Adds the query as in clause. This is upsert and id/_id safe
   * -  If value is not present in query, it appends a $in clause
   * - If value is present in query with same class of values, it checks values.contains(value)
   * - - If true, nothing is added
   * - - If false, throws exception
   * - If value is present in query with different class of values, it creates a $and query
   *
   * @param <T>    the type parameter
   * @param query  the query
   * @param key    the key
   * @param values the values
   * @return the query Returns new Query is query is null Returns query if values is empty
   */
  public static <T> Query inClause(Query query, final String key, T... values) {
    if (query == null) {
      query = new Query();
    }
    if (StringUtils.isBlank(key) || ArrayUtils.isEmpty(values)) {
      return query;
    }
    ObjectMapper mapper = new ObjectMapper();
    Map<String, Object> map = mapper.convertValue(query.getQueryObject(), Map.class);
    Object value = map.get(key);
    if (value == null) {
      if (key.equals("id")) {
        value = map.get("_id");
      } else if (key.equals("_id")) {
        value = map.get("id");
      }
    }
    if (value != null) {
      if (value.getClass() == values[0].getClass()) {
        if (!ArrayUtils.contains(values, value)) {
          throw new CoreRtException("Provided value for " + key + " is not accessible:" + value);
        }
      } else {
        query = and(query, Query.query(Criteria.where(key).in((Object[]) values)));
      }
    } else {
      query.addCriteria(Criteria.where(key).in((Object[]) values));
    }
    return query;
  }

  /**
   * Json string.
   *
   * @param query the query
   * @return the string
   */
  public static String json(Query query) {
    if (query == null) {
      return null;
    }
    Document document = query.getQueryObject();
    return document.toJson();
  }

  /**
   * Get sort sort.
   *
   * @param fields the fields
   * @param order  the order
   * @return the sort
   */
  public static Sort getSort(String fields, String order) {
    if (fields == null) {
      return null;
    }
    final Sort.Direction direction;
    if (StringUtils.equalsAnyIgnoreCase(order, "ASC", "ASCENDING")) {
      direction = Sort.Direction.ASC;
    } else if (StringUtils.equalsAnyIgnoreCase(order, "DESC", "DESCENDING")) {
      direction = Sort.Direction.DESC;
    } else {
      direction = null;
    }
    Sort sort = null;
    for (String field : fields.split(",")) {
      Sort.Direction dir = Sort.Direction.ASC;
      field = StringUtils.trim(field);
      if (field.startsWith("-")) {
        field = field.replace("-", "").trim();
        dir = Sort.Direction.DESC;
      }
      Sort fieldSort;
      if (direction == null) {
        fieldSort = Sort.by(dir, field);
      } else {
        fieldSort = Sort.by(direction, field);
      }
      if (sort == null) {
        sort = fieldSort;
      } else {
        sort = sort.and(fieldSort);
      }
    }
    return sort;
  }

  /**
   * Add criteria to the query.
   *
   * @param query the query
   * @param key   the key
   * @param value the value
   * @throws InvalidMongoDbApiUsageException if key is existing with a different value in the Query.
   */
  public static void addCriteria(Query query, String key, Object value) throws InvalidMongoDbApiUsageException {
    if (query == null) {
      return;
    }
    if (StringUtils.isNotBlank(key)) {
      try {
        query.addCriteria(Criteria.where(key).is(value));
      } catch (InvalidMongoDbApiUsageException e) {
        Object existing = query.getQueryObject().get(key);
        if (!Objects.equals(existing, value)) {
          throw e;
        }
      }
    }
  }

  /**
   * Search criteria criteria.
   *
   * @param searchText the search text
   * @param fields     the fields
   * @return the criteria
   */
  public static Criteria searchCriteria(String searchText, String... fields) {
    return searchCriteria(false, searchText, fields);
  }

  /**
   * Search criteria criteria.
   *
   * @param aggregation the aggregation
   * @param searchText  the search text
   * @param fields      the fields
   * @return the criteria
   */
  public static Criteria searchCriteria(boolean aggregation, String searchText, String... fields) {
    final boolean isNumber = NumberUtils.isParsable(searchText);
    Criteria criteria = new Criteria();
    if (StringUtils.isNotBlank(searchText)) {
      List<Criteria> ors = new ArrayList<>();
      for (String field : fields) {
        if (StringUtils.isNotBlank(field)) {
          if (isNumber && !aggregation) {
            ors.add(Criteria.where("$where").is(String.format("/.*%s.*/.test(this.%s)", searchText, field)));
          } else {
            ors.add(Criteria.where(field.trim()).regex(searchText, "i"));
          }
        }
      }
      criteria.orOperator(ors.toArray(new Criteria[0]));
    }
    return criteria;
  }
}
