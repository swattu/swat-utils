/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mongo.utils;

import org.bson.Document;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Field;
import org.springframework.data.mongodb.core.query.Meta;
import org.springframework.data.mongodb.core.query.Query;

import java.time.Duration;
import java.util.Set;

/**
 * This class is specifically to support AND/OR multiple {@link Query}'s
 * CAUTION:{@link Query#equals(Object)}, {@link Query#hashCode()} and {@link Query#querySettingsEquals(Query)} could not be overridden due to technical limitation.
 */
public class BasicQueryWrapper extends BasicQuery {
  private final Query query;

  /**
   * Instantiates a new Basic query wrapper.
   *
   * @param first the first
   * @param query the query
   */
  public BasicQueryWrapper(Query first, String query) {
    super(query);
    if (first == null) {
      first = new Query();
    }
    this.query = first;
  }

  @Override
  public Document getFieldsObject() {
    return query.getFieldsObject();
  }

  @Override
  public Document getSortObject() {
    return query.getSortObject();
  }

  @Override
  public Field fields() {
    return query.fields();
  }

  @Override
  public Query limit(int limit) {
    return query.limit(limit);
  }

  @Override
  public Query withHint(String name) {
    return query.withHint(name);
  }

  @Override
  public Query with(Pageable pageable) {
    return query.with(pageable);
  }

  @Override
  public Query with(Sort sort) {
    return query.with(sort);
  }

  @Override
  public Set<Class<?>> getRestrictedTypes() {
    return query.getRestrictedTypes();
  }

  @Override
  public Query restrict(Class<?> type, Class<?>... additionalTypes) {
    return query.restrict(type, additionalTypes);
  }

  @Override
  public long getSkip() {
    return query.getSkip();
  }

  @Override
  public int getLimit() {
    return query.getLimit();
  }

  @Override
  public String getHint() {
    return query.getHint();
  }

  @Override
  public Query maxTimeMsec(long maxTimeMsec) {
    return query.maxTimeMsec(maxTimeMsec);
  }

  @Override
  public Query maxTime(Duration duration) {
    return query.maxTime(duration);
  }

  @Override
  public Query comment(String comment) {
    return query.comment(comment);
  }

  @Override
  public Query noCursorTimeout() {
    return query.noCursorTimeout();
  }

  @Override
  public Query exhaust() {
    return query.exhaust();
  }

  @Override
  public Query allowSecondaryReads() {
    return query.allowSecondaryReads();
  }

  @Override
  public Query partialResults() {
    return query.partialResults();
  }

  @Override
  public Meta getMeta() {
    return query.getMeta();
  }

  @Override
  public void setMeta(Meta meta) {
    query.setMeta(meta);
  }
}
