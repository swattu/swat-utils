/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.apache.tomcat.jdbc.pool;

/**
 * A Hack to get hold of all properties defined in {@link org.apache.tomcat.jdbc.pool.DataSourceFactory}
 *
 * @author Swatantra Agrawal on 20-SEP-2017
 */
public class DataSourceConstants {
  /**
   * @return All configurable properties
   */
  public static String[] getAllProperties() {
    return DataSourceFactory.ALL_PROPERTIES;
  }
}
