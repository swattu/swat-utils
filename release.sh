#!/usr/bin/env bash
RELEASE_TEXT=`git log -1 | grep "prepare for next"`

TYPE=""
SKIP_TEST=""

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

while getopts "h?ftn" opt; do
    case "$opt" in
    h|\?)
        echo "Help: "
        echo "     -h to display this help"
        echo "     -n to normal release"
        echo "     -f to force release"
        echo "     -t to skip tests"
        exit 1
        ;;
    n)
    		TYPE="NORMAL"
        ;;
    f)
    		TYPE="FORCE"
        ;;
    t)
    		SKIP_TEST="SKIP_TEST"
        ;;
    esac
done

shift $((OPTIND-1))

if [[ "${RELEASE_TEXT}" =~ .*"prepare for next development iteration".* ]]
then
	if [[ "${TYPE}" =~ .*"FORCE".* ]]
	then
		echo "The artifact is already released. But releasing it again."
	else
		echo "The artifact is already released"
		exit 1
	fi
else
	if [[ "${TYPE}" =~ .*"NORMAL".* ]]
	then
		echo "This is a normal release."
	else
		echo "Select an option"
		./release.sh -h
		exit 1
	fi
fi

if [[ "${SKIP_TEST}" =~ .*"SKIP_TEST".* ]]
then
	echo "Skipping Tests"
	mvn --batch-mode clean release:prepare release:perform -Prelease -DskipTests -Darguments=-DskipTests
else
	echo "With Tests"
	mvn --batch-mode clean release:prepare release:perform -Prelease
fi

