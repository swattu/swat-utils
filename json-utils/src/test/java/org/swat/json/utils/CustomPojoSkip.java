/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.json.utils;

import lombok.Data;

@Data
public class CustomPojoSkip extends CustomPojo {
  @Override
  public boolean isValid(String key) {
    return supportedKeys().contains(key);
  }
}
