/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.json.utils;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Swatantra Agrawal
 * on 28-AUG-2017
 */
public class Person {
  private String name;
  private int age;
  private Date date;
  @JsonIgnore
  private String ignored;
  private List<Person> children;
  private Map<String, Object> info = new HashMap<String, Object>();

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getIgnored() {
    return ignored;
  }

  public void setIgnored(String ignored) {
    this.ignored = ignored;
  }

  public List<Person> getChildren() {
    return children;
  }

  public void setChildren(List<Person> children) {
    this.children = children;
  }

  @JsonAnyGetter
  public Map<String, Object> getInfo() {
    return info;
  }

  @JsonAnySetter
  public void set(String key, Object value) {
    info.put(key, value);
  }
}
