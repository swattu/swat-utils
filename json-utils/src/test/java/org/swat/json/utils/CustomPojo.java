/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.json.utils;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class CustomPojo extends CustomFields {
  private static final Set<String> supportedKeys = new HashSet<>();

  static {
    supportedKeys.add("color");
    supportedKeys.add("template");
  }

  @Override
  public Set<String> supportedKeys() {
    return supportedKeys;
  }
}
