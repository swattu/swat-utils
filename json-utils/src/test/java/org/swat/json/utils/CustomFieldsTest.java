/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.json.utils;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class CustomFieldsTest {
  @Test
  public void checkSuccess() throws Exception {
    String json = "{'color':'RED'}";
    json = json.replace('\'', '"');
    ObjectMapper mapper = new ObjectMapper();
    CustomPojo pojo = mapper.readValue(json, CustomPojo.class);
    assertEquals("RED", pojo.customField("color"));

    json = mapper.writeValueAsString(pojo);
    json = json.replace('"', '\'');
    assertEquals("{'color':'RED'}", json);
  }

  @Test(expected = JsonMappingException.class)
  public void serializeFailure() throws Exception {
    String json = "{'colour':'RED'}";
    json = json.replace('\'', '"');
    ObjectMapper mapper = new ObjectMapper();
    mapper.readValue(json, CustomPojo.class);
  }

  @Test(expected = JsonMappingException.class)
  public void deserializeFailure() throws Exception {
    CustomPojo pojo = new CustomPojo();

    pojo.customFields().put("colour", "BLUE");

    ObjectMapper mapper = new ObjectMapper();
    mapper.writeValueAsString(pojo);
  }

  @Test
  public void serializeSkip() throws Exception {
    String json = "{'colour':'BLUE', 'color':'RED'}";
    json = json.replace('\'', '"');
    ObjectMapper mapper = new ObjectMapper();
    CustomPojoSkip pojo = mapper.readValue(json, CustomPojoSkip.class);
    assertNull(pojo.customField("colour"));
    assertEquals("RED", pojo.customField("color"));
  }

  @Test
  public void deserializeSkip() throws Exception {
    CustomPojoSkip pojo = new CustomPojoSkip();

    pojo.customFields().put("colour", "BLUE");
    pojo.customFields().put("color", "RED");

    ObjectMapper mapper = new ObjectMapper();
    String json = mapper.writeValueAsString(pojo);
    json = json.replace('"', '\'');
    assertEquals("{'color':'RED'}", json);
  }

  @Test
  public void addToSet() throws Exception {
    CustomPojoSkip pojo = new CustomPojoSkip();
    String key = "key";
    pojo.addToSet(key, "one");
    assertNull(pojo.customField(key));
  }
}
