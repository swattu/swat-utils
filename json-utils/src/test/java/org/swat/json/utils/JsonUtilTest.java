/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.json.utils;

import org.junit.Test;
import org.swat.core.utils.CoreInterceptor;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * @author Swatantra Agrawal
 * on 28-AUG-2017
 */
public class JsonUtilTest {

  private JsonUtil JSON_SILENT = CoreInterceptor.silent(JsonConstants.JSON_UTIL);

  @Test
  public void asObject() throws Exception {
    String json = "{\"name\":\"Agrawal\",\"age\":44}";
    Person person = JSON_SILENT.readObject(json, Person.class);
    assertNotNull(person);
    assertEquals("Agrawal", person.getName());
    assertEquals(44, person.getAge());
  }

  @Test
  public void asList() throws Exception {
    String json = "[{\"name\":\"Agrawal\",\"age\":44}]";
    List<Person> persons = JSON_SILENT.readList(json, Person.class);
    assertNotNull(persons);
    Person person = persons.get(0);
    assertNotNull(person);
    assertEquals("Agrawal", person.getName());
    assertEquals(44, person.getAge());
  }

  @Test
  public void asMap() throws Exception {
    String json = "{\"person\":{\"name\":\"Agrawal\",\"age\":44}}";
    Map<String, Person> personMap = JSON_SILENT.readMap(json, String.class, Person.class);
    assertNotNull(personMap);
    Person person = personMap.get("person");
    assertNotNull(person);
    assertEquals("Agrawal", person.getName());
    assertEquals(44, person.getAge());
  }

  /**
   * A test to show how a flattened json can be converted to a POJO and vice-versa using Jackson
   */
  @Test
  public void anyGetterSetter() {
    Person person = new Person();
    person.setName("Agrawal");
    person.set("Address", "Hyderabad");
    person.setDate(new Date());
    person.setIgnored("Yes");
    String json = JSON_SILENT.toJsonString(person);
    Map<String, Object> map = JSON_SILENT.readMap(json, String.class, Object.class);
    assertEquals("Agrawal", map.get("name"));
    assertEquals("Hyderabad", map.get("Address"));
    assertFalse(map.containsKey("ignored"));
    person = JSON_SILENT.readObject(json, Person.class);
    assertNotNull(person.getInfo());
    assertEquals("Hyderabad", person.getInfo().get("Address"));
    assertNull(person.getIgnored());
  }
}
