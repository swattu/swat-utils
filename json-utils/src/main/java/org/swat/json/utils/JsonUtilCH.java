/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.json.utils;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.CollectionType;
import org.codehaus.jackson.map.type.MapType;
import org.codehaus.jackson.map.type.TypeFactory;
import org.swat.core.utils.CoreRtException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Utilities for Json. In encapsulates ObjectMapper and provides additional method to deal with Json
 * It is immutable
 *
 * @author Swatantra Agrawal on 02-JUN-2018
 */
public class JsonUtilCH implements JsonUtil {
  private final ObjectMapper OBJECT_MAPPER;

  /**
   * Instantiates a new Json util.
   */
  public JsonUtilCH() {
    this(new ObjectMapper());
  }

  /**
   * Instantiates a new Json util ch.
   *
   * @param objectMapper the object mapper
   */
  public JsonUtilCH(ObjectMapper objectMapper) {
    OBJECT_MAPPER = objectMapper;
  }

  @Override
  public <T> T readObject(InputStream jsonStream, Class<T> clazz) throws CoreRtException {
    try {
      return OBJECT_MAPPER.readValue(jsonStream, clazz);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }

  @Override
  public <T> List<T> readList(InputStream stream, Class<T> clazz) throws CoreRtException {
    TypeFactory typeFactory = TypeFactory.defaultInstance();
    CollectionType collectionType = typeFactory.constructCollectionType(ArrayList.class, clazz);
    try {
      return OBJECT_MAPPER.readValue(stream, collectionType);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }

  @Override
  public <T> List<T> readList(String json, Class<T> clazz) throws CoreRtException {
    TypeFactory typeFactory = TypeFactory.defaultInstance();
    CollectionType collectionType = typeFactory.constructCollectionType(ArrayList.class, clazz);
    try {
      return OBJECT_MAPPER.readValue(json, collectionType);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }

  @Override
  public <K, V> Map<K, V> readMap(String json, Class<K> keyClass, Class<V> valueClass) throws CoreRtException {
    TypeFactory typeFactory = TypeFactory.defaultInstance();
    MapType mapType = typeFactory.constructMapType(HashMap.class, keyClass, valueClass);
    try {
      return OBJECT_MAPPER.readValue(json, mapType);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }

  @Override
  public String toJsonString(Object object) throws CoreRtException {
    try {
      return OBJECT_MAPPER.writeValueAsString(object);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }

  public <T> T readObject(String json, Class<T> clazz) throws CoreRtException {
    try {
      return OBJECT_MAPPER.readValue(json, clazz);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }

  public String pretty(Object object) throws CoreRtException {
    try {
      return OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(object);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }
}
