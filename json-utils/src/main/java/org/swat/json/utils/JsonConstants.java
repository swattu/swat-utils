/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.json.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Created by swat
 * on 18/9/17.
 */
public class JsonConstants {
  /**
   * The constant JSON_UTIL. It uses fasterxml
   */
  public static final JsonUtil JSON_UTIL;

  /**
   * The constant JSON_UTIL_CH. It uses codehaus
   */
  public static final JsonUtil JSON_UTIL_CH;

  static {
    JsonUtil temp = null;
    try {
      temp = new JsonUtilFX().
          setSerializationInclusion(JsonInclude.Include.NON_NULL)
          .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true)
          .configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
    } catch (Throwable e) {
    }
    JSON_UTIL = temp;
    temp = null;
    try {
      temp = new JsonUtilCH();
    } catch (Throwable e) {
    }
    JSON_UTIL_CH = temp;
  }
}
