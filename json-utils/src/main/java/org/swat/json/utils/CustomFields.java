/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.json.utils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The type Custom fields. Base implementation of {@link CustomFieldAware}
 */
public class CustomFields implements CustomFieldAware {
  private Map<String, Object> customFields;

  @Override
  public Map<String, Object> customFields() {
    if (customFields == null) {
      customFields = new LinkedHashMap<>();
    }
    return customFields;
  }
}
