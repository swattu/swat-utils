/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.json.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.swat.core.utils.CoreRtException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Utilities for Json. In encapsulates ObjectMapper and provides additional method to deal with Json
 * It is immutable
 *
 * @author Swatantra Agrawal on 27-AUG-2017
 */
public class JsonUtilFX implements JsonUtil {
  private final ObjectMapper OBJECT_MAPPER;

  /**
   * Instantiates a new Json util.
   */
  public JsonUtilFX() {
    OBJECT_MAPPER = new ObjectMapper();
  }

  /**
   * Instantiates a new Json util.
   *
   * @param objectMapper the object mapper
   */
  public JsonUtilFX(ObjectMapper objectMapper) {
    OBJECT_MAPPER = objectMapper.copy();
  }

  /**
   * Configure json util.
   *
   * @param serializationFeature the serialization feature
   * @param state                the state
   * @return the json util
   */
  public JsonUtilFX configure(SerializationFeature serializationFeature, boolean state) {
    JsonUtilFX jsonUtil = new JsonUtilFX(OBJECT_MAPPER);
    jsonUtil.OBJECT_MAPPER.configure(serializationFeature, state);
    return jsonUtil;
  }

  /**
   * Set serialization inclusion json util.
   *
   * @param inclusion the inclusion
   * @return the json util
   */
  public JsonUtilFX setSerializationInclusion(JsonInclude.Include inclusion) {
    JsonUtilFX jsonUtil = new JsonUtilFX(OBJECT_MAPPER);
    jsonUtil.OBJECT_MAPPER.setSerializationInclusion(inclusion);
    return jsonUtil;
  }

  /**
   * Register modules json util.
   *
   * @param modules the modules
   * @return the json util
   */
  public JsonUtil registerModules(Module... modules) {
    JsonUtilFX jsonUtil = new JsonUtilFX(OBJECT_MAPPER);
    jsonUtil.OBJECT_MAPPER.registerModules(modules);
    return jsonUtil;
  }

  @Override
  public <T> T readObject(InputStream jsonStream, Class<T> clazz) throws CoreRtException {
    try {
      return OBJECT_MAPPER.readValue(jsonStream, clazz);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }

  @Override
  public <T> List<T> readList(InputStream stream, Class<T> clazz) throws CoreRtException {
    TypeFactory typeFactory = TypeFactory.defaultInstance();
    CollectionType collectionType = typeFactory.constructCollectionType(ArrayList.class, clazz);
    try {
      return OBJECT_MAPPER.readValue(stream, collectionType);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }

  /**
   * Gets a copy of object mapper.
   *
   * @return the object mapper
   */
  public ObjectMapper getOBJECT_MAPPER() {
    return OBJECT_MAPPER.copy();
  }

  @Override
  public <T> List<T> readList(String json, Class<T> clazz) throws CoreRtException {
    TypeFactory typeFactory = TypeFactory.defaultInstance();
    CollectionType collectionType = typeFactory.constructCollectionType(ArrayList.class, clazz);
    try {
      return OBJECT_MAPPER.readValue(json, collectionType);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }

  @Override
  public <K, V> Map<K, V> readMap(String json, Class<K> keyClass, Class<V> valueClass) throws CoreRtException {
    TypeFactory typeFactory = TypeFactory.defaultInstance();
    MapType mapType = typeFactory.constructMapType(HashMap.class, keyClass, valueClass);
    try {
      return OBJECT_MAPPER.readValue(json, mapType);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }

  @Override
  public String toJsonString(Object object) throws CoreRtException {
    try {
      return OBJECT_MAPPER.writeValueAsString(object);
    } catch (JsonProcessingException e) {
      throw new CoreRtException(e);
    }
  }

  @Override
  public <T> T readObject(String json, Class<T> clazz) throws CoreRtException {
    try {
      return OBJECT_MAPPER.readValue(json, clazz);
    } catch (IOException e) {
      throw new CoreRtException(e);
    }
  }

  @Override
  public String pretty(Object object) throws CoreRtException {
    try {
      return OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(object);
    } catch (JsonProcessingException e) {
      throw new CoreRtException(e);
    }
  }
}
