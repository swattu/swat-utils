/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.json.utils;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import org.swat.core.utils.CoreRtException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static org.swat.core.utils.ObjectUtil.cast;

/**
 * The interface Custom field aware.
 */
public interface CustomFieldAware {
  /**
   * Any getters map.
   *
   * @return the map
   */
  @JsonAnyGetter
  @org.codehaus.jackson.annotate.JsonAnyGetter
  default Map<String, Object> anyGetters() {
    Map<String, Object> customFields = customFields();
    for (String key : new HashSet<>(customFields.keySet())) {
      if (!isValid(key)) {
        customFields.remove(key);
      }
    }
    return customFields;
  }

  /**
   * Custom fields map.
   *
   * @return the map
   */
  Map<String, Object> customFields();

  /**
   * Add to set boolean.
   *
   * @param key   the key
   * @param value the value
   * @return the boolean
   */
  default boolean addToSet(String key, Object value) {
    if (!isValid(key) || value == null) {
      return false;
    }
    Object obj = customFields().get(key);
    if (!(obj instanceof Collection)) {
      obj = new LinkedHashSet<>();
      customFields().put(key, obj);
    }
    Collection<Object> collection = cast(obj);
    return collection.add(value);
  }

  /**
   * Add to list.
   *
   * @param key   the key
   * @param value the value
   */
  default void addToList(String key, Object value) {
    if (!isValid(key) || value == null) {
      return;
    }
    Object obj = customFields().get(key);
    if (!(obj instanceof Collection)) {
      obj = new ArrayList<>();
      customFields().put(key, obj);
    }
    Collection<Object> collection = cast(obj);
    collection.add(value);
  }

  /**
   * Add and get int.
   *
   * @param key   the key
   * @param delta the delta
   * @return the int
   */
  default int addAndGet(String key, int delta) {
    if (!isValid(key)) {
      return 0;
    }
    Object value = customField(key);
    if (!(value instanceof AtomicInteger)) {
      value = new AtomicInteger(0);
      putCustomField(key, value);
    }
    return ((AtomicInteger) value).addAndGet(delta);
  }

  /**
   * Custom field t.
   *
   * @param <T> the type parameter
   * @param key the key
   * @return the t
   */
  default <T> T customField(String key) {
    if (isValid(key)) {
      return cast(customFields().get(key));
    }
    return null;
  }

  /**
   * Put custom field.
   *
   * @param key   the key
   * @param value the value
   */
  @JsonAnySetter
  @org.codehaus.jackson.annotate.JsonAnySetter
  default void putCustomField(String key, Object value) {
    if (isValid(key)) {
      customFields().put(key, value);
    }
  }

  /**
   * Check key.
   *
   * @param key the key
   * @return the boolean
   */
  default boolean isValid(String key) {
    if (supportedKeys() != null && !supportedKeys().contains(key)) {
      throw new CoreRtException(key + " is not supported.");
    }
    return true;
  }

  /**
   * Supported keys set.
   *
   * @return the set
   */
  default Set<String> supportedKeys() {
    return null;
  }
}
