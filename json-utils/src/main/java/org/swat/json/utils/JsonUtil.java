/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.json.utils;

import org.swat.core.utils.CoreRtException;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * The Utilities for Json. In encapsulates ObjectMapper and provides additional method to deal with Json
 * It is immutable
 *
 * @author Swatantra Agrawal on 02-JUN-2018
 */
public interface JsonUtil {
  /**
   * Parses the json to desired type
   *
   * @param <T>        Generics
   * @param jsonStream The input stream
   * @param clazz      The Object type
   * @return The parsed Object.
   * @throws CoreRtException in case of any Exception
   */
  <T> T readObject(InputStream jsonStream, Class<T> clazz) throws CoreRtException;

  /**
   * Parses the json to List of desired type
   *
   * @param <T>    Generics
   * @param stream The JSON stream
   * @param clazz  The Object type
   * @return The parsed List.
   * @throws CoreRtException in case of any Exception
   */
  <T> List<T> readList(InputStream stream, Class<T> clazz) throws CoreRtException;

  /**
   * Parses the json to List of desired type
   *
   * @param <T>   Generics
   * @param json  The JSON
   * @param clazz The Object type
   * @return The parsed List.
   * @throws CoreRtException in case of any Exception
   */
  <T> List<T> readList(String json, Class<T> clazz) throws CoreRtException;

  /**
   * Parses the json to Map of desired type
   *
   * @param <K>        Generics
   * @param <V>        Generics
   * @param json       The JSON
   * @param keyClass   The Key Type
   * @param valueClass The Value Type
   * @return The parsed Map.
   * @throws CoreRtException in case of any Exception
   */
  <K, V> Map<K, V> readMap(String json, Class<K> keyClass, Class<V> valueClass) throws CoreRtException;

  /**
   * Method convert to json string for the input object
   *
   * @param object {@link Object} - The input object
   * @return {@link String} - The output json string
   * @throws CoreRtException in case of any Exception
   */
  String toJsonString(Object object) throws CoreRtException;

  /**
   * Reads json as Object
   *
   * @param <T>   Generics
   * @param json  The json
   * @param clazz The class of Object
   * @return The object
   * @throws CoreRtException in case of any Exception
   */
  <T> T readObject(String json, Class<T> clazz) throws CoreRtException;

  /**
   * Returns prettified json of the object
   *
   * @param object The object
   * @return The prettified json
   * @throws CoreRtException in case of any Exception
   */
  String pretty(Object object) throws CoreRtException;
}
