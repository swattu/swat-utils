# README #

This module is all about utilities. This does not need any deployment.

### What is this repository for? ###

* Contains utilities

### Contribution guidelines ###

* Nearly 100% code coverage is met using JUnit
* Code review is not done. 
* Kindly email for any requests, comments, suggestions

### Documentation ###
* https://www.javadoc.io/doc/org.bitbucket.swattu/json-utils

### Who do I talk to? ###

* Swatantra Agrawal
* swattu@gmail.com (Guaranteed response in a day or so)
* https://www.linkedin.com/in/aazad/