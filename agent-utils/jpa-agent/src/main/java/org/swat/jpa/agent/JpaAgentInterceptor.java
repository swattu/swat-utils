/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.jpa.agent;

import net.bytebuddy.implementation.bind.annotation.RuntimeType;
import net.bytebuddy.implementation.bind.annotation.SuperCall;
import org.swat.jpa.base.EntityManagerFactoryListener;

import javax.persistence.EntityManager;
import java.util.concurrent.Callable;

/**
 * The Interceptor to call {@link EntityManagerFactoryListener}
 */
public class JpaAgentInterceptor {
  /**
   * Calls the {@link EntityManagerFactoryListener}
   *
   * @param callable the callable
   * @return the object
   * @throws Throwable the throwable
   */
  @RuntimeType
  public Object invoke(@SuperCall Callable callable) throws Throwable {
    Object value = callable.call();
    if (value instanceof EntityManager) {
      EntityManagerFactoryListener.afterCreateEntityManager((EntityManager) value);
    }
    return value;
  }
}
