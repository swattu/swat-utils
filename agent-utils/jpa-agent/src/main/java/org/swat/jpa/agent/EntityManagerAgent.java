/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.jpa.agent;

import net.bytebuddy.agent.builder.AgentBuilder;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatcher;
import net.bytebuddy.matcher.ElementMatchers;
import net.bytebuddy.utility.JavaModule;

import javax.persistence.EntityManagerFactory;
import java.lang.instrument.Instrumentation;

/**
 * The Agent to intercept {@link EntityManagerFactory}.
 */
public class EntityManagerAgent {
  /**
   * Premain method to intercept {@link EntityManagerFactory}.
   *
   * @param agentArgs the agent args
   * @param inst      the inst
   * @throws Exception the exception
   */
  public static void premain(String agentArgs, Instrumentation inst) throws Exception {
    //    ElementMatchers.
    ElementMatcher matcher = ElementMatchers.isSubTypeOf(EntityManagerFactory.class);
    AgentBuilder.Identified.Narrowable narrowable = new AgentBuilder.Default().type(matcher);
    narrowable.transform(new AgentBuilder.Transformer() {
      @Override
      public DynamicType.Builder<?> transform(DynamicType.Builder<?> builder, TypeDescription typeDescription,
          ClassLoader classLoader, JavaModule module) {
        ElementMatcher methodMatcher = ElementMatchers.any();
        return builder.method(methodMatcher).intercept(MethodDelegation.to(new JpaAgentInterceptor()));
      }
    }).installOn(inst);
  }
}
