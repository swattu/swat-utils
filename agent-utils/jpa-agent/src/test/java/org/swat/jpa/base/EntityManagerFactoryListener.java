/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.jpa.base;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The listener for {@link EntityManagerFactory}
 */
public class EntityManagerFactoryListener {
  private static final ThreadLocal<AtomicInteger> TL = new ThreadLocal<>();

  /**
   * Called after {@link EntityManagerFactory#createEntityManager}'s are called.
   *
   * @param entityManager The created Entity Manager
   */
  public static void afterCreateEntityManager(EntityManager entityManager) {
    context().incrementAndGet();
  }

  public static AtomicInteger context() {
    AtomicInteger counter = TL.get();
    if (counter == null) {
      counter = new AtomicInteger();
      TL.set(counter);
    }
    return counter;
  }
}
