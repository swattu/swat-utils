/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.jpa.agent;

import org.junit.Test;
import org.swat.jpa.base.EntityManagerFactoryListener;

import static org.junit.Assert.assertEquals;

public class JpaIT {
  @Test
  public void verifyListenerCall() {
    EntityManagerFactoryWrapper emf = new EntityManagerFactoryWrapper();
    assertEquals(0, EntityManagerFactoryListener.context().get());
    emf.createEntityManager();
    assertEquals(1, EntityManagerFactoryListener.context().get());
  }
}
