/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.jpa.agent;import org.swat.jpa.agent.EntityManagerWrapper;

import javax.persistence.Cache;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.Query;
import javax.persistence.SynchronizationType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.metamodel.Metamodel;
import java.util.Map;

public class EntityManagerFactoryWrapper implements EntityManagerFactory {
  @Override
  public EntityManager createEntityManager() {
    return new EntityManagerWrapper();
  }

  @Override
  public EntityManager createEntityManager(Map map) {
    return new EntityManagerWrapper();
  }

  @Override
  public EntityManager createEntityManager(SynchronizationType synchronizationType) {
    return new EntityManagerWrapper();
  }

  @Override
  public EntityManager createEntityManager(SynchronizationType synchronizationType, Map map) {
    return new EntityManagerWrapper();
  }

  @Override
  public CriteriaBuilder getCriteriaBuilder() {
    return null;
  }

  @Override
  public Metamodel getMetamodel() {
    return null;
  }

  @Override
  public boolean isOpen() {
    return false;
  }

  @Override
  public void close() {

  }

  @Override
  public Map<String, Object> getProperties() {
    return null;
  }

  @Override
  public Cache getCache() {
    return null;
  }

  @Override
  public PersistenceUnitUtil getPersistenceUnitUtil() {
    return null;
  }

  @Override
  public void addNamedQuery(String name, Query query) {

  }

  @Override
  public <T> T unwrap(Class<T> cls) {
    return null;
  }

  @Override
  public <T> void addNamedEntityGraph(String graphName, EntityGraph<T> entityGraph) {

  }
}
