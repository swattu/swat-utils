# README #

This module contains a java-agent which intercepts EntityManagerFactory and calls Listener

### What is this repository for? ###

* Specifically useful when one wants to configure EntityManager after creation and before use.

### Configuration ###
* Create a Class org.swat.jpa.base.EntityManagerFactoryListener
* Add a method public static void afterCreateEntityManager(EntityManager entityManager)
* Add -javaagent:<path-to-jpa-agent>.jar when starting java
* Above method will be called whenever EntityManagerFactory.createEntityManager is executed. 

### Who do I talk to? ###

* Swatantra Agrawal
* swattu@gmail.com (Guaranteed response in a day or so)
* https://www.linkedin.com/in/aazad/