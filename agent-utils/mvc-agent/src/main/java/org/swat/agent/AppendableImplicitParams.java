/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.agent;

import io.swagger.annotations.ApiImplicitParam;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The interface Appendable implicit params.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AppendableImplicitParams {
    /**
     * A list of {@link ApiImplicitParam}s available to the API operation.
     *
     * @return the api implicit param [ ]
     */
    ApiImplicitParam[] value();
}
