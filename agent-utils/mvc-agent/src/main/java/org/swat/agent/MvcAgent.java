/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.agent;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import net.bytebuddy.agent.builder.AgentBuilder;
import net.bytebuddy.asm.AsmVisitorWrapper;
import net.bytebuddy.asm.MemberAttributeExtension;
import net.bytebuddy.description.annotation.AnnotationDescription;
import net.bytebuddy.description.annotation.AnnotationList;
import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.description.method.MethodList;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.matcher.ElementMatcher;
import net.bytebuddy.matcher.ElementMatchers;
import net.bytebuddy.utility.JavaModule;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.instrument.Instrumentation;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The type Mvc agent.
 */
public class MvcAgent {
  /**
   * Premain.
   *
   * @param agentArgs the agent args
   * @param inst      the inst
   * @throws Exception the exception
   */
  public static void premain(String agentArgs, Instrumentation inst) throws Exception {
    ElementMatcher.Junction<TypeDescription> matcher = ElementMatchers.isAnnotatedWith(RequestMapping.class);
    AgentBuilder.Identified.Narrowable narrowable = new AgentBuilder.Default().type(matcher);
    narrowable.transform(new AgentBuilder.Transformer() {
      @Override
      public DynamicType.Builder<?> transform(DynamicType.Builder<?> builder, TypeDescription typeDescription,
                                              ClassLoader classLoader, JavaModule module) {
        MethodList<MethodDescription.InDefinedShape> methodList = typeDescription.getDeclaredMethods();
        Iterator<MethodDescription.InDefinedShape> iterator = methodList.iterator();
        while (iterator.hasNext()) {
          MethodDescription.InDefinedShape method = iterator.next();
          AnnotationList annotationList = method.getDeclaredAnnotations();
          AnnotationDescription description = getAnnotationDescription(annotationList);
          if (description != null) {
            AsmVisitorWrapper visitor = new MemberAttributeExtension.ForMethod().annotateMethod(description).on(ElementMatchers.is(method));
            builder = builder.visit(visitor);
          }
        }
        return builder;
      }
    }).installOn(inst);
  }

  private static AnnotationDescription getAnnotationDescription(AnnotationList annotations) {
    AnnotationDescription.Loadable<ApiImplicitParams> apiLoadable = annotations.ofType(ApiImplicitParams.class);
    if (apiLoadable != null) {
      return null;
    }

    final Map<String, ApiImplicitParam> paramMap = new LinkedHashMap<>();
    AnnotationDescription.Loadable<AppendableImplicitParams> appendableLoadable = annotations.ofType(AppendableImplicitParams.class);
    if (appendableLoadable != null) {
      AppendableImplicitParams appendableImplicitParams = appendableLoadable.load();
      Arrays.stream(appendableImplicitParams.value()).forEach(param -> paramMap.putIfAbsent(param.name(), param));
    }

    for (AnnotationDescription annotation : annotations) {
      AnnotationList declaredAnnotations = annotation.getAnnotationType().getDeclaredAnnotations();
      AnnotationDescription.Loadable<ApiImplicitParams> implicitLoadable = declaredAnnotations.ofType(ApiImplicitParams.class);
      if (implicitLoadable != null) {
        ApiImplicitParams baseParams = implicitLoadable.load();
        Arrays.stream(baseParams.value()).forEach(param -> paramMap.putIfAbsent(param.name(), param));
      }
    }
    AnnotationDescription description = AnnotationDescription.Builder.ofType(ApiImplicitParams.class).defineAnnotationArray("value", ApiImplicitParam.class, paramMap.values().toArray(new ApiImplicitParam[0])).build(true);
    return description;
  }
}
