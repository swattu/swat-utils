/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.junit.Test;

import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;

public class DurationLoggerTest {
  @Test
  public void check() throws InterruptedException {
    DurationLoggerTest test = new DurationLoggerTest();
    AtomicInteger counter = new AtomicInteger();
    test = CoreInterceptor.intercept(test, new DurationLogger(1000) {
      @Override
      protected void log(Method method, long durationMillis) {
        counter.incrementAndGet();
        super.log(method, durationMillis);
      }
    });
    assertEquals(0, counter.get());

    test.sleep(500);
    assertEquals(0, counter.get());

    test.sleep(1000);
    assertEquals(1, counter.get());
  }

  public void sleep(long millis) throws InterruptedException {
    Thread.sleep(millis);
  }
}
