/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * @author Swatantra Agrawal
 * on 6/9/17.
 */
public class ArrayUtilTest {
  @Test
  public void asList() throws Exception {
    assertEquals(Arrays.asList(), ArrayUtil.asList(null));
    assertEquals(Arrays.asList(), ArrayUtil.asList(new Object[0]));

    assertEquals(Arrays.asList("ABC"), ArrayUtil.asList("ABC"));

    assertEquals(Arrays.asList("ABC", null, "XYZ"), ArrayUtil.asList(new String[]{"ABC", null, "XYZ"}));

    assertEquals(Arrays.asList(1L, 2L), ArrayUtil.asList(new long[]{1, 2}));
    assertEquals(Arrays.asList(1, 2), ArrayUtil.asList(new int[]{1, 2}));
    assertEquals(Arrays.asList((short) 1, (short) 2), ArrayUtil.asList(new short[]{1, 2}));
    assertEquals(Arrays.asList((char) 1, (char) 2), ArrayUtil.asList(new char[]{1, 2}));
    assertEquals(Arrays.asList((byte) 1, (byte) 2), ArrayUtil.asList(new byte[]{1, 2}));
    assertEquals(Arrays.asList(1D, 2D), ArrayUtil.asList(new double[]{1, 2}));
    assertEquals(Arrays.asList(1F, 2F), ArrayUtil.asList(new float[]{1, 2}));
    assertEquals(Arrays.asList(true, false), ArrayUtil.asList(new boolean[]{true, false}));
  }
}