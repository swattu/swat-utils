/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.swat.core.utils.ObjectUtil.convertValues;
import static org.swat.core.utils.ObjectUtil.defaultValue;

public class ObjectUtilTest {
  @Test
  public void defaultValueTest() {
    assertNull(defaultValue(String.class));
    assertEquals((Object) (byte) 0, defaultValue(byte.class));
    assertEquals((Object) (short) 0, defaultValue(short.class));
    assertEquals((Object) 0, defaultValue(int.class));
    assertEquals((Object) (char) 0, defaultValue(char.class));
    assertEquals((Object) 0L, defaultValue(long.class));
    assertEquals((Object) 0F, defaultValue(float.class));
    assertEquals((Object) 0D, defaultValue(double.class));
    assertEquals(false, defaultValue(boolean.class));
  }

  @Test
  public void convertValueTest() {
    ValueConverter converter = (path, value) -> value;
    assertNull(convertValues(null, converter));
    converter = (path, value) -> {
      if (value instanceof String) {
        return ((String) value).replaceAll("Swat", "Sattu");
      }
      return value;
    };

    Object object = "Swat";
    assertEquals("Sattu", convertValues(object, converter));
    object = new ArrayList<>(Arrays.asList("Swat", "Kshitij"));
    assertEquals(Arrays.asList("Sattu", "Kshitij"), convertValues(object, converter));

    Map<Object, Object> map = new LinkedHashMap<>();
    map.put("name", "Swat");
    object = map;
    map = new LinkedHashMap<>();
    map.put("name", "Sattu");
    assertEquals(map, convertValues(object, converter));

    List<Object> list = new ArrayList<>();
    map = new LinkedHashMap<>();
    map.put("name", "Swat");
    list.add(map);

    object = list;
    list = new ArrayList<>();
    map = new LinkedHashMap<>();
    map.put("name", "Sattu");
    list.add(map);
    assertEquals(list, convertValues(object, converter));

    //Circular Link
    list = new ArrayList<>();
    map = new LinkedHashMap<>();
    list.add(list);
    list.add(map);
    map.put("map", map);
    map.put("list", list);
    list.add("Swat");
    map.put("name", "Swat");

    object = list;
    Object converted = convertValues(object, converter);
    assertEquals(ArrayList.class, converted.getClass());
    assertSame(list, list.get(0));
    assertSame(map, list.get(1));
    assertEquals("Sattu", list.get(2));

    assertSame(map, map.get("map"));
    assertSame(list, map.get("list"));
    assertEquals("Sattu", map.get("name"));
  }
}
