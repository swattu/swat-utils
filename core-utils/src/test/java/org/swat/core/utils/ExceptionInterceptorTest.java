/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Swatantra Agrawal
 * on 05-Sep-2017
 */
public class ExceptionInterceptorTest {
  @Test
  public void check() {
    Exception exception = new Exception("ABC");
    exception = CoreInterceptor.exception(exception, "ABC", "XYZ");
    assertEquals("XYZ", exception.getMessage());
    assertEquals("ABC", exception.getLocalizedMessage());
  }
}