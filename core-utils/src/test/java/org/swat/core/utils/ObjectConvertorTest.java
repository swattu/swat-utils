/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.swat.reflect.utils.ReflectUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.swat.core.utils.ObjectUtil.cast;
import static org.swat.core.utils.ObjectUtil.wrapperClass;

@RunWith(Parameterized.class)
public class ObjectConvertorTest {
  private ObjectConvertor convertor;

  private final LocalDate localDate;
  private final LocalTime localTime;
  private final ZoneId zoneId;

  private final String dateStr;
  private final String timeStr;
  private final String dateTimeStr;

  private final LocalDateTime localDateTime;

  private final ZonedDateTime zonedDateTime;

  private final Date javaDate;
  private final java.sql.Date sqlDate;

  private final Time sqlTime;

  private final Timestamp timestamp;

  private final Calendar calendar;

  private final long epochMillis;

  private final long epochDays;

  private final long dayMillis;

  private final Object[] dateObjects;
  private final Object[] timeObjects;
  private final Object[] dateTimeObjects;
  private final Object[] intObjects;
  private final Object[] decObjects;

  private static final Map<String, Boolean> testMap = new TreeMap<>();

  public ObjectConvertorTest(LocalDate localDate, LocalTime localTime, ZoneId zoneId) {
    this.localDate = localDate;
    this.localTime = localTime;
    this.zoneId = zoneId;
    dateStr = localDate.toString();
    timeStr = localTime.toString();

    localDateTime = LocalDateTime.of(localDate, localTime);
    zonedDateTime = ZonedDateTime.of(localDateTime, zoneId);

    dateTimeStr = dateStr + "T" + timeStr;

    javaDate = new Date(zonedDateTime.toEpochSecond() * 1000);

    sqlTime = Time.valueOf(zonedDateTime.toLocalTime());

    timestamp = new Timestamp(zonedDateTime.toEpochSecond() * 1000);

    calendar = Calendar.getInstance();

    epochMillis = javaDate.getTime();

    epochDays = localDate.toEpochDay();

    dayMillis = localTime.toSecondOfDay() * 1000L;

    sqlDate = java.sql.Date.valueOf(localDate);
    calendar.setTime(javaDate);

    dateObjects = new Object[]{this.dateStr, localDate, sqlDate, epochDays};
    timeObjects = new Object[]{this.timeStr, localTime, sqlTime, dayMillis};
    dateTimeObjects =
        new Object[]{dateTimeStr, localDateTime, zonedDateTime, javaDate, timestamp, calendar, epochMillis};
    intObjects =
        new Object[]{"2", (byte) 2, (short) 2, 2, 2L, new AtomicInteger(2), new AtomicLong(2), BigInteger.valueOf(2)};
    decObjects = new Object[]{"3.1", 3.1F, 3.1D, BigDecimal.valueOf(3.1)};
  }

  @Parameterized.Parameters(name = "{0}T{1} => {2}")
  public static Collection<Object[]> params() {
    Collection<Object[]> list = new ArrayList<>();
    List<ZoneId> zoneIds = new ArrayList<>();
    zoneIds.add(ZoneId.of("UTC"));
    zoneIds.add(ZoneId.of("America/Los_Angeles"));
    zoneIds.add(ZoneId.of("Asia/Kolkata"));
    for (ZoneId zoneId : zoneIds) {
      list.add(new Object[]{LocalDate.of(2022, Month.DECEMBER, 25), LocalTime.of(0, 15, 20), zoneId});
      list.add(new Object[]{LocalDate.of(2022, Month.DECEMBER, 25), LocalTime.of(12, 15, 20), zoneId});
      list.add(new Object[]{LocalDate.of(2022, Month.DECEMBER, 25), LocalTime.of(23, 15, 20), zoneId});
    }
    return list;
  }

  @BeforeClass
  public static void beforeClass() {
    testMap.clear();
  }

  @AfterClass
  public static void afterClass() {
    AtomicInteger counter = new AtomicInteger();
    testMap.forEach((k, v) -> {
      if (!v) {
        System.out.println(counter.incrementAndGet() + " " + k);
      }
    });
  }

  @Before
  public void before() {
    convertor = ObjectConvertor.builder().zoneId(zoneId).build();
  }

  @Test
  public void toByte() {
    check((byte) 0, byte.class, (Object) null);
    check((byte) 2, byte.class, intObjects);
    checkSingle((byte) 3, byte.class, decObjects);
  }

  @Test
  public void toShort() {
    check((short) 0, short.class, (Object) null);
    check((short) 2, short.class, intObjects);
    checkSingle((short) 3, short.class, decObjects);
  }

  @Test
  public void toInt() {
    check(0, int.class, (Object) null);
    check(2, int.class, intObjects);
    checkSingle(3, int.class, decObjects);
  }

  @Test
  public void toLong() {
    check((long) 0, long.class, (Object) null);
    check(2L, long.class, intObjects);
    checkSingle(3L, long.class, decObjects);

    check(epochMillis, long.class, ArrayUtils.removeElements(dateTimeObjects, dateTimeStr));
    check(epochDays, long.class, ArrayUtils.removeElements(dateObjects, dateStr));
    check(dayMillis, long.class, ArrayUtils.removeElements(timeObjects, timeStr));
  }

  @Test
  public void toFloat() {
    check((float) 0, float.class, (Object) null);
    checkSingle(2F, float.class, intObjects);
    checkSingle(3.1F, float.class, decObjects);
  }

  @Test
  public void toDouble() {
    check((double) 0, double.class, (Object) null);
    checkSingle(2D, double.class, intObjects);
    check(3.1D, double.class, decObjects);
  }

  @Test
  public void toBigInteger() {
    check(null, BigInteger.class, (Object) null);
    check(BigInteger.valueOf(2), BigInteger.class, intObjects);
    checkSingle(BigInteger.valueOf(3), BigInteger.class, ArrayUtils.removeElements(decObjects, "3.1"));
  }

  @Test
  public void toBigDecimal() {
    check(null, BigDecimal.class, (Object) null);
    check(BigDecimal.valueOf(1), BigDecimal.class, "1");
    check(BigDecimal.valueOf(2D), BigDecimal.class, (byte) 2);
    check(BigDecimal.valueOf(2D), BigDecimal.class, (short) 2);
    check(BigDecimal.valueOf(2D), BigDecimal.class, 2);
    check(BigDecimal.valueOf(3D), BigDecimal.class, 3L);
    check(BigDecimal.valueOf(3D), BigDecimal.class, 3F);
    check(BigDecimal.valueOf(3D), BigDecimal.class, 3.0);
    check(BigDecimal.valueOf(3D), BigDecimal.class, new AtomicInteger(3));
    check(BigDecimal.valueOf(3D), BigDecimal.class, new AtomicLong(3));
    check(BigDecimal.valueOf(3D), BigDecimal.class, BigInteger.valueOf(3));
    check(BigDecimal.valueOf(3), BigDecimal.class, BigDecimal.valueOf(3));
  }

  @Test
  public void toAtomicInteger() {
    check(null, AtomicInteger.class, (Object) null);
    check(new AtomicInteger(2), AtomicInteger.class, intObjects);
    checkSingle(new AtomicInteger(3), AtomicInteger.class, decObjects);
  }

  @Test
  public void toAtomicLong() {
    check(null, AtomicLong.class, (Object) null);
    check(new AtomicLong(2), AtomicLong.class, intObjects);
    checkSingle(new AtomicLong(3), AtomicLong.class, decObjects);
  }

  @Test
  public void toBoolean() {
    check(true, boolean.class, "true");
    check(false, boolean.class, "false");
    checkSingle(false, boolean.class, "abc");
  }

  @Test
  public void toChar() {
    check((char) 0, char.class, (Object) null);
    check('a', char.class, "a");
    exception(char.class, "abc");
  }

  @Test
  public void toLocalDate() {
    check(localDate, LocalDate.class, dateObjects);
    checkSingle(localDate, LocalDate.class, dateTimeObjects);
    exception(LocalDate.class, ArrayUtils.removeElements(timeObjects, timeStr, dayMillis));
  }

  @Test
  public void toLocalTime() {
    check(localTime, LocalTime.class, timeObjects);
    checkSingle(localTime, LocalTime.class, dateTimeObjects);
    exception(LocalTime.class, ArrayUtils.removeElements(dateObjects, dateStr, epochDays));
  }

  @Test
  public void toLocalDateTime() {
    check(localDateTime, LocalDateTime.class, dateTimeObjects);
    exception(LocalDateTime.class, dateObjects);
    exception(LocalDateTime.class, timeObjects);
  }

  @Test
  public void toZonedDateTime() {
    check(zonedDateTime, ZonedDateTime.class, dateTimeObjects);
    exception(ZonedDateTime.class, dateObjects);
    exception(ZonedDateTime.class, timeObjects);
  }

  @Test
  public void toJavaDate() {
    check(javaDate, Date.class, dateTimeObjects);
    exception(Date.class, dateObjects);
    exception(Date.class, timeObjects);
  }

  @Test
  public void toSqlDate() {
    //    check(sqlDate, java.sql.Date.class, dateObjects);
    checkSingle(sqlDate, java.sql.Date.class, dateTimeObjects);
    exception(java.sql.Date.class, ArrayUtils.removeElements(timeObjects, timeStr, dayMillis));
  }

  @Test
  public void toSqlTime() {
    check(sqlTime, Time.class, timeObjects);
    checkSingle(sqlTime, Time.class, dateTimeObjects);
    exception(Time.class, ArrayUtils.removeElements(dateObjects, dateStr, epochDays));
  }

  @Test
  public void toTimestamp() {
    check(timestamp, Timestamp.class, dateTimeObjects);
    exception(Timestamp.class, dateObjects);
    exception(Timestamp.class, timeObjects);
  }

  @Test
  public void toCalendar() {
    check(calendar, Calendar.class, dateTimeObjects);
    exception(Calendar.class, dateObjects);
    exception(Calendar.class, timeObjects);
  }

  @Test
  public void testToString() {
    check("2", String.class, intObjects);
    check("3.1", String.class, decObjects);

    check("1", String.class, '1');
    check("true", String.class, true);

    check(dateStr, String.class, ArrayUtils.removeElements(dateObjects, epochDays));
    check(timeStr, String.class, ArrayUtils.removeElements(timeObjects, dayMillis));
    check(dateTimeStr, String.class, localDateTime);
    check(dateTimeStr, String.class, ArrayUtils.removeElements(dateTimeObjects, localDateTime, epochMillis));
  }

  private <T> void checkSingle(T expected, final Class<T> clazz, final Object... inputs) {
    for (Object input : inputs) {
      checkInternal(expected, clazz, input);
    }
  }

  private <T> void check(T expected, final Class<T> clazz, final Object... inputs) {
    for (Object input : inputs) {
      checkInternal(expected, clazz, input);
      if (input != null) {
        checkInternal(input, cast(input.getClass()), expected);
      }
    }
  }

  private <T> void checkInternal(T expected, Class<T> clazz, Object input) {
    if (clazz == GregorianCalendar.class) {
      clazz = cast(Calendar.class);
    }
    T actual = convertor.convert(input, clazz);
    String message =
        "Z: " + zoneId + ", E: " + getClass(expected) + ", A: " + getClass(actual) + ", I: " + input + "[" +
            getClass(input) + "]";
    if (expected instanceof AtomicInteger && actual instanceof AtomicInteger) {
      assertEquals(message, ((AtomicInteger) expected).get(), ((AtomicInteger) actual).get());
    } else if (expected instanceof AtomicLong && actual instanceof AtomicLong) {
      assertEquals(message, ((AtomicLong) expected).get(), ((AtomicLong) actual).get());
    } else if (expected instanceof Double) {
      assertEquals(message, (Double) expected, (Double) actual, 0.01);
    } else {
      assertEquals(message, expected, actual);
    }
    final Class<T> wrapperClass = wrapperClass(clazz);
    if (input == null) {
      testMap.put(null + " -> " + wrapperClass, true);
    } else {
      testMap.put(wrapperClass + " <- " + superClass(input), true);
      testMap.putIfAbsent(superClass(input) + " <- " + wrapperClass, false);
    }
  }

  private String getClass(Object obj) {
    if (obj == null) {
      return null;
    }
    return obj.getClass().getName();
  }

  private <T> void exception(Class<T> clazz, final Object... inputs) {
    for (Object input : inputs) {
      try {
        T actual = convertor.convert(input, clazz);
        fail(clazz + " <- " + input + "[" + input.getClass() + "] Actual: " + actual);
      } catch (RuntimeException ignore) {
        clazz = wrapperClass(clazz);
        if (input == null) {
          testMap.put(null + " -> " + clazz, true);
        } else {
          testMap.put(clazz + " <- " + superClass(input), true);
          testMap.putIfAbsent(superClass(input) + " <- " + clazz, false);
        }
      }
    }
  }

  private <T> Class<T> superClass(Object value) {
    Class<?> clazz = value.getClass();
    if (ReflectUtils.isAssignable(Calendar.class, clazz)) {
      clazz = Calendar.class;
    }
    return cast(clazz);
  }
}
