/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MultiLevelGetterTest {
  @Test
  public void checkDepthFirst() {
    Information level1 = new Information();
    Information level11 = new Information();
    Information level12 = new Information();
    level1.setObjects(level11, null, level12);
    Information level111 = new Information();
    level11.setObjects(level111);
    level1 = CoreInterceptor.intercept(level1, new MultiLevelGetter() {
      @Override
      protected boolean isValid(String fieldName, Object value) {
        if (StringUtils.equalsAnyIgnoreCase(fieldName, "age") && (Integer) value == 0) {
          return false;
        }
        if (StringUtils.equalsAnyIgnoreCase(fieldName, "working") && !((Boolean) value)) {
          return false;
        }
        return super.isValid(fieldName, value);
      }
    });

    level111.setName("Level111");
    assertEquals("Level111", level1.getName());
    assertEquals(0, level1.getAge());
    assertFalse(level1.isWorking());

    level111.setAge(47);
    assertEquals(47, level1.getAge());
    level111.setWorking(true);
    assertTrue(level1.isWorking());

    level12.setName("Level12");
    assertEquals("Level111", level1.getName());
  }

  @Test
  public void checkBreadthFirst() {
    Information level1 = new Information();
    Information level11 = new Information();
    Information level12 = new Information();
    level1.setObjects(level11, null, level12);
    Information level111 = new Information();
    level11.setObjects(level111);
    level1 = CoreInterceptor.intercept(level1, new MultiLevelGetter(true) {
      @Override
      protected boolean isValid(String fieldName, Object value) {
        if (StringUtils.equalsAnyIgnoreCase(fieldName, "age") && (Integer) value == 0) {
          return false;
        }
        if (StringUtils.equalsAnyIgnoreCase(fieldName, "working") && !((Boolean) value)) {
          return false;
        }
        return super.isValid(fieldName, value);
      }
    });

    level111.setName("Level111");
    assertEquals("Level111", level1.getName());
    assertEquals(0, level1.getAge());
    assertFalse(level1.isWorking());

    level111.setAge(47);
    assertEquals(47, level1.getAge());
    level111.setWorking(true);
    assertTrue(level1.isWorking());

    level12.setName("Level12");
    assertEquals("Level12", level1.getName());
  }

  @Data
  private class Information extends MultiLevelWrapper<Information> {
    private String name;
    private int age;
    private boolean working;
  }
}
