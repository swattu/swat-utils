/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;


import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;

public class DuplicateClassUtilTest {
  @Test
  public void check() {
    String folder = "core-utils/src/test/resources/jar";
    if (!new File(folder).exists()) {
      folder = "../" + folder;
    }
    DuplicateClassUtil util = new DuplicateClassUtil(true, folder);
    assertTrue(util.emitDuplicateInfo());
  }
}