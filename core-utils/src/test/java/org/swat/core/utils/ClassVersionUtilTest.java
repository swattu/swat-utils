/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;

public class ClassVersionUtilTest {
  @Test
  public void check() {
    String folder = "core-utils/src/test/resources/jar";
    if (!new File(folder).exists()) {
      folder = "../" + folder;
    }

    ClassVersionUtil versionUtil = new ClassVersionUtil(folder);
    assertTrue(versionUtil.versionMismatch(0));
  }
}
