/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.backup.utils;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BackupUtilTest {
  private final String source = "backupSource";
  private final String target = "backupTarget";
  private final String version = "backupVersion";

  @After
  @Before
  public void after() {
    FileUtils.deleteQuietly(new File(source));
    FileUtils.deleteQuietly(new File(target));
    FileUtils.deleteQuietly(new File(version));
  }

  @Test
  public void copy() throws Exception {
    BackupUtil util = new BackupUtil(source, target, null);
    String fileName = "a.txt";
    assertFalse(new File(source, fileName).exists());
    assertFalse(new File(target, fileName).exists());
    appendSomething(fileName);
    assertTrue(new File(source, fileName).exists());
    util.copy(false);
    assertTrue(new File(source, fileName).exists());
    assertTrue(new File(target, fileName).exists());
    assertTrue(FileUtils.contentEquals(new File(source, fileName), new File(target, fileName)));
  }

  @Test
  public void move() throws Exception {
    BackupUtil util = new BackupUtil(source, target, null);
    String fileName = "a.txt";
    assertFalse(new File(source, fileName).exists());
    assertFalse(new File(target, fileName).exists());
    appendSomething(fileName);
    assertTrue(new File(source, fileName).exists());
    util.copy(true);
    assertFalse(new File(source, fileName).exists());
    assertTrue(new File(target, fileName).exists());
  }

  @Test
  public void copyWithVersion() throws Exception {
    BackupUtil util = new BackupUtil(source, target, version);
    String fileName = "a.txt";
    assertFalse(new File(source, fileName).exists());
    assertFalse(new File(target, fileName).exists());
    appendSomething(fileName);
    assertTrue(new File(source, fileName).exists());
    util.copy(false);
    assertTrue(new File(source, fileName).exists());
    assertTrue(new File(target, fileName).exists());
    assertTrue(FileUtils.contentEquals(new File(source, fileName), new File(target, fileName)));
    assertFalse(new File(util.getVersion(), fileName).exists());

    appendSomething(fileName);
    assertTrue(new File(source, fileName).exists());
    assertFalse(FileUtils.contentEquals(new File(source, fileName), new File(target, fileName)));
    util.copy(false);
    assertTrue(new File(source, fileName).exists());
    assertTrue(new File(target, fileName).exists());
    assertTrue(FileUtils.contentEquals(new File(source, fileName), new File(target, fileName)));
    assertTrue(new File(util.getVersion(), fileName).exists());
  }

  private void appendSomething(String file) throws Exception {
    FileUtils.writeStringToFile(new File(source, file), UUID.randomUUID().toString(), StandardCharsets.UTF_8, true);
  }

  @Test
  public void dir2File() throws Exception {
    new File(target).createNewFile();
    BackupUtil util = new BackupUtil(source, target, null);
    String fileName = "a.txt";
    assertFalse(new File(source, fileName).exists());
    assertFalse(new File(target, fileName).exists());
    appendSomething(fileName);
    assertTrue(new File(source, fileName).exists());
    util.copy(false);
    assertTrue(new File(source, fileName).exists());
    assertTrue(new File(target, fileName).exists());
    assertTrue(FileUtils.contentEquals(new File(source, fileName), new File(target, fileName)));

    appendSomething(fileName);
    assertTrue(new File(source, fileName).exists());
    assertFalse(FileUtils.contentEquals(new File(source, fileName), new File(target, fileName)));
    util.copy(false);
    assertTrue(new File(source, fileName).exists());
    assertTrue(new File(target, fileName).exists());
    assertTrue(FileUtils.contentEquals(new File(source, fileName), new File(target, fileName)));
  }

  @Test
  public void file2Dir() throws Exception {
    new File(source).createNewFile();
    new File(target).mkdirs();
    BackupUtil util = new BackupUtil(source, target, null);
    assertTrue(new File(source).isFile());
    assertTrue(new File(target).isDirectory());

    util.copy(false);
    assertTrue(new File(source).isFile());
    assertTrue(new File(target).isFile());
    assertTrue(FileUtils.contentEquals(new File(source), new File(target)));
  }
}
