/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.regex.utils;

import org.junit.Test;
import org.swat.core.utils.CoreRtException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.swat.regex.utils.RegexBuilder.*;

public class RegexParserTest {
  @Test
  public void amountTest() {
    List<RegexInfo> infos = new ArrayList<>();
    List<String> lines = Arrays.asList("Positive 5.012", "Negative -5.012", "Text abc");
    RegexParser parser = new RegexParser(lines);

    //Any Amount
    RegexBuilder builder = RegexBuilder.builder();
    builder.append(RegexBuilder.anyChar().name("text"));
    builder.append(RegexBuilder.chars(" "));
    builder.append(RegexBuilder.amount().name("amount"));
    infos.add(builder.exact());
    parser.setRegexInfos(infos);

    List<MatchedRow> rows = parser.parse(true);
    assertEquals(2, rows.size());
    assertEquals("Positive", rows.get(0).get("text"));
    assertEquals(5.012D, rows.get(0).asDouble("amount"), .01);
    assertEquals("Negative", rows.get(1).get("text"));
    assertEquals(-5.012D, rows.get(1).asDouble("amount"), .01);

    //Positive Amount
    infos.clear();
    builder = RegexBuilder.builder();
    builder.append(RegexBuilder.anyChar().name("text"));
    builder.append(RegexBuilder.chars(" "));
    builder.append(RegexBuilder.amount().positiveOnly(true).name("amount"));
    infos.add(builder.exact());
    parser.setRegexInfos(infos);

    rows = parser.parse(true);
    assertEquals(1, rows.size());
    assertEquals("Positive", rows.get(0).get("text"));
    assertEquals(5.012D, rows.get(0).asDouble("amount"), .01);

    //Negative Amount
    infos.clear();
    builder = RegexBuilder.builder();
    builder.append(RegexBuilder.anyChar().name("text"));
    builder.append(RegexBuilder.chars(" "));
    builder.append(RegexBuilder.amount().positiveOnly(false).name("amount"));
    infos.add(builder.exact());
    parser.setRegexInfos(infos);

    rows = parser.parse(true);
    assertEquals(1, rows.size());
    assertEquals("Negative", rows.get(0).get("text"));
    assertEquals(-5.012D, rows.get(0).asDouble("amount"), .01);
  }

  @Test
  public void textTest() {
    List<RegexInfo> infos = new ArrayList<>();
    List<String> lines = Arrays.asList("Positive 5.012", "Negative -5.012", "Text abc");
    RegexParser parser = new RegexParser(lines);

    //Any Amount
    RegexBuilder builder = RegexBuilder.builder();
    builder.append(text("Positive").name("text"));
    infos.add(builder.starts());
    parser.setRegexInfos(infos);

    List<MatchedRow> rows = parser.parse(true);
    assertEquals(1, rows.size());
    assertEquals("Positive", rows.get(0).get("text"));
  }

  @Test
  public void multiLineTest() {
    List<RegexInfo> infos = new ArrayList<>();
    List<String> lines = Arrays.asList("I", "am", "Swat.", "Who", "are", "you?");
    RegexParser parser = new RegexParser(lines);

    //Any Amount
    RegexBuilder builder = RegexBuilder.builder();
    builder.append(text("I am"));
    builder.append(chars(" "));
    builder.append(alpha().name("name"));
    builder.append(chars(". "));
    builder.append(text("who"));
    infos.add(builder.starts());
    parser.setRegexInfos(infos);

    List<MatchedRow> rows = parser.parse(true);
    assertEquals(0, rows.size());

    infos.clear();
    infos.addAll(builder.exact(1, 4));
    rows = parser.parse(true);
    assertEquals(1, rows.size());
    assertEquals("Swat", rows.get(0).get("name"));
  }

  @Test
  public void multiLine2Test() {
    List<RegexInfo> infos = new ArrayList<>();
    List<String> lines = Arrays.asList("Salary 500 600", "Some comments", "Salary 600 600");
    RegexParser parser = new RegexParser(lines);

    //Any Amount
    RegexBuilder builder = RegexBuilder.builder();
    builder.append(text("Salary"));
    builder.append(chars(" "));
    builder.append(amount().name("salary"));
    builder.append(chars(" "));
    builder.append(anyChar());
    infos.addAll(builder.exact(1, 3));
    parser.setRegexInfos(infos);

    List<MatchedRow> rows = parser.parse(true);
    assertEquals(2, rows.size());

    assertEquals("500", rows.get(0).get("salary"));
    assertEquals("600", rows.get(1).get("salary"));
  }

  @Test
  public void monthTest() {
    List<RegexInfo> infos = new ArrayList<>();
    List<String> lines = Arrays.asList("1", "01", "Jan", "January");
    RegexParser parser = new RegexParser(lines);

    //Any Amount
    RegexBuilder builder = RegexBuilder.builder();
    builder.append(months(1).name("month"));
    infos.add(builder.exact());
    parser.setRegexInfos(infos);

    List<MatchedRow> rows = parser.parse(true);
    assertEquals(1, rows.size());
    assertEquals("1", rows.get(0).get("month"));

    infos.clear();
    builder = builder();
    builder.append(months(2).name("month"));
    infos.add(builder.exact());
    rows = parser.parse(true);
    assertEquals(1, rows.size());
    assertEquals("01", rows.get(0).get("month"));

    infos.clear();
    builder = builder();
    builder.append(months(3).name("month"));
    infos.add(builder.exact());
    rows = parser.parse(true);
    assertEquals(1, rows.size());
    assertEquals("Jan", rows.get(0).get("month"));

    infos.clear();
    builder = builder();
    builder.append(months(4).name("month"));
    infos.add(builder.exact());
    rows = parser.parse(true);
    assertEquals(1, rows.size());
    assertEquals("January", rows.get(0).get("month"));
  }

  @Test
  public void ignoredTest() {
    List<RegexInfo> infos = new ArrayList<>();
    List<String> lines = Arrays.asList("One", "Two", "Three");

    RegexBuilder builder = builder();
    builder.append(text("Two"));
    builder.ignored();
    infos.add(builder.exact());

    builder = builder();
    builder.append(anyChar().name("text"));
    infos.add(builder.exact());

    RegexParser parser = new RegexParser(lines);
    parser.setRegexInfos(infos);
    List<MatchedRow> rows = parser.parse(true);
    assertEquals(2, rows.size());
    assertEquals("One", rows.get(0).get("text"));
    assertEquals("Three", rows.get(1).get("text"));
  }

  @Test
  public void amountWithDecimalTest() {
    List<RegexInfo> infos = new ArrayList<>();
    List<String> lines = Arrays.asList("1", "2.0", "3.00", "4.", ".", "22");

    RegexBuilder builder = builder();
    builder.append(amount().name("amount"));
    infos.add(builder.exact());

    RegexParser parser = new RegexParser(lines);
    parser.setRegexInfos(infos);
    List<MatchedRow> rows = parser.parse(true);
    assertEquals(5, rows.size());

    infos.clear();
    builder = builder();
    builder.append(amount().decimals(0, 0).name("amount"));
    infos.add(builder.exact());

    rows = parser.parse(true);
    assertEquals(3, rows.size());
    assertEquals("1", rows.get(0).get("amount"));
    assertEquals("4.", rows.get(1).get("amount"));
    assertEquals("22", rows.get(2).get("amount"));

    infos.clear();
    builder = builder();
    builder.append(amount().decimals(0, 1).name("amount"));
    infos.add(builder.exact());

    rows = parser.parse(true);
    assertEquals(4, rows.size());
    assertEquals("1", rows.get(0).get("amount"));
    assertEquals("2.0", rows.get(1).get("amount"));
    assertEquals("4.", rows.get(2).get("amount"));
    assertEquals("22", rows.get(3).get("amount"));

    infos.clear();
    builder = builder();
    builder.append(amount().decimals(1, 1).name("amount"));
    infos.add(builder.exact());

    rows = parser.parse(true);
    assertEquals(1, rows.size());
    assertEquals("2.0", rows.get(0).get("amount"));

    infos.clear();
    builder = builder();
    builder.append(amount().decimals(1, 2).name("amount"));
    infos.add(builder.exact());

    rows = parser.parse(true);
    assertEquals(2, rows.size());
    assertEquals("2.0", rows.get(0).get("amount"));
    assertEquals("3.00", rows.get(1).get("amount"));
  }

  @Test
  public void amountWithIntegerTest() {
    List<RegexInfo> infos = new ArrayList<>();
    List<String> lines = Arrays.asList("1", "22.0", "333.00", "4444.", ".00", ".");

    RegexBuilder builder = builder();
    builder.append(amount().name("amount"));
    infos.add(builder.exact());

    RegexParser parser = new RegexParser(lines);
    parser.setRegexInfos(infos);
    List<MatchedRow> rows = parser.parse(true);
    assertEquals(5, rows.size());

    infos.clear();
    builder = builder();
    builder.append(amount().integers(0, 0).name("amount"));
    infos.add(builder.exact());

    rows = parser.parse(true);
    assertEquals(1, rows.size());
    assertEquals(".00", rows.get(0).get("amount"));

    infos.clear();
    builder = builder();
    builder.append(amount().integers(0, 1).name("amount"));
    infos.add(builder.exact());

    rows = parser.parse(true);
    assertEquals(2, rows.size());
    assertEquals("1", rows.get(0).get("amount"));
    assertEquals(".00", rows.get(1).get("amount"));

    infos.clear();
    builder = builder();
    builder.append(amount().integers(1, 1).name("amount"));
    infos.add(builder.exact());

    rows = parser.parse(true);
    assertEquals(1, rows.size());
    assertEquals("1", rows.get(0).get("amount"));

    infos.clear();
    builder = builder();
    builder.append(amount().integers(1, 2).name("amount"));
    infos.add(builder.exact());

    rows = parser.parse(true);
    assertEquals(2, rows.size());
    assertEquals("1", rows.get(0).get("amount"));
    assertEquals("22.0", rows.get(1).get("amount"));
  }

  @Test(expected = CoreRtException.class)
  public void amountEdge() {
    RegexBuilder builder = builder();
    builder.append(amount().decimals(0).integers(0));
    builder.ends();
  }
}
