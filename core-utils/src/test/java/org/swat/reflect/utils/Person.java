/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.reflect.utils;

import lombok.Data;

@Data
public class Person {
  private String name;
}
