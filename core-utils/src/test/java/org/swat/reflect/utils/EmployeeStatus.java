/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.reflect.utils;

public enum EmployeeStatus {
  ACTIVE, INACTIVE
}
