/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.reflect.utils;

import java.math.BigDecimal;

public class Employee extends Person {
  private int SALARY;
  private String sName;
  private BigDecimal bigDecimal;
  private EmployeeStatus status;

  public int getSALARY() {
    return SALARY;
  }

  public void setSALARY(int SALARY) {
    this.SALARY = SALARY;
  }

  public String getsName() {
    return sName;
  }

  public void setsName(String sName) {
    this.sName = sName;
  }

  public BigDecimal getBigDecimal() {
    return bigDecimal;
  }

  public void setBigDecimal(BigDecimal bigDecimal) {
    this.bigDecimal = bigDecimal;
  }

  public EmployeeStatus getStatus() {
    return status;
  }

  public void setStatus(EmployeeStatus status) {
    this.status = status;
  }

  public BigDecimal getTotal(){
    return bigDecimal;
  }

  public BigDecimal myTotal(){
    return bigDecimal;
  }
}
