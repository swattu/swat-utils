/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.reflect.utils;

import org.junit.Test;
import org.swat.core.utils.CoreRtException;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ReflectUtilsTest {
  @Test
  public void checkGetter() {
    List<FieldInfo> getters = ReflectUtils.getters(Employee.class, 1);
    assertEquals(6, getters.size());
    Employee employee = new Employee();
    employee.setName("Swatantra");
    employee.setSALARY(47);
    employee.setsName("Swat");
    Object value = ReflectUtils.getValue(employee, "name");
    assertEquals("Swatantra", value);
    value = ReflectUtils.getValue(employee, "SALARY");
    assertEquals(47, value);
    value = ReflectUtils.getValue(employee, "sName");
    assertEquals("Swat", value);
  }

  @Test
  public void checkGetterSize() {
    ReflectOption option = new ReflectOption();
    option.setMethod(true);
    List<FieldInfo> getters = ReflectUtils.getters(Employee.class, 1, option);
    assertEquals(7, getters.size());
    Employee employee = new Employee();
    employee.setName("Swatantra");
    employee.setSALARY(47);
    employee.setsName("Swat");
    Object value = ReflectUtils.getValue(employee, "name");
    assertEquals("Swatantra", value);
    value = ReflectUtils.getValue(employee, "SALARY");
    assertEquals(47, value);
    value = ReflectUtils.getValue(employee, "sName");
    assertEquals("Swat", value);
  }
  @Test
  public void checkBigDecimal() {
    Employee employee = new Employee();
    employee.setBigDecimal(BigDecimal.valueOf(23.01));
    Object value = ReflectUtils.getValue(employee, "bigDecimal");
    assertEquals(BigDecimal.valueOf(23.01), value);
    value = ReflectUtils.getValue(employee, "total");
    assertEquals(BigDecimal.valueOf(23.01), value);
    ReflectOption option = new ReflectOption();
    option.setMethod(true);
    value = ReflectUtils.getValue(employee, "myTotal", option);
    assertEquals(BigDecimal.valueOf(23.01), value);
  }

  @Test(expected = CoreRtException.class)
  public void checkException() {
    Employee employee = new Employee();
    Object value = ReflectUtils.getValue(employee, "myTotal");
    assertEquals(BigDecimal.valueOf(23.01), value);
  }
}
