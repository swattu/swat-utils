/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.context.utils;

import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class RequestCacheTest {
  @Test
  public void check() {
    ContextAware.resetAll();
    final String expected = UUID.randomUUID().toString();
    String actual = RequestCache.put("id", expected);
    assertNull(actual);
    actual = RequestCache.put("id", expected);
    assertEquals(expected, actual);
    actual = RequestCache.get("id");
    assertEquals(expected, actual);
    actual = RequestCache.remove("id");
    assertEquals(expected, actual);

    RequestCache.put("id", expected);
    actual = RequestCache.get("id");
    assertEquals(expected, actual);
    ContextAware.resetAll();
    actual = RequestCache.get("id");
    assertNull(actual);
  }
}
