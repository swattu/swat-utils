/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.sort.utils;

import lombok.Data;

/**
 * The type Sort pojo.
 */
@Data
public class SortPojo {
  private String name;
  private Integer age;

  /**
   * Instantiates a new Sort pojo.
   *
   * @param name the name
   * @param age  the age
   */
  public SortPojo(String name, Integer age) {
    this.name = name;
    this.age = age;
  }
}
