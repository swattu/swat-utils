/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.sort.utils;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * The type Sort utils test.
 */
public class SortUtilsTest {
  /**
   * Check sort.
   */
  @Test
  public void checkSort() {
    List<SortPojo> pojos = new ArrayList<>();

    SortUtils.sort(pojos, false, "name", "age");
    pojos.add(new SortPojo("B", 10));
    SortUtils.sort(pojos, false, "name", "age");

    pojos.add(new SortPojo("a", 5));
    SortUtils.sort(pojos, false, "name", "age");
    SortPojo pojo = pojos.get(0);
    assertEquals(new Integer(10), pojo.getAge());

    SortUtils.sort(pojos, true, "name", "age");
    pojo = pojos.get(0);
    assertEquals(new Integer(5), pojo.getAge());

    SortUtils.sort(pojos, false, true, "name", "age");
    pojo = pojos.get(0);
    assertEquals(new Integer(5), pojo.getAge());

    pojos.add(new SortPojo(null, 5));
    SortUtils.sort(pojos, false, "name", "age");
    pojo = pojos.get(2);
    assertNull(pojo.getName());

    SortUtils.sort(pojos, true, "name", "age");
    pojo = pojos.get(0);
    assertNull(pojo.getName());

    SortUtils.sort(pojos, false, false, true, "name", "age");
    pojo = pojos.get(0);
    assertNull(pojo.getName());

    SortUtils.sort(pojos, true, false, true, "name", "age");
    pojo = pojos.get(2);
    assertNull(pojo.getName());
  }

  /**
   * Performance.
   */
  @Test
  @Ignore
  public void performance() {
    List<SortPojo> pojos = new ArrayList<>();

    for (int x = 0; x < 10000; x++) {
      pojos.add(new SortPojo(UUID.randomUUID().toString(), 10));
    }
    StopWatch watch = StopWatch.createStarted();
    SortUtils.sort(pojos, "name", "age");
    System.out.println(watch);
  }
}
