/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.date.utils;

import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.Assert.assertEquals;
import static org.swat.date.utils.DateManipulationUtil.add;
import static org.swat.date.utils.DateManipulationUtil.minus;

public class DateManipulationUtilTest {
  @Test
  public void minusManipulation() {
    LocalDate given = LocalDate.of(2020, Month.FEBRUARY, 29);
    LocalDate actual = minus(given, "1Y", false);
    LocalDate expected = LocalDate.of(2019, Month.FEBRUARY, 28);
    assertEquals(expected, actual);

    actual = minus(given, "1M", false);
    expected = LocalDate.of(2020, Month.JANUARY, 29);
    assertEquals(expected, actual);

    actual = minus(given, "1Y 2M 5D", false);
    expected = LocalDate.of(2018, Month.DECEMBER, 23);
    assertEquals(expected, actual);

    actual = minus(given, "-1Y", false);
    expected = LocalDate.of(2021, Month.FEBRUARY, 28);
    assertEquals(expected, actual);
  }

  @Test
  public void plusManipulation() {
    LocalDate given = LocalDate.of(2020, Month.FEBRUARY, 29);
    LocalDate actual = add(given, "1Y", false);
    LocalDate expected = LocalDate.of(2021, Month.FEBRUARY, 28);
    assertEquals(expected, actual);

    actual = add(given, "1M", false);
    expected = LocalDate.of(2020, Month.MARCH, 29);
    assertEquals(expected, actual);

    actual = add(given, "1Y 2M 5D", false);
    expected = LocalDate.of(2021, Month.MAY, 3);
    assertEquals(expected, actual);

    actual = add(given, "-1Y", false);
    expected = LocalDate.of(2019, Month.FEBRUARY, 28);
    assertEquals(expected, actual);
  }

  @Test
  public void checkManipulationFollowEOM() {
    LocalDate given = LocalDate.of(2020, Month.FEBRUARY, 29);
    LocalDate actual = add(given, "1Y", true);
    LocalDate expected = LocalDate.of(2021, Month.FEBRUARY, 28);
    assertEquals(expected, actual);

    actual = add(given, "1M", true);
    expected = LocalDate.of(2020, Month.MARCH, 31);
    assertEquals(expected, actual);

    actual = add(given, "1Y 2M 5D", true);
    expected = LocalDate.of(2021, Month.MAY, 5);
    assertEquals(expected, actual);

    actual = add(given, "-1Y", true);
    expected = LocalDate.of(2019, Month.FEBRUARY, 28);
    assertEquals(expected, actual);
  }
}
