/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.date.utils;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;


public class LocalDateUtilTest {
  @Test
  public void convert() {
    LocalDateUtil localDateUtil = new LocalDateUtil("UTC");
    LocalDate date = localDateUtil.parse("18-10-11 10:30:15.000", "yy-MM-dd HH:mm:ss.SSS");
    assertEquals("11-10-2018", localDateUtil.format(date, "dd-MM-yyyy"));
    date = localDateUtil.getStartOfMonth(date);
    assertEquals("01-10-2018", localDateUtil.format(date, "dd-MM-yyyy"));
    date = localDateUtil.getEndOfMonth(date);
    assertEquals("31-10-2018", localDateUtil.format(date, "dd-MM-yyyy"));
  }

  @Test
  public void parse() {
    LocalDateUtil localDateUtil = new LocalDateUtil("UTC");
    LocalDate date = localDateUtil.parse("18-OCT-11", "yy-MMM-dd");
    assertEquals("11-10-2018", localDateUtil.format(date, "dd-MM-yyyy"));
    date = localDateUtil.parse("18-OCTOBER-11", "yy-MMMM-dd");
    assertEquals("11-10-2018", localDateUtil.format(date, "dd-MM-yyyy"));

    date = localDateUtil.parse("18-oct-11", "yy-MMM-dd");
    assertEquals("11-10-2018", localDateUtil.format(date, "dd-MM-yyyy"));
    date = localDateUtil.parse("18-october-11", "yy-MMMM-dd");
    assertEquals("11-10-2018", localDateUtil.format(date, "dd-MM-yyyy"));

     date = localDateUtil.parse("18-OcT-11", "yy-MMM-dd");
    assertEquals("11-10-2018", localDateUtil.format(date, "dd-MM-yyyy"));
    date = localDateUtil.parse("18-OcToBeR-11", "yy-MMMM-dd");
    assertEquals("11-10-2018", localDateUtil.format(date, "dd-MM-yyyy"));
  }
}
