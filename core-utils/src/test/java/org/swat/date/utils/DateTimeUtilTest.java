/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.date.utils;

import org.junit.Test;

import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;


public class DateTimeUtilTest {
  @Test
  public void convert() {
    DateTimeUtil dateUtil = new DateTimeUtil("UTC");
    ZonedDateTime date = dateUtil.parse("18-10-11 10:30:15.000", "yy-MM-dd HH:mm:ss.SSS");
    assertEquals("11-10-2018 10:30:15.000", dateUtil.format(date, "dd-MM-yyyy HH:mm:ss.SSS"));
    date = dateUtil.getStartOfMonth(date);
    assertEquals("01-10-2018 00:00:00.000", dateUtil.format(date, "dd-MM-yyyy HH:mm:ss.SSS"));
    date = dateUtil.getEndOfMonth(date);
    assertEquals("31-10-2018 23:59:59.999", dateUtil.format(date, "dd-MM-yyyy HH:mm:ss.SSS"));
  }
}