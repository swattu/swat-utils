/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.map.utils;

import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The type Map utils.
 */
public class MapUtils {
  /**
   * Join list.
   *
   * @param <K>        the type parameter
   * @param <V>        the type parameter
   * @param leftList      the left list
   * @param rightList      the right list
   * @param leftOuter  the left
   * @param rightOuter the right
   * @param keys       the keys
   * @return the list
   */
  public static <K, V> List<Map<K, V>> join(List<Map<K, V>> leftList, List<Map<K, V>> rightList, boolean leftOuter,
      boolean rightOuter, K... keys) {
    List<Map<K, V>> list = new ArrayList<>();
    if (leftList == null && rightList == null) {
      return list;
    }
    if (leftList == null) {
      if (rightOuter) {
        return rightList;
      } else {
        return list;
      }
    }
    if (rightList == null) {
      if (leftOuter) {
        return leftList;
      } else {
        return list;
      }
    }

    Map<Map<K, V>, List<Map<K, V>>> leftMap = asMap(leftList, keys);
    Map<Map<K, V>, List<Map<K, V>>> rightMap = asMap(rightList, keys);
    Set<Map<K, V>> allKeys = new LinkedHashSet<>(leftMap.keySet());
    allKeys.addAll(rightMap.keySet());

    for (Map<K, V> key : allKeys) {
      leftList = leftMap.computeIfAbsent(key, v -> new ArrayList<>());
      rightList = rightMap.computeIfAbsent(key, v -> new ArrayList<>());
      if (leftOuter && CollectionUtils.isEmpty(rightList)) {
        list.addAll(leftList);
        continue;
      }
      if (rightOuter && CollectionUtils.isEmpty(leftList)) {
        list.addAll(rightList);
        continue;
      }
      for (Map<K, V> ele1 : leftList) {
        for (Map<K, V> ele2 : rightList) {
          Map<K, V> ele = new LinkedHashMap<>(ele1);
          ele.putAll(ele2);
          list.add(ele);
        }
      }
    }

    return list;
  }

  private static <K, V> Map<Map<K, V>, List<Map<K, V>>> asMap(List<Map<K, V>> list, K... keys) {
    Map<Map<K, V>, List<Map<K, V>>> map = new LinkedHashMap<>();
    for (Map<K, V> element : list) {
      Map<K, V> keyMap = new LinkedHashMap<>();
      for (K key : keys) {
        keyMap.put(key, element.get(key));
      }
      map.computeIfAbsent(keyMap, v -> new ArrayList<>()).add(element);
    }
    return map;
  }

  /**
   * Get map map.
   *
   * @param <K>  the type parameter
   * @param <V>  the type parameter
   * @param map  the map
   * @param keys the keys
   * @return the map
   */
  public static <K, V> Map<K, V> getMap(Map<K, V> map, K... keys) {
    Map<K, V> idMap = new LinkedHashMap<>();
    for (K key : keys) {
      idMap.put(key, map.get(key));
    }
    return idMap;
  }
}
