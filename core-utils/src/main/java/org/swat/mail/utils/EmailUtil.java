/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mail.utils;

/**
 * Utilities for Email
 */
public class EmailUtil {
  /**
   * Without domain string.
   *
   * @param email the email
   * @return the string
   */
  public static String withoutDomain(String email) {
    final String withoutDomain;
    int index = email.indexOf("<");
    String from = email.substring(index + 1);
    if (from.contains("@")) {
      withoutDomain = from.substring(0, from.indexOf("@"));
    } else {
      withoutDomain = from;
    }
    return withoutDomain;
  }
}
