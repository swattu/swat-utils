/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mail.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * The type Gmail util.
 * This class needs following dependencies
 * <BR>
 * com.sun.mail:javax.mail:1.6.0
 * <BR>
 * javax.mail:javax.mail-api:1.6.0
 * <BR>
 *
 * @author Swatantra Agrawal on 18-Sep-2017
 */
public final class GmailUtil implements IEmail {
  private static final String SMTP_HOST_NAME = "smtp.gmail.com";
  private static final String SMTP_PORT = "465";
  private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
  private static final Logger LOGGER = LoggerFactory.getLogger(GmailUtil.class);
  private final String FROM;
  private final String PASSWORD;
  private Session session;

  /**
   * Instantiates a new Gmail util.
   *
   * @param from     the from
   * @param password the password
   */
  public GmailUtil(String from, String password) {
    this.FROM = from;
    this.PASSWORD = password;
  }

  /**
   * Send ssl message boolean.
   *
   * @param recipients the recipients
   * @param subject    the subject
   * @param message    the message
   * @return the boolean
   */
  @Override
  public boolean sendSSLMessage(String[] recipients, String subject, String message) {
    return sendSSLMessage(recipients, subject, message, null);
  }

  /**
   * Send ssl message boolean.
   *
   * @param recipients the recipients
   * @param subject    the subject
   * @param message    the message
   * @param files      the files
   * @return the boolean
   */
  @Override
  public boolean sendSSLMessage(String[] recipients, String subject, String message, File[] files) {
    EmailVO emailVO = new EmailVO();
    emailVO.setTo(recipients);
    emailVO.setSubject(subject);
    emailVO.setBody(message);
    emailVO.setFiles(files);
    return sendSSLMessage(emailVO);
  }

  /**
   * Send ssl message boolean.
   *
   * @param emailVO the email vo
   * @return the boolean
   */
  @Override
  public boolean sendSSLMessage(EmailVO emailVO) {
    boolean status = true;
    try {

      InternetAddress[] addressTo = addEmail(emailVO.getTo());
      InternetAddress[] addressCc = addEmail(emailVO.getCc());
      InternetAddress[] addressBcc = addEmail(emailVO.getBcc());
      InternetAddress addressFrom = new InternetAddress(FROM);
      Session session = getSMTPSession();
      Message msg = new MimeMessage(session);
      msg.setFrom(addressFrom);
      if (addressTo.length > 0) {
        msg.setRecipients(Message.RecipientType.TO, addressTo);
      }
      if (addressCc.length > 0) {
        msg.setRecipients(Message.RecipientType.CC, addressCc);
      }
      if (addressBcc.length > 0) {
        msg.setRecipients(Message.RecipientType.BCC, addressBcc);
      }

      Multipart mp = new MimeMultipart();

      MimeBodyPart textPart = new MimeBodyPart();
      textPart.setContent(emailVO.getBody() + "", emailVO.getContentType());

      mp.addBodyPart(textPart);
      for (File file : emailVO.getFiles()) {
        MimeBodyPart attachFilePart = new MimeBodyPart();
        FileDataSource fds = new FileDataSource(file);
        attachFilePart.setDataHandler(new DataHandler(fds));
        attachFilePart.setFileName(fds.getName());

        mp.addBodyPart(attachFilePart);
      }

      msg.setSubject(emailVO.getSubject());
      msg.setContent(mp);
      if (emailVO.isReadReceipt()) {
        msg.setHeader("Disposition-Notification-To", FROM);
      }
      Transport.send(msg);
      print("TO ", addressTo);
      print("CC ", addressCc);
      print("BCC", addressBcc);

    } catch (Exception e) {
      e.printStackTrace();
      status = false;
    }
    return status;
  }

  private static InternetAddress[] addEmail(String[] emails) throws AddressException {
    List<InternetAddress> list = new ArrayList<>();
    for (int x = 0; x < emails.length; x++) {
      if (emails[x] != null) {
        list.add(new InternetAddress(emails[x]));
        emails[x] = null;
      }
    }
    return list.toArray(new InternetAddress[0]);
  }

  private Session getSMTPSession() {
    if (session != null) {
      return session;
    }
    Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

    Properties props = new Properties();
    props.put("mail.smtp.host", SMTP_HOST_NAME);
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.port", SMTP_PORT);
    props.put("mail.smtp.socketFactory.port", SMTP_PORT);
    props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
    props.put("mail.smtp.socketFactory.fallback", "false");
    props.setProperty("mail.debug", "false");
    session = Session.getInstance(props, new javax.mail.Authenticator() {
      @Override
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(EmailUtil.withoutDomain(FROM), PASSWORD);
      }
    });

    return session;
  }

  private static void print(String type, InternetAddress... addresses) {
    for (InternetAddress address : addresses) {
      LOGGER.debug(type + " " + address.getAddress());
    }
  }
}
