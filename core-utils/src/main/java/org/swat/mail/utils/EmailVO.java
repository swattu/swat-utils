/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mail.utils;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * The type Email vo.
 *
 * @author Swatantra Agrawal on 18-Sep-2017
 */
public class EmailVO implements Serializable {
  private static final long serialVersionUID = -1L;

  private static final String[] STRING = new String[0];

  private String body = "";

  private String[] to = STRING;

  private String[] cc = STRING;

  private String[] bcc = STRING;

  private String subject = "";

  private String contentType = "text/html";

  private File[] files = new File[0];

  private long sendingTime = System.currentTimeMillis();

  private boolean readReceipt;

  /**
   * Instantiates a new Email vo.
   */
  public EmailVO() {
  }

  /**
   * Get to string [ ].
   *
   * @return the string [ ]
   */
  public String[] getTo() {
    return to;
  }

  /**
   * Sets to.
   *
   * @param to the to
   */
  public void setTo(String... to) {
    if (to == null) {
      to = STRING;
    }
    this.to = STRING;
    for (String str : to) {
      addTo(str);
    }
  }

  /**
   * Add to.
   *
   * @param to the to
   */
  public void addTo(String to) {
    if (to == null || to.trim().equals("")) {
      return;
    }
    this.to = append(this.to, to);
  }

  private String[] append(String[] original, String str) {
    List<String> list = new ArrayList<>(Arrays.asList(original));
    list.add(str);
    return list.toArray(STRING);
  }

  /**
   * Get cc string [ ].
   *
   * @return the string [ ]
   */
  public String[] getCc() {
    return cc;
  }

  /**
   * Sets cc.
   *
   * @param cc the cc
   */
  public void setCc(String... cc) {
    if (cc == null) {
      cc = STRING;
    }
    this.cc = STRING;
    for (String str : cc) {
      addCc(str);
    }
  }

  /**
   * Add cc.
   *
   * @param cc the cc
   */
  public void addCc(String cc) {
    if (cc == null || cc.trim().equals("")) {
      return;
    }
    this.cc = append(this.cc, cc);
  }

  /**
   * Get bcc string [ ].
   *
   * @return the string [ ]
   */
  public String[] getBcc() {
    return bcc;
  }

  /**
   * Sets bcc.
   *
   * @param bcc the bcc
   */
  public void setBcc(String... bcc) {
    if (bcc == null) {
      bcc = STRING;
    }
    this.bcc = STRING;
    for (String str : bcc) {
      addBcc(str);
    }
  }

  /**
   * Add bcc.
   *
   * @param bcc the bcc
   */
  public void addBcc(String bcc) {
    if (bcc == null || bcc.trim().equals("")) {
      return;
    }
    this.bcc = append(this.bcc, bcc);
  }

  /**
   * Gets sending time.
   *
   * @return the sending time
   */
  public long getSendingTime() {
    return sendingTime;
  }

  /**
   * Sets sending time.
   *
   * @param sendingTime the sending time
   */
  public void setSendingTime(long sendingTime) {
    this.sendingTime += sendingTime;
  }

  /**
   * Gets content type.
   *
   * @return the content type
   */
  public String getContentType() {
    return contentType;
  }

  /**
   * Sets content type.
   *
   * @param contentType the content type
   */
  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  /**
   * Contains boolean.
   *
   * @param email the email
   * @return the boolean
   */
  public boolean contains(String email) {
    if (email == null || email.trim().equals("")) {
      return false;
    }
    email = email.trim().toLowerCase();
    Set<String> allEmails = new TreeSet<>(Arrays.asList(to));
    allEmails.addAll(Arrays.asList(cc));
    allEmails.addAll(Arrays.asList(bcc));
    for (String id : allEmails) {
      if (id == null) {
        continue;
      }
      if (id.toLowerCase().contains(email)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Copy email vo.
   *
   * @return the email vo
   */
  public EmailVO copy() {
    EmailVO newVO = new EmailVO();
    newVO.setSubject(getSubject());
    newVO.setFiles(getFiles());
    newVO.setBody(getBody());
    return newVO;
  }

  /**
   * Gets subject.
   *
   * @return the subject
   */
  public String getSubject() {
    return this.subject;
  }

  /**
   * Sets subject.
   *
   * @param subject the subject
   */
  public void setSubject(String subject) {
    if (subject == null) {
      return;
    }
    this.subject = subject;
  }

  /**
   * Get files file [ ].
   *
   * @return the file [ ]
   */
  public File[] getFiles() {
    return files;
  }

  /**
   * Gets body.
   *
   * @return the body
   */
  public String getBody() {
    return this.body;
  }

  /**
   * Sets body.
   *
   * @param body the body
   */
  public void setBody(String body) {
    if (body == null) {
      return;
    }
    this.body = body;
  }

  /**
   * Sets files.
   *
   * @param files the files
   */
  public void setFiles(File... files) {
    if (files == null) {
      files = new File[0];
    }
    this.files = files;
  }

  /**
   * Is read receipt boolean.
   *
   * @return the boolean
   */
  public boolean isReadReceipt() {
    return readReceipt;
  }

  /**
   * Sets read receipt.
   *
   * @param readReceipt the read receipt
   */
  public void setReadReceipt(boolean readReceipt) {
    this.readReceipt = readReceipt;
  }
}
