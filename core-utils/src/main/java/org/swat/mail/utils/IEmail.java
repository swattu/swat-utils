/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.mail.utils;

import java.io.File;

/**
 * @author Swatantra Agrawal on 18-Sep-2017
 */
public interface IEmail {
  /**
   * Send ssl message boolean.
   *
   * @param recipients the recipients
   * @param subject    the subject
   * @param message    the message
   * @return the boolean
   */
  boolean sendSSLMessage(String[] recipients, String subject, String message);

  /**
   * Send ssl message boolean.
   *
   * @param recipients the recipients
   * @param subject    the subject
   * @param message    the message
   * @param files      the files
   * @return the boolean
   */
  boolean sendSSLMessage(String[] recipients, String subject, String message, File[] files);

  /**
   * Send ssl message boolean.
   *
   * @param emailVO the email vo
   * @return the boolean
   */
  boolean sendSSLMessage(EmailVO emailVO);
}
