/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.context.utils;

import java.util.HashMap;
import java.util.Map;

import static org.swat.core.utils.ObjectUtil.cast;

/**
 * The type Request cache.
 */
public class RequestCache extends ContextAware {
  private static final ThreadLocal<Map<String, Object>> REQUEST_CACHE = new ThreadLocal<>();

  static {
    register(new RequestCache());
  }

  @Override
  protected void remove() {
    REQUEST_CACHE.remove();
  }

  private static Map<String, Object> context() {
    Map<String, Object> context = REQUEST_CACHE.get();
    if (context == null) {
      context = new HashMap<>();
      REQUEST_CACHE.set(context);
    }
    return context;
  }

  /**
   * Get t.
   *
   * @param <T> the type parameter
   * @param key the key
   * @return the t
   */
  public static <T> T get(String key) {
    return cast(context().get(key));
  }

  /**
   * Contains boolean.
   *
   * @param key the key
   * @return the boolean
   */
  public static boolean contains(String key) {
    return cast(context().containsKey(key));
  }

  /**
   * Put t.
   *
   * @param <T>   the type parameter
   * @param key   the key
   * @param value the value
   * @return the t
   */
  public static <T> T put(String key, T value) {
    return cast(context().put(key, value));
  }

  /**
   * Remove t.
   *
   * @param <T> the type parameter
   * @param key the key
   * @return the t
   */
  public static <T> T remove(String key) {
    return cast(context().remove(key));
  }
}
