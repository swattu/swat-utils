/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.context.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Servlet context.
 */
public class ServletContext extends ContextAware {
  private static final ThreadLocal<ServletInfo> CONTEXT = new ThreadLocal<>();

  @Override
  public void remove() {
    CONTEXT.remove();
  }

  static {
    register(new ServletContext());
  }

  /**
   * Sets context.
   *
   * @param servletInfo the servlet info
   */
  public static void setContext(ServletInfo servletInfo) {
    CONTEXT.set(servletInfo);
  }

  /**
   * Context servlet info.
   *
   * @return the servlet info
   */
  public static ServletInfo context() {
    ServletInfo info = CONTEXT.get();
    if (info == null) {
      info = new ServletInfo();
      setContext(info);
    }
    return info;
  }

  /**
   * Gets parameter.
   *
   * @param key the key
   * @return the parameter
   */
  public static String getParameter(String key) {
    String[] values = context().getParams().get(key);
    if (values != null && values.length > 0) {
      return values[0];
    }
    return null;
  }

  /**
   * Gets value.
   *
   * @param key the key
   * @return the value
   */
  public static String getValue(String key) {
    List<String> values = getValues(key);
    if (values.isEmpty()) {
      return null;
    }
    return values.get(0);
  }

  /**
   * Gets int.
   *
   * @param key          the key
   * @param defaultValue the default value
   * @return the int
   */
  public static int getInt(String key, int defaultValue) {
    return NumberUtils.toInt(getValue(key), defaultValue);
  }

  /**
   * Gets long.
   *
   * @param key          the key
   * @param defaultValue the default value
   * @return the long
   */
  public static long getLong(String key, long defaultValue) {
    return NumberUtils.toLong(getValue(key), defaultValue);
  }

  /**
   * Gets float.
   *
   * @param key          the key
   * @param defaultValue the default value
   * @return the float
   */
  public static float getFloat(String key, float defaultValue) {
    return NumberUtils.toFloat(getValue(key), defaultValue);
  }

  /**
   * Gets double.
   *
   * @param key          the key
   * @param defaultValue the default value
   * @return the double
   */
  public static double getDouble(String key, double defaultValue) {
    return NumberUtils.toDouble(getValue(key), defaultValue);
  }

  /**
   * Gets values.
   *
   * @param key the key
   * @return the values
   */
  public static List<String> getValues(String key) {
    String[] values = context().getParams().get(key);
    if (values == null || values.length == 0) {
      Object value = getAttribute(key);
      if (value instanceof String) {
        values = new String[]{(String) value};
      } else if (value instanceof String[]) {
        values = (String[]) value;
      }
    }
    List<String> list = new ArrayList<>();
    if (values != null) {
      for (String value : values) {
        list.add(StringUtils.trim(value));
      }
    }
    return list;
  }

  /**
   * Sets attribute.
   *
   * @param key   the key
   * @param value the value
   */
  public static void setAttribute(String key, Object value) {
    context().getAttributes().put(key, value);
  }

  /**
   * Gets attribute.
   *
   * @param key the key
   * @return the attribute
   */
  public static Object getAttribute(String key) {
    return context().getAttributes().get(key);
  }
}
