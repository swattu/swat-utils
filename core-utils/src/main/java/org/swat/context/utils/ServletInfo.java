/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.context.utils;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The type Servlet info.
 */
@Data
public class ServletInfo {
  private Map<String, String[]> params;
  private Map<String, Object> attributes;

  /**
   * Gets params.
   *
   * @return the params
   */
  public Map<String, String[]> getParams() {
    if (params == null) {
      params = new LinkedHashMap<>();
    }
    return params;
  }

  /**
   * Gets attributes.
   *
   * @return the attributes
   */
  public Map<String, Object> getAttributes() {
    if (attributes == null) {
      attributes = new LinkedHashMap<>();
    }
    return attributes;
  }
}
