/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.context.utils;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * The type Context aware util.
 */
public abstract class ContextAware {
  private static Set<ContextAware> contexts = new LinkedHashSet<>();

  /**
   * Register.
   *
   * @param contextAware the context aware
   */
  public static void register(ContextAware contextAware) {
    contexts.add(contextAware);
  }

  /**
   * Reset all.
   */
  public static void resetAll() {
    for (ContextAware context : contexts) {
      context.remove();
    }
  }

  /**
   * Reset.
   */
  protected abstract void remove();
}
