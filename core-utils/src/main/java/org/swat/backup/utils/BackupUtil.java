/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.backup.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.swat.core.utils.CoreRtException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * The type Backup util.
 */
public class BackupUtil {
  private final File source;
  private final File target;
  private final File version;
  private String vSuffix = "";

  /**
   * Gets version.
   *
   * @return the version
   */
  public File getVersion() {
    if (version == null) {
      return null;
    }
    return new File(version, vSuffix);
  }

  /**
   * Instantiates a new Backup util.
   *
   * @param source  the source
   * @param target  the target
   * @param version the version
   */
  public BackupUtil(String source, String target, String version) {
    this.source = new File(source);
    this.target = new File(target);

    if (StringUtils.isNotBlank(version)) {
      this.version = new File(version);
    } else {
      this.version = null;
    }
  }

  /**
   * Copy.
   *
   * @param move the move
   */
  public void copy(boolean move) {
    if (version != null) {
      vSuffix = uniqueSuffix();
      copy(source, target, new File(version, vSuffix), move);
    } else {
      copy(source, target, null, move);
    }
  }

  private String uniqueSuffix() {
    final String suffix = versionSuffix();
    File file = new File(version, suffix);
    int counter = 1;
    while (file.exists()) {
      file = new File(version, suffix + "-" + counter++);
    }
    return file.getName();
  }

  /**
   * Version suffix string.
   * By default it is current UTC time in 'yyyy-MM-dd hh:mm:ss' format.
   *
   * @return the string
   */
  protected String versionSuffix() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    return sdf.format(new Date());
  }

  private void copy(final File source, final File target, final File version, final boolean move) {
    if (source.isDirectory() && (!target.exists() || target.isDirectory())) {
      File[] files = source.listFiles();
      if (files != null) {
        for (File file : files) {
          String name = file.getName();
          if (version != null) {
            copy(new File(source, name), new File(target, name), new File(version, name), move);
          } else {
            copy(new File(source, name), new File(target, name), null, move);
          }
        }
        return;
      }
    }
    if (!shouldCopy(source, target)) {
      if (move && target.exists()) {
        boolean status = source.delete();
        if (!status) {
          throw new CoreRtException("Could not delete file " + source);
        }
      }
      return;
    }
    rename(target, version);
    FileUtils.deleteQuietly(target);

    if (move) {
      rename(source, target);
    } else {
      copyFileOrDirectory(source, target);
    }
  }

  private void copyFileOrDirectory(File source, File target) {
    try {
      if (source.isDirectory()) {
        FileUtils.copyDirectory(source, target, true);
      } else {
        FileUtils.copyFile(source, target, true);
      }
    } catch (IOException e) {
      throw new CoreRtException("Error while copying " + source + " to " + target + " : " + e.getMessage(), e);
    }
  }

  private void rename(File source, File target) {
    if (!source.exists() || target == null) {
      return;
    }
    boolean status = target.getParentFile().mkdirs();
    if (!status) {
      throw new CoreRtException("Could not create directory " + target);
    }
    status = source.renameTo(target);
    if (!status) {
      throw new CoreRtException("Could not rename " + source + " to " + target);
    }
  }

  /**
   * Should copy boolean.
   *
   * @param source the source
   * @param target the target
   * @return the boolean
   */
  protected boolean shouldCopy(File source, File target) {
    if (!target.exists()) {
      return true;
    }
    if (source.lastModified() != target.lastModified()) {
      return true;
    }
    if (source.length() != target.length()) {
      return true;
    }
    return false;
  }
}
