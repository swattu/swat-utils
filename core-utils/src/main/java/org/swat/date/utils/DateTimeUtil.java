/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.date.utils;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

/**
 * Utils for Date and TimeZone using java.time package
 */
public class DateTimeUtil {

  private final String timeZone;

  /**
   * Instantiates a new DateTime util.
   *
   * @param timeZone the time zone
   */
  public DateTimeUtil(String timeZone) {
    this.timeZone = timeZone;
  }

  /**
   * Convert DateTime String
   *
   * @param date       the date String from fromFormat to toFormat
   * @param fromFormat the from Pattern
   * @param toFormat   the to Pattern
   * @return the string
   */
  public String convert(String date, String fromFormat, String toFormat) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(toFormat);
    formatter = formatter.withZone(ZoneId.of(timeZone));
    TemporalAccessor dateTime = parse(date, fromFormat);
    return formatter.format(dateTime);
  }

  /**
   * Parse zoned date time.
   *
   * @param date   the date
   * @param format the format
   * @return the zoned date time
   */
  public ZonedDateTime parse(String date, String format) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
    formatter = formatter.withZone(ZoneId.of(timeZone));
    return ZonedDateTime.parse(date, formatter);
  }

  /**
   * Gets start of month.
   *
   * @param date the date
   * @return the start of month
   */
  public ZonedDateTime getStartOfMonth(ZonedDateTime date) {
    return date.withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
  }

  /**
   * Gets end of month.
   *
   * @param date the date
   * @return the end of month
   */
  public ZonedDateTime getEndOfMonth(ZonedDateTime date) {
    return getStartOfMonth(date).plusMonths(1).minusNanos(1);
  }

  /**
   * Format string.
   *
   * @param date   the date
   * @param format the format
   * @return the string
   */
  public String format(ZonedDateTime date, String format) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
    formatter = formatter.withZone(ZoneId.of(timeZone));
    return formatter.format(date);
  }
}
