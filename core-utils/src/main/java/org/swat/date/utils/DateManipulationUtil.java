/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.date.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.swat.core.utils.CoreRtException;

import java.time.LocalDate;

/**
 * The type Date manipulation util.
 */
public class DateManipulationUtil {
  /**
   * Minus local date.
   *
   * @param date      the date
   * @param valueStr  the value str
   * @param followEOM the follow eom
   * @return the local date
   */
  public static LocalDate minus(LocalDate date, String valueStr, boolean followEOM) {
    if (StringUtils.isBlank(valueStr)) {
      return date;
    }
    valueStr = StringUtils.trim(valueStr);
    if (valueStr.startsWith("-")) {
      valueStr = valueStr.substring(1);
    } else {
      valueStr = "-" + valueStr;
    }
    return add(date, valueStr, followEOM);
  }

  /**
   * Add local date.
   *
   * @param date      the date
   * @param valueStr  the value str
   * @param followEOM the follow eom
   * @return the local date
   */
  public static LocalDate add(LocalDate date, final String valueStr, boolean followEOM) {
    if (date == null || StringUtils.isBlank(valueStr)) {
      return date;
    }
    String value = valueStr.toUpperCase().trim();
    int multiple = 1;
    if (value.startsWith("-")) {
      multiple = -1;
      value = value.substring(1);
    }
    if (value.contains("-")) {
      throw new CoreRtException("Multiple - is not allowed:" + valueStr);
    }
    int index = value.indexOf('Y');
    if (index >= 0) {
      String deltaStr = value.substring(0, index).trim();
      int delta = NumberUtils.toInt(deltaStr, Integer.MAX_VALUE);
      if (delta == Integer.MAX_VALUE) {
        throw new CoreRtException("Not a valid year:" + deltaStr);
      }
      value = value.substring(index + 1);
      date = date.plusYears(delta * multiple);
    }
    index = value.indexOf('M');
    boolean lastDayOfMonth = date.getDayOfMonth() == date.lengthOfMonth();

    if (index >= 0) {
      String deltaStr = value.substring(0, index).trim();
      int delta = NumberUtils.toInt(deltaStr, Integer.MAX_VALUE);
      if (delta == Integer.MAX_VALUE) {
        throw new CoreRtException("Not a valid month:" + deltaStr);
      }
      value = value.substring(index + 1);
      date = date.plusMonths(delta * multiple);
    }
    if (followEOM && lastDayOfMonth) {
      date = date.withDayOfMonth(date.lengthOfMonth());
    }
    index = value.indexOf('D');
    if (index >= 0) {
      String deltaStr = value.substring(0, index).trim();
      int delta = NumberUtils.toInt(deltaStr, Integer.MAX_VALUE);
      if (delta == Integer.MAX_VALUE) {
        throw new CoreRtException("Not a valid day:" + deltaStr);
      }
      date = date.plusDays(delta * multiple);
    }
    return date;
  }
}
