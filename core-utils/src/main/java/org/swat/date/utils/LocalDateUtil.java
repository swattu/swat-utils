/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.date.utils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

/**
 * Utils for DateTime and TimeZone using java.time package
 */
public class LocalDateUtil {

  private final String timeZone;

  /**
   * Instantiates a new Date util.
   *
   * @param timeZone the time zone
   */
  public LocalDateUtil(String timeZone) {
    this.timeZone = timeZone;
  }

  /**
   * Convert Date String
   *
   * @param date       the date String from fromFormat to toFormat
   * @param fromFormat the from Pattern
   * @param toFormat   the to Pattern
   * @return the string
   */
  public String convert(String date, String fromFormat, String toFormat) {
    date = DateUtils.normalize(date);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(toFormat);
    formatter = formatter.withZone(ZoneId.of(timeZone));
    TemporalAccessor dateTime = parse(date, fromFormat);
    return formatter.format(dateTime);
  }

  /**
   * Parse local date. Parsing is case insensitive.
   *
   * @param date   the date
   * @param format the format
   * @return the local date
   */
  public LocalDate parse(String date, String format) {
    if (format.contains("MMM")) {
      date = DateUtils.normalize(date);
    }
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
    formatter = formatter.withZone(ZoneId.of(timeZone));
    return LocalDate.parse(date, formatter);
  }

  /**
   * Gets start of month.
   *
   * @param date the date
   * @return the start of month
   */
  public LocalDate getStartOfMonth(LocalDate date) {
    return date.withDayOfMonth(1);
  }

  /**
   * Gets end of month.
   *
   * @param date the date
   * @return the end of month
   */
  public LocalDate getEndOfMonth(LocalDate date) {
    return date.withDayOfMonth(1).plusMonths(1).minusDays(1);
  }

  /**
   * Format string.
   *
   * @param date   the date
   * @param format the format
   * @return the string
   */
  public String format(LocalDate date, String format) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
    formatter = formatter.withZone(ZoneId.of(timeZone));
    return formatter.format(date);
  }
}
