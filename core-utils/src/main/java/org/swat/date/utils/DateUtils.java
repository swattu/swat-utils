/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.date.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * The type Date utils.
 */
public class DateUtils {
  private static final List<String> REPLACEMENTS;

  /**
   * Normalize string.
   *
   * @param dateStr the date str
   * @return the string
   */
  public static String normalize(String dateStr) {
    if (dateStr == null) {
      return dateStr;
    }
    dateStr = dateStr.toLowerCase();
    for (String month : REPLACEMENTS) {
      dateStr = dateStr.replace(month.toLowerCase(), month);
    }
    return dateStr;
  }

  static {
    SimpleDateFormat sdf = new SimpleDateFormat("MMMM");
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.DAY_OF_MONTH, 15);
    List<String> replacements = new ArrayList<>();
    for (int x = Calendar.JANUARY; x <= Calendar.DECEMBER; x++) {
      calendar.set(Calendar.MONTH, x);
      replacements.add(sdf.format(calendar.getTime()));
    }
    sdf = new SimpleDateFormat("MMM");
    for (int x = Calendar.JANUARY; x <= Calendar.DECEMBER; x++) {
      calendar.set(Calendar.MONTH, x);
      replacements.add(sdf.format(calendar.getTime()));
    }
    REPLACEMENTS = Collections.unmodifiableList(replacements);
  }
}
