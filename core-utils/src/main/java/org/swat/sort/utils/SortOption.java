/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.sort.utils;

import lombok.Builder;
import lombok.Getter;

/**
 * The type Sort option.
 */
@Getter
@Builder
public class SortOption {
  private final boolean descending;
  private final boolean caseInsensitive;
  private final boolean nullIsLess;
}
