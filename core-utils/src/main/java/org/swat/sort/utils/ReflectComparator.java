/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.sort.utils;

import org.apache.commons.lang3.StringUtils;
import org.swat.core.utils.CoreRtException;
import org.swat.reflect.utils.CachedReflectUtils;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The type Reflect comparator.
 *
 * @param <T> the type parameter
 */
public class ReflectComparator<T> implements Comparator<T> {
  private final CachedReflectUtils<T> reflectUtils;
  private final LinkedHashMap<String, SortOption> fieldMap;

  /**
   * Instantiates a new Reflect comparator.
   *
   * @param fieldMap the field map
   */
  public ReflectComparator(LinkedHashMap<String, SortOption> fieldMap) {
    this.reflectUtils = new CachedReflectUtils<T>();
    this.fieldMap = fieldMap;
    for (Map.Entry<String, SortOption> entry : fieldMap.entrySet()) {
      if (entry.getValue() == null) {
        fieldMap.put(entry.getKey(), SortOption.builder().build());
      }
    }
  }

  @Override
  public int compare(T first, T second) {
    int status;
    for (Map.Entry<String, SortOption> entry : fieldMap.entrySet()) {
      Object firstValue = reflectUtils.getValue(first, entry.getKey());
      Object secondValue = reflectUtils.getValue(second, entry.getKey());
      SortOption option = entry.getValue();
      if (firstValue == secondValue) {
        continue;
      }
      final int multiplier = option.isDescending() ? -1 : 1;
      if (firstValue == null) {
        return (option.isNullIsLess() ? -1 : 1) * multiplier;
      }
      if (secondValue == null) {
        return (option.isNullIsLess() ? 1 : -1) * multiplier;
      }
      if (option.isCaseInsensitive() && firstValue instanceof CharSequence && secondValue instanceof CharSequence) {
        status = StringUtils.compareIgnoreCase(firstValue.toString(), secondValue.toString());
      } else if (firstValue instanceof Comparable && secondValue instanceof Comparable) {
        status = ((Comparable) firstValue).compareTo(secondValue);
      } else if (!(firstValue instanceof Comparable)) {
        throw new CoreRtException(entry.getKey() + " is not Comparable, and of type " + firstValue.getClass());
      } else {
        throw new CoreRtException(entry.getKey() + " is not Comparable, and of type " + secondValue.getClass());
      }
      if (status != 0) {
        return status * multiplier;
      }
    }
    return 0;
  }
}
