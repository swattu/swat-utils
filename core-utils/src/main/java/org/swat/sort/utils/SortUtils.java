/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.sort.utils;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * The type Sort utils.
 */
public class SortUtils {
  /**
   * Sort.
   *
   * @param <T>     the type parameter
   * @param records the records
   * @param fields  the fields
   */
  public static <T> void sort(List<T> records, String... fields) {
    sort(records, false, fields);
  }

  /**
   * Sort.
   *
   * @param <T>     the type parameter
   * @param records the records
   * @param desc    the desc
   * @param fields  the fields
   */
  public static <T> void sort(List<T> records, boolean desc, String... fields) {
    sort(records, desc, false, fields);
  }

  /**
   * Sort.
   *
   * @param <T>             the type parameter
   * @param records         the records
   * @param desc            the desc
   * @param caseInsensitive the case insensitive
   * @param fields          the fields
   */
  public static <T> void sort(List<T> records, boolean desc, boolean caseInsensitive, String... fields) {
    sort(records, desc, caseInsensitive, false, fields);

  }

  /**
   * Sort.
   *
   * @param <T>             the type parameter
   * @param records         the records
   * @param desc            the desc
   * @param caseInsensitive the case insensitive
   * @param nullsLast       the nulls last
   * @param fields          the fields
   */
  public static <T> void sort(List<T> records, boolean desc, boolean caseInsensitive, boolean nullsLast,
      String... fields) {
    LinkedHashMap<String, SortOption> fieldMap = new LinkedHashMap<>();
    for (String field : fields) {
      SortOption option =
          SortOption.builder().descending(desc).caseInsensitive(caseInsensitive).nullIsLess(nullsLast).build();
      fieldMap.put(field, option);
    }
    sort(records, fieldMap);
  }

  /**
   * Sort.
   *
   * @param <T>      the type parameter
   * @param records  the records
   * @param fieldMap the field map
   */
  public static <T> void sort(List<T> records, LinkedHashMap<String, SortOption> fieldMap) {
    if (records == null || records.isEmpty() || fieldMap == null || fieldMap.isEmpty()) {
      return;
    }
    ReflectComparator<T> comparator = new ReflectComparator<>(fieldMap);
    records.sort(comparator);
  }
}
