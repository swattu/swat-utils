/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.regex.utils;

import lombok.Getter;

/**
 * The type Abstract limit regex.
 *
 * @param <T> the type parameter
 */
@Getter
public abstract class LimitRegex<T extends LimitRegex> extends NamedRegex<T> {
  private int min = 1;
  private int max = -1;

  /**
   * Min t.
   *
   * @param min the min
   * @return the t
   */
  public T min(int min) {
    this.min = min;
    return (T) this;
  }

  /**
   * Max t.
   *
   * @param max the max
   * @return the t
   */
  public T max(int max) {
    this.max = max;
    return (T) this;
  }

  /**
   * Exact t.
   *
   * @param exact the exact
   * @return the t
   */
  public T exact(int exact) {
    this.min = exact;
    this.max = exact;
    return (T) this;
  }

  /**
   * Gets regex.
   *
   * @return the regex
   */
  protected abstract String getRegex();

  public final String getPattern() {
    if (min < 0) {
      min = 0;
    }
    String suffix = "{" + min + ",";
    if (max >= min) {
      suffix += max + "}";
    } else {
      suffix += "}";
    }
    suffix = suffix.replace("{0,}", "*");
    suffix = suffix.replace("{1,}", "+");
    return getRegex() + suffix + "";
  }
}
