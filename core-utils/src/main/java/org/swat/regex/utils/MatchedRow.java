/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.regex.utils;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * The type Matched row.
 */
@Getter
public class MatchedRow {
  private final Map<String, String> row = new LinkedHashMap<>();
  private final String line;
  private final int lineIndex;
  private final int regexIndex;
  private static final String[][] MONTH_ALIASES =
      {{"January", "Jan"}, {"February", "Feb"}, {"March", "Mar"}, {"April", "Apr"}, {"May", "May"}, {"June", "Jun"},
          {"July", "Jul"}, {"August", "Aug"}, {"September", "Sep"}, {"October", "Oct"}, {"November", "Nov"},
          {"December", "Dec"}};

  /**
   * Instantiates a new Matched row.
   *
   * @param row        the row
   * @param line       the line
   * @param lineIndex  the line index
   * @param regexIndex the regex index
   */
  public MatchedRow(Map<String, String> row, String line, int lineIndex, int regexIndex) {
    this.lineIndex = lineIndex;
    this.row.putAll(row);
    this.line = line;
    this.regexIndex = regexIndex;
  }

  /**
   * Get string.
   *
   * @param key the key
   * @return the string
   */
  public String get(String key) {
    return row.get(key);
  }

  /**
   * All keys set.
   *
   * @return the set
   */
  public Set<String> keys() {
    return Collections.unmodifiableSet(row.keySet());
  }

  /**
   * Values map.
   *
   * @return the map
   */
  public Map<String, String> getRow() {
    return Collections.unmodifiableMap(row);
  }

  /**
   * Contains key boolean.
   *
   * @param key the key
   * @return the boolean
   */
  public boolean containsKey(String key) {
    return row.containsKey(key);
  }

  private String sanitizedNumber(String key) {
    String value = row.get(key);
    if (value == null) {
      return value;
    }
    return value.replaceAll(",", "").trim();
  }

  /**
   * As int int.
   *
   * @param key the key
   * @return the int
   */
  public int asInt(String key) {
    return asInt(key, 0);
  }

  /**
   * As int int.
   *
   * @param key          the key
   * @param defaultValue the default value
   * @return the int
   */
  public int asInt(String key, int defaultValue) {
    return NumberUtils.toInt(sanitizedNumber(key), defaultValue);
  }

  /**
   * As long long.
   *
   * @param key the key
   * @return the long
   */
  public long asLong(String key) {
    return asLong(key, 0L);
  }

  /**
   * As long long.
   *
   * @param key          the key
   * @param defaultValue the default value
   * @return the long
   */
  public long asLong(String key, long defaultValue) {
    return NumberUtils.toLong(sanitizedNumber(key), defaultValue);
  }

  /**
   * As float float.
   *
   * @param key the key
   * @return the float
   */
  public float asFloat(String key) {
    return asFloat(key, 0F);
  }

  /**
   * As float float.
   *
   * @param key          the key
   * @param defaultValue the default value
   * @return the float
   */
  public float asFloat(String key, float defaultValue) {
    return NumberUtils.toFloat(sanitizedNumber(key), defaultValue);
  }

  /**
   * As double.
   *
   * @param key the key
   * @return the double
   */
  public double asDouble(String key) {
    return asDouble(key, 0);
  }

  /**
   * As double.
   *
   * @param key          the key
   * @param defaultValue the default value
   * @return the double
   */
  public double asDouble(String key, double defaultValue) {
    return NumberUtils.toDouble(sanitizedNumber(key), defaultValue);
  }

  /**
   * As month int.
   * Error as 0.
   * January as 1.
   *
   * @param key the key
   * @return the int
   */
  public int asMonth(String key) {
    String value = row.get(key);
    if (value == null) {
      return 0;
    }
    value = value.trim();
    for (int x = 0; x < 12; x++) {
      for (String alias : MONTH_ALIASES[x]) {
        if (StringUtils.equalsIgnoreCase(value, alias)) {
          return x + 1;
        }
      }
    }
    return 0;
  }

  /**
   * As big decimal.
   *
   * @param key the key
   * @return the big decimal
   */
  public BigDecimal asBigDecimal(String key) {
    return asBigDecimal(key, BigDecimal.ZERO);
  }

  /**
   * As big decimal.
   *
   * @param key          the key
   * @param defaultValue the default value
   * @return the big decimal
   */
  public BigDecimal asBigDecimal(String key, BigDecimal defaultValue) {
    try {
      return new BigDecimal(sanitizedNumber(key));
    } catch (Exception e) {
      return defaultValue;
    }
  }
}
