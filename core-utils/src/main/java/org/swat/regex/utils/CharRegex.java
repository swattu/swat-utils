/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.regex.utils;

/**
 * The type Char regex.
 */
public class CharRegex extends LimitRegex<CharRegex> {
  private final String chars;
  private String extraChars = "";

  /**
   * Instantiates a new Char regex.
   *
   * @param chars the chars
   */
  public CharRegex(String chars) {
    this.chars = chars;
  }

  /**
   * Extra chars char regex.
   *
   * @param extraChars the extra chars
   * @return the char regex
   */
  public CharRegex extraChars(String extraChars) {
    if (extraChars != null) {
      this.extraChars = extraChars;
    }
    return this;
  }

  @Override
  protected String getRegex() {
    return "[" + chars + extraChars + "]";
  }
}
