/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.regex.utils;

import org.apache.commons.lang3.StringUtils;
import org.swat.core.utils.CoreRtException;

/**
 * The type Char regex.
 */
public class AmountRegex extends NamedRegex<AmountRegex> {
  private Boolean positiveOnly;
  private int minIntegers = 0;
  private int maxIntegers = -1;
  private int minDecimals = 0;
  private int maxDecimals = -1;

  /**
   * Positive only amount regex.
   *
   * @param positiveOnly true for Positive Only, false for Negative only, null for any.
   * @return the amount regex
   */
  public AmountRegex positiveOnly(Boolean positiveOnly) {
    this.positiveOnly = positiveOnly;
    return this;
  }

  /**
   * Integers amount regex.
   *
   * @param exact the exact
   * @return the amount regex
   */
  public AmountRegex integers(int exact) {
    return integers(exact, exact);
  }

  /**
   * Integers amount regex. The count includes Thousand Separator also.
   *
   * @param min the min
   * @param max the max
   * @return the amount regex
   */
  public AmountRegex integers(int min, int max) {
    this.minIntegers = min;
    this.maxIntegers = max;
    return this;
  }

  /**
   * Decimals amount regex.
   *
   * @param exact the exact
   * @return the amount regex
   */
  public AmountRegex decimals(int exact) {
    return decimals(exact, exact);
  }

  /**
   * Decimals amount regex.
   *
   * @param min the min
   * @param max the max
   * @return the amount regex
   */
  public AmountRegex decimals(int min, int max) {
    this.minDecimals = min;
    this.maxDecimals = max;
    return this;
  }

  private void validate() {
    if (minIntegers == 0 && maxIntegers == 0 && minDecimals == 0 && maxDecimals == 0) {
      throw new CoreRtException("All min/max Integers/Decimals cannot be 0.");
    }
    if (minIntegers < 0) {
      minIntegers = 0;
    }
    if (minDecimals < 0) {
      minDecimals = 0;
    }
    if (maxIntegers < 0) {
      maxIntegers = -1;
    } else if (maxIntegers < minIntegers) {
      maxIntegers = minIntegers;
    }
    if (maxDecimals < 0) {
      maxDecimals = -1;
    } else if (maxDecimals < minDecimals) {
      maxDecimals = minDecimals;
    }
  }

  @Override
  public String getPattern() {
    validate();
    String regexLeft =
        "S[0-9,]{" + Math.max(1, minIntegers) + "," + maxIntegers + "}\\.?[0-9]{" + minDecimals + "," + maxDecimals +
            "}";
    if (minDecimals > 0) {
      regexLeft = regexLeft.replaceAll("\\?", "");
    }
    String regexRight =
        "S[0-9,]{" + minIntegers + "," + maxIntegers + "}\\.[0-9]{" + Math.max(1, minDecimals) + "," + maxDecimals +
            "}";
    if (maxIntegers == 0) {
      regexLeft = "";
    }
    if (maxDecimals == 0) {
      regexRight = "";
    }
    String regex = regexLeft;
    if (StringUtils.isNoneBlank(regexLeft, regexRight)) {
      regex += "|";
    }
    regex += regexRight;
    regex = regex.replace("-1", "");
    regex = regex.replace("{0,}", "*");
    regex = regex.replace("{1,}", "+");
    regex = regex.replace("{0,1}", "?");
    if (positiveOnly == null) {
      regex = regex.replaceAll("S", "-?");
    } else if (positiveOnly) {
      regex = regex.replaceAll("S", "");
    } else {
      regex = regex.replaceAll("S", "-");
    }
    return "(" + regex + ")";
  }
}
