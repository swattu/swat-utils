/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.regex.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * The type Regex info.
 */
public class RegexInfo {
  private final int flags;
  private final String regex;
  private final Pattern pattern;
  private final List<String> names = new ArrayList<>();
  private final int linesCount;
  private final boolean ignored;

  /**
   * Instantiates a new Regex info.
   *  @param regex      the regex
   * @param names      the names
   * @param flags      the flags
   * @param ignored Ignored or not
   * @param linesCount lines to match
   */
  public RegexInfo(String regex, List<String> names, int flags, boolean ignored, int linesCount) {
    this.regex = regex;
    this.flags = flags;
    this.ignored = ignored;
    if (linesCount < 1) {
      linesCount = 1;
    }
    this.linesCount = linesCount;
    if (names != null) {
      this.names.addAll(names);
    }
    pattern = Pattern.compile(regex, flags);
  }

  /**
   * Gets names.
   *
   * @return the names
   */
  public List<String> getNames() {
    return new ArrayList<>(names);
  }

  /**
   * Gets regex.
   *
   * @return the regex
   */
  public String getRegex() {
    return regex;
  }

  /**
   * Gets flags.
   *
   * @return the flags
   */
  public int getFlags() {
    return flags;
  }

  /**
   * Gets pattern.
   *
   * @return the pattern
   */
  public Pattern getPattern() {
    return pattern;
  }

  /**
   * Gets lines count.
   *
   * @return the lines count
   */
  public int getLinesCount() {
    return linesCount;
  }

  /**
   * Is ignored boolean.
   *
   * @return the boolean
   */
  public boolean isIgnored() {
    return ignored;
  }
}
