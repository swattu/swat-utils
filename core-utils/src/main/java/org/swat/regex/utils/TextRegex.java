/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.regex.utils;

/**
 * The type Text regex.
 */
public class TextRegex extends NamedRegex<TextRegex> {
  private String regex;

  /**
   * Instantiates a new Text regex.
   *
   * @param regex the regex
   */
  public TextRegex(String regex) {
    this.regex = regex;
  }

  /**
   * Or text regex.
   *
   * @param regex the regex
   * @return the text regex
   */
  public TextRegex or(String regex) {
    this.regex += "|" + regex;
    return this;
  }

  @Override
  public String getPattern() {
    return "(" + regex + ")";
  }
}
