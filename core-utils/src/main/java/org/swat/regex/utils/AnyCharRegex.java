/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.regex.utils;

/**
 * The type Any char regex.
 */
public class AnyCharRegex extends LimitRegex<AnyCharRegex> {
  @Override
  protected String getRegex() {
    return ".";
  }
}
