/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.regex.utils;

import org.swat.core.utils.CoreRtException;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Multi line util.
 */
public class MultiLineUtil {
  private final List<LineInfo> lines = new ArrayList<>();
  private int maxLines = 1;

  /**
   * Append.
   *
   * @param line the line
   */
  public void append(String line) {
    lines.add(0, new LineInfo(line));
    if (lines.size() > maxLines + 10) {
      lines.remove(lines.size() - 1);
    }
  }

  /**
   * Line string.
   *
   * @param linesCount the lines count
   * @return the string
   */
  public String line(int linesCount) {
    if (lines.size() == 0) {
      throw new CoreRtException("Append has to be called before this method.");
    }
    if (linesCount < 0) {
      linesCount = 1;
    }
    if (linesCount == 1) {
      return lines.get(0).line;
    }
    maxLines = Math.max(linesCount, maxLines);
    for (int x = lines.size(); x < maxLines + 10; x++) {
      lines.add(new LineInfo(""));
    }
    StringBuilder line = new StringBuilder();
    for (int x = linesCount - 1; x >= 0; x--) {
      LineInfo lineInfo = lines.get(x);
      if (lineInfo.matched) {
        return "";
      }
      line.append(' ').append(lines.get(x).line);
    }
    if (line.length() > 1) {
      return line.substring(1);
    }
    return line.toString();
  }

  /**
   * Reset.
   */
  public void reset() {
    lines.clear();
    maxLines = 1;
  }

  /**
   * Matched.
   *
   * @param linesCount the lines count
   */
  public void matched(int linesCount) {
    if (linesCount < 0) {
      linesCount = 1;
    }
    maxLines = Math.max(linesCount, maxLines);
    for (int x = linesCount - 1; x >= 0; x--) {
      lines.get(x).matched = true;
    }
  }

  private class LineInfo {
    private final String line;
    /**
     * The Matched.
     */
    boolean matched;

    private LineInfo(String line) {
      this.line = line;
    }
  }
}
