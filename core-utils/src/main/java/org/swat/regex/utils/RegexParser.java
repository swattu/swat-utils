/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.regex.utils;

import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type Regex section.
 */
@Data
public class RegexParser {
  private final List<String> lines;
  private List<RegexInfo> regexInfos;
  private int flags = Pattern.CASE_INSENSITIVE;

  /**
   * Sets regex infos.
   *
   * @param regexInfos the regex infos
   * @return the regex infos
   */
  public RegexParser setRegexInfos(List<RegexInfo> regexInfos) {
    this.regexInfos = regexInfos;
    return this;
  }

  /**
   * Sets regex infos.
   *
   * @param regexInfos the regex infos
   * @return the regex infos
   */
  public RegexParser setRegexInfos(RegexInfo... regexInfos) {
    this.regexInfos = Arrays.asList(regexInfos);
    return this;
  }

  /**
   * Instantiates a new Regex section.
   *
   * @param lines the lines
   */
  public RegexParser(List<String> lines) {
    this.lines = lines;
  }

  /**
   * Parse list.
   *
   * @param trim the trim
   * @return the list
   */
  public List<MatchedRow> parse(boolean trim) {
    List<MatchedRow> rows = new ArrayList<>();
    int lineIndex = 0;
    MultiLineUtil multiLineUtil = new MultiLineUtil();
    for (String line : lines) {
      lineIndex++;
      if (trim) {
        line = line.trim();
      }
      multiLineUtil.append(line);
      int counter = -1;
      for (RegexInfo regexInfo : regexInfos) {
        line = multiLineUtil.line(regexInfo.getLinesCount());
        Pattern pattern = regexInfo.getPattern();
        Matcher matcher = pattern.matcher(line);
        counter++;
        if (matcher.matches()) {
          if (regexInfo.isIgnored()) {
            break;
          }
          Map<String, String> rowMap = new HashMap<>();
          for (String name : regexInfo.getNames()) {
            rowMap.put(name, matcher.group(name));
          }
          MatchedRow row = new MatchedRow(rowMap, line, lineIndex, counter);
          rows.add(row);
          multiLineUtil.matched(regexInfo.getLinesCount());
          break;
        }
      }
    }
    return rows;
  }
}
