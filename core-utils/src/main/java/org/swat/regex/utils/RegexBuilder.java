/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.regex.utils;

import org.apache.commons.lang3.StringUtils;
import org.swat.core.utils.CoreRtException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

/**
 * The type Regex builder.
 */
public class RegexBuilder {
  private final List<String> names = new ArrayList<>();
  private int flags = Pattern.CASE_INSENSITIVE;
  private String regex = "";
  private int linesCount = 1;
  private boolean ignored = false;

  /**
   * Get names of all the Regex.
   *
   * @return the string array
   */
  public String[] getNames() {
    return names.toArray(new String[0]);
  }

  /**
   * Instantiate the Builder
   *
   * @return the regex builder
   */
  public static RegexBuilder builder() {
    return new RegexBuilder();
  }

  /**
   * Gets the Named Regex for the pattern
   *
   * @param pattern the pattern
   * @return the text regex
   */
  public static TextRegex text(String pattern) {
    return new TextRegex(pattern);
  }

  /**
   * Alphanum char regex.
   *
   * @return the char regex
   */
  public static CharRegex alphanum() {
    return new CharRegex("A-Za-z0-9");
  }

  /**
   * Any char char regex.
   *
   * @return the char regex
   */
  public static AnyCharRegex anyChar() {
    return new AnyCharRegex();
  }

  /**
   * Digit char regex.
   *
   * @return the char regex
   */
  public static CharRegex digit() {
    return new CharRegex("0-9");
  }

  /**
   * Alpha char regex.
   *
   * @return the char regex
   */
  public static CharRegex alpha() {
    return new CharRegex("A-Za-z");
  }

  /**
   * Percent char regex.
   *
   * @return the char regex
   */
  public static CharRegex percent() {
    return new CharRegex("0-9.,%");
  }

  /**
   * Chars char regex.
   *
   * @param chars the chars
   * @return the char regex
   */
  public static CharRegex chars(String chars) {
    return new CharRegex(chars);
  }

  /**
   * Numeric char regex.
   *
   * @return the char regex
   */
  public static AmountRegex amount() {
    return new AmountRegex();
  }

  /**
   * Custom regex.
   *
   * @param regex The custom regex
   * @return the char regex
   */
  public static CustomRegex custom(String regex) {
    return new CustomRegex(regex);
  }

  /**
   * Months text regex.
   *
   * @param count The length of Month String
   *            <BR> 1 => 1, 2
   *            <BR> 2 => 01, 02
   *            <BR> 3 => Jan, Feb
   *            <BR> 4 => January, February
   * @return the text regex
   */
  public static TextRegex months(int count) {
    if (count < 1) {
      count = 1;
    }
    if (count > 4) {
      count = 4;
    }
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.DAY_OF_MONTH, 15);
    String format = "";
    for (int x = 0; x < count; x++) {
      format += "M";
    }
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    TextRegex textRegex = null;
    for (int month = Calendar.JANUARY; month <= Calendar.DECEMBER; month++) {
      calendar.set(Calendar.MONTH, month);
      String monthStr = sdf.format(calendar.getTime());
      if (textRegex == null) {
        textRegex = new TextRegex(monthStr);
      } else {
        textRegex.or(monthStr);
      }
    }
    return textRegex;
  }

  /**
   * With lines regex builder.
   *
   * @param linesCount the lines count
   * @return the regex builder
   */
  public RegexBuilder withLines(int linesCount) {
    if (linesCount < 1) {
      linesCount = 1;
    }
    this.linesCount = linesCount;
    return this;
  }

  /**
   * With flags regex builder.
   *
   * @param flags the flags
   * @return the regex builder
   */
  public RegexBuilder withFlags(int flags) {
    this.flags = flags;
    return this;
  }

  /**
   * Exact regex info.
   *
   * @return the regex info
   */
  public RegexInfo exact() {
    return new RegexInfo(regex, names, flags, ignored, linesCount);
  }

  /**
   * Exact regex info.
   *
   * @param minLines the min lines
   * @param maxLines the max lines
   * @return the regex info
   */
  public List<RegexInfo> exact(int minLines, int maxLines) {
    List<RegexInfo> regexInfos = new ArrayList<>();
    for (int x = minLines; x <= maxLines; x++) {
      regexInfos.add(withLines(x).exact());
    }
    return regexInfos;
  }

  /**
   * Starts regex info.
   *
   * @return the regex info
   */
  public RegexInfo starts() {
    return new RegexInfo(regex + ".*", names, flags, ignored, linesCount);
  }

  /**
   * Contains regex info.
   *
   * @return the regex info
   */
  public RegexInfo contains() {
    return new RegexInfo(".*" + regex + ".*", names, flags, ignored, linesCount);
  }

  /**
   * Ends regex info.
   *
   * @return the regex info
   */
  public RegexInfo ends() {
    return new RegexInfo(".*" + regex, names, flags, ignored, linesCount);
  }

  /**
   * Append regex builder.
   *
   * @param namedRegex the regex
   * @return the regex builder
   */
  public RegexBuilder append(NamedRegex<?> namedRegex) {
    RegexInfo info = namedRegex.build();
    return append(info, null);
  }

  /**
   * Append regex builder.
   *
   * @param info the info
   * @param name the name
   * @return the regex builder
   */
  public RegexBuilder append(RegexInfo info, String name) {
    List<String> otherNames = info.getNames();
    if (StringUtils.isNotBlank(name)) {
      if (otherNames.contains(name)) {
        throw new CoreRtException("Duplicate name present:" + name);
      }
      regex += "(?<" + name + ">" + info.getRegex() + ")";
      names.add(name);
    } else {
      regex += info.getRegex();
    }
    names.addAll(info.getNames());
    return this;
  }

  /**
   * Ignored regex builder.
   *
   * @return the regex builder
   */
  public RegexBuilder ignored() {
    this.ignored = true;
    return this;
  }
}
