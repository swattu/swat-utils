/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.regex.utils;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.regex.Pattern;

/**
 * The type Named regex.
 *
 * @param <T> the type parameter
 */
@Getter
public abstract class NamedRegex<T extends NamedRegex> {
  private String name;

  /**
   * Name t.
   *
   * @param name the name
   * @return the t
   */
  public T name(String name) {
    this.name = name;
    return (T) this;
  }

  /**
   * Build regex info.
   *
   * @param pattern the pattern
   * @return the regex info
   */
  public final RegexInfo build() {
    String pattern = getPattern();
    if (StringUtils.isBlank(getName())) {
      return new RegexInfo(pattern, null, Pattern.CASE_INSENSITIVE, false, 1);
    }
    pattern = "(?<" + getName() + ">" + pattern + ")";
    return new RegexInfo(pattern, Collections.singletonList(getName()), Pattern.CASE_INSENSITIVE, false, 1);
  }

  /**
   * Build regex info.
   *
   * @return the regex info
   */
  public abstract String getPattern();
}
