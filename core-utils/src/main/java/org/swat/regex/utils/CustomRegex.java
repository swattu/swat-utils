/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.regex.utils;

/**
 * The type Custom regex.
 */
public class CustomRegex extends NamedRegex<CustomRegex> {
  private final String pattern;

  /**
   * Instantiates a new Custom regex.
   *
   * @param pattern the pattern
   */
  public CustomRegex(String pattern) {
    this.pattern = pattern;
  }

  @Override
  public String getPattern() {
    return pattern;
  }
}
