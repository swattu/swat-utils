/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

/**
 * The interface Value converter.
 */
public interface ValueConverter {
  /**
   * Convert object.
   *
   * @param path  the path
   * @param value the value
   * @return the object
   */
  Object convert(String path, Object value);
}
