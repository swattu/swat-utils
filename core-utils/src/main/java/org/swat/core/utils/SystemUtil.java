/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import lombok.extern.slf4j.Slf4j;

/**
 * The type System util.
 */
@Slf4j
public class SystemUtil {
  private static volatile boolean shutdown;
  private static long shutdownAt;
  private static volatile boolean initialized;

  /**
   * Shutdown.
   */
  public static void shutdown() {
    if (!shutdown) {
      shutdown = true;
      shutdownAt = System.currentTimeMillis();
    }
  }

  /**
   * Init.
   */
  public static void init() {
    init(5000);
  }

  /**
   * Init.
   *
   * @param waitMillis       the wait millis
   */
  public static synchronized void init(int waitMillis) {
    if (initialized) {
      return;
    }
    if (waitMillis > 0) {
      Thread hook = new Thread(() -> {
        shutdown();
        sleep(waitMillis);
      });
      Runtime.getRuntime().addShutdownHook(hook);
      log.info("Shutdown hook added with sleep time as {} mSec", waitMillis);
    }
    if (Thread.getDefaultUncaughtExceptionHandler() == null) {
      Thread.UncaughtExceptionHandler handler = new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread thread, Throwable throwable) {
          log.error(throwable.getMessage(), throwable);
        }
      };
      Thread.setDefaultUncaughtExceptionHandler(handler);
      log.info("Default Uncaught Exception Handler added");
    }
    initialized = true;
  }

  /**
   * Is shutdown boolean.
   *
   * @return the boolean
   */
  public static boolean isShutdown() {
    return shutdown;
  }

  /**
   * Is running boolean.
   *
   * @return the boolean
   */
  public static boolean isRunning() {
    return !shutdown;
  }

  /**
   * Gets shutdown at.
   *
   * @return the shutdown at
   */
  public static long getShutdownAt() {
    return shutdownAt;
  }

  /**
   * Sleep.
   *
   * @param millis the millis
   */
  public static void sleep(long millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }
}
