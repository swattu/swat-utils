/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.aopalliance.intercept.MethodInterceptor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.aop.TargetSource;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.target.SingletonTargetSource;

/**
 * Utility to get Various interceptors
 *
 * @author Swatantra Agrawal on 27-AUG-2017
 */
public class CoreInterceptor {
  /**
   * Returns an intercepted object, which logs all the exception(with trace) thrown by method and swallows it
   *
   * @param <T>    Generics
   * @param object The object to intercept
   * @return The intercepted object
   */
  public static <T> T logged(T object) {
    return logged(object, true);
  }

  /**
   * Returns an intercepted object, which logs all the exception thrown by method and swallows it
   *
   * @param <T>       Generics
   * @param object    The object to intercept
   * @param withTrace true if the stack trace needs to be logged.
   * @return The intercepted object
   */
  public static <T> T logged(T object, boolean withTrace) {
    if (object == null) {
      return null;
    }
    return intercept(object, new LoggedMethodInterceptor(object.getClass(), withTrace));
  }

  /**
   * Intercepts the Object.
   *
   * @param <T>         the type parameter
   * @param object      the object
   * @param interceptor the interceptor
   * @return the intercepted object
   */
  public static <T> T intercept(T object, MethodInterceptor interceptor) {
    if (object == null) {
      return null;
    }
    return intercept(object, interceptor, null);
  }

  /**
   * Intercept t.
   *
   * @param <T>         the type parameter
   * @param object      the object
   * @param interceptor the interceptor
   * @param clazz       the clazz
   * @return the t
   */
  public static <T> T intercept(T object, MethodInterceptor interceptor, Class<T> clazz) {
    if (object == null) {
      return null;
    }
    ProxyFactory proxyFactory = new ProxyFactory();
    proxyFactory.addAdvice(interceptor);
    TargetSource targetSource = new SingletonTargetSource(object) {
      private static final long serialVersionUID = -1L;

      @Override
      public Class<?> getTargetClass() {
        if (clazz == null) {
          return super.getTargetClass();
        }
        return clazz;
      }
    };
    proxyFactory.setTargetSource(targetSource);
    object = (T) proxyFactory.getProxy();
    return object;
  }

  /**
   * Returns an intercepted object, which does not throw any Exception for any of the methods
   *
   * @param <T>    Generics
   * @param object The object to intercept
   * @return The intercepted object
   */
  public static <T> T silent(T object) {
    if (object == null) {
      return null;
    }
    return intercept(object, new SilentMethodInterceptor());
  }

  /**
   * Returns an intercepted object, which modifies the Exception message based on pattern and replaceWith
   *
   * @param <T>         Generics
   * @param object      The object to intercept
   * @param pattern     The regex to find
   * @param replaceWith The regex to replace with
   * @return The intercepted object
   */
  public static <T> T exception(T object, String pattern, String replaceWith) {
    if (object == null) {
      return null;
    }
    if (StringUtils.isAnyBlank(pattern, replaceWith)) {
      return object;
    }
    return intercept(object, new ExceptionInterceptor(pattern, replaceWith));
  }
}
