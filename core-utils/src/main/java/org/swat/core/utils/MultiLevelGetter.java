/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;


import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Null safe setter.
 */
public class MultiLevelGetter implements MethodInterceptor {
  private final boolean breadthFirst;

  /**
   * Instantiates a new Multi level getter.
   */
  public MultiLevelGetter() {
    this(false);
  }

  /**
   * Instantiates a new Multi level getter.
   *
   * @param breadthFirst the breadth first
   */
  public MultiLevelGetter(boolean breadthFirst) {
    this.breadthFirst = breadthFirst;
  }

  @Override
  public Object invoke(MethodInvocation methodInvocation) throws Throwable {
    Method method = methodInvocation.getMethod();
    Object target = methodInvocation.getThis();
    Object response = methodInvocation.proceed();
    if (methodInvocation.getArguments().length == 0) {
      String methodName = method.getName();
      if (methodName.startsWith("get") || methodName.startsWith("is")) {
        String fieldName;
        if (methodName.startsWith("get")) {
          fieldName = methodName.substring(3);
        } else {
          fieldName = methodName.substring(2);
        }
        fieldName = StringUtils.uncapitalize(fieldName);
        response = invoke(fieldName, target, method);
      }
    }
    return response;
  }

  private void addTargets(List<MultiLevelAware> targets, MultiLevelAware target) {
    if (target == null) {
      return;
    }
    MultiLevelAware[] targetArray = target.objects();
    if (targetArray == null) {
      return;
    }
    if (breadthFirst) {
      if (targetArray != null) {
        CollectionUtils.addAll(targets, targetArray);
        for (MultiLevelAware temp : targetArray) {
          addTargets(targets, temp);
        }
      }
    } else {
      for (MultiLevelAware temp : targetArray) {
        targets.add(temp);
        addTargets(targets, temp);
      }
    }
  }

  private Object invoke(String fieldName, Object target, Method method) throws Exception {
    Object response = method.invoke(target);
    if (!isValid(fieldName, response) && target instanceof MultiLevelAware) {
      List<MultiLevelAware> targets = new ArrayList<>();
      addTargets(targets, (MultiLevelAware) target);
      for (MultiLevelAware temp : targets) {
        if (temp != null) {
          response = method.invoke(temp);
          if (isValid(fieldName, response)) {
            break;
          }
        }
      }
    }
    return response;
  }

  /**
   * Is valid value for the field.
   *
   * @param fieldName the field name, after removing get/is and un-capitalizing first letter
   * @param value     the value
   * @return the boolean
   */
  protected boolean isValid(String fieldName, Object value) {
    return value != null;
  }
}
