/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Properties;

/**
 * Utils to deal with {@link java.util.Properties}
 *
 * @author Swatantra Agrawal on 26-AUG-2017
 */
public class PropertyUtils {
  /**
   * Parses the given properties and creates new Properties with desired keys
   *
   * @param properties the properties
   * @param keys       The desired keys
   * @param prefixes   the prefixes
   * @return The Properties
   */
  public static Properties getProperties(Properties properties, String[] keys, String... prefixes) {
    final Properties props = new Properties();
    for (String prefix : prefixes) {
      if (StringUtils.isNotBlank(prefix)) {
        if (!prefix.endsWith(".")) {
          prefix += ".";
        }
      }
      for (String key : keys) {
        if (props.containsKey(key)) {
          continue;
        }
        String value = properties.getProperty(prefix + key);
        if (StringUtils.isNotBlank(value)) {
          props.setProperty(key, value);
        }
      }
    }
    return props;
  }

  /**
   * Is the key configured
   *
   * @param p   the p
   * @param key the key
   * @return the boolean
   */
  public static boolean isConfigured(Properties p, String key) {
    String value = p.getProperty(key);
    return StringUtils.isNotBlank(value);
  }

  /**
   * Gets value as String
   *
   * @param p   the p
   * @param key the key
   * @return the string
   */
  public static String getString(Properties p, String key) {
    return p.getProperty(key);
  }

  /**
   * Gets value as int
   *
   * @param p   the p
   * @param key the key
   * @return the int
   */
  public static int getInt(Properties p, String key) {
    return Integer.parseInt(p.getProperty(key));
  }

  /**
   * Gets value as boolean
   *
   * @param p   the p
   * @param key the key
   * @return the boolean
   */
  public static boolean getBoolean(Properties p, String key) {
    return Boolean.parseBoolean(p.getProperty(key));
  }
}
