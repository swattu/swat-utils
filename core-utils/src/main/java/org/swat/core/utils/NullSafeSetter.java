/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;


import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;

/**
 * The type Null safe setter.
 */
public class NullSafeSetter implements MethodInterceptor {
  @Override
  public Object invoke(MethodInvocation methodInvocation) throws Throwable {
    Method method = methodInvocation.getMethod();
    String methodName = method.getName();
    if (methodName.startsWith("set") && methodInvocation.getArguments().length == 1) {
      Object argument = methodInvocation.getArguments()[0];
      String fieldName = StringUtils.uncapitalize(methodName.substring(3));
      if (argument == null || !valid(fieldName, argument)) {
        return ObjectUtil.defaultValue(methodInvocation.getMethod().getReturnType());
      }
    }
    return methodInvocation.proceed();
  }

  /**
   * Valid boolean.
   *
   * @param fieldName the field name. Field name is arrived by remove first set and lower casing the first character.
   * @param argument  the argument
   * @return the boolean
   */
  protected boolean valid(String fieldName, Object argument) {
    return true;
  }
}
