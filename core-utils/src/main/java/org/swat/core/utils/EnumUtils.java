/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.swat.core.utils.ObjectUtil.cast;

/**
 * The type Enum utils.
 */
public class EnumUtils {
  private static final Map<Class<Enum<?>>, Map<String, Enum<?>>> enumMap = new HashMap<>();

  /**
   * Enums of set.
   *
   * @param <T>       the type parameter
   * @param strValues the str values
   * @param clazz     the clazz
   * @return the set
   */
  @SneakyThrows
  public static <T extends Enum<T>> Set<T> enumsOf(Set<String> strValues, Class<T> clazz) {
    Set<T> enums = new LinkedHashSet<>(enumsOf(new ArrayList<>(strValues), clazz));
    return enums;
  }

  /**
   * Enums of list.
   *
   * @param <T>       the type parameter
   * @param strValues the str values
   * @param clazz     the clazz
   * @return the list
   */
  @SneakyThrows
  public static <T extends Enum<T>> List<T> enumsOf(List<String> strValues, Class<T> clazz) {
    List<T> enums = new ArrayList<>();
    if (strValues == null) {
      return enums;
    }
    strValues.forEach(strValue -> {
      T value = enumOf(strValue, clazz);
      if (value != null) {
        enums.add(value);
      }
    });
    return enums;
  }

  /**
   * Enum of t.
   *
   * @param <T>      the type parameter
   * @param strValue the str value
   * @param clazz    the clazz
   * @return the t
   */
  @SneakyThrows
  public static <T extends Enum<T>> T enumOf(String strValue, Class<T> clazz) {
    if (StringUtils.isBlank(strValue) || clazz == null) {
      return null;
    }
    Map<String, T> valueMap = cast(enumMap.get(clazz));
    if (valueMap == null) {
      valueMap = new HashMap<>();
      Method method = clazz.getMethod("values");
      Enum<T>[] values = cast(method.invoke(null));
      for (Enum<T> value : values) {
        put(cast(valueMap), value.name(), value);
      }
      enumMap.put(cast(clazz), cast(valueMap));
    }
    return valueMap.get(strValue.toLowerCase());
  }

  private static void put(Map<String, Object> map, String key, Object value) {
    key = key.toLowerCase();
    if (map.containsKey(key)) {
      throw new RuntimeException(key + " is already defined for " + value);
    }
    map.put(key, value);
  }
}
