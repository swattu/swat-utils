/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

/**
 * Runtime exception thrown by utilities
 *
 * @author Swatantra Agrawal on 27-AUG-2017
 */
public class CoreRtException extends RuntimeException {
  private static final long serialVersionUID = -1L;

  /**
   * Instantiates a new Core Runtime exception.
   */
  public CoreRtException() {
  }

  /**
   * Instantiates a new Core Runtime exception.
   *
   * @param s the s
   */
  public CoreRtException(String s) {
    super(s);
  }

  /**
   * Instantiates a new Core Runtime exception.
   *
   * @param s         the s
   * @param throwable the throwable
   */
  public CoreRtException(String s, Throwable throwable) {
    super(s, throwable);
  }

  /**
   * Instantiates a new Core Runtime exception.
   *
   * @param throwable the throwable
   */
  public CoreRtException(Throwable throwable) {
    super(throwable);
  }

  /**
   * Instantiates a new Core Runtime exception.
   *
   * @param s         the s
   * @param throwable the throwable
   * @param b         the b
   * @param b1        the b 1
   */
  public CoreRtException(String s, Throwable throwable, boolean b, boolean b1) {
    super(s, throwable, b, b1);
  }
}
