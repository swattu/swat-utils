/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Util to find Duplicate class is a classpath
 */
public class DuplicateClassUtil {
  private static final Logger LOGGER = LoggerFactory.getLogger(DuplicateClassUtil.class);
  private final String[] locations;
  private final String commonPath;
  private final boolean compare;
  private final byte[] ZERO_BYTE = new byte[0];
  private final String[] jarExts = {".jar", ".war"};
  private Map<String, Map<String, DuplicateInfo>> duplicateMap = new TreeMap<>();

  /**
   * Instantiates a new Duplicate class util.
   * It compares the bytes of classes
   *
   * @param locations the locations. These can be a folder or an archive
   */
  public DuplicateClassUtil(String... locations) {
    this(true, locations);
  }

  /**
   * Instantiates a new Duplicate class util.
   *
   * @param compare   Compare the bytes of classes or not
   * @param locations the locations. These can be a folder or an archive
   */
  public DuplicateClassUtil(boolean compare, String... locations) {
    if (locations == null || locations.length == 0) {
      throw new CoreRtException("At least one location must be provided.");
    }
    this.compare = compare;
    this.locations = locations;
    String cPath = locations[0];
    for (String location : locations) {
      int minLength = Math.min(cPath.length(), location.length());
      for (int index = minLength; index >= 0; index--) {
        cPath = cPath.substring(0, index);
        if (cPath.equalsIgnoreCase(location.substring(0, index))) {
          break;
        }
      }
    }
    commonPath = cPath;
  }

  /**
   * Emits duplicate information.
   *
   * @return true, if duplicates were found
   */
  public boolean emitDuplicateInfo() {
    int counter = 0;
    boolean status = false;

    for (Map.Entry<String, Map<String, DuplicateInfo>> classEntry : duplicateInfos().entrySet()) {
      counter++;
      LOGGER.info(counter + "\t" + classEntry.getKey());
      for (Map.Entry<String, DuplicateInfo> uidEntry : classEntry.getValue().entrySet()) {
        DuplicateInfo duplicateInfo = uidEntry.getValue();
        LOGGER.info("\t\t" + duplicateInfo.bytes.length + "\t" + duplicateInfo.locations);
        status = true;
      }
    }
    return status;
  }

  /**
   * Is jar file boolean. This can be overridden to support more extensions.
   * By default jar and war are supported.
   *
   * @param name The File Name
   * @return true if it's a jar file
   */
  protected boolean isJarFile(String name) {
    for (String jarExt : jarExts) {
      if (name.endsWith(jarExt)) {
        return true;
      }
    }
    return false;
  }

  private Map<String, Map<String, DuplicateInfo>> duplicateInfos() {
    for (String location : locations) {
      processFolderOrFile(location, new File(location));
    }
    for (String className : new HashSet<>(duplicateMap.keySet())) {
      if (compare) {
        if (duplicateMap.get(className).size() < 2) {
          duplicateMap.remove(className);
        }
      } else {
        DuplicateInfo duplicateInfo = duplicateMap.get(className).values().iterator().next();
        if (duplicateInfo.locations.size() < 2) {
          duplicateMap.remove(className);
        }
      }
    }
    return duplicateMap;
  }

  private void processFolderOrFile(final String location, File file) {
    if (file == null || !file.exists()) {
      return;
    }
    if (file.isDirectory()) {
      File[] files = file.listFiles();
      if (files == null) {
        return;
      }
      for (File tmp : files) {
        if (tmp.isDirectory()) {
          processFolderOrFile(location + "/" + tmp.getName(), tmp);
        } else {
          processFolderOrFile(location, tmp);
        }
      }
      return;
    }
    if (file.getName().endsWith(".class")) {
      byte[] bytes = new byte[0];
      InputStream inputStream = null;
      try {
        inputStream = new FileInputStream(file);
        bytes = compare ? IOUtils.toByteArray(inputStream) : ZERO_BYTE;
      } catch (IOException ignore) {
      } finally {
        IOUtils.closeQuietly(inputStream);
      }
      addLocation(file.getName(), location, bytes);
    } else {
      if (isJarFile(file.getName())) {
        processJar(location + "/" + file.getName(), file);
      }
    }
  }

  private void processJar(String location, File jarFile) {

    try (ZipFile zipFile = new ZipFile(jarFile)) {
      Enumeration<? extends ZipEntry> entries = zipFile.entries();
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        InputStream inputStream = null;
        try {
          if (entry.getName().endsWith(".class")) {
            byte[] bytes = compare ? IOUtils.toByteArray(zipFile.getInputStream(entry)) : ZERO_BYTE;
            addLocation(entry.getName(), location, bytes);
          } else {
            if (isJarFile(entry.getName())) {
              File file = File.createTempFile("Temp", ".jar");
              inputStream = zipFile.getInputStream(entry);
              FileUtils.copyInputStreamToFile(inputStream, file);
              processJar(location + "!" + entry.getName(), file);
              file.delete();
            }
          }
        } catch (IOException ignore) {
        } finally {
          IOUtils.closeQuietly(inputStream);
        }
      }
    } catch (IOException ignore) {
    }
  }

  private void addLocation(String fileName, String location, byte[] bytes) {
    if (fileName.contains("$")) {
      return;
    }
    location = location.replaceAll(commonPath, "");
    Map<String, DuplicateInfo> uidMap = duplicateMap.get(fileName);
    if (uidMap == null) {
      uidMap = new HashMap<>();
      duplicateMap.put(fileName, uidMap);
    } else {
      for (DuplicateInfo duplicateInfo : uidMap.values()) {
        if (Objects.deepEquals(duplicateInfo.bytes, bytes)) {
          duplicateInfo.locations.add(location);
          return;
        }
      }
    }
    DuplicateInfo duplicateInfo = new DuplicateInfo(fileName + "[" + bytes.length + "]", bytes);
    uidMap.put(UUID.randomUUID().toString(), duplicateInfo);
    duplicateInfo.locations.add(location);
  }

  private class DuplicateInfo {
    private final String fileName;
    private final Set<String> locations = new TreeSet<>();
    private final byte[] bytes;

    private DuplicateInfo(String fileName, byte[] bytes) {
      this.fileName = fileName;
      this.bytes = bytes;
    }

    @Override
    public String toString() {
      return fileName + " => " + locations;
    }
  }
}
