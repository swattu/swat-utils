/*
 * Copyright © 2018 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * Logs the time taken by a method call, if greater than the threshold.
 */
public class DurationLogger implements MethodInterceptor {
  private static final Logger LOGGER = LoggerFactory.getLogger(DurationLogger.class);
  private final int thresholdMillis;

  /**
   * Instantiates a new Duration Logger.
   *
   * @param thresholdMillis the threshold
   */
  public DurationLogger(int thresholdMillis) {
    this.thresholdMillis = thresholdMillis;
  }

  @Override
  public Object invoke(MethodInvocation methodInvocation) throws Throwable {
    long duration = System.currentTimeMillis();
    Method method = methodInvocation.getMethod();
    try {
      return methodInvocation.proceed();
    } finally {
      duration = System.currentTimeMillis() - duration;
      if (duration >= this.thresholdMillis) {
        log(method, duration);
      }
    }
  }

  /**
   * Log the information when the duration is above threshold.
   *
   * @param method         the method
   * @param durationMillis the duration millis
   */
  protected void log(Method method, long durationMillis) {
    LOGGER.info("{} Time taken {} millis.", method.toString(), durationMillis);
  }
}
