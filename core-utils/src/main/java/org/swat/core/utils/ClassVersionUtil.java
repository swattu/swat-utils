/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Util to find Duplicate class is a classpath
 */
public class ClassVersionUtil {
  private static final Logger LOGGER = LoggerFactory.getLogger(ClassVersionUtil.class);
  private final String[] locations;
  private final String commonPath;
  private final String[] jarExts = {".jar", ".war"};
  private List<DuplicateInfo> classList = new ArrayList<>();

  /**
   * Instantiates a new Duplicate class util.
   *
   * @param locations the locations. These can be a folder or an archive
   */
  public ClassVersionUtil(String... locations) {
    if (locations == null || locations.length == 0) {
      throw new CoreRtException("At least one location must be provided.");
    }
    this.locations = locations;
    String cPath = locations[0];
    for (String location : locations) {
      int minLength = Math.min(cPath.length(), location.length());
      for (int index = minLength; index >= 0; index--) {
        cPath = cPath.substring(0, index);
        if (cPath.equalsIgnoreCase(location.substring(0, index))) {
          break;
        }
      }
    }
    commonPath = cPath;
  }

  /**
   * Assert version boolean.
   * Java SE 12 = 56 (0x38 hex)
   * Java SE 11 = 55 (0x37 hex)
   * Java SE 10 = 54
   * Java SE 9 = 53
   * Java SE 8 = 52
   * Java SE 7 = 51
   * Java SE 6.0 = 50
   * Java SE 5.0 = 49
   * JDK 1.4 = 48
   * JDK 1.3 = 47
   * JDK 1.2 = 46
   * JDK 1.1 = 45
   *
   * @param version the version
   * @return the boolean
   */
  public boolean versionMismatch(int version) {
    initialize();
    boolean status = false;
    for (DuplicateInfo duplicateInfo : classList) {
      if (StringUtils.containsIgnoreCase(duplicateInfo.fileName, "module-info.class")) {
        continue;
      }
      if (duplicateInfo.version() > version) {
        LOGGER.info("\t\t" + duplicateInfo.fileName + "\t" + duplicateInfo.locations + "\t" + duplicateInfo.version());
        status = true;
      }
    }
    return status;
  }

  /**
   * Is jar file boolean. This can be overridden to support more extensions.
   * By default jar and war are supported.
   *
   * @param name The File Name
   * @return true if it's a jar file
   */
  protected boolean isJarFile(String name) {
    for (String jarExt : jarExts) {
      if (name.endsWith(jarExt)) {
        return true;
      }
    }
    return false;
  }

  private List<DuplicateInfo> initialize() {
    if (!classList.isEmpty()) {
      return classList;
    }
    for (String location : locations) {
      processFolderOrFile(location, new File(location));
    }
    return classList;
  }

  private void processFolderOrFile(final String location, File file) {
    if (file == null || !file.exists()) {
      return;
    }
    if (file.isDirectory()) {
      File[] files = file.listFiles();
      if (files == null) {
        return;
      }
      for (File tmp : files) {
        if (tmp.isDirectory()) {
          processFolderOrFile(location + "/" + tmp.getName(), tmp);
        } else {
          processFolderOrFile(location, tmp);
        }
      }
      return;
    }
    if (file.getName().endsWith(".class")) {
      byte[] bytes = new byte[0];
      InputStream inputStream = null;
      try {
        inputStream = new FileInputStream(file);
        bytes = IOUtils.toByteArray(inputStream);
      } catch (IOException ignore) {
      } finally {
        IOUtils.closeQuietly(inputStream);
      }
      addLocation(file.getName(), location, bytes);
    } else {
      if (isJarFile(file.getName())) {
        processJar(location + "/" + file.getName(), file);
      }
    }
  }

  private void processJar(String location, File jarFile) {
    try (ZipFile zipFile = new ZipFile(jarFile)) {
      Enumeration<? extends ZipEntry> entries = zipFile.entries();
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        InputStream inputStream = null;
        try {
          if (entry.getName().endsWith(".class")) {
            byte[] bytes = IOUtils.toByteArray(zipFile.getInputStream(entry));
            addLocation(entry.getName(), location, bytes);
          } else {
            if (isJarFile(entry.getName())) {
              File file = File.createTempFile("Temp", ".jar");
              inputStream = zipFile.getInputStream(entry);
              FileUtils.copyInputStreamToFile(inputStream, file);
              processJar(location + "!" + entry.getName(), file);
              file.delete();
            }
          }
        } catch (IOException ignore) {
        } finally {
          IOUtils.closeQuietly(inputStream);
        }
      }
    } catch (IOException ignore) {
    }
  }

  private void addLocation(String fileName, String location, byte[] bytes) {
    if (fileName.contains("$")) {
      return;
    }
    location = location.replaceAll(commonPath, "");
    DuplicateInfo duplicateInfo = new DuplicateInfo(fileName + "[" + bytes.length + "]", bytes);
    duplicateInfo.locations.add(location);
    classList.add(duplicateInfo);
  }

  private class DuplicateInfo {
    private final String fileName;
    private final Set<String> locations = new TreeSet<>();
    private final byte[] bytes;

    private DuplicateInfo(String fileName, byte[] bytes) {
      this.fileName = fileName;
      this.bytes = bytes;
    }

    /**
     * Version int.
     *
     * @return the int
     */
    public int version() {
      if (ArrayUtils.getLength(bytes) > 8) {
        return bytes[6] * 256 + bytes[7];
      }
      return 0;
    }

    @Override
    public String toString() {
      return fileName + " => " + locations;
    }
  }
}
