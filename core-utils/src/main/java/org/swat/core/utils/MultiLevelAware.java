/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

/**
 * The getters
 *
 * @param <T> the type parameter
 */
public interface MultiLevelAware<T extends MultiLevelAware> {
  /**
   * Gets object.
   *
   * @return the object
   */
  T[] objects();
}
