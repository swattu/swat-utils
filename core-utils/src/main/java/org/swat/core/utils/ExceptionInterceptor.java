/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.Method;

/**
 * This replaces Exception message based on regex matching
 *
 * @author Swatantra Agrawal on 05-Sep-2017
 */
public class ExceptionInterceptor implements MethodInterceptor {
  private final String pattern;
  private final String replaceWith;

  /**
   * The initializer
   *
   * @param pattern     The regex to match
   * @param replaceWith The regex to replace
   */
  public ExceptionInterceptor(String pattern, String replaceWith) {
    this.pattern = pattern;
    this.replaceWith = replaceWith;
  }

  @Override
  public Object invoke(MethodInvocation invocation) throws Throwable {
    Method method = invocation.getMethod();
    Object object = invocation.proceed();
    if (method.getParameterTypes().length == 0 && method.getName().equals("getMessage") && object instanceof String) {
      String value = (String) object;
      try {
        value = value.replaceAll(pattern, replaceWith);
      } catch (Exception ignore) {
      }
      return value;
    }
    return object;
  }
}
