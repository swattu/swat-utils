/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

/**
 * The type Instance util.
 */
public class InstanceUtil {
  private static final String INSTANCE_ID = UUID.randomUUID().toString();

  /**
   * Gets instance id.
   *
   * @return the instance id
   */
  public static final String getInstanceId() {
    String instanceId = System.getProperty("INSTANCE_ID");
    if (StringUtils.isBlank(instanceId)) {
      instanceId = INSTANCE_ID;
    }
    return instanceId;
  }
}
