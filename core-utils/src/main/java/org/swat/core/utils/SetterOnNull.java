/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.Method;

/**
 * The type Setter on null.
 */
public class SetterOnNull implements MethodInterceptor {
  @Override
  public Object invoke(MethodInvocation methodInvocation) throws Throwable {
    Method method = methodInvocation.getMethod();
    String methodName = method.getName();
    if (methodName.startsWith("set") && methodInvocation.getArguments().length == 1) {
      Object argument = methodInvocation.getArguments()[0];
      if (argument == null) {
        return ObjectUtil.defaultValue(methodInvocation.getMethod().getReturnType());
      }
      Class paramClass = method.getParameterTypes()[0];
      if (!paramClass.isPrimitive()) {
        String getterName = "get" + methodName.substring(3);
        Method getter = method.getClass().getMethod(getterName);
        if (getter != null) {
          Object object = getter.invoke(methodInvocation.getThis());
          Object result = getter.invoke(object);
          if (result != null) {
            return ObjectUtil.defaultValue(methodInvocation.getMethod().getReturnType());
          }
        }
      }
    }
    return methodInvocation.proceed();
  }
}
