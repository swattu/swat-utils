/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import lombok.Data;

/**
 * The type Multi level aware.
 *
 * @param <T> the type parameter
 */
@Data
public class MultiLevelWrapper<T extends MultiLevelAware> implements MultiLevelAware<T> {
  private T[] objects;

  /**
   * Sets objects.
   *
   * @param objects the objects
   */
  public void setObjects(T... objects) {
    this.objects = objects;
  }

  @Override
  public T[] objects() {
    return objects;
  }
}
