/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.swat.reflect.utils.ReflectUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static org.swat.core.utils.ObjectUtil.cast;
import static org.swat.core.utils.ObjectUtil.defaultValue;
import static org.swat.core.utils.ObjectUtil.wrapperClass;

/**
 * The type Object convertor.
 */
public class ObjectConvertor {
  private final DateTimeUtils dateTimeUtils;
  private final DateTimeFormatter dateFormatter;
  private final DateTimeFormatter timeFormatter;
  private final DateTimeFormatter dateTimeFormatter;
  private final ZoneId zoneId;

  private ObjectConvertor(Builder builder) {
    Set<String> dtFormats = new LinkedHashSet<>();
    for (String dtSeparator : builder.dateTimeSeparators) {
      for (String dateFormat : builder.dateFormats) {
        for (String timeFormat : builder.timeFormats) {
          dtFormats.add(dateFormat + dtSeparator + timeFormat);
        }
      }
    }
    dtFormats.addAll(Arrays.asList(builder.dateFormats));
    dtFormats.addAll(Arrays.asList(builder.timeFormats));
    List<DateTimeFormatter> dtFormatters = new ArrayList<>();
    zoneId = builder.zoneId;
    dtFormats.forEach(v -> dtFormatters.add(new DateTimeFormatterBuilder().appendPattern(v).toFormatter()
        .withZone(zoneId)));
    dateTimeUtils = new DateTimeUtils(dtFormatters);
    dateFormatter = new DateTimeFormatterBuilder().appendPattern(builder.dateFormats[0]).toFormatter().withZone(zoneId);
    timeFormatter = new DateTimeFormatterBuilder().appendPattern(builder.timeFormats[0]).toFormatter().withZone(zoneId);
    dateTimeFormatter = new DateTimeFormatterBuilder().appendPattern(
        builder.dateFormats[0] + builder.dateTimeSeparators[0] + builder.timeFormats[0]).toFormatter().withZone(zoneId);
  }

  /**
   * Convert input to expected type.
   * {@link LocalDateTime} to {@link ZonedDateTime} in provided {@link ZoneId}
   * {@link Date}, {@link Calendar}, {@link Timestamp}, {@link LocalDateTime}, {@link ZonedDateTime} to long (epoch millis)
   * {@link java.sql.Date}, {@link LocalDate} to long (epoch days)
   * {@link Time}, {@link LocalTime} to long (millis of the day)
   *
   * @param <T>   the type parameter
   * @param input the input
   * @param clazz the clazz
   * @return the t
   */
  public <T> T convert(final Object input, Class<T> clazz) {
    return cast(convertInternal(input, clazz));
  }

  private <T> Object convertInternal(final Object input, Class<T> clazz) {
    Object value = input;
    if (value == null) {
      return defaultValue(clazz);
    }
    if (clazz.isPrimitive()) {
      clazz = wrapperClass(clazz);
    }
    if (value.getClass() == clazz) {
      return value;
    }
    if (clazz == BigDecimal.class) {
      return toBigDecimal(value);
    }
    if (clazz == BigInteger.class) {
      return toBigInteger(value);
    }
    if (clazz == UUID.class) {
      if (value instanceof String) {
        return UUID.fromString((String) value);
      }
    }
    if (Number.class.isAssignableFrom(clazz)) {
      if (value instanceof String) {
        value = new BigDecimal((String) value);
      }
      if (value instanceof java.sql.Date) {
        value = ((java.sql.Date) value).toLocalDate();
      }
      if (value instanceof Time) {
        value = ((Time) value).toLocalTime();
      }
      if (value instanceof Calendar) {
        value = ((Calendar) value).getTime();
      }
      if (value instanceof Date) {
        value = ((Date) value).getTime();
      }
      if (value instanceof LocalTime) {
        value = ((LocalTime) value).toSecondOfDay() * 1000L;
      }
      if (value instanceof LocalDate) {
        value = ((LocalDate) value).toEpochDay();
      }
      if (value instanceof LocalDateTime) {
        value = ((LocalDateTime) value).atZone(zoneId);
      }
      if (value instanceof ZonedDateTime) {
        value = ((ZonedDateTime) value).toEpochSecond() * 1000;
      }
      return toNumber(clazz, (Number) value);
    }
    if (clazz.isEnum() && value instanceof String) {
      value = EnumUtils.enumOf((String) value, cast(clazz));
      if (value == null) {
        throw new RuntimeException("Could not cast " + input + " to " + clazz);
      }
    }
    if (clazz == LocalDate.class) {
      return toLocalDate(value);
    }
    if (clazz == LocalTime.class) {
      return toLocalTime(value);
    }
    if (clazz == LocalDateTime.class) {
      return toLocalDateTime(value);
    }
    if (clazz == ZonedDateTime.class) {
      return toZonedDateTime(value);
    }
    if (clazz == Date.class) {
      return new Date(toLocalDateTime(value).atZone(zoneId).toEpochSecond() * 1000);
    }
    if (clazz == Timestamp.class) {
      return new Timestamp(toZonedDateTime(value).toEpochSecond() * 1000);
    }
    if (clazz == java.sql.Date.class) {
      return java.sql.Date.valueOf(toLocalDate(value));
    }
    if (clazz == Time.class) {
      return toSqlTime(value);
    }
    if (clazz == Calendar.class) {
      Calendar calendar = Calendar.getInstance();
      calendar.setTimeInMillis(toLocalDateTime(value).atZone(zoneId).toEpochSecond() * 1000);
      return calendar;
    }
    if (clazz == String.class) {
      value = toString(value);
    }
    if (clazz == Boolean.class) {
      value = toBoolean(value);
    }
    if (clazz == Character.class) {
      value = toChar(value);
    }
    return value;
  }

  private Time toSqlTime(Object value) {
    return Time.valueOf(toLocalTime(value));
  }

  private Character toChar(Object value) {
    if (value instanceof String && ((String) value).length() == 1) {
      return ((String) value).charAt(0);
    }
    return cast(value);
  }

  private Boolean toBoolean(Object value) {
    if (value instanceof String) {
      return Boolean.valueOf((String) value);
    }
    return cast(value);
  }

  private String toString(Object value) {
    if (ReflectUtils.isAssignable(Calendar.class, value.getClass())) {
      value = ((Calendar) value).getTime();
    }
    if (value instanceof ZonedDateTime) {
      value = ((ZonedDateTime) value).toLocalDateTime();
    }
    if (value instanceof java.sql.Date) {
      value = ((java.sql.Date) value).toLocalDate();
    } else if (value instanceof Time) {
      value = ((Time) value).toLocalTime();
    } else if (value instanceof Timestamp) {
      value = ZonedDateTime.ofInstant(((Timestamp) value).toInstant(), zoneId);
    } else if (value instanceof Date) {
      value = LocalDateTime.ofInstant(((Date) value).toInstant(), zoneId);
    }
    if (value instanceof LocalDate) {
      value = ((LocalDate) value).format(dateFormatter);
    } else if (value instanceof LocalTime) {
      value = ((LocalTime) value).format(timeFormatter);
    } else if (value instanceof LocalDateTime) {
      value = ((LocalDateTime) value).format(dateTimeFormatter);
    } else if (value instanceof ZonedDateTime) {
      value = ((ZonedDateTime) value).format(dateTimeFormatter);
    }
    return String.valueOf(value);
  }

  private LocalDate toLocalDate(Object value) {
    if (value instanceof LocalDate) {
      return (LocalDate) value;
    }
    if (value instanceof Calendar) {
      value = ((Calendar) value).getTime();
    }
    if (value instanceof Number) {
      if (((Number) value).longValue() < Integer.MAX_VALUE) {
        return LocalDate.ofEpochDay(((Number) value).longValue());
      }
      value = new Date(((Number) value).longValue());
    }
    if (value instanceof Time || value instanceof LocalTime) {
      throw new CoreRtException("Cannot convert " + value.getClass() + " to LocalDate");
    }
    if (value instanceof java.sql.Date) {
      return ((java.sql.Date) value).toLocalDate();
    }
    if (value instanceof Date) {
      return LocalDateTime.ofInstant(Instant.ofEpochMilli(((Date) value).getTime()), zoneId).toLocalDate();
    }
    if (value instanceof ZonedDateTime) {
      value = ((ZonedDateTime) value).toLocalDateTime();
    }
    if (value instanceof LocalDateTime) {
      return ((LocalDateTime) value).toLocalDate();
    }
    if (value instanceof String) {
      return dateTimeUtils.parseLocalDate((String) value);
    }
    return cast(value);
  }

  private LocalTime toLocalTime(Object value) {
    if (value instanceof LocalTime) {
      return (LocalTime) value;
    }
    if (value instanceof Number) {
      long millis = ((Number) value).longValue();
      if (millis > Integer.MAX_VALUE) {
        return Instant.ofEpochMilli(((Number) value).longValue()).atZone(zoneId).toLocalTime();
      } else {
        return LocalTime.ofSecondOfDay(((Number) value).longValue() / 1000);
      }
    }
    if (value instanceof Time) {
      return ((Time) value).toLocalTime();
    }
    if (value instanceof String) {
      return dateTimeUtils.parseLocalTime((String) value);
    }
    return toZonedDateTime(value).toLocalTime();
  }

  private LocalDateTime toLocalDateTime(Object value) {
    ZonedDateTime zonedDateTime = toZonedDateTime(value);
    return zonedDateTime.toLocalDateTime();
  }

  private ZonedDateTime toZonedDateTime(Object value) {
    if (value instanceof ZonedDateTime) {
      return ((ZonedDateTime) value).withZoneSameInstant(zoneId);
    }
    if (value instanceof Number) {
      if (((Number) value).longValue() > Integer.MAX_VALUE) {
        return ZonedDateTime.ofInstant(Instant.ofEpochMilli(((Number) value).longValue()), zoneId);
      }
    }
    if (value instanceof LocalDateTime) {
      return ZonedDateTime.of((LocalDateTime) value, zoneId);
    }
    if (value instanceof Timestamp) {
      return ZonedDateTime.ofInstant(Instant.ofEpochMilli(((Timestamp) value).getTime()), zoneId);
    }
    if (value instanceof Calendar) {
      return ZonedDateTime.ofInstant(((Calendar) value).toInstant(), zoneId);
    }
    if (value instanceof Date) {
      return ZonedDateTime.ofInstant(((Date) value).toInstant(), zoneId);
    }
    if (value instanceof String) {
      return dateTimeUtils.parseLocalDateTime((String) value).atZone(zoneId);
    }
    return cast(value);
  }

  private <T> T toNumber(Class<T> clazz, Number value) {
    if (clazz == Double.class || clazz == double.class) {
      return cast(value.doubleValue());
    }
    if (clazz == Float.class || clazz == float.class) {
      return cast(value.floatValue());
    }
    if (clazz == Long.class || clazz == long.class) {
      return cast(value.longValue());
    }
    if (clazz == Integer.class || clazz == int.class) {
      return cast(value.intValue());
    }
    if (clazz == Byte.class || clazz == byte.class) {
      return cast(value.byteValue());
    }
    if (clazz == Short.class || clazz == short.class) {
      return cast(value.shortValue());
    }
    if (clazz == AtomicInteger.class) {
      return cast(new AtomicInteger(value.intValue()));
    }
    if (clazz == AtomicLong.class) {
      return cast(new AtomicLong(value.longValue()));
    }
    return cast(value);
  }

  private BigDecimal toBigDecimal(Object value) {
    if (value instanceof CharSequence) {
      return new BigDecimal((String) value);
    } else if (value instanceof Number) {
      return BigDecimal.valueOf(((Number) value).doubleValue());
    }
    return cast(value);
  }

  private BigInteger toBigInteger(Object value) {
    if (value instanceof CharSequence) {
      return new BigInteger((String) value);
    } else if (value instanceof Number) {
      return BigInteger.valueOf(((Number) value).longValue());
    }
    return cast(value);
  }

  /**
   * Builder object convertor . builder.
   *
   * @return the object convertor . builder
   */
  public static ObjectConvertor.Builder builder() {
    return new Builder();
  }

  /**
   * The type Builder.
   */
  public static class Builder {
    private String[] dateFormats;
    private String[] timeFormats;
    private String[] dateTimeSeparators;
    private ZoneId zoneId;

    /**
     * Date time formatter builder.
     *
     * @param value the value
     * @return the builder
     */
    public Builder dateFormats(String[] value) {
      this.dateFormats = value;
      return this;
    }

    /**
     * Time formats builder.
     *
     * @param value the value
     * @return the builder
     */
    public Builder timeFormats(String[] value) {
      this.timeFormats = value;
      return this;
    }

    /**
     * Date time separator builder.
     *
     * @param value the value
     * @return the builder
     */
    public Builder dateTimeSeparators(String[] value) {
      this.dateTimeSeparators = value;
      return this;
    }

    /**
     * Zone id builder.
     *
     * @param value the value
     * @return the builder
     */
    public Builder zoneId(ZoneId value) {
      this.zoneId = value;
      return this;
    }

    /**
     * Build object convertor.
     *
     * @return the object convertor
     */
    public ObjectConvertor build() {
      if (dateFormats == null) {
        dateFormats = new String[]{"yyyy-MM-dd"};
      }
      if (timeFormats == null) {
        timeFormats = new String[]{"HH:mm:ss"};
      }
      if (dateTimeSeparators == null) {
        dateTimeSeparators = new String[]{"'T'"};
      }
      if (zoneId == null) {
        zoneId = ZoneId.systemDefault();
      }
      return new ObjectConvertor(this);
    }
  }
}
