/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.apache.commons.collections4.CollectionUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;

/**
 * The type Date time utils.
 */
public class DateTimeUtils {
  private final Collection<DateTimeFormatter> formatters;

  /**
   * Instantiates a new Date time utils.
   *
   * @param formatters the formatters
   */
  public DateTimeUtils(Collection<DateTimeFormatter> formatters) {
    if (CollectionUtils.isEmpty(formatters)) {
      throw new CoreRtException("Formatters cannot be empty");
    }
    this.formatters = formatters;
  }

  /**
   * Parse local date local date.
   *
   * @param dateStr the date str
   * @return the local date
   */
  public LocalDate parseLocalDate(String dateStr) {
    RuntimeException exception = null;
    for (DateTimeFormatter formatter : formatters) {
      try {
        return LocalDate.parse(dateStr, formatter);
      } catch (RuntimeException e) {
        if (exception == null) {
          exception = e;
        }
      }
    }
    if (exception != null) {
      throw exception;
    }
    return null;
  }

  /**
   * Parse local date time local date time.
   *
   * @param dateStr the date str
   * @return the local date time
   */
  public LocalDateTime parseLocalDateTime(String dateStr) {
    RuntimeException exception = null;
    for (DateTimeFormatter formatter : formatters) {
      try {
        return LocalDateTime.parse(dateStr, formatter);
      } catch (RuntimeException e) {
        if (exception == null) {
          exception = e;
        }
      }
    }
    if (exception != null) {
      throw exception;
    }
    return null;
  }

  /**
   * Parse local time local time.
   *
   * @param dateStr the date str
   * @return the local time
   */
  public LocalTime parseLocalTime(String dateStr) {
    RuntimeException exception = null;
    for (DateTimeFormatter formatter : formatters) {
      try {
        return LocalTime.parse(dateStr, formatter);
      } catch (RuntimeException e) {
        if (exception == null) {
          exception = e;
        }
      }
    }
    if (exception != null) {
      throw exception;
    }
    return null;
  }
}
