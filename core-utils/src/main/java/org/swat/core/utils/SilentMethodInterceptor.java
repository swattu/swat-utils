/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * An interceptor which swallows all Exceptions raised by any of the method calls. It does not log anything.
 *
 * @author Swatantra Agrawal on 01-Jul-2017 03:50-IST
 */
public class SilentMethodInterceptor implements MethodInterceptor {
  private final int maxExceptions;
  private Set<String> exceptions = new LinkedHashSet<>();

  /**
   * Instantiates a new Silent method interceptor.
   */
  public SilentMethodInterceptor() {
    this(10);
  }

  /**
   * Instantiates a new Silent method interceptor.
   *
   * @param maxExceptions the max exceptions
   */
  public SilentMethodInterceptor(int maxExceptions) {
    if (maxExceptions < 10) {
      maxExceptions = 10;
    }
    if (maxExceptions > 100) {
      maxExceptions = 100;
    }
    this.maxExceptions = maxExceptions;
  }

  @Override
  public Object invoke(MethodInvocation invocation) {
    return proceedQuietly(invocation);
  }

  private Object proceedQuietly(MethodInvocation invocation) {
    try {
      return invocation.proceed();
    } catch (Throwable th) {//NOSONAR
      if (exceptions.size() < maxExceptions) {
        String message =
            log(invocation.getMethod().getDeclaringClass().getName(), invocation.getMethod().getName(), th, invocation
                .getArguments());
        if (StringUtils.isNotBlank(message)) {
          exceptions.add(message);
        }
      }
      return ObjectUtil.defaultValue(invocation.getMethod().getReturnType());
    }
  }

  /**
   * Gets exceptions.
   *
   * @return the exceptions
   */
  public Set<String> getExceptions() {
    return exceptions;
  }

  /**
   * Logs the exception
   *
   * @param className  the class name
   * @param methodName the method name
   * @param throwable  the throwable
   * @param arguments  the arguments
   * @return the string
   */
  protected String log(String className, String methodName, Throwable throwable, Object... arguments) {
    return methodName + " - " + StringUtils.join(arguments, ",") + " : " + throwable.getMessage();
  }
}
