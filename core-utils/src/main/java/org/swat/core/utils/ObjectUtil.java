/*
 * Copyright © 2022 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The type Object util.
 */
public class ObjectUtil {
  /**
   * Is not blank empty.
   * Array / Iterable / Map - Not empty
   * Object - Not null
   *
   * @param value the value
   * @return the boolean
   */
  public static boolean isNotNullEmpty(Object value) {
    if (value == null) {
      return false;
    }
    if (value instanceof Iterable) {
      return ((Iterable<?>) value).iterator().hasNext();
    }
    if (value instanceof Map) {
      return !((Map<?, ?>) value).isEmpty();
    }
    if (value.getClass().isArray()) {
      return Array.getLength(value) > 0;
    }
    return true;
  }

  /**
   * Wrapper class class.
   *
   * @param <T>   the type parameter
   * @param clazz the clazz
   * @return the class
   */
  public static <T> Class<T> wrapperClass(Class<T> clazz) {
    if (clazz == byte.class) {
      return cast(Byte.class);
    }
    if (clazz == short.class) {
      return cast(Short.class);
    }
    if (clazz == int.class) {
      return cast(Integer.class);
    }
    if (clazz == char.class) {
      return cast(Character.class);
    }
    if (clazz == long.class) {
      return cast(Long.class);
    }
    if (clazz == float.class) {
      return cast(Float.class);
    }
    if (clazz == double.class) {
      return cast(Double.class);
    }
    if (clazz == boolean.class) {
      return cast(Boolean.class);
    }
    return clazz;
  }

  /**
   * Return default object.
   *
   * @param <T>   the type parameter
   * @param clazz the clazz
   * @return the object
   */
  public static <T> T defaultValue(Class<T> clazz) {
    if (clazz == byte.class) {
      return cast((byte) 0);
    }
    if (clazz == short.class) {
      return cast((short) 0);
    }
    if (clazz == int.class) {
      return cast(0);
    }
    if (clazz == char.class) {
      return cast('\u0000');
    }
    if (clazz == long.class) {
      return cast(0L);
    }
    if (clazz == float.class) {
      return cast(0F);
    }
    if (clazz == double.class) {
      return cast(0D);
    }
    if (clazz == boolean.class) {
      return cast(false);
    }
    return null;
  }

  /**
   * Cast the object to the desired type.
   *
   * @param <T>   the type parameter
   * @param value the value
   * @return the t
   */
  public static <T> T cast(Object value) {
    return (T) value;
  }

  /**
   * Convert values of underlying Collection and Map.
   * Collection and Map containing convertible objects must be mutable.
   * This is circular link safe.
   *
   * @param object    the object
   * @param converter the convertor
   * @return the object
   */
  public static Object convertValues(Object object, ValueConverter converter) {
    return convertObject(object, converter, "", new ArrayList<>());
  }

  private static Object convertObject(Object object, ValueConverter converter, String path,
      Collection<Object> iterated) {
    if (object instanceof Collection) {
      convertCollection(cast(object), converter, path, iterated);
    } else if (object instanceof Map) {
      convertMap(cast(object), converter, path, iterated);
    } else {
      object = converter.convert(path, object);
    }
    return object;
  }

  private static void convertCollection(Collection<Object> collection, ValueConverter converter, String path,
      Collection<Object> iterated) {
    if (contains(iterated, collection)) {
      return;
    }
    iterated.add(collection);
    List<Object> values = new ArrayList<>();
    int index = 0;
    boolean found = false;
    for (Object value : collection) {
      Object converted = convertObject(value, converter, path + "[" + index + "]", iterated);
      values.add(converted);
      if (converted != value) {
        found = true;
      }
      index++;
    }
    if (found) {
      collection.clear();
      collection.addAll(values);
    }
  }

  private static void convertMap(Map<Object, Object> map, ValueConverter converter, String path,
      Collection<Object> iterated) {
    if (contains(iterated, map)) {
      return;
    }
    iterated.add(map);
    if (StringUtils.isNotBlank(path)) {
      path = path + ".";
    }
    Set<Object> keys = new LinkedHashSet<>(map.keySet());
    for (Object key : keys) {
      Object value = map.get(key);
      Object converted = convertObject(value, converter, path + key, iterated);
      if (converted != value) {
        map.put(key, converted);
      }
    }
  }

  private static boolean contains(Collection<Object> iterated, Object value) {
    for (Object element : iterated) {
      if (element == value) {
        return true;
      }
    }
    return false;
  }

  /**
   * Equals any boolean.
   *
   * @param toMatch  the to match
   * @param elements the elements
   * @return the boolean
   */
  public static boolean equalsAny(Object toMatch, Object... elements) {
    for (Object element : elements) {
      if (toMatch == element) {
        return true;
      }
    }
    return false;
  }
}
