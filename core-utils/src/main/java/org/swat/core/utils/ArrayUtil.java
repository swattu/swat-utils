/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Utility to deal with Array
 *
 * @author Swatantra Agrawal on 06-SEP-2017
 */
public class ArrayUtil {
  /**
   * Returns a list of String for provided Array/Collection/Object
   *
   * @param array The object
   * @return List of String
   */
  public static List<String> asStringList(Object array) {
    List<Object> list = asList(array);
    List<String> stringList = new ArrayList<>();
    for (Object object : list) {
      if (object == null) {
        stringList.add(null);
      } else {
        stringList.add(object.toString());
      }
    }
    return stringList;
  }

  /**
   * Returns a list of Object for provided Array/Collection/Object
   *
   * @param array The object
   * @return List of Object
   */
  public static List<Object> asList(Object array) {
    List<Object> collection = new ArrayList<>();
    if (array instanceof Collection) {
      collection.addAll((Collection<?>) array);
    }
    try {
      if (ArrayUtils.getLength(array) > 0) {
        if (array instanceof Object[]) {
          Collections.addAll(collection, (Object[]) array);
        }
        if (array instanceof long[]) {
          Collections.addAll(collection, ArrayUtils.toObject((long[]) array));
        }
        if (array instanceof int[]) {
          Collections.addAll(collection, ArrayUtils.toObject((int[]) array));
        }
        if (array instanceof double[]) {
          Collections.addAll(collection, ArrayUtils.toObject((double[]) array));
        }
        if (array instanceof float[]) {
          Collections.addAll(collection, ArrayUtils.toObject((float[]) array));
        }
        if (array instanceof boolean[]) {
          Collections.addAll(collection, ArrayUtils.toObject((boolean[]) array));
        }
        if (array instanceof short[]) {
          Collections.addAll(collection, ArrayUtils.toObject((short[]) array));
        }
        if (array instanceof char[]) {
          Collections.addAll(collection, ArrayUtils.toObject((char[]) array));
        }
        if (array instanceof byte[]) {
          Collections.addAll(collection, ArrayUtils.toObject((byte[]) array));
        }
      }
    } catch (IllegalArgumentException e) {
      collection.add(array);
    }
    return collection;
  }
}
