/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.core.utils;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An interceptor which swallows all Exceptions raised by any of the method calls. However, it logs the exception in warn mode.
 *
 * @author Swatantra Agrawal on 01-Jul-2017 03:50-IST
 */
public class LoggedMethodInterceptor implements MethodInterceptor {
  private final Logger LOGGER;
  private final boolean withTrace;

  /**
   * Instantiates a new Logged method interceptor.
   *
   * @param clazz the clazz
   */
  LoggedMethodInterceptor(Class clazz) {
    this(clazz, true);
  }

  /**
   * Instantiates a new Logged method interceptor.
   *
   * @param clazz     the clazz
   * @param withTrace the with trace
   */
  LoggedMethodInterceptor(Class clazz, boolean withTrace) {
    LOGGER = LoggerFactory.getLogger(clazz);
    this.withTrace = withTrace;
  }

  @Override
  public Object invoke(MethodInvocation invocation) throws Throwable {
    return proceedQuietly(invocation);
  }

  private Object proceedQuietly(MethodInvocation invocation) {
    try {
      return invocation.proceed();
    } catch (Throwable th) {//NOSONAR
      if (withTrace) {
        LOGGER.warn("Error", th);
      } else {
        LOGGER.warn(th.getMessage());
      }
      return ObjectUtil.defaultValue(invocation.getMethod().getReturnType());
    }
  }
}
