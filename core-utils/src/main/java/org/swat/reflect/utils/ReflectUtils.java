/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.reflect.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.reflections.Reflections;
import org.springframework.util.ConcurrentReferenceHashMap;
import org.swat.core.utils.CoreRtException;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.swat.core.utils.ObjectUtil.cast;

/**
 * The type Reflect utils.
 *
 * @author Swatantra Agrawal on 16-Aug-2017
 */
public abstract class ReflectUtils {
  private static final Map<Class, String> BASE_TYPES = new HashMap<>();
  private static final Set<Class> BASE_ASSIGNABLE_TYPES = new HashSet<>();
  private static final Set<Class> NON_RECURSE_TYPES = new HashSet<>();
  private static final Set<Class> NON_OBJECT_TYPES = new HashSet<>();
  private static final Map<String, Set<String>> fieldMap =
      new ConcurrentReferenceHashMap<>(10, ConcurrentReferenceHashMap.ReferenceType.WEAK);

  /**
   * Is the class of Object Only. Map, List, Array etc return false
   *
   * @param clazz The class
   * @return boolean boolean
   */
  public static boolean isObjectOnly(Class clazz) {
    boolean objectOnly = !clazz.isArray();
    if (!objectOnly) {
      return false;
    }
    return !isAssignable(NON_OBJECT_TYPES, clazz);
  }

  /**
   * Extracts a list of getters of the provided Class. The names are DOT separated.
   *
   * @param clazz The class
   * @param depth The recursion depth
   * @return The getters
   */
  public static List<FieldInfo> getters(Class clazz, int depth) {
    return getters(clazz, depth, null);
  }

  /**
   * Methods list.
   *
   * @param clazz  the clazz
   * @param depth  the depth
   * @param option the option
   * @return the list
   */
  public static List<FieldInfo> getters(Class clazz, int depth, ReflectOption option) {
    if (option == null) {
      option = new ReflectOption();
    }
    Map<String, FieldInfo> map = new LinkedHashMap<>();
    if (option.isGetterFirst() && option.isGetter()) {
      getters(clazz, depth, true).forEach(info -> map.putIfAbsent(info.getName(), info));
    }
    if (option.isMethod()) {
      getters(clazz, depth, false).forEach(info -> map.putIfAbsent(info.getName(), info));
    }
    if (!option.isGetterFirst() && option.isGetter()) {
      getters(clazz, depth, true).forEach(info -> map.putIfAbsent(info.getName(), info));
    }
    return new ArrayList<>(map.values());
  }

  /**
   * Gets display.
   *
   * @param name the name
   * @return the display
   */
  public static String getDisplay(String name) {
    if (name == null) {
      return null;
    }
    name = name.replaceAll("([a-z])([A-Z])", "$1 $2");
    name = name.replaceAll("([A-Z])([a-z])", " $1$2");
    name = name.replaceAll("  ", " ");
    name = StringUtils.capitalize(name);
    return name;
  }

  private static List<FieldInfo> getters(Class clazz, int depth, boolean getter) {
    List<FieldInfo> list = new ArrayList<>();
    try {
      Method[] methods = clazz.getMethods();
      Map<String, Class> classMap = new LinkedHashMap<>();
      for (Method method : methods) {
        if (Modifier.isStatic(method.getModifiers())) {
          continue;
        }
        if (!Modifier.isPublic(method.getModifiers())) {
          continue;
        }
        if (Modifier.isNative(method.getModifiers())) {
          continue;
        }
        final Class type = method.getReturnType();
        if (type == Void.class) {
          continue;
        }
        if (!isObjectOnly(type)) {
          continue;
        }
        if (method.getParameterTypes().length != 0) {
          continue;
        }
        String name = method.getName();
        if (StringUtils.equalsAny(name, "getClass", "toString", "hashCode")) {
          continue;
        }
        if (name.contains("$")) {
          continue;
        }
        if (getter) {
          if (!name.startsWith("get") && !name.startsWith("is")) {
            continue;
          }
          if (name.startsWith("get")) {
            name = name.substring(3);
          } else {
            name = name.substring(2);
          }
          name = StringUtils.uncapitalize(name);
        } else {
          if (name.startsWith("get") || name.startsWith("is")) {
            continue;
          }
        }
        if (BASE_TYPES.containsKey(type)) {
          FieldInfo fieldInfo = new FieldInfo(name).setType(BASE_TYPES.get(type));
          fieldInfo.setDisplay(getDisplay(fieldInfo.getName()));
          list.add(fieldInfo);
          continue;
        }
        if (isAssignable(Number.class, type)) {
          FieldInfo fieldInfo = new FieldInfo(name).setType("Number");
          fieldInfo.setDisplay(getDisplay(fieldInfo.getName()));
          list.add(fieldInfo);
          continue;
        }
        if (type.isEnum()) {
          FieldInfo fieldInfo = new FieldInfo(name).setType("Enum");
          fieldInfo.setDisplay(getDisplay(fieldInfo.getName()));
          list.add(fieldInfo);
          continue;
        }
        if (isAssignable(BASE_ASSIGNABLE_TYPES, type)) {
          FieldInfo fieldInfo = new FieldInfo(name);
          fieldInfo.setDisplay(getDisplay(fieldInfo.getName()));
          list.add(fieldInfo);
          continue;
        }
        if (isAssignable(NON_RECURSE_TYPES, type)) {
          FieldInfo fieldInfo = new FieldInfo(name);
          fieldInfo.setDisplay(getDisplay(fieldInfo.getName()));
          list.add(fieldInfo);
          continue;
        }
        if (depth > 0) {
          classMap.put(name, type);
        }
      }
      for (Map.Entry<String, Class> entry : classMap.entrySet()) {
        List<FieldInfo> fieldInfos = getters(entry.getValue(), depth - 1);
        for (FieldInfo fieldInfo : fieldInfos) {
          fieldInfo.setName(entry.getKey() + "." + fieldInfo.getName());
          list.add(fieldInfo);
        }
      }
      return list;
    } catch (Exception e) {
      return list;
    }
  }

  /**
   * Identifies if subClass is a sub class of any of the superClasses
   *
   * @param superClasses The super classes
   * @param subClass     The sub class
   * @return true if assignable else false
   */
  public static boolean isAssignable(Collection<Class> superClasses, Class subClass) {
    for (Class superClass : superClasses) {
      if (isAssignable(superClass, subClass)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Creates an instance of class
   *
   * @param <T>   The type
   * @param clazz The class
   * @return The object. Null if any erro occurs
   */
  public static <T> T getInstance(Class<T> clazz) {
    try {
      if (clazz.isInterface() || Modifier.isAbstract(clazz.getModifiers())) {
        Reflections reflections = new Reflections("com.plummb");
        Set<Class<? extends T>> classes = reflections.getSubTypesOf(clazz);
        if (classes.size() != 1) {
          throw new CoreRtException(clazz + " has " + classes.size() + " implementations. Required only 1.");
        }
        clazz = cast(classes.iterator().next());
      }
      return clazz.newInstance();
    } catch (Exception e) {
      throw new CoreRtException(clazz + " could not be instantiated", e);
    }
  }

  /**
   * Gets parsed value.
   *
   * @param value  the value
   * @param object the object
   * @return the parsed value
   * @throws CoreRtException the core rt exception
   */
  public static String getParsedValue(final String value, Object object) throws CoreRtException {
    if (object == null || value == null) {
      return value;
    }
    Set<String> fields = fieldMap.get(value);
    String parsedValue = value;
    if (fields == null) {
      fields = getFields(value);
      fieldMap.put(value, fields);
    }
    if (!fields.isEmpty()) {
      Map<String, Object> valueMap = new HashMap<>();
      for (String field : fields) {
        valueMap.put(field, getValue(object, field));
      }
      parsedValue = StringSubstitutor.replace(value, valueMap);
    }
    return parsedValue;
  }

  /**
   * Gets fields.
   *
   * @param key the key
   * @return the fields
   */
  public static Set<String> getFields(String key) {
    Pattern pattern = Pattern.compile("\\$\\{([a-zA-Z0-9._]*)}");
    Matcher matcher = pattern.matcher(key);
    Set<String> set = new HashSet<>();
    while (matcher.find()) {
      set.add(matcher.group(1));
    }
    return set;
  }

  /**
   * Gets the value of provided property. The property can be DOT separated if nested.
   *
   * @param object    The object
   * @param longField The property
   * @return The value.
   * @throws CoreRtException in case of any exception
   */
  public static Object getValue(Object object, final String longField) throws CoreRtException {
    return getValue(object, longField, null);
  }

  /**
   * Gets the value of provided property. The property can be DOT separated if nested.
   *
   * @param object    The object
   * @param longField The property
   * @param option    the option
   * @return The value.
   * @throws CoreRtException in case of any exception
   */
  public static Object getValue(Object object, final String longField, ReflectOption option) throws CoreRtException {
    if (object == null) {
      return null;
    }
    try {
      String[] fields = longField.split("\\.");
      Object value = null;
      for (String field : fields) {
        if (object instanceof Map) {
          value = ((Map) object).get(field);
        } else {
          Class clazz = object.getClass();
          Method method = ReflectUtils.method(clazz, field, option);
          value = method.invoke(object);
        }
        if (value == null) {
          break;
        }
        object = value;
      }
      return value;
    } catch (Exception e) {
      throw new CoreRtException(e.getMessage(), e);
    }
  }

  /**
   * Method method.
   *
   * @param clazz    the clazz
   * @param property the property
   * @param option   the option
   * @return the method
   */
  public static Method method(Class clazz, String property, ReflectOption option) {
    if (option == null) {
      option = new ReflectOption();
    }
    if (!option.isGetter() && !option.isMethod()) {
      throw new CoreRtException("At least one of getter or method should be true.");
    }
    Method method = null;
    if (option.isGetterFirst()) {
      if (option.isGetter()) {
        method = getterOnly(clazz, property);
      }
      if (method == null && option.isMethod()) {
        method = methodOnly(clazz, property);
      }
    } else {
      if (option.isMethod()) {
        method = methodOnly(clazz, property);
      }
      if (method == null && option.isGetter()) {
        method = getterOnly(clazz, property);
      }
    }
    if (method == null) {
      throw new CoreRtException("Method / Getter for " + property + " not found for class:" + clazz);
    }
    return method;
  }

  private static Method getterOnly(Class clazz, String property) {
    BeanInfo beanInfo;
    try {
      beanInfo = Introspector.getBeanInfo(clazz, Object.class);
      PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
      for (PropertyDescriptor descriptor : descriptors) {
        if (StringUtils.equalsAny(descriptor.getName(), property, StringUtils.capitalize(property))) {
          Method method = descriptor.getReadMethod();
          if (method != null) {
            return method;
          }
          break;
        }
      }
    } catch (IntrospectionException e) {
    }
    return null;
  }

  private static Method methodOnly(Class clazz, String property) {
    try {
      return clazz.getMethod(property);
    } catch (NoSuchMethodException e) {
      return null;
    }
  }

  /**
   * Method or getter method.
   *
   * @param clazz    the clazz
   * @param property the property
   * @return the method
   */
  public static Method method(Class clazz, String property) {
    Method method;
    try {
      method = clazz.getMethod(property);
    } catch (NoSuchMethodException e) {
      throw new CoreRtException("Method for " + property + " not found for class:" + clazz);
    }
    return method;
  }

  /**
   * Finds the getter method of the property
   *
   * @param clazz    The class
   * @param property The property
   * @return The Method
   */
  public static Method getter(Class clazz, String property) {
    Method method = method(clazz, property, null);
    if (method == null) {
      throw new CoreRtException("Getter for " + property + " not found for class:" + clazz);
    }
    return method;
  }

  /**
   * Identifies if subClass is a sub class of the superClass
   *
   * @param superClass The super class
   * @param subClass   The sub class
   * @return true if assignable else false
   */
  public static boolean isAssignable(Class superClass, Class subClass) {
    return superClass.isAssignableFrom(subClass);
  }

  static {
    BASE_TYPES.put(boolean.class, "Boolean");
    BASE_TYPES.put(Boolean.class, "Boolean");

    BASE_TYPES.put(short.class, "Number");
    BASE_TYPES.put(byte.class, "Number");
    BASE_TYPES.put(int.class, "Number");
    BASE_TYPES.put(long.class, "Number");
    BASE_TYPES.put(Short.class, "Number");
    BASE_TYPES.put(Byte.class, "Number");
    BASE_TYPES.put(Integer.class, "Number");
    BASE_TYPES.put(Long.class, "Number");

    BASE_TYPES.put(float.class, "Number");
    BASE_TYPES.put(double.class, "Number");
    BASE_TYPES.put(Float.class, "Number");
    BASE_TYPES.put(Double.class, "Number");

    BASE_TYPES.put(char.class, "String");
    BASE_TYPES.put(Character.class, "String");
    BASE_TYPES.put(String.class, "String");

    BASE_TYPES.put(LocalDate.class, "Date");
    BASE_TYPES.put(ZonedDateTime.class, "Date");
    BASE_TYPES.put(LocalDateTime.class, "Date");

    BASE_ASSIGNABLE_TYPES.add(CharSequence.class);

    NON_OBJECT_TYPES.add(Iterator.class);
    NON_OBJECT_TYPES.add(Iterable.class);
    NON_OBJECT_TYPES.add(Map.class);
    NON_OBJECT_TYPES.add(AutoCloseable.class);

    NON_RECURSE_TYPES.add(Calendar.class);
    NON_RECURSE_TYPES.add(Date.class);
    NON_RECURSE_TYPES.add(Instant.class);
  }
}
