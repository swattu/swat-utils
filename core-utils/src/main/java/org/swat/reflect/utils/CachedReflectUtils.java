/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.reflect.utils;

import org.swat.core.utils.CoreRtException;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Reflect utils.
 *
 * @param <T> the type parameter
 * @author Swatantra Agrawal on 16-Aug-2017
 */
public class CachedReflectUtils<T> {
  private final Map<String, Method> methodMap = new HashMap<>();
  private final ReflectOption option;

  /**
   * Instantiates a new Cached reflect utils.
   */
  public CachedReflectUtils() {
    this(new ReflectOption());
  }

  /**
   * Instantiates a new Cached reflect utils.
   *
   * @param option the option
   */
  public CachedReflectUtils(ReflectOption option) {
    if (option == null) {
      option = new ReflectOption();
    }
    this.option = option;
  }

  /**
   * Gets the value of provided property. The property can be DOT separated if nested.
   *
   * @param object    The object
   * @param longField The property
   * @return The value.
   * @throws CoreRtException in case of any exception
   */
  public Object getValue(T object, final String longField) throws CoreRtException {
    if (object == null) {
      return null;
    }
    try {
      String[] fields = longField.split("\\.");
      Object value;
      String methodKey = "";
      Object theObject = object;
      for (String field : fields) {
        methodKey += field;
        if (object instanceof Map) {
          theObject = ((Map) object).get(field);
        } else {
          Object temp = theObject;
          Method method = methodMap.computeIfAbsent(methodKey, v -> ReflectUtils.method(temp.getClass(), field, option));
          theObject = method.invoke(theObject);
        }
        if (theObject == null) {
          break;
        }
      }
      value = theObject;
      return value;
    } catch (Exception e) {
      throw new CoreRtException(e.getMessage(), e);
    }
  }
}
