/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.reflect.utils;

import lombok.Data;

/**
 * The type Reflect option.
 */
@Data
public class ReflectOption {
  private boolean getter = true;
  private boolean method = false;
  private boolean getterFirst = true;
}
