/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.reflect.utils;

/**
 * The type Field info.
 */
public class FieldInfo {
  private String name;
  private String type;
  private boolean multi;
  private String display;

  /**
   * Instantiates a new Field info.
   */
  public FieldInfo() {
  }

  /**
   * Instantiates a new Field info.
   *
   * @param name the name
   */
  public FieldInfo(String name) {
    this(name, "STRING");
  }

  /**
   * Instantiates a new Field info.
   *
   * @param name the name
   * @param type the type
   */
  public FieldInfo(String name, String type) {
    this(name, type, false);
  }

  /**
   * Instantiates a new Field info.
   *
   * @param name  the name
   * @param type  the type
   * @param multi the multi
   */
  public FieldInfo(String name, String type, boolean multi) {
    this.name = name;
    this.type = type;
    this.multi = multi;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets name.
   *
   * @param name the name
   * @return the name
   */
  public FieldInfo setName(String name) {
    this.name = name;
    return this;
  }

  /**
   * Gets type.
   *
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * Sets type.
   *
   * @param type the type
   * @return the type
   */
  public FieldInfo setType(String type) {
    this.type = type;
    return this;
  }

  /**
   * Is multi boolean.
   *
   * @return the boolean
   */
  public boolean isMulti() {
    return multi;
  }

  /**
   * Sets multi.
   *
   * @param multi the multi
   * @return the multi
   */
  public FieldInfo setMulti(boolean multi) {
    this.multi = multi;
    return this;
  }

  /**
   * Gets display.
   *
   * @return the display
   */
  public String getDisplay() {
    return display;
  }

  /**
   * Sets display.
   *
   * @param display the display
   * @return the display
   */
  public FieldInfo setDisplay(String display) {
    this.display = display;
    return this;
  }
}
