# README #
This repository is all about utilities. This does not need any deployment.

### What is this repository for? ###
* Contains utilities for Configuration, Mongo, Postgres, Core, Spring MVC etc.

### Issues ###
* Issues can be found at [here](|https://bitbucket.org/swattu/swat-utils/issues?status=new&status=open)

### Contribution guidelines ###
* Nearly 100% code coverage is met using JUnit
* Code review is not done. 
* Raise a PR or Email for any requests, comments, suggestions

### Who do I talk to? ###
* Swatantra Agrawal
* Email me at [swattu@gmail.com](mailto:swattu@gmail.com)
* Profile at [LinkedIn](https://www.linkedin.com/in/aazad/)

## Other useful links ##
* Let's make software robust - [Plummb](http://www.plummb.com)
* Working [Java Examples](https://github.com/plummb/java-examples)
* Connect at [Facebook](https://www.facebook.com/swatz4u)
