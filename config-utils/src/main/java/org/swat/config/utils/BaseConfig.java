/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.config.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Properties;

/**
 * The abstract class for Base Configuration
 *
 * @author Swatantra Agrawal on 25/8/17.
 */
public abstract class BaseConfig {
  /**
   * Gets the {@link ConfigInfo}
   *
   * @param keys The keys in order
   * @return The Config Info
   */
  public ConfigInfo getConfigInfo(String... keys) {
    if (keys == null) {
      return ConfigInfo.NULL_INFO;
    }
    for (String key : keys) {
      String value = getStringInternal(key);
      if (StringUtils.isNotBlank(value)) {
        return new ConfigInfo(getLevel(), key, value);
      }
    }
    return ConfigInfo.NULL_INFO;
  }

  /**
   * Gets the String value of provided key
   *
   * @param key The key
   * @return The value
   */
  protected String getStringInternal(String key) {
    return getProperties().getProperty(key);
  }

  /**
   * The level of the configuration.
   *
   * @return The level
   */
  public abstract String getLevel();

  /**
   * Gets all the properties.
   *
   * @return Properties properties
   */
  public abstract Properties getProperties();

  /**
   * Overrides a property. To unset the value should be null.
   *
   * @param key   the key
   * @param value the value
   */
  public void setOverride(String key, String value) {
    if (key == null) {
      return;
    }
    if (value == null) {
      getProperties().remove(key);
    } else {
      getProperties().setProperty(key, value);
    }
  }

  /**
   * Gets int value
   *
   * @param defaultValue The default value
   * @param keys         The keys in order
   * @return The int
   */
  public int getInteger(int defaultValue, String... keys) {
    String value = getString(String.valueOf(defaultValue), keys);
    return NumberUtils.toInt(value, defaultValue);
  }

  /**
   * Returns the String value
   *
   * @param defaultValue The default value
   * @param keys         The keys in order
   * @return The String
   */
  public String getString(String defaultValue, String... keys) {
    if (keys == null) {
      return defaultValue;
    }
    for (String key : keys) {
      String value = getStringInternal(key);
      if (StringUtils.isNotBlank(value)) {
        return value;
      }
    }
    return defaultValue;
  }

  /**
   * Gets value.
   *
   * @param keys the keys
   * @return the value. Null if nothing found.
   */
  public String getValue(String... keys) {
    return getString(null, keys);
  }

  /**
   * Gets the Wrapper Integer
   *
   * @param keys The keys in order
   * @return The Integer
   */
  public Integer getIntegerValue(String... keys) {
    String value = getString(null, keys);
    if (value == null) {
      return null;
    }
    return Integer.parseInt(value);
  }

  /**
   * Returns the long value
   *
   * @param defaultValue The default value
   * @param keys         The keys in order
   * @return The long
   */
  public long getLong(long defaultValue, String... keys) {
    String value = getString(String.valueOf(defaultValue), keys);
    return NumberUtils.toLong(value, defaultValue);
  }

  /**
   * Returns the Wrapper long value
   *
   * @param keys The keys in order
   * @return The long
   */
  public Long getLongValue(String... keys) {
    String value = getString(null, keys);
    if (value == null) {
      return null;
    }
    return Long.parseLong(value);
  }

  /**
   * Gets double.
   *
   * @param defaultValue the default value
   * @param keys         the keys
   * @return the double
   */
  public double getDouble(double defaultValue, String... keys) {
    String value = getString(String.valueOf(defaultValue), keys);
    return NumberUtils.toDouble(value, defaultValue);
  }

  /**
   * Gets double value.
   *
   * @param keys the keys
   * @return the double value
   */
  public Double getDoubleValue(String... keys) {
    String value = getString(null, keys);
    if (value == null) {
      return null;
    }
    return Double.parseDouble(value);
  }

  /**
   * Returns the boolean value
   *
   * @param defaultValue The default value
   * @param keys         The keys in order
   * @return The boolean
   */
  public boolean getBoolean(boolean defaultValue, String... keys) {
    String value = getString(String.valueOf(defaultValue), keys);
    return Boolean.parseBoolean(value);
  }

  /**
   * Returns the Wrapper boolean value
   *
   * @param keys The keys in order
   * @return The Boolean
   */
  public Boolean getBooleanValue(String... keys) {
    String value = getString(null, keys);
    if (value == null) {
      return null;
    }
    return Boolean.valueOf(value);
  }
}
