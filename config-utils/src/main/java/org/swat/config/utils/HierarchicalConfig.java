/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.config.utils;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Properties;

import static org.swat.config.utils.ConfigInfo.NULL_INFO;

/**
 * Hierarchical Configuration
 *
 * @author Swatantra Agrawal on 25/8/17.
 */
public class HierarchicalConfig extends BaseConfig {
  private BaseConfig[] configs;

  /**
   * Instantiates a new Hierarchical config.
   *
   * @param configs the configs
   */
  public HierarchicalConfig(BaseConfig... configs) {
    if (ArrayUtils.isNotEmpty(configs) && configs[0] instanceof OverridingConfig) {
      this.configs = configs;
    } else {
      this.configs = ArrayUtils.insert(0, configs, new OverridingConfig());
    }
  }

  /**
   * With config hierarchical config.
   *
   * @param config the config
   * @return the hierarchical config
   */
  public HierarchicalConfig withConfig(BaseConfig config) {
    return new HierarchicalConfig(ArrayUtils.add(configs, config));
  }

  /**
   * Add config at the start
   *
   * @param config the config
   */
  public void addConfigFirst(BaseConfig config) {
    if (config != null) {
      configs = ArrayUtils.insert(1, configs, config);
    }
  }

  /**
   * Add config at the end
   *
   * @param config the config
   */
  public void addConfig(BaseConfig config) {
    if (config != null) {
      configs = ArrayUtils.add(configs, config);
    }
  }

  @Override
  public ConfigInfo getConfigInfo(String... keys) {
    for (BaseConfig config : configs) {
      ConfigInfo info = config.getConfigInfo(keys);
      if (StringUtils.isNotBlank(info.getString())) {
        return info;
      }
    }
    return NULL_INFO;
  }

  @Override
  protected String getStringInternal(String key) {
    throw new UnsupportedOperationException("This method must never be called.");
  }

  /**
   * Helpful in Overriding Configs
   *
   * @author Swatantra Agrawal on 16-Sep-2017.
   */
  public class OverridingConfig extends BaseConfig {
    private Properties properties = new Properties();

    @Override
    public String getLevel() {
      return "OVERRIDE";
    }

    @Override
    public Properties getProperties() {
      return properties;
    }
  }

  @Override
  public String getString(String defaultValue, final String... keys) {
    for (BaseConfig config : configs) {
      String value = config.getString(null, keys);
      if (StringUtils.isNotBlank(value)) {
        return value;
      }
    }
    return defaultValue;
  }

  @Override
  public String getLevel() {
    throw new UnsupportedOperationException("This method must never be called.");
  }

  @Override
  public Properties getProperties() {
    Properties properties = new Properties();
    for (int x = configs.length - 1; x >= 0; x--) {
      properties.putAll(configs[x].getProperties());
    }
    return properties;
  }

  @Override
  public void setOverride(String key, String value) {
    configs[0].setOverride(key, value);
  }
}
