/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.config.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by swat
 * on 13/9/17.
 */
public class ConfigConstants {
  /**
   * The constant SYS_ENV_CONFIG. Reads from System Environment.
   */
  public static final BaseConfig SYS_ENV_CONFIG = new SystemConfig(true);
  /**
   * The constant SYS_PRP_CONFIG. Reads from System Properties.
   */
  public static final BaseConfig SYS_PRP_CONFIG = new SystemConfig(false);
  /**
   * The constant SYS_PRP_ENV_CONFIG. Reads from System Properties and then Environment.
   */
  public static final BaseConfig SYS_PRP_ENV_CONFIG = new HierarchicalConfig(SYS_PRP_CONFIG, SYS_ENV_CONFIG);
  /**
   * The constant SYS_ENV_PRP_CONFIG. Reads from System Environment and then Properties.
   */
  public static final BaseConfig SYS_ENV_PRP_CONFIG = new HierarchicalConfig(SYS_ENV_CONFIG, SYS_PRP_CONFIG);

  /**
   * The Application config. Defined by swat.application.configs as Comma Separated Filenames.
   */
  public static final HierarchicalConfig APP_CONFIG;

  static {
    String fileNames = SYS_PRP_ENV_CONFIG.getString(null, "swat.application.configs");
    HierarchicalConfig config = new HierarchicalConfig();
    if (StringUtils.isNotBlank(fileNames)) {
      FileConfig fileConfig = new FileConfig();
      for (String fileName : fileNames.split(",")) {
        fileConfig.addFiles(true, fileName.trim());
      }
      config = config.withConfig(fileConfig);
    }
    APP_CONFIG = config;
  }
}
