/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.config.utils;

import java.util.Properties;

/**
 * The type System config.
 *
 * @author Swatantra Agrawal on 24-Aug-2017.
 */
public class SystemConfig extends BaseConfig {
  private final boolean environment;
  private final Properties sysProperties = new Properties();
  private final Properties properties = new Properties(sysProperties);

  /**
   * Instantiates a new System config.
   *
   * @param environment the environment
   */
  public SystemConfig(boolean environment) {
    this.environment = environment;
  }

  @Override
  public String getLevel() {
    return environment ? "SYS-ENV" : "SYS-PRP";
  }

  @Override
  public Properties getProperties() {
    sysProperties.clear();
    if (environment) {
      sysProperties.putAll(System.getenv());
    } else {
      sysProperties.putAll(System.getProperties());
    }
    return properties;
  }
}
