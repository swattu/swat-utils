/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.config.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * The File Config can read properties from File or Resource Stream
 *
 * @author Swatantra Agrawal on 24-Aug-2017.
 */
public class FileConfig extends BaseConfig {
  private final Properties fileProperties = new Properties();
  private final Properties properties = new Properties(fileProperties);

  /**
   * Instantiates a new File config.
   *
   * @param fileNames the file names
   */
  public FileConfig(String... fileNames) {
    for (String fileName : fileNames) {
      loadFile(true, fileName);
    }
  }

  private void loadFile(boolean resource, String fileName) {
    InputStream inputStream = null;
    try {
      if (resource) {
        inputStream = FileConfig.class.getClassLoader().getResourceAsStream(fileName);
      } else {
        File file = new File(fileName);
        if (file.exists()) {
          inputStream = new FileInputStream(file);
        }
      }
      if (inputStream != null) {
        fileProperties.load(inputStream);
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Add files.
   *
   * @param resource  true for resource stream, else false
   * @param fileNames the file names
   */
  public void addFiles(boolean resource, String... fileNames) {
    for (String fileName : fileNames) {
      loadFile(resource, fileName);
    }
  }

  @Override
  public String getLevel() {
    return "FILE";
  }

  @Override
  public Properties getProperties() {
    return properties;
  }
}
