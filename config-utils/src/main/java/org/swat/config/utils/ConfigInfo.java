/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.config.utils;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * @author Swatantra Agrawal
 * on 25/8/17.
 */
public final class ConfigInfo {
  /**
   * The singleton for Null Value
   */
  public static final ConfigInfo NULL_INFO = new ConfigInfo(null, null, null);
  private final String level;
  private final String key;
  private final String value;

  /**
   * Initializer
   *
   * @param level The level
   * @param key   The key
   * @param value The value
   */
  public ConfigInfo(String level, String key, String value) {
    this.level = level;
    this.key = key;
    this.value = value;
  }

  /**
   * Gets level.
   *
   * @return the level
   */
  public String getLevel() {
    return level;
  }

  /**
   * Gets key.
   *
   * @return the key
   */
  public String getKey() {
    return key;
  }

  /**
   * Gets string.
   *
   * @return the string
   */
  public String getString() {
    return value;
  }

  /**
   * Gets string.
   *
   * @param defaultValue the default value
   * @return the string
   */
  public String getString(String defaultValue) {
    if (value == null) {
      return defaultValue;
    }
    return value;
  }

  /**
   * Gets integer.
   *
   * @return the integer
   */
  public int getInteger() {
    return NumberUtils.toInt(value, 0);
  }

  /**
   * Gets integer.
   *
   * @param defaultValue the default value
   * @return the integer
   */
  public int getInteger(int defaultValue) {
    return NumberUtils.toInt(value, defaultValue);
  }

  /**
   * Gets long.
   *
   * @return the long
   */
  public long getLong() {
    return NumberUtils.toLong(value, 0L);
  }

  /**
   * Gets long.
   *
   * @param defaultValue the default value
   * @return the long
   */
  public long getLong(long defaultValue) {
    return NumberUtils.toLong(value, defaultValue);
  }

  /**
   * Gets boolean.
   *
   * @return the boolean
   */
  public boolean getBoolean() {
    return getBoolean(false);
  }

  /**
   * Gets boolean.
   *
   * @param defaultValue the default value
   * @return the boolean
   */
  public boolean getBoolean(boolean defaultValue) {
    if (value == null) {
      return defaultValue;
    }
    return Boolean.parseBoolean(value);
  }
}
