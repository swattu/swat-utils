/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.config.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Swatantra Agrawal
 * on 25/8/17.
 */
public class FileConfigTest {
  @Test
  public void verifyFileLoad() {
    FileConfig fileConfig = new FileConfig("org/swat/config/utils/test.properties");
    assertEquals("someValue", fileConfig.getString(null, "someKey"));
  }
}