/*
 * Copyright © 2017 Swatantra Agrawal. All rights reserved.
 */

package org.swat.config.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Swatantra Agrawal
 * on 25/8/17.
 */
public class HierarchicalConfigTest {
  @Test
  public void verifyLevels() {
    System.setProperty("someKey", "");

    HierarchicalConfig hierarchicalConfig = new HierarchicalConfig(new SystemConfig(false));
    ConfigInfo configInfo = hierarchicalConfig.getConfigInfo("someKey");
    assertEquals(null, configInfo.getLevel());

    System.setProperty("someKey", "systemValue");
    configInfo = hierarchicalConfig.getConfigInfo("someKey");
    assertEquals("SYS-PRP", configInfo.getLevel());
    assertEquals("systemValue", configInfo.getString());
    FileConfig fileConfig = new FileConfig("org/swat/config/utils/test.properties");
    hierarchicalConfig = hierarchicalConfig.withConfig(fileConfig);

    configInfo = hierarchicalConfig.getConfigInfo("someKey");
    assertEquals("SYS-PRP", configInfo.getLevel());
    assertEquals("systemValue", configInfo.getString());

    System.setProperty("someKey", "");

    configInfo = hierarchicalConfig.getConfigInfo("someKey");
    assertEquals("FILE", configInfo.getLevel());
    assertEquals("someValue", configInfo.getString());

    assertEquals(-1, hierarchicalConfig.getInteger(-1, "someInteger"));
    assertEquals(123, hierarchicalConfig.getInteger(-1, "someInt"));
    assertEquals(456, hierarchicalConfig.getLong(-1, "someLong"));
    assertEquals(true, hierarchicalConfig.getBoolean(false, "someBoolean"));
  }
}