/*
 * Copyright © 2023 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.service;

import org.junit.Before;
import org.swat.sql.SqlBaseTest;

public class SqlLockServiceImplTest extends LockServiceImplTest {
  private final SqlBaseTest baseTest = new SqlBaseTest();
  private AbstractSqlLockService lockService;

  @Override
  public AbstractSqlLockService getLockService() {
    return lockService;
  }

  @Before
  public void before() {
    baseTest.before();
    lockService = new AbstractSqlLockService(baseTest.getDataSource()) {
    };
    lockService.createIndexes();
  }
}
