/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.service;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class ProcessOptionsTest {
  @Test
  public void refreshAnytime() {
    ProcessOptions options = new ProcessOptions();
    LocalDate localDate = LocalDate.now();
    LocalDateTime dateTime;

    dateTime = LocalDateTime.of(localDate, LocalTime.now());
    assertRefresh(true, options, dateTime);

    dateTime = LocalDateTime.of(localDate, LocalTime.of(0, 0, 0));
    assertRefresh(true, options, dateTime);

    dateTime = LocalDateTime.of(localDate, LocalTime.of(23, 59, 59));
    assertRefresh(true, options, dateTime);
  }

  @Test
  public void refreshStart2End() {
    ProcessOptions options = new ProcessOptions();
    LocalDate localDate = LocalDate.now();
    LocalDateTime dateTime;

    options.setPreferredRefreshFrom(LocalTime.of(13, 0, 0,0));
    options.setPreferredRefreshTo(LocalTime.of(14, 0, 0));

    dateTime = LocalDateTime.of(localDate, LocalTime.of(12, 59, 59));
    assertRefresh(false, options, dateTime);

    dateTime = LocalDateTime.of(localDate, LocalTime.of(13, 0, 0));
    assertRefresh(true, options, dateTime);

    dateTime = LocalDateTime.of(localDate, LocalTime.of(13, 0, 1));
    assertRefresh(true, options, dateTime);

    dateTime = LocalDateTime.of(localDate, LocalTime.of(13, 59, 59));
    assertRefresh(true, options, dateTime);

    dateTime = LocalDateTime.of(localDate, LocalTime.of(14, 0, 0));
    assertRefresh(true, options, dateTime);

    dateTime = LocalDateTime.of(localDate, LocalTime.of(14, 0, 1));
    assertRefresh(false, options, dateTime);
  }

  @Test
  public void refreshEnd2Start() {
    ProcessOptions options = new ProcessOptions();
    LocalDate localDate = LocalDate.now();
    LocalDateTime dateTime;

    options.setPreferredRefreshFrom(LocalTime.of(14, 0, 0));
    options.setPreferredRefreshTo(LocalTime.of(13, 0, 0));

    dateTime = LocalDateTime.of(localDate, LocalTime.of(12, 59, 59));
    assertRefresh(true, options, dateTime);

    dateTime = LocalDateTime.of(localDate, LocalTime.of(13, 0, 0));
    assertRefresh(true, options, dateTime);

    dateTime = LocalDateTime.of(localDate, LocalTime.of(13, 0, 1));
    assertRefresh(false, options, dateTime);

    dateTime = LocalDateTime.of(localDate, LocalTime.of(13, 59, 59));
    assertRefresh(false, options, dateTime);

    dateTime = LocalDateTime.of(localDate, LocalTime.of(14, 0, 0));
    assertRefresh(true, options, dateTime);

    dateTime = LocalDateTime.of(localDate, LocalTime.of(14, 0, 1));
    assertRefresh(true, options, dateTime);
  }

  private void assertRefresh(boolean status, ProcessOptions options, LocalDateTime dateTime) {
    Date date = Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
    assertEquals(status, options.shouldRefresh(date));
  }
}
