/*
 * Copyright © 2023 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.service;

import org.junit.Test;
import org.swat.lock.bean.InstanceType;
import org.swat.lock.bean.Lock;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public abstract class LockServiceImplTest {

  @Test
  public void mutexCheck() {
    String key = UUID.randomUUID().toString();
    Lock lock = getLockService().mutex(key, 5);
    assertNotNull(lock);
    String lockId = lock.getId();
    lock = getLockService().mutex(key, 5);
    assertNull(lock);

    //As it's not deferred, lock will not be released.
    getLockService().releaseMutex(key);
    lock = getLockService().mutex(key, 5);
    assertNull(lock);

    lock = getLockService().defer(lockId, 10);
    assertNotNull(lock);
    getLockService().releaseMutex(key);
    lock = getLockService().mutex(key, 5);
    assertNotNull(lock);
  }

  public abstract LockService getLockService();

  @Test
  public void otherCheck() {
    String unique = UUID.randomUUID().toString();
    String key = UUID.randomUUID().toString();
    Lock lock1 = getLockService().lock("read", key, unique, 2, 10);
    assertNotNull(lock1);
    Lock lock2 = getLockService().lock("read", key, unique, 2, 10);
    assertNotNull(lock2);
    Lock lock3 = getLockService().lock("read", key, unique, 2, 10);
    assertNull(lock3);
  }

  @Test
  public void mixedCheck() {
    String unique = UUID.randomUUID().toString();
    String key = UUID.randomUUID().toString();
    Lock lock1 = getLockService().lock("read", key, unique, 2, 10);
    assertNotNull(lock1);
    Lock lock2 = getLockService().lock("update", key, unique, 2, 10);
    assertNull(lock2);
    getLockService().release(lock1.getId());
    lock2 = getLockService().lock("update", key, unique, 2, 10);
    assertNotNull(lock2);
    lock1 = getLockService().lock("read", key, unique, 2, 10);
    assertNull(lock1);
  }

  @Test
  public void assignDefer() {
    String unique = UUID.randomUUID().toString();
    String key = UUID.randomUUID().toString();
    Lock lock1 = getLockService().lock("read", key, unique, 2, 10);
    assertNotNull(lock1);
    String lockId = lock1.getId();
    assertEquals(InstanceType.SINGLE, lock1.getInstanceType());
    Lock lock2 = getLockService().assign(lockId);
    assertNull(lock2);

    lock2 = getLockService().defer(lockId, 10);
    assertNotNull(lock2);
    assertEquals(lockId, lock2.getId());
    assertEquals(InstanceType.MULTIPLE, lock2.getInstanceType());

    lock2 = getLockService().defer(lockId, 10);
    assertNull(lock2);

    lock2 = getLockService().assign(lockId);
    assertNotNull(lock2);
    assertEquals(lockId, lock2.getId());
    assertEquals(InstanceType.SINGLE, lock2.getInstanceType());
  }
}
