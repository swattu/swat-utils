/*
 * Copyright © 2023 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.service;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@ComponentScan(basePackages = {"org.swat.lock"})
@ContextConfiguration(classes = MongoLockServiceImplTest.class)
@DataMongoTest
@TestPropertySource(properties = "spring.mongodb.embedded.version=3.5.5")
public class MongoLockServiceImplTest extends LockServiceImplTest {
  @Autowired
  private MongoLockServiceImpl lockService;

  @Override
  public MongoLockServiceImpl getLockService() {
    return lockService;
  }
}
