/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import org.swat.lock.dao.AbstractLockDao;

@Service
public class LockDaoImpl extends AbstractLockDao {
  @Autowired
  private MongoTemplate mongoTemplate;

  @Override
  protected MongoTemplate getMongoTemplate() {
    return mongoTemplate;
  }
}
