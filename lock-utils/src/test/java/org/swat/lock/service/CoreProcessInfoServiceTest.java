/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.swat.lock.bean.ProcessInfo;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@ComponentScan(basePackages = {"org.swat.lock"})
@ContextConfiguration(classes = CoreProcessInfoServiceTest.class)
@DataMongoTest
@TestPropertySource(properties = "spring.mongodb.embedded.version=3.5.5")
public class CoreProcessInfoServiceTest {
  @Autowired
  private CoreProcessInfoService processInfoService;

  @Test
  public void check() {
    String name = UUID.randomUUID().toString();
    ProcessInfo processInfo = processInfoService.start(name);
    assertNotNull(processInfo);
    assertNull(processInfo.getLastProcessed());

    processInfo = processInfoService.start(name);
    assertNull(processInfo);

    String lastProcessed = "Interim-1";
    processInfo = processInfoService.interim(name, lastProcessed);
    assertNotNull(processInfo);
    assertEquals(lastProcessed, processInfo.getLastProcessed());

    lastProcessed = "Interim-2";
    processInfo = processInfoService.interim(name, lastProcessed);
    assertNotNull(processInfo);
    assertEquals(lastProcessed, processInfo.getLastProcessed());

    lastProcessed = "Perfect";
    processInfo = processInfoService.complete(name, lastProcessed);
    assertNotNull(processInfo);
    assertEquals(lastProcessed, processInfo.getLastProcessed());

    processInfo = processInfoService.interim(name, lastProcessed);
    assertNull(processInfo);

    processInfo = processInfoService.complete(name, lastProcessed);
    assertNull(processInfo);
  }


  @Test
  public void checkExpiry() throws InterruptedException {
    String name = UUID.randomUUID().toString();
    ProcessInfo processInfo = processInfoService.start(name, 1);
    assertNotNull(processInfo);
    assertNull(processInfo.getLastProcessed());
    Thread.sleep(2000);

    String lastProcessed = "Perfect";
    processInfo = processInfoService.complete(name, lastProcessed);
    assertNotNull(processInfo);
    assertEquals(lastProcessed, processInfo.getLastProcessed());

    processInfo = processInfoService.start(name, 5);
    assertNotNull(processInfo);
    assertNull(processInfo.getLastProcessed());
    processInfo = processInfoService.complete(name, lastProcessed);
    assertNotNull(processInfo);
    assertEquals(lastProcessed, processInfo.getLastProcessed());

    Thread.sleep(2000);
    processInfo = processInfoService.start(name, 5);
    assertNotNull(processInfo);
    assertNotNull(processInfo.getLastProcessed());
  }

  @Test
  public void checkTimeout() throws InterruptedException {
    String name = UUID.randomUUID().toString();
    ProcessOptions options = new ProcessOptions();
    options.setDeemedTimeoutSeconds(5);
    ProcessInfo processInfo = processInfoService.start(name, options);
    assertNotNull(processInfo);
    assertNull(processInfo.getLastProcessed());
    Thread.sleep(2000);

    String lastProcessed = "Perfect";
    processInfo = processInfoService.start(name, options);
    assertNull(processInfo);

    Thread.sleep(4000);
    processInfo = processInfoService.start(name, options);
    assertNotNull(processInfo);
    assertNull(processInfo.getLastProcessed());
  }

  @Test
  public void checkRefresh() {
    String name = UUID.randomUUID().toString();
    ProcessInfo processInfo = processInfoService.start(name);
    assertNotNull(processInfo);
    assertNull(processInfo.getLastProcessed());

    String lastProcessed = "Perfect";
    processInfo = processInfoService.complete(name, lastProcessed);
    assertNotNull(processInfo);
    assertEquals(lastProcessed, processInfo.getLastProcessed());

    processInfoService.refresh(name);

    processInfo = processInfoService.start(name);
    assertNotNull(processInfo);
    assertNull(processInfo.getLastProcessed());
  }

  @Test
  public void multiInstance() {
    System.setProperty("INSTANCE_ID", "1");
    String name = UUID.randomUUID().toString();
    ProcessInfo processInfo = processInfoService.start(name);
    assertNotNull(processInfo);
    assertNull(processInfo.getLastProcessed());

    processInfo = processInfoService.start(name);
    assertNull(processInfo);

    System.setProperty("INSTANCE_ID", "2");
    String lastProcessed = "Interim-1";
    processInfo = processInfoService.interim(name, lastProcessed);
    assertNull(processInfo);

    lastProcessed = "Perfect";
    processInfo = processInfoService.complete(name, lastProcessed);
    assertNull(processInfo);

    System.setProperty("INSTANCE_ID", "1");
    processInfo = processInfoService.interim(name, lastProcessed);
    assertNotNull(processInfo);
    assertEquals(lastProcessed, processInfo.getLastProcessed());

    processInfo = processInfoService.complete(name, lastProcessed);
    assertNotNull(processInfo);
    assertEquals(lastProcessed, processInfo.getLastProcessed());

    System.setProperty("INSTANCE_ID", "");
  }
}
