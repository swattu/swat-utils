/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.config;

import org.springframework.beans.BeansException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.swat.lock.dao.CoreProcessInfoDao;
import org.swat.lock.service.CoreProcessInfoService;

/**
 * The type Lock beans.
 */
@Configuration
public class LockBeans implements ApplicationContextAware {
  private ApplicationContext applicationContext;

  /**
   * Process info dao abstract process info dao.
   *
   * @param mongoTemplate the mongo template
   * @return the abstract process info dao
   */
  @Bean
  @Lazy
  @ConditionalOnMissingBean
  public CoreProcessInfoDao processInfoDao(MongoTemplate mongoTemplate) {
    return autowire(new CoreProcessInfoDao() {
      @Override
      protected MongoTemplate getMongoTemplate() {
        return mongoTemplate;
      }
    });
  }

  /**
   * Process info service abstract process info service.
   *
   * @return the abstract process info service
   */
  @Bean
  @Lazy
  @ConditionalOnMissingBean
  public CoreProcessInfoService processInfoService() {
    CoreProcessInfoService bean = new CoreProcessInfoService();
    return autowire(bean);
  }

  private <T> T autowire(T bean) {
    applicationContext.getAutowireCapableBeanFactory().autowireBean(bean);
    return bean;
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }
}
