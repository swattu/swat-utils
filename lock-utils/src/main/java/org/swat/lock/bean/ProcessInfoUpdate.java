/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.bean;

import org.springframework.data.mongodb.core.query.Update;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;

/**
 * The type Process info update.
 */
public class ProcessInfoUpdate extends Update {
  private final Date OPEN_DATE = Date.from(LocalDate.of(2100, 12, 31).atStartOfDay().toInstant(ZoneOffset.UTC));
  /**
   * Started at process info update.
   *
   * @param value the value
   * @return the process info update
   */
  public ProcessInfoUpdate startedAt(Date value) {
    set("startedAt", value);
    return this;
  }

  /**
   * Last processed at process info update.
   *
   * @param value the value
   * @return the process info update
   */
  public ProcessInfoUpdate lastProcessed(Object value) {
    set("lastProcessed", value);
    return this;
  }

  /**
   * Instance id process info update.
   *
   * @param value the value
   * @return the process info update
   */
  public ProcessInfoUpdate instanceId(String value) {
    set("instanceId", value);
    return this;
  }

  /**
   * Status process info update.
   *
   * @param value the value
   * @return the process info update
   */
  public ProcessInfoUpdate status(ProcessStatus value) {
    set("status", value);
    return this;
  }

  /**
   * Expires after process info update.
   *
   * @param fullRefreshSeconds the full refresh seconds
   * @return the process info update
   */
  public ProcessInfoUpdate expiresAfter(int fullRefreshSeconds) {
    if (fullRefreshSeconds > 0) {
      setOnInsert("expiresAt", new Date(System.currentTimeMillis() + fullRefreshSeconds * 1000L));
    } else {
      setOnInsert("expiresAt", OPEN_DATE);
    }
    return this;
  }
}
