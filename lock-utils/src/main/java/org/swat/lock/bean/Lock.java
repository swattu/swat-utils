/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.bean;

import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.swat.json.utils.CustomFieldAware;
import org.swat.mongo.dao.AbstractEntityInternal;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The type Lock.
 */
@Data
@CompoundIndexes(@CompoundIndex(name = "unique_index", unique = true, def = "{key:1, unique:1, counter:1}"))
public class Lock extends AbstractEntityInternal implements CustomFieldAware {
  private String id;
  private String instanceId;
  private InstanceType instanceType;
  private String group;
  private String key;
  private String unique;
  private int counter;
  @Indexed(expireAfterSeconds = 0)
  private Date expiresAt;
  private Map<String, Object> customFields = new LinkedHashMap<>();

  @Override
  public Map<String, Object> customFields() {
    return customFields;
  }
}
