/*
 * Copyright © 2023 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.bean;

import lombok.Data;

/**
 * The type Lock options.
 */
@Data
public class LockOptions {
  private String key;
  private long waitSecs;
  private String group;
  private String unique;
  private int max;
}
