/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.bean;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

/**
 * The type Process info query.
 */
public class ProcessInfoQuery extends Query {
    /**
     * Name process info query.
     *
     * @param value the value
     * @return the process info query
     */
    public ProcessInfoQuery name(String value) {
        addCriteria(Criteria.where("name").is(value));
        return this;
    }

    /**
     * Status process info query.
     *
     * @param value the value
     * @return the process info query
     */
    public ProcessInfoQuery status(ProcessStatus value) {
        addCriteria(Criteria.where("status").is(value));
        return this;
    }

    /**
     * Instance id process info query.
     *
     * @param value the value
     * @return the process info query
     */
    public ProcessInfoQuery instanceId(String value) {
        addCriteria(Criteria.where("instanceId").is(value));
        return this;
    }
}
