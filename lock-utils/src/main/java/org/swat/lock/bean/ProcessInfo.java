/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.bean;

import lombok.Data;
import org.swat.json.utils.CustomFieldAware;
import org.swat.mongo.dao.AbstractEntityInternal;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The type Process info.
 */
@Data
public class ProcessInfo extends AbstractEntityInternal implements CustomFieldAware {
    private String id;
    private String name;
    private ProcessStatus status;
    private Date expiresAt;
    private Date startedAt;
    private Object lastProcessed;
    private String instanceId;
    private int counter;
    private final Map<String, Object> customFields = new LinkedHashMap<>();

    @Override
    public Map<String, Object> customFields() {
        return customFields;
    }
}
