/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.bean;

/**
 * The enum Instance type.
 */
public enum InstanceType {
  /**
   * Single instance type.
   */
  SINGLE,
  /**
   * Multiple instance type.
   */
  MULTIPLE
}
