/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.bean;

/**
 * The enum Process status.
 */
public enum ProcessStatus {
  /**
   * Running process status.
   */
  RUNNING,
  /**
   * Completed process status.
   */
  COMPLETED;
}
