/*
 * Copyright © 2023 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.service;

import org.swat.lock.bean.Lock;
import org.swat.lock.bean.LockOptions;

/**
 * The interface Lock service.
 */
public interface LockService {
  /**
   * The constant MUTEX.
   */
  String MUTEX = "MUTEX";
  /**
   * The constant EXPIRE_SEC.
   */
  int EXPIRE_SEC = 300;

  /**
   * Mutex lock.
   *
   * @param key      the key
   * @param waitSecs the wait secs
   * @return the lock
   */
  @Deprecated
  Lock mutex(String key, long waitSecs);

  /**
   * Mutex lock.
   *
   * @param options the options
   * @return the lock
   */
  Lock mutex(LockOptions options);

  /**
   * Lock lock.
   *
   * @param group    the group
   * @param key      the key
   * @param unique   the unique
   * @param max      the max
   * @param waitSecs the wait secs
   * @return the lock
   */
  @Deprecated
  Lock lock(String group, String key, String unique, int max, long waitSecs);

  /**
   * Lock lock.
   *
   * @param options the options
   * @return the lock
   */
  Lock lock(LockOptions options);

  /**
   * Release.
   *
   * @param lockId the lock id
   */
  void release(String lockId);

  /**
   * Release mutex only if it is in deferred state.
   *
   * @param key the key
   */
  void releaseMutex(String key);

  /**
   * Assign lock.
   *
   * @param lockId the lock id
   * @return the lock
   */
  Lock assign(String lockId);

  /**
   * Defer lock.
   *
   * @param lockId    the lock id
   * @param deferSecs the defer secs
   * @return the lock
   */
  Lock defer(String lockId, int deferSecs);

  /**
   * Find lock.
   *
   * @param lockId the lock id
   * @return the lock
   */
  Lock find(String lockId);
}
