/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.swat.core.utils.InstanceUtil;
import org.swat.lock.bean.ProcessInfo;
import org.swat.lock.bean.ProcessInfoQuery;
import org.swat.lock.bean.ProcessInfoUpdate;
import org.swat.lock.bean.ProcessStatus;
import org.swat.lock.dao.CoreProcessInfoDao;

import javax.annotation.PreDestroy;
import java.time.Instant;
import java.util.Date;

import static org.swat.lock.bean.ProcessStatus.COMPLETED;
import static org.swat.lock.bean.ProcessStatus.RUNNING;

/**
 * The type Core process info service.
 */
public class CoreProcessInfoService {
  @Autowired
  private CoreProcessInfoDao processInfoDao;

  /**
   * Start process info.
   *
   * @param name the name
   * @return the process info
   */
  public ProcessInfo start(String name) {
    return start(name, -1);
  }

  /**
   * Inspect process info.
   *
   * @param name the name
   * @return the process info
   */
  public ProcessInfo inspect(String name) {
    ProcessInfoQuery query = new ProcessInfoQuery();
    query.name(name);

    ProcessInfo processInfo = processInfoDao.findOne(query);
    return processInfo;
  }

  /**
   * Start process info.
   *
   * @param name               the name
   * @param fullRefreshSeconds the full refresh seconds.
   * @return the process info
   */
  public ProcessInfo start(String name, int fullRefreshSeconds) {
    ProcessOptions options = new ProcessOptions();
    options.setFullRefreshSeconds(fullRefreshSeconds);
    return start(name, options);
  }

  /**
   * Start process info.
   *
   * @param name    the name
   * @param options the options
   * @return the process info
   */
  public ProcessInfo start(String name, ProcessOptions options) {
    ProcessInfo processInfo = processInfoDao.findOrCreate(name, options.getFullRefreshSeconds());
    Date now = new Date();
    if (processInfo.getExpiresAt().before(now) && options.shouldRefresh(now)) {
      ProcessInfoQuery query = new ProcessInfoQuery();
      query.name(name);
      query.status(COMPLETED);
      processInfoDao.removeAll(query);
      processInfoDao.findOrCreate(name, options.getFullRefreshSeconds());
    }

    ProcessInfoQuery query = new ProcessInfoQuery();
    query.name(name);
    Criteria orCriteria = new Criteria();
    Criteria completed = Criteria.where("status").is(COMPLETED);
    Criteria timedOut = Criteria.where("status").is(RUNNING).and("updatedAt")
        .lt(Date.from(Instant.now().minusSeconds(options.getDeemedTimeoutSeconds())));
    orCriteria.orOperator(completed, timedOut);
    query.addCriteria(orCriteria);

    ProcessInfoUpdate update = new ProcessInfoUpdate();
    update.status(ProcessStatus.RUNNING);
    update.startedAt(now);
    update.instanceId(InstanceUtil.getInstanceId());
    update.inc("counter", 1);
    processInfo = processInfoDao.modifyAndFind(query, update);
    return processInfo;
  }

  /**
   * Marks the info for full refresh. It still allows existing process to complete.
   *
   * @param name the name
   */
  public void refresh(String name) {
    ProcessInfoQuery query = new ProcessInfoQuery();
    query.name(name);

    ProcessInfoUpdate update = new ProcessInfoUpdate();
    update.set("expiresAt", new Date());

    processInfoDao.modifyAndFind(query, update);
  }

  /**
   * Interim process info.
   *
   * @param name          the name
   * @param lastProcessed the last processed
   * @return the process info
   */
  public ProcessInfo interim(String name, Object lastProcessed) {
    return update(name, lastProcessed, true);
  }

  /**
   * Complete process info.
   *
   * @param name          the name
   * @param lastProcessed the last processed
   * @return the process info
   */
  public ProcessInfo complete(String name, Object lastProcessed) {
    return update(name, lastProcessed, false);
  }

  private ProcessInfo update(String name, Object lastProcessed, boolean interim) {
    ProcessInfoQuery query = new ProcessInfoQuery();
    query.name(name);
    query.status(ProcessStatus.RUNNING);
    query.instanceId(InstanceUtil.getInstanceId());

    ProcessInfoUpdate update = new ProcessInfoUpdate();
    if (!interim) {
      update.status(COMPLETED);
    }
    update.lastProcessed(lastProcessed);

    return processInfoDao.modifyAndFind(query, update);
  }

  /**
   * Release all.
   */
  @PreDestroy
  public void releaseAll() {
    ProcessInfoQuery query = new ProcessInfoQuery();
    query.status(ProcessStatus.RUNNING);
    query.instanceId(InstanceUtil.getInstanceId());

    ProcessInfoUpdate update = new ProcessInfoUpdate();
    update.status(COMPLETED);

    processInfoDao.updateMulti(query, update);
  }
}
