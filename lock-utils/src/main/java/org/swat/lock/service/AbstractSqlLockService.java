/*
 * Copyright © 2023 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.swat.core.utils.CoreRtException;
import org.swat.core.utils.InstanceUtil;
import org.swat.lock.bean.InstanceType;
import org.swat.lock.bean.Lock;
import org.swat.lock.bean.LockOptions;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * The type Lock service.
 */
@Slf4j
public abstract class AbstractSqlLockService implements LockService {
  private final DataSource dataSource;

  /**
   * Instantiates a new Abstract sql lock service.
   *
   * @param dataSource the data source
   */
  public AbstractSqlLockService(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  /**
   * Create indexes.
   */
  @PostConstruct
  public void createIndexes() {
    execute(
        "CREATE TABLE IF NOT EXISTS LOCK (ID VARCHAR2, INSTANCE_ID VARCHAR2, INSTANCE_TYPE VARCHAR2, LOCK_GROUP VARCHAR2, " +
            "LOCK_KEY VARCHAR2, LOCK_UNIQUE VARCHAR2, COUNTER NUMBER, EXPIRES_AT TIMESTAMP)");
  }

  private int execute(String sql, Object... values) {
    try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
      for (int index = 1; index <= values.length; index++) {
        statement.setObject(index, values[index - 1]);
      }
      return statement.executeUpdate();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Lock mutex(String key, long waitSecs) {
    return lock(MUTEX, key, null, 1, waitSecs);
  }

  @Override
  public Lock mutex(LockOptions options) {
    options.setGroup(MUTEX);
    return lock(options);
  }

  @Override
  public Lock lock(String group, String key, String unique, int max, long waitSecs) {
    LockOptions options = new LockOptions();
    options.setGroup(group);
    options.setKey(key);
    options.setUnique(unique);
    options.setMax(max);
    options.setWaitSecs(waitSecs);
    return lock(options);
  }

  @Override
  public Lock lock(LockOptions options) {
    String group = options.getGroup();
    String key = options.getKey();
    String unique = options.getUnique();
    int max = options.getMax();
    long waitSecs = options.getWaitSecs();
    if (waitSecs < 2) {
      waitSecs = 2;
    }
    group = StringUtils.upperCase(group);
    if (max < 1 || StringUtils.equals(group, MUTEX)) {
      max = 1;
    }
    if (StringUtils.equals(group, MUTEX)) {
      unique = null;
    }
    Lock lock = new Lock();
    lock.setInstanceId(InstanceUtil.getInstanceId());
    lock.setInstanceType(InstanceType.SINGLE);
    lock.setGroup(group);
    lock.setKey(key);
    lock.setUnique(unique);
    String lockId = null;
    final long startTime = System.currentTimeMillis();
    while (true) {
      try {
        if (System.currentTimeMillis() - startTime > waitSecs * 1000) {
          return null;
        }
        int counter = canLock(key, group, max, lockId);
        if (counter < 0) {
          throw new CoreRtException(group + " Lock for " + key + " is not available");
        }
        if (StringUtils.isNotBlank(lockId)) {
          return lock;
        }
        lock.setCounter(counter);
        lock.setExpiresAt(new Date(System.currentTimeMillis() + EXPIRE_SEC * 1000L));

        lock.setId(UUID.randomUUID().toString());
        execute("INSERT INTO LOCK (ID, INSTANCE_ID, INSTANCE_TYPE, LOCK_GROUP, LOCK_KEY, LOCK_UNIQUE, COUNTER, EXPIRES_AT) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", lock.getId(), lock.getInstanceId(), lock.getInstanceType()
            .toString(), lock.getGroup(), lock.getKey(), lock.getUnique(), lock.getCounter(), Timestamp.valueOf(lock.getExpiresAt()
            .toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()));
        lockId = lock.getId();
      } catch (Exception e) {
        log.warn("Exception while taking " + group + " lock for " + key + " - {}", e.getMessage());
      }
      try {
        Thread.sleep(1000);
      } catch (InterruptedException ignore) {
        throw new CoreRtException(ignore);
      }
    }
  }

  @Override
  public void release(String lockId) {
    execute("DELETE LOCK WHERE ID = ? AND INSTANCE_ID = ? AND INSTANCE_TYPE = ?", lockId, InstanceUtil.getInstanceId(), InstanceType.SINGLE.toString());
  }

  @Override
  public void releaseMutex(String key) {
    execute("DELETE LOCK WHERE LOCK_GROUP = ? AND LOCK_KEY = ? AND INSTANCE_TYPE = ?", MUTEX, key, InstanceType.MULTIPLE.toString());
  }

  @Override
  public Lock assign(String lockId) {
    int status =
        execute("UPDATE LOCK SET EXPIRES_AT = ?, INSTANCE_ID = ?, INSTANCE_TYPE = ? WHERE ID = ? AND INSTANCE_TYPE = ?", new Timestamp(
            System.currentTimeMillis() + EXPIRE_SEC *
                1000), InstanceUtil.getInstanceId(), InstanceType.SINGLE.toString(), lockId, InstanceType.MULTIPLE.toString());
    if (status == 1) {
      return find(lockId);
    }
    return null;
  }

  @Override
  public Lock defer(String lockId, int deferSecs) {
    if (deferSecs < EXPIRE_SEC) {
      deferSecs = EXPIRE_SEC;
    }

    int status =
        execute("UPDATE LOCK SET EXPIRES_AT = ?, INSTANCE_TYPE = ? WHERE ID = ? AND INSTANCE_ID = ? AND INSTANCE_TYPE = ?", new Timestamp(
            System.currentTimeMillis() + deferSecs *
                1000L), InstanceType.MULTIPLE.toString(), lockId, InstanceUtil.getInstanceId(), InstanceType.SINGLE.toString());
    if (status == 1) {
      return find(lockId);
    }
    return null;
  }

  public Lock find(String lockId) {
    List<Lock> locks = findAll(lockId, null, null, null);
    if (locks.size() != 1) {
      return null;
    }
    return locks.get(0);
  }

  private int canLock(String key, String group, int max, String lockId) {
    List<Integer> counterList = counterList(max);

    List<Lock> locks = findAll(null, null, null, key);
    int counter = -1;
    for (Lock lock : locks) {
      if (!StringUtils.equals(group, lock.getGroup())) {
        return -1;
      }
      if (StringUtils.equals(lock.getId(), lockId)) {
        counter = lock.getCounter();
      }
      counterList.remove((Integer) lock.getCounter());
    }
    if (counter != -1) {
      return counter;
    }
    if (counterList.isEmpty()) {
      return -1;
    }
    Collections.shuffle(counterList);
    return counterList.get(0);
  }

  private List<Integer> counterList(int max) {
    List<Integer> list = new ArrayList<>();
    for (int index = 0; index < max; index++) {
      list.add(index);
    }
    return list;
  }

  private List<Lock> findAll(String lockId, String instanceId, InstanceType instanceType, String key) {
    String sql = "SELECT * FROM LOCK WHERE 1 = 1";
    List<Object> values = new ArrayList<>();
    if (StringUtils.isNotBlank(lockId)) {
      sql += " AND ID = ?";
      values.add(lockId);
    }
    if (StringUtils.isNotBlank(instanceId)) {
      sql += " AND INSTANCE_ID = ?";
      values.add(instanceId);
    }
    if (instanceType != null) {
      sql += " AND INSTANCE_TYPE = ?";
      values.add(instanceType.toString());
    }
    if (StringUtils.isNotBlank(key)) {
      sql += " AND LOCK_KEY = ?";
      values.add(key);
    }
    try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
      for (int x = 0; x < values.size(); x++) {
        statement.setObject(x + 1, values.get(x));
      }
      ResultSet resultSet = statement.executeQuery();
      List<Lock> locks = new ArrayList<>();
      while (resultSet.next()) {
        Lock lock = new Lock();
        lock.setId(resultSet.getString("ID"));
        lock.setInstanceId(resultSet.getString("INSTANCE_ID"));
        lock.setInstanceType(InstanceType.valueOf(resultSet.getString("INSTANCE_TYPE")));
        lock.setGroup(resultSet.getString("LOCK_GROUP"));
        lock.setKey(resultSet.getString("LOCK_KEY"));
        lock.setCounter(resultSet.getInt("COUNTER"));
        lock.setExpiresAt(resultSet.getTimestamp("EXPIRES_AT"));
        lock.setUnique(resultSet.getString("LOCK_UNIQUE"));
        locks.add(lock);
      }
      return locks;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Extend locks.
   */
  @Scheduled(initialDelay = 60000, fixedDelay = 120000)
  public void extendLocks() {
    execute("UPDATE LOCK SET EXPIRES_AT = ? WHERE INSTANCE_ID = ? AND INSTANCE_TYPE = ?", new Timestamp(
        System.currentTimeMillis() + EXPIRE_SEC * 1000), InstanceUtil.getInstanceId(), InstanceType.SINGLE.toString());
  }
}
