/*
 * Copyright © 2023 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.swat.core.utils.CoreRtException;
import org.swat.core.utils.InstanceUtil;
import org.swat.lock.bean.InstanceType;
import org.swat.lock.bean.Lock;
import org.swat.lock.bean.LockOptions;
import org.swat.lock.dao.AbstractLockDao;
import org.swat.lock.dao.LockDao;
import org.swat.lock.dao.LockQuery;
import org.swat.lock.dao.LockUpdate;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * The type Lock service.
 */
@Slf4j
public abstract class AbstractMongoLockService implements LockService {
  @Autowired
  private LockDao lockDao;

  /**
   * Create indexes.
   */
  @PostConstruct
  public void createIndexes() {
    if (lockDao instanceof AbstractLockDao) {
      ((AbstractLockDao) lockDao).createIndexes();
    }
  }

  @Override
  public Lock mutex(String key, long waitSecs) {
    return lock(MUTEX, key, null, 1, waitSecs);
  }

  @Override
  public Lock mutex(LockOptions options) {
    options.setGroup(MUTEX);
    return lock(options);
  }

  @Override
  public Lock lock(String group, String key, String unique, int max, long waitSecs) {
    LockOptions options = new LockOptions();
    options.setGroup(group);
    options.setKey(key);
    options.setUnique(unique);
    options.setMax(max);
    options.setWaitSecs(waitSecs);
    return lock(options);
  }

  @Override
  public Lock lock(LockOptions options) {
    String group = options.getGroup();
    String key = options.getKey();
    String unique = options.getUnique();
    int max = options.getMax();
    long waitSecs = options.getWaitSecs();
    if (waitSecs < 2) {
      waitSecs = 2;
    }
    group = StringUtils.upperCase(group);
    if (max < 1 || StringUtils.equals(group, MUTEX)) {
      max = 1;
    }
    if (StringUtils.equals(group, MUTEX)) {
      unique = null;
    }
    Lock lock = new Lock();
    onCreate(lock);
    lock.setInstanceId(InstanceUtil.getInstanceId());
    lock.setInstanceType(InstanceType.SINGLE);
    lock.setGroup(group);
    lock.setKey(key);
    lock.setUnique(unique);
    String lockId = null;
    final long startTime = System.currentTimeMillis();
    while (true) {
      try {
        if (System.currentTimeMillis() - startTime > waitSecs * 1000) {
          return null;
        }
        int counter = canLock(key, group, max, lockId);
        if (counter < 0) {
          throw new CoreRtException(group + " Lock for " + key + " is not available");
        }
        if (StringUtils.isNotBlank(lockId)) {
          return lock;
        }
        lock.setCounter(counter);
        lock.setExpiresAt(new Date(System.currentTimeMillis() + EXPIRE_SEC * 1000L));
        lockDao.insert(lock);
        lockId = lock.getId();
      } catch (Exception e) {
        log.warn("Exception while taking " + group + " lock for " + key + " - {}", e.getMessage());
      }
      try {
        Thread.sleep(1000);
      } catch (InterruptedException ignore) {
        throw new CoreRtException(ignore);
      }
    }
  }

  @Override
  public void release(String lockId) {
    LockQuery query = new LockQuery();
    query.lockId(lockId);
    query.instanceId(InstanceUtil.getInstanceId());
    query.instanceType(InstanceType.SINGLE);
    lockDao.removeAll(query);
  }

  @Override
  public void releaseMutex(String key) {
    LockQuery query = new LockQuery();
    query.group(MUTEX);
    query.key(key);
    query.instanceType(InstanceType.MULTIPLE);
    lockDao.removeAll(query);
  }

  @Override
  public Lock assign(String lockId) {
    LockQuery query = new LockQuery();
    query.lockId(lockId);
    query.instanceType(InstanceType.MULTIPLE);

    LockUpdate update = new LockUpdate();
    update.expiresAt(System.currentTimeMillis() + EXPIRE_SEC * 1000L);
    update.instanceId(InstanceUtil.getInstanceId());
    update.instanceType(InstanceType.SINGLE);
    onAssign(update);
    return lockDao.modifyAndFind(query, update);
  }

  @Override
  public Lock defer(String lockId, int deferSecs) {
    if (deferSecs < EXPIRE_SEC) {
      deferSecs = EXPIRE_SEC;
    }
    LockQuery query = new LockQuery();
    query.lockId(lockId);
    query.instanceId(InstanceUtil.getInstanceId());
    query.instanceType(InstanceType.SINGLE);

    LockUpdate update = new LockUpdate();
    update.expiresAt(System.currentTimeMillis() + deferSecs * 1000L);
    update.instanceType(InstanceType.MULTIPLE);
    onDefer(update);
    return lockDao.modifyAndFind(query, update);
  }

  @Override
  public Lock find(String lockId) {
    LockQuery query = new LockQuery();
    query.lockId(lockId);
    query.instanceType(InstanceType.SINGLE);
    query.instanceId(InstanceUtil.getInstanceId());

    return lockDao.findOne(query);
  }

  private int canLock(String key, String group, int max, String lockId) {
    List<Integer> counterList = counterList(max);
    LockQuery query = new LockQuery();
    query.key(key);

    List<Lock> locks = lockDao.findAll(query);
    int counter = -1;
    for (Lock lock : locks) {
      if (!StringUtils.equals(group, lock.getGroup())) {
        return -1;
      }
      if (StringUtils.equals(lock.getId(), lockId)) {
        counter = lock.getCounter();
      }
      counterList.remove((Integer) lock.getCounter());
    }
    if (counter != -1) {
      return counter;
    }
    if (counterList.isEmpty()) {
      return -1;
    }
    Collections.shuffle(counterList);
    return counterList.get(0);
  }

  private List<Integer> counterList(int max) {
    List<Integer> list = new ArrayList<>();
    for (int index = 0; index < max; index++) {
      list.add(index);
    }
    return list;
  }

  /**
   * On create.
   *
   * @param lock the lock
   */
  protected void onCreate(Lock lock) {
  }

  /**
   * On assign.
   *
   * @param update the update
   */
  protected void onAssign(LockUpdate update) {
  }

  /**
   * On defer.
   *
   * @param update the update
   */
  protected void onDefer(LockUpdate update) {
  }

  /**
   * Extend locks.
   */
  @Scheduled(initialDelay = 60000, fixedDelay = 120000)
  public void extendLocks() {
    LockQuery query = new LockQuery();
    query.instanceId(InstanceUtil.getInstanceId());
    query.instanceType(InstanceType.SINGLE);

    LockUpdate update = new LockUpdate();
    update.expiresAt(System.currentTimeMillis() + EXPIRE_SEC * 1000);
    lockDao.updateMulti(query, update);
  }
}
