/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.service;

import lombok.Data;

import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * The type Process
 */
@Data
public class ProcessOptions {
  /**
   * Full refresh happens after this duration.
   */
  private int fullRefreshSeconds;
  /**
   * The process is deemed to be completed if it is still running after this duration.
   */
  private int deemedTimeoutSeconds = 60 * 60;
  private LocalTime preferredRefreshFrom = LocalTime.of(0, 0);
  private LocalTime preferredRefreshTo = preferredRefreshFrom.minusNanos(1);

  /**
   * Should refresh boolean.
   *
   * @param date the date
   * @return the boolean
   */
  public boolean shouldRefresh(Date date) {
    LocalTime now = date.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
    if (getPreferredRefreshTo().isBefore(getPreferredRefreshFrom())) {
      return !getPreferredRefreshTo().isBefore(now) || !getPreferredRefreshFrom().isAfter(now);
    }
    return !getPreferredRefreshFrom().isAfter(now) && !getPreferredRefreshTo().isBefore(now);
  }
}
