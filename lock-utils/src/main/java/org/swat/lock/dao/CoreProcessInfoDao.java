/*
 * Copyright © 2020 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.dao;

import com.mongodb.BasicDBObject;
import com.mongodb.client.model.IndexOptions;
import org.apache.commons.lang3.StringUtils;
import org.swat.core.utils.CoreInterceptor;
import org.swat.lock.bean.ProcessInfo;
import org.swat.lock.bean.ProcessInfoQuery;
import org.swat.lock.bean.ProcessInfoUpdate;
import org.swat.lock.bean.ProcessStatus;
import org.swat.mongo.dao.AbstractEntityDaoInternal;

import javax.annotation.PostConstruct;

/**
 * The type Core process info dao.
 */
public abstract class CoreProcessInfoDao extends AbstractEntityDaoInternal<ProcessInfo> {
  /**
   * Instantiates a new Core process info dao.
   */
  public CoreProcessInfoDao() {
    super(ProcessInfo.class, "");
  }

  /**
   * Ensure indexes.
   */
  @PostConstruct
  public void ensureIndexes() {
    IndexOptions options = new IndexOptions();
    options.name("name");
    options.unique(true);

    BasicDBObject bson = new BasicDBObject();
    bson.put("name", 1);
    bson.put("unique", 1);

    ensureIndex(bson, options);

    // Removed Expiry Index to support preferred full refresh time range.
    CoreInterceptor.silent(getMongoTemplate().getCollection(getCollectionName())).dropIndex("expiry");
  }

  /**
   * Find or create process info.
   *
   * @param name               the name
   * @param fullRefreshSeconds the full refresh in Seconds
   * @return the process info
   */
  public ProcessInfo findOrCreate(String name, int fullRefreshSeconds) {
    ProcessInfoQuery query = new ProcessInfoQuery();
    query.name(name);

    ProcessInfoUpdate update = new ProcessInfoUpdate();

    update.expiresAfter(fullRefreshSeconds);
    update.setOnInsert("status", ProcessStatus.COMPLETED);
    if (StringUtils.isNotBlank(getCreatedByKey())) {
      update.setOnInsert(getCreatedKey(), getNow());
    }
    if (StringUtils.isNotBlank(getUpdatedKey())) {
      update.setOnInsert(getUpdatedKey(), getNow());
    }

    return withDirectUpdate().upsert(query, update);
  }

  @Override
  protected void setEntityId(ProcessInfo entity, String id) {
  }
}
