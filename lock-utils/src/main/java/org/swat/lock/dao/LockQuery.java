/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.dao;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.swat.lock.bean.InstanceType;

/**
 * The type Lock query.
 */
public class LockQuery extends Query {
  /**
   * Group lock query.
   *
   * @param value the value
   * @return the lock query
   */
  public LockQuery group(String value) {
    addCriteria(Criteria.where("group").is(value));
    return this;
  }

  /**
   * Key lock query.
   *
   * @param value the value
   * @return the lock query
   */
  public LockQuery key(String value) {
    addCriteria(Criteria.where("key").is(value));
    return this;
  }

  /**
   * Lock id lock query.
   *
   * @param value the value
   * @return the lock query
   */
  public LockQuery lockId(String value) {
    addCriteria(Criteria.where("_id").is(value));
    return this;
  }

  /**
   * Instance id lock query.
   *
   * @param value the value
   * @return the lock query
   */
  public LockQuery instanceId(String value) {
    addCriteria(Criteria.where("instanceId").is(value));
    return this;
  }

  /**
   * Instance type lock query.
   *
   * @param value the value
   * @return the lock query
   */
  public LockQuery instanceType(InstanceType value) {
    addCriteria(Criteria.where("instanceType").is(value));
    return this;
  }
}
