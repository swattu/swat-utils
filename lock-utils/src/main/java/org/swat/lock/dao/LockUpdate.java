/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.dao;

import org.springframework.data.mongodb.core.query.Update;
import org.swat.lock.bean.InstanceType;

import java.util.Date;

/**
 * The type Lock update.
 */
public class LockUpdate extends Update {
  /**
   * Instance type lock update.
   *
   * @param value the value
   * @return the lock update
   */
  public LockUpdate instanceType(InstanceType value) {
    set("instanceType", value);
    return this;
  }

  /**
   * Instance id lock update.
   *
   * @param value the value
   * @return the lock update
   */
  public LockUpdate instanceId(String value) {
    set("instanceId", value);
    return this;
  }

  /**
   * Expires at lock update.
   *
   * @param value the value
   * @return the lock update
   */
  public LockUpdate expiresAt(long value) {
    set("expiresAt", new Date(value));
    return this;
  }
}
