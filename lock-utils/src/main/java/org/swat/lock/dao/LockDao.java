/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.dao;

import org.swat.lock.bean.Lock;
import org.swat.mongo.dao.AbstractEntityDao;

/**
 * The interface Lock dao.
 */
public interface LockDao  extends AbstractEntityDao<Lock> {
}
