/*
 * Copyright © 2019 Swatantra Agrawal. All rights reserved.
 */

package org.swat.lock.dao;

import com.mongodb.BasicDBObject;
import com.mongodb.client.model.IndexOptions;
import org.swat.lock.bean.Lock;
import org.swat.mongo.dao.AbstractEntityDaoImpl;

import java.util.concurrent.TimeUnit;

/**
 * The type Lock dao.
 */
public abstract class AbstractLockDao extends AbstractEntityDaoImpl<Lock> implements LockDao {
  /**
   * Instantiates a new Lock dao.
   */
  protected AbstractLockDao() {
    super(Lock.class);
  }

  /**
   * Create indexes.
   */
  public void createIndexes() {
    BasicDBObject keys = new BasicDBObject();
    keys.put("key", 1);
    keys.put("unique", 1);
    keys.put("counter", 1);
    IndexOptions options = new IndexOptions();
    options.name("unique_index");
    options.unique(true);
    ensureIndex(keys, options);

    keys = new BasicDBObject();
    keys.put("expiresAt", 1);
    options = new IndexOptions();
    options.name("expiry");
    options.expireAfter(0L, TimeUnit.SECONDS);
    ensureIndex(keys, options);
  }

  @Override
  protected void setEntityId(Lock entity, String id) {
    entity.setId(id);
  }

  @Override
  protected String getEntityId(Lock entity) {
    return entity.getId();
  }
}
